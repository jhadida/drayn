
template <class Time, class Data>
struct tspool_state: public _indexed<Data>
{
    using time_type = Time;
    using data_type = Data;
    using value_type = data_type;
    DRAYN_TRAITS(data_type)

    using data_ctn = pvec<data_type>;
    using iterator_type = typename data_ctn::iterator_type;
    
    // ----------  =====  ----------
    
    time_type time;
    data_ctn data;

    tspool_state( time_type t, data_ctn d )
        : time(t), data(d) {}

    inline bool valid() const { return data; }
    inline uidx_t size() const { return m_data.size(); }
    inline ref_t operator[] (uidx_t k) const { return m_data[k]; }

    
};