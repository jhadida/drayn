
#include <vector>
#include <iostream>
#include <type_traits>



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



using uidx_t = size_t;

template <class D, class V = typename std::enable_if<
    std::is_pointer<D>::value, 
    typename std::remove_pointer<D>::type
>::type >
class container
{
public:

    using self       = container<D,V>;
    using data_type  = D;
    using value_type = V;

    container()
        { clear(); }
    container( const D& data, uidx_t size )
        { assign(data,size); }

    void clear() {
        m_data = nullptr;
        m_size = 0;
    }

    void assign( const D& data, uidx_t size ) {
        std::cout << "Assigning generic container." << std::endl;
        m_data = &data;
        m_size = size;
    }

    inline uidx_t size() const 
        { return m_size; }

    inline operator bool() const 
        { return m_data && m_size > 0; }

    inline V& operator[] ( const uidx_t& k ) const
        { return const_cast<V&>((*m_data)[k]); }

private:

    const D*  m_data;
    uidx_t    m_size;
};

template <class T>
class container<T*,T>
{
public:

    using self       = container<T*,T>;
    using data_type  = T*;
    using value_type = T;

    container()
        { clear(); }
    container( T *data, uidx_t size )
        { assign(data,size); }

    void clear() {
        m_data = nullptr;
        m_size = 0;
    }

    void assign( T *data, uidx_t size ) {
        std::cout << "Assigning pointer container." << std::endl;
        m_data = data;
        m_size = size;
    }

    inline uidx_t size() const 
        { return m_size; }

    inline operator bool() const 
        { return m_data && m_size > 0; }

    inline T& operator[] ( const uidx_t& k ) const
        { return m_data[k]; }

private:

    T*      m_data;
    uidx_t  m_size;
};



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



template <class D, class V>
void print( const container<D,V>& C )
{
    std::cout << "Container with " << C.size() << " element(s):" << std::endl;
    for ( size_t k=0; k < C.size(); ++k )
        std::cout << " " << C[k];
    std::cout << std::endl;
}

int main() 
{
    std::vector<float> data1 = {1,2,3,4};
    float data2[] = {5,6,7,8};

    print(container<std::vector<float>,float>(data1,4));
    print(container<float*>(data2,4));
}
