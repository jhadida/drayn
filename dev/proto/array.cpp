
#include <stdexcept>
#include <functional>

#include <cstdint>

using uidx_t = uint64_t;
using sidx_t = int64_t;

// ------------------------------------------------------------------------

template <class Value>
struct _indexed
{
    using value_type = Value;

    // to be implemented
    virtual bool valid() const =0;
    virtual uidx_t size() const =0;
    virtual Value& operator[] (uidx_t) const =0;

    // basic features
    inline bool empty() const { return size() == 0; }
    inline operator bool() const { return valid() && !empty(); }

    Value& at( uidx_t n ) const {
        if ( n >= size() )
			throw std::out_of_range("Index error.");
		return operator[](n);
    }

    inline Value& front() const { return at(0); }
    inline Value& back() const { return at(size()-1); }
};

// ------------------------------------------------------------------------

template <class Indexed>
class sequence
{
public:

    using container_type = Indexed;
    using value_type = typename container_type::value_type;
    using data_type = std::reference_wrapper<container_type>;

    sequence() 
        { clear(); }
    sequence( data_type seq, uidx_t first, uidx_t last, sidx_t step=1 )
        { assign(seq,first,last,step); }

    // get underlying container
          container_type& get()       { return m_data.get(); }
    const container_type& get() const { return m_data.get(); }



private:
    data_type m_data;
};

// ------------------------------------------------------------------------

int main()
{

}
