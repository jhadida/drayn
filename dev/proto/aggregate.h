
//==================================================
// @title        aggregate.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <deque>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class TC, 
    class Data=typename TC::data_type,
    class Time=typename TC::time_type>
class aggregate_ts: public _timeseries<Data,Time>
{
public:

    using self = aggregate_ts<TC,Data,Time>;
    
    using tc_type = TC;
    using chan_type = tc_type;

    using time_type = Time;
    using data_type = Data;
    using value_type = data_type;

    using time_ctn = typename tc_type::time_ctn;
    using data_sub = typename tc_type::data_ctn;
    using data_ctn = std::deque<data_sub>;
    
    // ----------  =====  ----------
    
    aggregate_ts(): m_fixed(false) {}
    aggregate_ts( const TC& tc ) { append(tc); }

    self& append( const TC& tc );

    inline self& remove( uidx_t i ) 
        { m_data.erase( m_data.begin() + i ); }

    inline chan_type channel( uidx_t i ) const 
        { return tc_type( m_time, m_data[i], m_fixed ); }

    inline const time_ctn& time() const { return m_time; }
    inline const data_ctn& data() const { return m_data; }

    // ----------  =====  ----------

    inline bool is_fixed_step() const { return m_fixed; }
    inline uidx_t ntime() const { return m_time.size(); }
    inline uidx_t nchan() const { return m_data.size(); }

    inline time_type& time( uidx_t i ) const { return m_time[i]; }
    inline data_type& data( uidx_t i, uidx_t j ) const { return m_data[i][j]; }

private:

    bool m_fixed;
    time_ctn m_time;
    data_ctn m_data;
};



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



template <class TC, class D, class T>
auto aggregate_ts<TC,D,T>::append( const TC& in ) -> self&
{
    DRAYN_ASSERT_ERR( in.nchan()==1, "Only timecourses are accepted." )

    if ( nchan() == 0 ) {

        // initialise from input timecourse
        m_fixed = in.is_fixed_step();
        m_time = in.time();
        m_data.push_back(in.data());

    } else {

        // check that input timecourses match
        DRAYN_ASSERT_ERR( distance_inf(m_time,in.time()) < this->tstep()/1e6, "Misaligned timecourse." )
        m_data.push_back(in.data());

    }
}

DRAYN_NS_END
