
#include <cstdio>
#include <tuple>

struct A 
{
    int a;
    char b;

    A( int a, char b )
        : a(a), b(b) {}

    bool operator== (const A& that) const 
        { return std::tie(a,b) == std::tie(that.a,that.b); }
};

int main()
{
    A a1(23,'a'), a2(23,'a'), a3(42,'b');
    if (a1 == a2) printf("Ok\n");
    if (a1 == a3) printf("Nok\n");
}
