
#include <iostream>

// NOK: reference to local temporary object (ie dangling reference)
struct A {
    int a, b;

    A(): a(1), b(2) {}
    const int& operator[] ( int n ) const {
        return a + n*b;
    }
};

// NOK: c is now shared across instances of B
struct B {
    int a, b;

    B(): a(1), b(2) {}
    const int& operator[] ( int n ) const {
        static int c;
        return c = a + n*b;
    }
};

// OK
struct C {
    int a, b;
    mutable int c;

    C(): a(1), b(2) {}
    const int& operator[] ( int n ) const {
        return c = a + n*b;
    }
};

int main() {
    A a; std::cout << a[1] << std::endl;
    B b; std::cout << b[2] << std::endl;
    C c; std::cout << c[3] << std::endl;
}
