
template <class T>
struct A {
protected:
    T foo;
};

template <class T>
struct B: public A<T> {
    
    B() { foo = 5; }

protected:
    using A<T>::foo; // this is needed
};

int main() {
    B<int> b;
}
