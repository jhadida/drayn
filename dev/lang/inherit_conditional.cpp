
#include <type_traits>
#include <iostream>

struct English {
    void greet() const { std::cout << "Hello!" << std::endl; }
};

struct French {
    void greet() const { std::cout << "Bonjour!" << std::endl; }
};


template <bool C>
struct Person
    : public std::conditional<C,English,French>::type 
{};

int main() {
    Person<true> P1;
    Person<false> P2;

    P1.greet();
    P2.greet();
}
