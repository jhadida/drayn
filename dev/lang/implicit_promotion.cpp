
#include <iostream>
#include <type_traits>

template <class T>
void narrow_input( T a, T b ) {
    std::cout << a+b << std::endl;
}

template<class Out>
struct diverse_op
{
    template <class T, class U>
    Out operator() ( T a, U b ) const
        { return static_cast<Out>(a+b); }
};

int main() {
    diverse_op<unsigned> op;

    unsigned u = 42;
    int i = -23;
    float f = 3.14f;
    double d = 1.0;

    // compile-time error
    // narrow_input(d,f);

    std::cout << op(d,f) << std::endl;
    std::cout << op(i,u) << std::endl;
    std::cout << op(u,f) << std::endl;
}
