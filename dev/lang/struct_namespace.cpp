
#include <iostream>

template <class T>
struct foo
{
    using value_type = T;
};

namespace bar = foo<double>;
using bar;

int main() 
{
    value_type x = 42;
    std::cout << x;
}
