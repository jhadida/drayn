
#include <iostream>

struct A 
{
    virtual unsigned age() const =0;
};

struct B: public A 
{
    unsigned a;
    B(): a(0) {}
    inline unsigned age() const { return a; }
};

int main()
{
    A *x = new B();
    x->a = 42;
    std::cout << x->age() << std::endl;
}
