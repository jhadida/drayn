
#include <iostream>

int main() 
{
    int a = -10;
    unsigned b = 20;
    std::cout << a*b << std::endl;

    unsigned c=1, d=2;
    std::cout << c-d << std::endl;
    std::cout << static_cast<int>(c-d) << std::endl;
}
