
#include <iostream>
#include <type_traits>

// ------------------------------------------------------------------------

template <class T>
struct A 
{
    virtual void print() const =0;
    T x;
};

struct B: public A<int>
{
    B() { this->x = 42; }
    void print() const 
        { std::cout << "The answer is: " << this->x << std::endl; }
};

struct C {};

// ------------------------------------------------------------------------

template<template<class> class T, class U>
struct isDerivedFrom
{
private:
    template<class V>
    static decltype(static_cast<const T<V>&>(std::declval<U>()), std::true_type{})
    test(const T<V>&);

    static std::false_type test(...);
public:
    static constexpr bool value = decltype(isDerivedFrom::test(std::declval<U>()))::value;
};

template <class T>
struct is_A: public isDerivedFrom<A,T> {};

// ------------------------------------------------------------------------

int main() {
    std::cout << std::boolalpha;
    std::cout << "B: " << is_A<B>::value << std::endl;
    std::cout << "C: " << is_A<C>::value << std::endl;
}
