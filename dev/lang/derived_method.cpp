
#include <iostream>

struct A 
{
    std::string name;
};

struct B: public A 
{
    B( std::string n ) 
        { this->name = n; }
    
    void print() const
        { std::cout << this->name << std::endl; }
};

void foo( const A& x ) { x.print(); }

int main() {
    foo(B("John"));
}
