
// Test whether reference wrappers extend the lifetime of variables.
// ANSWER: it does not, the code below is UB.

#include <cstdio>
#include <functional>

struct A 
{
    int val;
    A( int v ): val(v) {}
};

using rwA = std::reference_wrapper<A>;
rwA create( int v ) {
    A a(v);
    return rwA(a);
}

int main() {
    auto r = create(42);
    printf( "%d\n", r.get().val );
}
