
#include <iostream>
#include <algorithm>
#include <array>

int main() {
    std::array<unsigned,3> x = {1,2,3};
    std::swap( x[0], x[1] );

    for ( auto& xi: x ) std::cout << xi << " ";
    std::cout << std::endl;
}
