
#include <algorithm>
#include <iostream>
#include <vector>

template <class T>
inline size_t upper( const std::vector<T>& vec, T q ) 
    { return std::upper_bound( vec.begin(), vec.end(), q ) - vec.begin(); }

template <class T>
inline size_t lower( const std::vector<T>& vec, T q ) 
    { return std::lower_bound( vec.begin(), vec.end(), q ) - vec.begin(); }

int main() {
    std::vector<int> vec = {1,2,3,5,6};
    
    std::cout << "Upper bounds:" << std::endl;
    std::cout << upper( vec, 7 ) << std::endl;
    std::cout << upper( vec, 6 ) << std::endl;
    std::cout << upper( vec, 4 ) << std::endl;
    std::cout << upper( vec, 0 ) << std::endl;

    std::cout << "Lower bounds:" << std::endl;
    std::cout << lower( vec, 0 ) << std::endl;
    std::cout << lower( vec, 1 ) << std::endl;
    std::cout << lower( vec, 4 ) << std::endl;
    std::cout << lower( vec, 7 ) << std::endl;
}
