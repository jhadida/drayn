
#include <string>
#include <iostream>
#include <stdexcept>
#include <unordered_map>

// ----------  =====  ----------

template <class T> 
struct singleton 
    { static thread_local T instance; };

template <class T> 
thread_local T singleton<T>::instance = T();

template <class T>
constexpr T& static_val()
    { return singleton<T>::instance; }

// ----------  =====  ----------

struct NameMap
{
    std::unordered_map<std::string,int> map;

    NameMap() {
        define( "foo", 1 );
        define( "bar", 2 );
    }

    void define( const std::string& key, int val ) 
        { map[key] = val; }

    bool has( const std::string& key ) const 
        { return map.find(key) != map.end(); }

    int get( const std::string& key ) const {
        if ( !has(key) ) throw std::runtime_error("Key not found.");
        return map.find(key)->second;
    }
};

// ----------  =====  ----------

int main() {
    std::cout << static_val<NameMap>().get("bar") << std::endl;
}
