
## Allocation

Use shared pointers to manage allocated memory.

## Layout

> Using `operator[]`.

The layout specifies how to access things in memory, and has little to do with iteration.
It is not iterable without an indexing. 

```
Layout
    is_contiguous
    size
    step
    zero

NdLayout
    is_contiguous
    size[d]
    step[d]
    zero[d]
    ind2sub
    sub2ind
```

## Indexer

The indexing is not necessarily contiguous, but that's not the same contiguous
as for the layout; in case of a layout, we are talking about elements being stored in
memory, and in the case of indexing, we are talking about those elements being pointed to. 

An indexing can be iterated, and the iterator should yield an index object.
This object should be able to tell its distance to another index, as a function
of the order of the dimensions; then it is really easy to provide random-access
iteration.

```
Index
    size[d]
    step[d]
    ind
    sub[d]
```

## Accessor

> Using `operator()`.

Accessors work on top of indexers, and are used mainly for interpolation and extrapolation.
