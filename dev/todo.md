
First `sequence_iterator` should be defined, taking a container and an iterator in input which should be `uidx_t` valued.

Define `range` and `range_iterator` to work with plain arrays.

Define `index` to represent multi-dimensional indices. It should have both size and strides and index, and all should be positive. It should be able to tell its size, support arithmetic operations with other index objects, and support increment/setting specific indices by/to specific amounts. Perhaps define a `layout` and `index` separately?

Matrix and volumes are simply the combination of a container and an index.

Then define `index_iterator` which takes an index and multiple ranges (one for each dimension).

All of these should work with `sequence_iterator`, without defining any container.

Then, define `_indexed` abstract class, give it a default `iter()` method, and enhance it with arithmetic / comparable / copyable methods depending on value-type.

Then define concrete index containers (e.g. pointer and shared).
