
CC = clang++
# CC = g++

WARNING = -Wall
DEFINE = -DDRAYN_SHOW_INFO
CXXFLAGS = $(DEFINE) $(WARNING) -std=c++11

clean:
	rm -f bin/*.o 

drayn:
	$(CC) -c -o bin/drayn.o $(CXXFLAGS) src/drayn.cpp
