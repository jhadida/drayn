
Short term:

 - Add images+volume containers `data/img` with basic resampling and filtering methods.

Long term:

 - Add graph data-structures and basic algorithms 
 - Add specialised tree data structures (e.g. heap, trie)
 