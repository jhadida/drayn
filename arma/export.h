
//==================================================
// @title        export.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <utility>
#include <new>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T, class U = typename std::remove_const<T>::type >
inline arma::Row<U>
dr2arma_row( const array<T>& row, bool copymem = false, bool lock = true )
{
	copymem = copymem || !is_contiguous(row);
	INFO_REJECT( copymem, "Data will be copied to arma::Row." );

	if ( copymem )
	{
		arma::Row<U> out( row.size() );
		for ( usize_t i = 0; i < row.size(); ++i )
			out(i) = row[i];
		return out;
	}
	else return arma::Row<U>( const_cast<U*>(&row[0]), row.size(), false, lock );
}

// ------------------------------------------------------------------------

template <class T, class U = typename std::remove_const<T>::type >
inline arma::Col<U>
dr2arma_col( const array<T>& col, bool copymem = false, bool lock = true )
{
	copymem = copymem || !is_contiguous(col);
	INFO_REJECT( copymem, "Data will be copied to arma::Col." );

	if ( copymem )
	{
		arma::Col<U> out( col.size() );
		for ( usize_t i = 0; i < col.size(); ++i )
			out(i) = col[i];
		return out;
	}
	else return arma::Col<U>( const_cast<U*>(&col[0]), col.size(), false, lock );
}

// ------------------------------------------------------------------------

template <class T, class C, class U = typename std::remove_const<T>::type >
typename arma::Mat<U>
dr2arma_mat( const matrix<T,C>& mat, bool copymem = false, bool lock = true )
{
	arma::Mat<U> out;

	copymem = copymem || !(
		is_contiguous(mat.data()) &&
		mat.stride(0) == 1 &&
		mat.stride(1) == mat.dim(0)
	);
	INFO_REJECT( copymem, "Data will be copied to arma::Mat." );

	if ( copymem )
	{
		out = arma::Mat<U>( mat.nrows(), mat.ncols() );
		for ( auto it = mat.iter(); it; ++it )
			out( it.sub(0), it.sub(1) ) = *it;
	}
	else
		out = arma::Mat<U>( const_cast<U*>(mat.memptr()), mat.nrows(), mat.ncols(), false, lock );

	return out;
}

// ------------------------------------------------------------------------

template <class T, class C, class U = typename std::remove_const<T>::type >
typename arma::Cube<U>
dr2arma_cube( const volume<T,C>& vol, bool copymem = false, bool lock = true )
{
	arma::Cube<U> out;

	copymem = copymem || !(
		is_contiguous(vol.data()) &&
		vol.stride(0) == 1 &&
		vol.stride(1) == vol.dim(0) &&
		vol.stride(2) == vol.dim(0)*vol.dim(1)
	);
	INFO_REJECT( copymem, "Data will be copied to arma::Cube." );

	if ( copymem )
	{
		out = arma::Cube<U>( vol.nrows(), vol.ncols(), vol.nslices() );
		for ( auto it = vol.iter(); it; ++it )
			out( it.sub(0), it.sub(1), it.sub(2) ) = *it;
	}
	else
		out = arma::Cube<U>( const_cast<U*>(vol.memptr()), vol.nrows(), vol.ncols(), vol.nslices(), false, lock );

	return out;
}

DRAYN_NS_END_
