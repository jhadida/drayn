
//==================================================
// @title        import.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <stdexcept>
#include <type_traits>

/**
 * BUG NOTICE:
 *
 * There is a serious bug that hasn't been fixed yet.
 * Please only use these functions for local processing; that is,
 * if D is the Drayn container that wraps the Armadillo container
 * A, then only use D when it is in the same scope as A. This is
 * a strong restriction but it will be fixed soon. (May 27th 15)
 *
 * The problem is caused by memory preallocation in Armadillo.
 * If an Armadillo array using preallocation goes out of scope then
 * its memory pointer becomes invalid, and so does the Drayn container.
 * Unfortunately, memory preallocations cannot be "stolen" because they
 * are implemented as fixed-size C-style arrays. The fix will simply
 * force a deep copy of the data in those cases.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T>
struct arma2dr
{
	template <bool steal>
	using alloc_type = typename std::conditional< steal,
		allocator_armadillo<T>, allocator_noalloc<T> >::type;

	template <bool steal> using shared_type = shared< T, alloc_type<steal> >;
	template <bool steal> using array_type  = array_shared< T, alloc_type<steal> >;
	template <bool steal> using matrix_type = matrix< T, array_type<steal> >;
	template <bool steal> using volume_type = volume< T, array_type<steal> >;


	// ----------  =====  ----------

	static inline array_type<false> reference( arma::Row<T>& row )
		{ return array_type<false>(_make_shared<false>(row)); }

	static inline array_type<false> reference( arma::Col<T>& col )
		{ return array_type<false>(_make_shared<false>(col)); }

	static inline matrix_type<false> reference( arma::Mat<T>& mat )
	{
		return matrix_type<false>(
			array_type<false>(_make_shared<false>(mat)),
			wrap_inilst<usize_t>({ mat.n_rows, mat.n_cols }), MemoryLayout::Columns
		);
	}

	static inline volume_type<false> reference( arma::Cube<T>& vol )
	{
		return volume_type<false>(
			array_type<false>(_make_shared<false>(vol)),
			wrap_inilst<usize_t>({ vol.n_rows, vol.n_cols, vol.n_slices }), MemoryLayout::Columns
		);
	}

	// ----------  =====  ----------

	static inline array_type<true> steal( arma::Row<T>& row, bool lock=false )
		{ return array_type<true>(_make_shared<true>( row, lock )); }

	static inline array_type<true> steal( arma::Col<T>& col, bool lock=false )
		{ return array_type<true>(_make_shared<true>( col, lock )); }

	static inline matrix_type<true> steal( arma::Mat<T>& mat, bool lock=false )
	{
		return matrix_type<true>(
			array_type<true>(_make_shared<true>( mat, lock )),
			wrap_inilst<usize_t>({ mat.n_rows, mat.n_cols }), MemoryLayout::Columns
		);
	}

	static inline volume_type<true> steal( arma::Cube<T>& vol, bool lock=false )
	{
		return volume_type<true>(
			array_type<true>(_make_shared<true>( vol, lock )),
			wrap_inilst<usize_t>({ vol.n_rows, vol.n_cols, vol.n_slices }), MemoryLayout::Columns
		);
	}

	// ----------  =====  ----------

	static inline vector<T> copy( arma::Row<T>& row )
		{ return _copy_data(row); }

	static inline vector<T> copy( arma::Col<T>& col )
		{ return _copy_data(col); }

	static inline matrix<T> copy( arma::Mat<T>& mat )
	{
		return matrix<T>(
			_copy_data(mat), wrap_inilst<usize_t>({ mat.n_rows, mat.n_cols }), MemoryLayout::Columns
		);
	}

	static inline volume<T> copy( arma::Cube<T>& vol )
	{
		return volume<T>(
			_copy_data(vol), wrap_inilst<usize_t>({ vol.n_rows, vol.n_cols, vol.n_slices }), MemoryLayout::Columns
		);
	}


private:

	template <class A>
	static vector<T> _copy_data( A& arma_container )
	{
		vector<T> out( arma_container.n_elem );
		map( iterable_get_iterator(arma_container), out.iter(), f_cast<T>() );
		return out;
	}

	template < bool steal, class A >
	static shared_type<steal> _make_shared( A& arma_container, bool lock=false )
	{
		auto ptr = arma_container.memptr();
		auto n   = arma_container.n_elem;

		if ( steal )
		{
			// Lock if necessary
			arma::access::rw(arma_container.mem_state) = lock? 2 : 1;

			// If the container is smaller than preallocation amount, do a deep copy
			if ( n <= arma::arma_config::mat_prealloc )
				throw std::runtime_error(
					"You are trying to steal the contents of an Armadillo container with preallocated memory.\n"
					"The nature of preallocated memory makes it impossible to prevent its deletion when the original "
					"container gets out of scope, therefore stealing is forbidden.\n"
					"Consider referencing if you remain in the same scope as the original container, "
					"or copying if you intend to process the data elsewhere."
				);
		}

		return shared_type<steal>(ptr,n);
	}
};

DRAYN_NS_END_
