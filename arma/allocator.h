
//==================================================
// @title        allocator.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// Custom tag
struct tag_allocator_armadillo {};

/**
 * Allocator for Armadillo containers.
 * This is used mainly to "steal" the memory off Armadillo's containers,
 * and ensure that it is released properly, as the library intended.
 *
 * No allocation is possible using this allocator (use armadillo directly
 * if needed); a warning is thrown and a nullptr returned at any attempt.
 *
 * The allocator is used mainly by shared pointers to free the memory
 * stolen from Armadillo's containers.
 */
template <class T>
struct allocator_armadillo
{
	CORE_TRAITS(T);
	typedef allocator_armadillo<T> self;
	typedef tag_allocator_armadillo tag;

	static_assert( !std::is_const<T>::value,
		"allocator_armadillo cannot be initialized with constant value-types." );


	static ptr_t allocate( usize_t n )
	{
		DRAYN_WARN("Armadillo's memory allocation through Drayn is forbidden.");
		return nullptr;
	}

	static void release( ptr_t ptr, usize_t n )
	{
		if ( ptr && n > arma::arma_config::mat_prealloc )
			arma::memory::release(ptr);
	}

	struct deleter
	{
		inline void operator() ( ptr_t ptr, usize_t n ) const
			{ self::release(ptr,n); }
	};
};

DRAYN_NS_END_
