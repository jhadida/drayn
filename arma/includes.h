#ifndef DRAYN_ARMADILLO_H_INCLUDED
#define DRAYN_ARMADILLO_H_INCLUDED
#ifdef  DRAYN_USING_ARMADILLO

//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Type customisation
#ifdef DRAYN_USE_64BITS_SIZE
    
    #define ARMA_64BIT_WORD
    #ifdef DRAYN_USING_MATLAB
        #define ARMA_BLAS_LONG_LONG
    #endif

#else

    #define ARMA_32BIT_WORD
    #ifdef DRAYN_USING_MATLAB
        #define ARMA_BLAS_LONG
    #endif

#endif

#include "code/include/armadillo"

// ------------------------------------------------------------------------

#include "allocator.h"
#include "export.h"
#include "import.h"

#endif
#endif
