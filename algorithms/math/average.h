
//==================================================
// @title        average.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cmath>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class S>
inline double avg_arithmetic( const S& data )
	{ return alg_sum(data) / data.size(); }

template <class S>
inline double avg_geometric( const S& data, bool use_log = true )
{
	if ( use_log )
		return exp(
			reduce( data.iter(), 0.0, f_add<double>(), []( double x ){ return log(x); } )
		/ data.size() );
	else
		return std::pow( alg_prod(data), 1.0/data.size() );
}

// ------------------------------------------------------------------------

template <class S>
inline double avg_1( const S& data )
	{ return sum_1(data) / data.size(); }

template <class S>
inline double avg_2( const S& data )
	{ return sum_2(data) / data.size(); }

template <class S>
inline double avg_p( const S& data, unsigned p )
	{ return sum_p(data,p) / data.size(); }

template <class S>
inline double avg( const S& data )
	{ return avg_arithmetic(data); }

template <class S>
inline double rms( const S& data )
	{ return sqrt(avg_2(data)); }

// ------------------------------------------------------------------------

/**
 * Arithmetic average of n values avoiding overflows.
 * The average is computed as:
 * 	\sum_j ( \sum_{i\in S_j} x_i ) / N
 * where
 * 	\cup_j S_j = \{ 1 .. N \}
 * and
 * 	\cap_j S_j = \varnothing
 */

class accumulator_avg
{
public:

	accumulator_avg( usize_t n = 0 )
		{ reset(n); }

	inline void reset( usize_t n )
		{ acc = val = 0.0; n = Drayn_MAX(n,1); w = 1.0/n; }

	template <class T>
	void add( const T& x )
	{
		if ( op_abs(acc) > c_num<double>::max - op_abs(x) )
			value(); // flush accumulator

		acc += x;
	}

	inline double value()
		{ val += acc*w; acc = 0.0; return val; }

private:

	double acc, val, w;
};

DRAYN_NS_END_
