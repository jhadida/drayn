
//==================================================
// @title        polynomial.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <iostream>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

class Polynomial
{
public:

	Polynomial( usize_t degree = 0 )
		{ set_degree(degree); }

	// Enable cast to array of coefficients
	inline array<const double>  coefs() const { return m_coef; }
	inline usize_t              size () const { return n_coef(); }

	// Initialize the polynomial
	inline void clear()
		{ set_degree(0); }
	inline void set_degree( usize_t d )
		{ m_coef.create( d+1, 0.0 ); }

	// Dimensions
	inline usize_t n_coef() const { return m_coef.size(); }
	inline usize_t degree() const { return n_coef()-1; }

	// Access individual coefficients
	inline       double& operator[] ( usize_t k )       { return m_coef[k]; }
	inline const double& operator[] ( usize_t k ) const { return m_coef[k]; }

	// Evaluate the polynomial at x
	template <class T>
	double operator() ( const T& x ) const;

private:

	vector<double> m_coef;
};

// ------------------------------------------------------------------------

// Horner's method
template <class T>
double Polynomial::operator() ( const T& x ) const
{
	const usize_t d = degree();

	double out = m_coef[d];
	for ( usize_t i = 1; i <= d; ++i )
		out = out*x + m_coef[d-i];

	return out;
}

// ------------------------------------------------------------------------

inline std::ostream& operator<<( std::ostream& os, const Polynomial& P )
	{ return os << "[polynomial] " << P.coefs(); }

DRAYN_NS_END_
