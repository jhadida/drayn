
//==================================================
// @title        distance.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class Sa, class Sb> // Sum Absolute Differences
double distance_sad( const Sa& A, const Sb& B )
{
	ASSERT_RVAL( A.size() == B.size(), c_num<double>::max, "Size mismatch." );

	return inner( A.iter(), B.iter(), 0.0,
		[]( double a, double b ){ return op_abs(a-b); } );
}

// ------------------------------------------------------------------------

template <class Sa, class Sb>
inline double distance_1( const Sa& A, const Sb& B )
	{ return distance_sad(A,B); }

template <class Sa, class Sb> // Mean Absolute Difference
inline double distance_mad( const Sa& A, const Sb& B )
	{ return distance_sad(A,B) / A.size(); }

// ------------------------------------------------------------------------

template <class Sa, class Sb> // Sum Squared Differences
double distance_ssd( const Sa& A, const Sb& B )
{
	ASSERT_RVAL( A.size() == B.size(), c_num<double>::max, "Size mismatch." );

	return inner( A.iter(), B.iter(), 0.0,
		[]( double a, double b ){ return (a-b)*(a-b); } );
}

// ------------------------------------------------------------------------

template <class Sa, class Sb>
inline double distance_2( const Sa& A, const Sb& B )
	{ return sqrt(distance_ssd(A,B)); }

template <class Sa, class Sb> // Root Mean Squared Difference
inline double distance_rmsd( const Sa& A, const Sb& B )
	{ return sqrt(distance_ssd(A,B) / A.size()); }

// ------------------------------------------------------------------------

template <class Sa, class Sb> // Minimum Absolute Difference
double distance_0( const Sa& A, const Sb& B )
{
	static auto f = []( double x, double y ){ return op_abs(x-y); };
	ASSERT_RVAL( A.size() == B.size(), c_num<double>::max, "Size mismatch." );

	double d = c_num<double>::max;
	auto   a = A.iter();
	auto   b = B.iter();

	for(; a && b; a.next(), b.next() )
		d = Drayn_MIN( d, f(*a,*b) );
	return d;
}

// ------------------------------------------------------------------------

template <class Sa, class Sb> // Maximum Absolute Difference
double distance_inf( const Sa& A, const Sb& B )
{
	static auto f = []( double x, double y ){ return op_abs(x-y); };
	ASSERT_RVAL( A.size() == B.size(), c_num<double>::max, "Size mismatch." );

	double d = 0.0;
	auto   a = A.iter();
	auto   b = B.iter();

	for(; a && b; a.next(), b.next() )
		d = Drayn_MAX( d, f(*a,*b) );
	return d;
}

// ------------------------------------------------------------------------

template <class Sa, class Sb> // Sum, Absolute, Power Difference
double distance_sapd( const Sa& A, const Sb& B, unsigned p )
{
	ASSERT_RVAL( A.size() == B.size(), c_num<double>::max, "Size mismatch." );

	return inner( A.iter(), B.iter(), 0.0,
		[p]( double a, double b ){ return op_pow(op_abs(a-b),p); } );
}

// ------------------------------------------------------------------------

template <class Sa, class Sb>
inline double distance_p( const Sa& A, const Sb& B, unsigned p )
	{ return std::pow( distance_sapd(A,B,p), 1.0/p ); }

template <unsigned p, class Sa, class Sb>
inline double distance_p( const Sa& A, const Sb& B )
	{ return distance_p(A,B,p); }

DRAYN_NS_END_
