
//==================================================
// @title        interval.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <iostream>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class Val, class Real = double>
struct interval
{
	Val a, b;

	interval() {}
	interval( Val low, Val up )
		{ assign(low,up); }
	interval( const inilst<Val>& lst )
		{ assign(lst); }

	inline operator bool() const
		{ return delta<double>() > c_num<double>::eps; }

	inline void assign( Val low, Val up ) { a = low; b = up; }
	inline void assign( const inilst<Val>& lst )
	{
		ASSERT_R( lst.size() == 2, "Two values required." );
		a = lst.begin()[0];
		b = lst.begin()[1];
	}

	template <class T = Val>
	inline T    delta   () const { return static_cast<T>(b-a); }
	inline Real midpoint() const { return static_cast<Real>(a+b) / 2; }

	inline Real ratio   ( Real x ) const { return ratio_ab(x); }
	inline Real ratio_ab( Real x ) const { return static_cast<Real>(x-a)/delta<Real>(); }
	inline Real ratio_ba( Real x ) const { return static_cast<Real>(b-x)/delta<Real>(); }
};

// ------------------------------------------------------------------------

template <class V, class R>
inline std::ostream& operator<<( std::ostream& os, const interval<V,R>& I )
{
	return os << "[interval] (";
		numeric_ostream<V>(os)<< I.a << ", ";
		numeric_ostream<V>(os)<< I.b << ") " << std::endl; 
}

DRAYN_NS_END_
