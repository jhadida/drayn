
//==================================================
// @title        norm.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// L1: taxicab / manhattan / absolute
template <class S> 
double sum_1( const S& data )
{
	return reduce( data.iter(), 0.0, 
		[]( double s, double x ){ return s + op_abs(x); } );
}

template <class S> 
inline double norm_1( const S& data ) 
	{ return sum_1(data); }

// ------------------------------------------------------------------------

// L2: euclidean
template <class S> 
double sum_2( const S& data )
{
	return reduce( data.iter(), 0.0, 
		[]( double s, double x ){ return s + x*x; } );
}

template <class S> 
inline double norm_2( const S& data ) 
	{ return sqrt(sum_2(data)); }

// ------------------------------------------------------------------------

// L0 and Linf
template <class S> 
inline double norm_0( const S& data ) 
	{ return minimum_abs(data); }
template <class S> 
inline double norm_inf( const S& data ) 
	{ return maximum_abs(data); }

// ------------------------------------------------------------------------

// General Lp functions
template <class S>
double sum_p( const S& data, unsigned p )
{
	return reduce( data.iter(), 0.0, 
		[p]( double s, double x ){ return s + op_pow(x,p); } );
}

template <class S>
inline double norm_p( const S& data, unsigned p )
	{ return std::pow( sum_p(data,p), 1.0/p ); }

// ------------------------------------------------------------------------

template <unsigned p, class S>
inline double sum_p( const S& data )
	{ return sum_p(data,p); }

template <unsigned p, class S>
inline double norm_p( const S& data )
	{ return norm_p(data,p); }

DRAYN_NS_END_