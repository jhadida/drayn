
//==================================================
// @title        extrema.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Find min and max elements in iterable containers.
 */

#include <algorithm>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class S, class U>
void extrema( const S& data, U& m, U& M )
{
	auto r = std::minmax_element( data.begin(), data.end() );
	m = static_cast<U>( *(r.first)  ); 
	M = static_cast<U>( *(r.second) );
}

// ------------------------------------------------------------------------

template <class S>
double maximum_difference( const S& data )
{
	double min, max;
	extrema( data, min, max );
	return max-min;
}

// ------------------------------------------------------------------------

template <class S, class T = typename S::val_t> 
inline T& minimum( const S& data )
	{ return *std::min_element( data.begin(), data.end() ); }
template <class S, class T = typename S::val_t> 
inline T& maximum( const S& data )
	{ return *std::max_element( data.begin(), data.end() ); }

// ------------------------------------------------------------------------

template <class S, class T = typename S::val_t> 
T minimum_abs( const S& data )
{
	return reduce( data.iter(), op_abs(data[0]), 
		[]( const T& m, const T& x ){ return Drayn_MIN( m, op_abs(x) ); } );
}

template <class S, class T = typename S::val_t> 
T maximum_abs( const S& data )
{
	return reduce( data.iter(), op_abs(data[0]), 
		[]( const T& m, const T& x ){ return Drayn_MAX( m, op_abs(x) ); } );
}

DRAYN_NS_END_
