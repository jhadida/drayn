#ifndef DRAYN_ALGORITHMS_H_INCLUDED
#define DRAYN_ALGORITHMS_H_INCLUDED

//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Sort-related
// #include "sort/standard.h"
#include "sort/rank.h"
#include "sort/ordering.h"
#include "sort/ternary.h"

// Maths algorithms
#include "math/extrema.h"
#include "math/norm.h"
#include "math/distance.h"
#include "math/metric.h"
#include "math/average.h"
#include "math/interval.h"
#include "math/polynomial.h"

// Interpolators
#include "interp/linear.h"

// Stats related
#include "stats/basic.h"
#include "stats/range.h"
#include "stats/shape.h"
#include "stats/histogram.h"

#endif
