
//==================================================
// @title        linear.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * Implement linear interpolator as a sequential accessor.
 */

template <class Container, template <class> class Accessor = accessor_default>
class interp_linear
{
public:

	FORWARD_TRAITS( Container );
	ACCESSOR_TRAITS

	typedef interp_linear<Container,Accessor>  self;
	typedef Accessor<Container>                accessor_type;

	// ----------  =====  ----------


	interp_linear()
		{ m_accessor.clear(); }
	interp_linear( const Container& ctn )
		{ set_container(ctn); }


	inline void clear         ()                       { m_accessor.clear(); }
	inline void set_container ( const Container& ctn ) { m_accessor.assign(ctn); }

	inline const accessor_type& get_accessor() const { return m_accessor; }
	inline       accessor_type& get_accessor()       { return m_accessor; }


	inline ref_t operator() ( uidx x ) const { return m_accessor(x); }
	inline ref_t operator() ( iidx x ) const { return m_accessor(x); }

	double operator() ( ridx x ) const
	{
		isize_t ileft = op_ifloor(x); // no :( comeback!
		val_t
			vleft  = static_cast<double>(m_accessor(ileft)),
			vright = static_cast<double>(m_accessor(ileft+1));

		return vleft + (x-ileft) * (vright-vleft);
	}

private:

	accessor_type m_accessor;
};

DRAYN_NS_END_
