
//==================================================
// @title        basic.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cmath>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

struct stats_basic
{
	typedef stats_basic self;
	double mu, sigma;

	// ----------  =====  ----------
	

	// Ctors
	stats_basic()
		{ clear(); }

	template <class S>
	stats_basic( const S& sample )
		{ operator() (sample); }

	inline void clear()
		{ mu = sigma = 0.0; }

	template <class S>
	self& operator() ( const S& sample )
	{
		accumulator_avg acc( sample.size() );

		// First pass for the average
		for ( auto s=sample.iter(); s; s.next() ) 
			acc.add( *s );
			mu = acc.value();

		// Second pass for the std
		acc.reset( sample.size() );
		for ( auto s=sample.iter(); s; s.next() ) 
		{
			sigma = *s - mu;
			acc.add( sigma*sigma );
		}

		sigma = sample.size() / (sample.size() - 1.0);
		sigma = sqrt( sigma * acc.value() );

		return *this;
	}
};

DRAYN_NS_END_
