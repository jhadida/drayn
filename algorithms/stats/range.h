
//==================================================
// @title        stats.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T>
struct stats_range_data
{
	typedef stats_range_data<T> self;
	T min, max;
	double avg;

	// ----------  =====  ----------
	

	// Ctors
	stats_range_data()
		{ clear(); }

	template <class S>
	stats_range_data( const S& sample )
		{ operator()(sample); }

	// Cast as an interval
	inline operator interval<double>() const
		{ return interval<double>(min,max); } 


	inline void clear()
		{ min = max = 0; avg = 0.0; }

	// operator() initializes this structure by calling the following function
	template <class S>
	inline self& operator() ( const S& sample )
		{ stats_range( sample, *this ); return *this; }
};

// ------------------------------------------------------------------------

template <class S, class T = typename S::val_t>
void stats_range( const S& sample, stats_range_data<T>& out )
{
	accumulator_avg avg( sample.size() );

	// Initialize output
	out.min = out.max = sample[0];

	// Iterate on the sample
	for ( auto s=sample.iter(); s; s.next() )
	{
		avg.add(*s);

		if ( *s > out.max ) out.max = *s;
		else if ( *s < out.min ) out.min = *s;
	}

	out.avg = avg.value();
}

// ------------------------------------------------------------------------

template <class S>
inline void stats_rescale( const S& sample, const interval<double>& new_range )
	{ stats_rescale( sample, stats_range_data<double>(sample), new_range ); }

template <class S>
inline void stats_rescale( const S& sample, const interval<double>& old_range, const interval<double>& new_range )
	{ for ( auto& s: sample ) s = new_range.a + new_range.delta()*old_range.ratio(s); }

DRAYN_NS_END_
