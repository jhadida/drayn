
//==================================================
// @title        histogram.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

void Histogram::clear()
{
	m_bounds.assign(0.0, 0.0);
	m_bins.clear();
}

// ------------------------------------------------------------------------

void Histogram::reset()
{
	fill( m_bins.iter(), 0 );
}

// ------------------------------------------------------------------------

void Histogram::init( const bounds_type& itv, usize_t nbins )
{
	m_bounds = itv;
	m_bins.create( nbins, 0 );
}

// ------------------------------------------------------------------------

usize_t Histogram::bin_index( double x ) const
{
	isize_t n = op_iround( n_bins() * m_bounds.ratio(x) );
	return op_clamp<isize_t>( n, 0, n_bins()-1 );
}

DRAYN_NS_END_
