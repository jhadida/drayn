
//==================================================
// @title        shape.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cmath>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

struct stats_shape_data
{
	typedef stats_shape_data self;
	double sdev, adev, skew;

	// ----------  =====  ----------
	

	// Ctors
	stats_shape_data()
		{ clear(); }
	template <class S>
	stats_shape_data( const S& sample )
		{ operator()(sample); }
		

	inline void clear()
		{ sdev = adev = skew = 0.0; }

	// operator() initializes this structure by calling the following function
	// Note that it returns the range statistics (needed for the average).
	template <class S, class T = typename S::val_t>
	self& operator() ( const S& sample )
	{
		stats_range_data<T> range(sample);
		stats_shape( sample, range.avg, *this );
		return *this;
	}
};

// ------------------------------------------------------------------------

template <class S>
void stats_shape( const S& sample, double avg, stats_shape_data& out )
{
	// Create accumulators for the three statistics
	accumulator_avg sdev( sample.size() );
	accumulator_avg adev( sample.size() );
	accumulator_avg skew( sample.size() );

	// Temporary variable to hold deviations
	double dev;

	for ( auto s=sample.iter(); s; s.next() )
	{
		dev = *s - avg;

		adev.add( op_abs(dev) );
		sdev.add( dev*dev );
		skew.add( dev*dev*dev );
	}

	// Correction factor for sdev bias
	double correction = sample.size();
	correction /= (correction - 1);

	// Set output
	out.adev = adev.value();
	out.sdev = sqrt( sdev.value() * correction );
	out.skew = skew.value() / op_pow(out.sdev,3);
}

DRAYN_NS_END_
