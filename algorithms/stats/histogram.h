
//==================================================
// @title        histogram.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

class Histogram
{
public:

	typedef vector<usize_t>   bins_type;
	typedef interval<double>  bounds_type;


	Histogram()
		{ clear(); }
	Histogram( const bounds_type& b, usize_t nbins )
		{ init(b,nbins); }


	void clear();
	void reset();
	void init( const bounds_type& b, usize_t nbins );

	// Validity
	inline bool     valid() const { return m_bins && m_bounds; }
	inline bool     empty() const { return m_bins.empty(); }
	inline operator bool () const { return valid() && !empty(); }

	// Accessors
	inline usize_t size  () const { return m_bins.size(); }
	inline usize_t n_bins() const { return size(); }
	inline usize_t operator[] ( usize_t k ) const
		{ return m_bins[k]; }

	inline const bounds_type& bounds() const { return m_bounds; }
	inline const bins_type&   bins  () const { return m_bins; }

	// Find bin index of a value x
	usize_t bin_index( double x ) const;



	// ------------------------------------------------------------------------
	// Initialize from input sample
	//
	// Sample should be standard-iterable {begin(), end()}, implement size(),
	// and implement offset dereference operator[].

	// Full specification
	template <class S>
	Histogram( const S& sample, const bounds_type& itv, usize_t nbins )
		{ operator()(sample,itv,nbins); }
	template <class S>
	void operator() ( const S& sample, const bounds_type& itv, usize_t nbins )
		{ init(itv,nbins); ASSERT_R(*this,"[dr.Histogram] Not ready."); _process_sample(sample); }


	// Implicit range specification: call range statistics on sample
	template <class S>
	Histogram( const S& sample, usize_t nbins )
		{ operator()(sample,nbins); }
	template <class S>
	void operator() ( const S& sample, usize_t nbins )
		{ stats_range_data<S> range(sample); operator()(sample,range,nbins); }


	// Full implicit specification: reset counts and use current bounds
	template <class S>
	void operator() ( const S& sample )
		{ ASSERT_R(*this,"[dr.Histogram] Not ready."); reset(); _process_sample(sample); }


private:

	// Internal method to process samples
	template <class S>
	inline void _process_sample( const S& sample )
		{ for ( auto s=sample.iter(); s; s.next() ) ++m_bins[ bin_index(*s) ]; }


	bounds_type m_bounds;
	bins_type   m_bins;
};

DRAYN_NS_END_
