
//==================================================
// @title        ordering.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class S, class T = typename S::val_t>
bool is_ascending( const S& seq, bool strict = false )
{
	auto cur  = seq.iter();
	auto next = cur; ++next;
	return strict?
		all( next, cur, f_gt<T>()  ) :
		all( next, cur, f_geq<T>() ) ;
}

// ------------------------------------------------------------------------

template <class S, class T = typename S::val_t>
bool is_descending( const S& seq, bool strict = false )
{
	auto cur  = seq.iter();
	auto next = cur; ++next;
	return strict?
		all( next, cur, f_lt<T>()  ) :
		all( next, cur, f_leq<T>() ) ;
}

DRAYN_NS_END_
