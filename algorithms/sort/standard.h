
//==================================================
// @title        standard.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * In-place stl-only implementations of mergesort, heapsort and insertion sort.
 * All slower than std::sort for numeric types, but kept for implementation.
 *
 * Runtime ranking:
 * 	std::sort < heap_sort < merge_sort < insertion_sort
 */

#include <functional>
#include <algorithm>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template<class raI>
void heap_sort(raI begin, raI end) 
{
	std::make_heap(begin, end);
	std::sort_heap(begin, end);
}

// ------------------------------------------------------------------------

template <class raI, class Order>
void merge_sort( raI first, raI last, Order order )
{
	if (last - first > 1)
	{
		raI middle = first + (last - first) / 2;
		merge_sort(first, middle, order);
		merge_sort(middle, last, order);
		std::inplace_merge(first, middle, last, order);
	}
}

template <class raI>
void merge_sort( raI first, raI last )
{
	merge_sort(first, last, std::less<typename std::iterator_traits<raI>::value_type>());
}

// ------------------------------------------------------------------------

template<class Iterator>
void insertion_sort( Iterator a, Iterator end )
{
    std::iter_swap( a, std::min_element( a, end ) );
 
    for ( Iterator b = a; ++b < end; a = b )
        for( Iterator c = b; *c < *a; --c, --a )
            std::iter_swap( a, c );
}

DRAYN_NS_END_


