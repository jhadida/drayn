
//==================================================
// @title        ternary.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Ternary search functions.
 * All the following functions expect an INCREASINGLY SORTED array in input
 * (haystack) and return a matching/closest element or insertion point for
 * a given value (needle).
 *
 * NOTE:
 * These functions are highly optimized, do NOT alter them in any way.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * Ternary Match
 *
 * This function returns the index of the element in haystack that
 * matches the value of needle. If no element matches, it returns
 * n (the size of haystack). Note that if there are several copies
 * of the match in haystack (doublons) then the matching element is
 * one of the copies, but which one is undefined.
 *
 * Required of T: operators <, <= (and their opposites) and ==.
 */
template <class H, class T>
usize_t ternary_match( const H& haystack, usize_t n, const T& needle )
{
	if ( n == 0 ) return n;
	usize_t l, r, lt, rt;
	bool gr, ll;

	// Early cancellation
	l = 0; r = n-1;
	if ( needle < haystack[l] ) return n;
	if ( needle > haystack[r] ) return n;

	while ( r - l > 1 )
	{
		// Compute middle points
		lt = (2*l + r) / 3;
		rt = (l + 2*r) / 3;

		// Update bounds
		gr = needle >= haystack[rt];
		ll = !gr && needle <= haystack[lt];
		l  = gr*rt + ll*l  + (!(ll||gr))*lt;
		r  = gr*r  + ll*lt + (!(ll||gr))*rt;

			/*
			Equivalent to:
			if ( needle >= haystack[rt] ) l = rt;
			else if ( needle <= haystack[lt] ) r = lt;
			else { l = lt; r = rt; }
			 */

		// Termination
		if ( needle == haystack[l] ) return l;
		if ( needle == haystack[r] ) return r;
	}

	// No match found
	return n;
}

// ------------------------------------------------------------------------

/**
 * Ternary Insertion Point
 *
 * This function returns the index of the element in haystack that would be
 * pushed by needle if it was inserted. If the index K is returned, and the
 * haystack is strictly increasing, then:
 *    haystack[K-1] <= needle < haystack[K]
 *
 * Required of T: operators < and <= (and their opposites).
 * Note: this is equivalent to std::upper_bound
 */
template <class H, class T>
usize_t ternary_insert( const H& haystack, usize_t n, const T& needle )
{
	if ( n == 0 ) return n;
	usize_t l, r, lt, rt;
	bool gr, ll;

	// Detect outbound
	l = 0; r = n-1;
	if ( needle <  haystack[l] ) return 0;
	if ( needle >= haystack[r] ) return n;

	while ( r - l > 1 )
	{
		// Compute middle points
		lt = (2*l + r) / 3;
		rt = (l + 2*r) / 3;

		// Update bounds
		gr = needle >= haystack[rt];
		ll = !gr && needle < haystack[lt];
		l  = gr*rt + ll*l  + (!(ll||gr))*lt;
		r  = gr*r  + ll*lt + (!(ll||gr))*rt;
	}

	// Return insertion point
	return r;
}

// ------------------------------------------------------------------------

/**
 * Ternary Closest Match
 *
 * This function returns the index of the element in haystack that is cloest
 * in value to needle. If we have needle == (h[i] + h[i+1])/2 for some i, then
 * it returns i and not i+1 (ie, left preferred).
 *
 * Required of T: same as insertion + subtraction operator.
 */
template <class H, class T>
usize_t ternary_closest( const H& haystack, usize_t n, const T& needle )
{
	if ( n == 0 ) return n;
	usize_t l, r;

	// Detect outbound
	l = 0; r = n-1;
	if ( needle <= haystack[l] ) return l;
	if ( needle >= haystack[r] ) return r;

	// Find insertion point
	r = ternary_insert( haystack, n, needle );
	l = r-1; // no underflow guaranteed because of previous tests

	// Return closest
	return (l + (needle-haystack[l] > haystack[r]-needle));
}

DRAYN_NS_END_
