
//==================================================
// @title        rank.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <type_traits>
#include <functional>
#include <algorithm>
#include <iterator>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// These are required because gcc doesn't like pointer<T> as random-iterators
template <class I, class R>
inline void _rank_sort( I it_begin, I it_end, R robj )
{
	std::sort( it_begin, it_end, robj );
}

template <class T, class R>
inline void _rank_sort( const pointer<T>& it_begin, const pointer<T>& it_end, R robj )
{
	std::sort( it_begin.memptr(), it_end.memptr(), robj );
}

inline vector<usize_t> rank( usize_t n, std::function<bool(usize_t,usize_t)> comp )
{
	auto r = xrange(n);
	_rank_sort( r.begin(), r.end(), comp );
	return r;
}

template <class S>
inline vector<usize_t> rank( const S& data )
{
	return rank( data.size(), [ &data ]( usize_t i, usize_t j ) -> bool { return data[i] < data[j]; } );
}

DRAYN_NS_END_
