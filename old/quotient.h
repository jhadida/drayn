
//==================================================
// @title        quotient.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T>
inline T _quotient( T a, T b, std::true_type const ) // integral type
{
    JCL_DCHK( b != 0, "[jcl._quotient] Division by zero." );
    return (b != 0) ? a/b : 0;
}

template <class T>
T _quotient( T a, T b, std::false_type const ) // non-integral type
{
    static const T M = c_num<T>::max;
    static const T m = c_num<T>::min;

    int s = op_sign(a) * op_sign(b); 
    a = std::abs(a);
    b = std::abs(b);

    if ( b < 1 )
    {
        JCL_DCHK( a < b*M, "[jcl._quotient] Division overflow." );
        return a < b*M ? a/b : s*M;
    }
    else
    {
        JCL_DCHK( a >= b*m, "[jcl._quotient] Division underflow." );
        return a >= b*m ? a/b : s*m;
    }
}

// ------------------------------------------------------------------------

template <class T>
inline T op_quotient( T a, T b )
    { return _quotient( a, b, std::is_integral<T>() ); }
