
//==================================================
// @title        subsequence.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Container, class Iterator, class Value = typename Container::value_type>
class subsequence_iterator
    : public _iterator<Value>
{
public:

    DRAYN_TRAITS(Value)

    subsequence_iterator( const Container& ctn, const Iterator& it )
        : m_data(&ctn), m_index(it) {}

    inline operator bool() const { return m_index; }
    inline ref_t value() const { return safeIndex(*m_data,*m_index); }
    inline void next() { m_index.next(); }

protected:
    const Container *m_data;
    Iterator m_index;
};

// ------------------------------------------------------------------------

// forward definition
template <class Container, class Indexer, class Value> class subsequence; 

template <class Container, class Indexer, class Value = typename Container::value_type>
struct subsequence_traits
{
    using container_type        = Container;
    using value_type            = Value;

    using indexer_type          = Indexer;
    using indexer_iterator      = typename Indexer::iterator_type;

    using self_type             = subsequence<container_type,indexer_type,value_type>;
    using iterator_type         = subsequence_iterator<container_type,indexer_iterator,value_type>;
    
    using indexed_parent        = _indexed<value_type>;
    using iterable_parent       = _iterable<iterator_type>;
    using arithmetic_parent     = _arithmetic<value_type,self_type>;
    using comparable_parent     = _comparable<value_type,self_type>;
};

// ------------------------------------------------------------------------

template <class Container, class Indexer, class Value = typename Container::value_type>
class subsequence: 
    public subsequence_traits<Container,Indexer,Value>::indexed_parent,
    public subsequence_traits<Container,Indexer,Value>::iterable_parent,
    public subsequence_traits<Container,Indexer,Value>::arithmetic_parent,
    public subsequence_traits<Container,Indexer,Value>::comparable_parent
{
public:

    using self = typename subsequence_traits<Container,Indexer,Value>::self_type;
    using value_type = typename subsequence_traits<Container,Indexer,Value>::value_type;
    using iterator_type = typename subsequence_traits<Container,Indexer,Value>::iterator_type;

    DRAYN_TRAITS(value_type)

    subsequence( const Container& ctn, const Indexer& ind )
        : m_data(ctn), m_index(ind) {}

    subsequence( const sequence<Container,Value>& seq, const Indexer& ind )
        : m_data(seq.data()), m_index(ind) {}

    subsequence( const self& other )
        : m_data(other.m_data), m_index(other.m_index) {}

    virtual ~subsequence() {} // -Wdelete-non-virtual-dtor

    inline const Container& data() const { return m_data; }
    inline operator const Container&() const { return m_data; }

    // proxy to container implementation
    inline bool valid() const { return m_data && m_index; }
    inline uidx_t size() const { return m_index.size(); }
    inline ref_t operator[] (uidx_t k) const { return safeIndex(m_data,m_index[k]); }

    inline iterator_type iter() const 
        { return iterator_type(m_data,m_index.iter()); }

protected:
    Container m_data;
    Indexer m_index;
};

DRAYN_NS_END
