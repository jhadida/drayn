
//==================================================
// @title        concepts.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// protect indices against overflow
#ifdef JCL_PROTECT_INDICES
	#define JCL_IDX(k,s) (k % s)
#else
 	#define JCL_IDX(k,s) k
#endif

// Iterator wrapper
#include "iterator/jcl.h"
#include "utils/iterator_wrapper.h"
#include "utils/iterator_functional.h"

// Iterators
#include "iterator/forward.h"
#include "iterator/bidirectional.h"
#include "iterator/random_access.h"

// Concepts
#include "sequential.h"
#include "iterable.h"
#include "copyable.h"
#include "arithmetic.h"
#include "comparable.h"

// Utils building on the previous concepts
#include "utils/iterable_ostream.h"
#include "utils/iterable_algebra.h"

