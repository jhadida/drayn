
//==================================================
// @title        jcl.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iterator>

/**
 * JCL iterators are bidirectional and provide explicit manipulation methods:
 *     X.next(), X.prev() // move forward/backward
 *     (bool) X // check current offset validity
 *     X.value() // access current value (offset should be valid)
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <
	class Value,
	class Tag = std::bidirectional_iterator_tag
>
struct _jcl_iterator
	: public std::iterator<Tag,Value>
{
	CORE_TRAITS(Value);

	virtual operator bool () const =0;
	virtual ref_t    value() const =0;
	virtual void     next () =0;
	virtual void     prev () =0;
};

JCL_NS_END_
