
//==================================================
// @title        ra_iterator.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iterator>
#include <stdexcept>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class T, class F>
struct _random_access_iterator
	: public _jcl_iterator< T, std::random_access_iterator_tag >
{
	CORE_TRAITS(T);
	typedef F self;

	// CRTP
	inline       F& _ra_iterator_crtp()       { return static_cast<      F&>(*this); }
	inline const F& _ra_iterator_crtp() const { return static_cast<const F&>(*this); }

	// ----------  =====  ----------

	// Interface
	virtual void clear() =0;
	virtual void swap( self& that ) =0;

	// JCL iterator methods
	virtual void increment( uidx_t n ) =0;
	virtual void decrement( uidx_t n ) =0;

	// Address comparison
	virtual bool  comparable ( const self& that ) const =0;
	virtual sidx_t operator- ( const self& that ) const =0;

	// Offset dereference
	virtual ref_t operator[] ( uidx_t n ) const =0;

	// Validity
	virtual operator bool() const =0;

	// ----------  =====  ----------

	// R-value
	virtual
	inline ref_t value      () const { return operator[](0); }
	inline ref_t operator*  () const { return  value(); }
	inline ptr_t operator-> () const { return &value(); }
	inline ptr_t operator&  () const { return &value(); }

	// Increments/drecrements
	inline void next() { increment(1); }
	inline void prev() { decrement(1); }

	inline self& operator ++()    { next(); return _ra_iterator_crtp(); }
	inline self& operator --()    { prev(); return _ra_iterator_crtp(); }

	inline self  operator ++(int) { self P(_ra_iterator_crtp()); next(); return P; }
	inline self  operator --(int) { self P(_ra_iterator_crtp()); prev(); return P; }

	// Arithmetic operators
	template <class Integer>
	inline self operator +( Integer n ) const
		{ return (n < 0) ? operator-(static_cast<uidx_t>(-n)) : operator+(static_cast<uidx_t>(n)); }

	inline self operator +( uidx_t n ) const
		{ self P(_ra_iterator_crtp()); P.increment(n); return P; }
	inline self operator -( uidx_t n ) const
		{ self P(_ra_iterator_crtp()); P.decrement(n); return P; }

	inline self& operator +=( uidx_t n ) { increment(n); return _ra_iterator_crtp(); }
	inline self& operator -=( uidx_t n ) { decrement(n); return _ra_iterator_crtp(); }

	// Comparison
	virtual
	inline bool operator== ( const self& that ) const { return operator-(that) == 0; }
	inline bool operator!= ( const self& that ) const { return !operator==(that); }

	// Ordering
	virtual
	inline bool operator<  ( const self& that ) const { return operator-(that) < 0; }
	virtual
	inline bool operator>  ( const self& that ) const { return operator-(that) > 0; }

	inline bool operator<= ( const self& that ) const { return !operator>(that); }
	inline bool operator>= ( const self& that ) const { return !operator<(that); }

};

JCL_NS_END_
