
//==================================================
// @title        traits.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



JCL_NS_START_

template <class S,class T> class sequence_iterator;

template <
	class Value, class Container, class Self,
	class StdIterator = sequence_iterator< Container, Value >,
	class JCLIterator = sequence_iterator< Container, Value >
>
struct sequence_traits
{
	using self_type          = Self;
	using container_type     = Container;

	using value_type         = Value;
	using is_mutable         = bool_negate<std::is_const<value_type>>;

	using std_iterator       = StdIterator;
	using jcl_iterator       = JCLIterator;

	using iterable_parent    = _iterable< Value, StdIterator, JCLIterator >;
	using sequential_parent  = _sequential< Value, Container, Self >;
	using arithmetic_parent  = _arithmetic< Value, Self >;
	using comparable_parent  = _comparable< Value, Self >;
	using copyable_parent    = _copyable< Value, JCLIterator, is_mutable::value >;

	using data_type          = typename sequential_traits<sequential_parent>::data_type;
};

JCL_NS_END_
