
//==================================================
// @title        container.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



JCL_NS_START_

template <class D, class V = typename std::enable_if<
    std::is_pointer<D>::value, 
    typename std::remove_pointer<D>::type
>::type >
class container
{
public:

    using self       = container<D,V>;
    using data_type  = D;
    using value_type = V;

    container()
        { clear(); }
    container( const D& data, uidx_t size )
        { assign(data,size); }

    void clear() {
        m_data = nullptr;
        m_size = 0;
    }

    void assign( const D& data, uidx_t size ) {
        m_data = &data;
        m_size = size;
    }

    inline uidx_t size() const 
        { return m_size; }

    inline operator bool() const 
        { return m_data && m_size > 0; }

    inline V& operator[] ( const uidx_t& k ) const
        { return const_cast<V&>((*m_data)[JCL_IDX( k, m_size )]); }

private:

    const D*  m_data;
    uidx_t    m_size;
};

/**
 * Specialisation for pointer container.
 */
template <class T>
class container<T*,T>
{
public:

    using self       = container<T*,T>;
    using data_type  = T*;
    using value_type = T;

    container()
        { clear(); }
    container( T *data, uidx_t size )
        { assign(data,size); }

    void clear() {
        m_data = nullptr;
        m_size = 0;
    }

    void assign( T *data, uidx_t size ) {
        m_data = data;
        m_size = size;
    }

    inline uidx_t size() const 
        { return m_size; }

    inline operator bool() const 
        { return m_data && m_size > 0; }

    inline T& operator[] ( const uidx_t& k ) const
        { return m_data[JCL_IDX( k, m_size )]; }

private:

    T*      m_data;
    uidx_t  m_size;
};


/*

template <class C>
struct sequence_container
{
    using self      = sequence_container<C>;
    using data_type = const C&;

    const C *m_ptr;

    sequence_container() { clear(); }
    sequence_container( data_type c ) { set(c); }

    inline void clear() { m_ptr = nullptr; }
    inline operator bool() const { return m_ptr; }

    inline self&     set ( data_type c ) { m_ptr = &c; return *this; }
    inline data_type get () const { return *m_ptr; }
};

template <class T>
struct sequence_container<T*> // pointer specialisation
{
    using self      = sequence_container<T*>;
    using data_type = T*;

    T *m_ptr;

    sequence_container() { clear(); }
    sequence_container( data_type p ) { set(p); }

    inline void clear() { m_ptr = nullptr; }
    inline operator bool() const { return m_ptr; }

    inline self&     set ( data_type p ) { m_ptr = p; return *this; }
    inline data_type get () const { return m_ptr; }
};

*/

JCL_NS_END_
