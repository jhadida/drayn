
//==================================================
// @title        iterator.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <tuple>
#include <utility>



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class S, class T = typename S::val_t >
class sequence_iterator
	: public _random_access_iterator< T, sequence_iterator<S,T> >
{
public:

	CORE_TRAITS(T);
	using self        = sequence_iterator<S,T>;
	using wrapper_t   = sequence_wrapper<S,T>;
	using data_t      = typename wrapper_t::data_t;
	using is_mutable  = bool_negate<std::is_const<T>>;

	// ----------  =====  ----------


	sequence_iterator()
		{ clear(); }
	sequence_iterator( const wrapper_t& seq )
		{ assign(seq); }
	sequence_iterator( data_t seq, uidx_t first, uidx_t last, sidx_t step = 1 )
		{ assign(seq,first,last,step); }


	// Legacy: _random_access_iterator
	inline void clear ()             { m_seq.clear(); m_ind=0; }
	inline void swap  ( self& that ) { m_seq.swap(that.m_seq); bit_swap(m_ind,that.m_ind); }

	inline void increment ( uidx_t n=1 ) { m_ind += n; }
	inline void decrement ( uidx_t n=1 ) { m_ind -= std::min(n,m_ind); }
	inline void reset     ()              { m_ind  = 0; }

	inline bool comparable ( const self& that ) const
		{ return m_seq == that.m_seq; }
	inline sidx_t operator-  ( const self& that ) const
	{
		if ( !comparable(that) )
			throw std::invalid_argument( "[jcl.sequence_iterator.operator-] Input is not comparable." );

		return static_cast<sidx_t>(m_ind) - static_cast<sidx_t>(that.m_ind);
	}

	// Value access
	inline const wrapper_t& data() const { return m_seq; }
	inline uidx_t           size() const { return m_seq.size(); }

	inline ref_t operator[] ( uidx_t n ) const { return m_seq[m_ind+n]; }

	// Validity
	inline bool      valid () const { return m_seq.valid(); }
	inline operator  bool  () const { return valid() && m_ind < size(); }

	// Assignment
	inline void assign( data_t seq, uidx_t first, uidx_t last, sidx_t step = 1 )
		{ m_seq.assign(seq,first,last,step); reset(); }

	inline void assign( const wrapper_t& seq )
		{ m_seq = seq; reset(); }

private:

	uidx_t     m_ind;
	wrapper_t  m_seq;
};

JCL_NS_END_
