
//==================================================
// @title        wrapper.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <utility>
#include <tuple>



        /********************     **********     ********************/
        /********************     **********     ********************/



JCL_NS_START_

// See: containers/sequence/sequence_wrapper
template <class S,class T> class sequence_wrapper;
template <class S,class T> using sequence_wrapper_traits =
	sequence_traits< T, S, sequence_wrapper<S,T> >;

template <class S, class T = typename S::val_t >
class sequence_wrapper :
    public sequence_wrapper_traits<S,T>::sequential_parent,
    public sequence_wrapper_traits<S,T>::iterable_parent,
    public sequence_wrapper_traits<S,T>::arithmetic_parent,
    public sequence_wrapper_traits<S,T>::comparable_parent,
    public sequence_wrapper_traits<S,T>::copyable_parent
{
public:

    CORE_TRAITS(T);
    using self           = sequence_wrapper<S,T>;
    using traits         = sequence_wrapper_traits<S,T>;

    using subarray_type  = self;
    using std_iterator   = typename traits::std_iterator;
    using jcl_iterator   = typename traits::jcl_iterator;

    using seq_t          = sequence_container<S>;
    using data_t         = typename seq_t::data_type;

    // ----------  =====  ----------


    sequence_wrapper()
        { clear(); }
    sequence_wrapper( data_t seq, uidx_t first, uidx_t last, sidx_t step = 1 )
        { assign(seq,first,last,step); }


    void clear();
    void swap( self& that );

    // Extract subarray
    inline subarray_type subarray( uidx_t i, uidx_t j, sidx_t step = 1 ) const
        { return subarray_type( m_seq.get(), index(i), index(j), m_step*step ); }

    // Convert to contiguous array (may throw error)
    inline array<T> to_array() const;


    // Legacy: _sequential
    inline data_t  data  () const { return m_seq.get(); }
    inline uidx_t  size  () const { return m_size; }
    inline bool    valid () const { return m_seq && m_step != 0 && m_size > 0; }

    // Legacy: _iterable
    inline std_iterator  begin () const { return std_iterator( *this ); }
    inline std_iterator    end () const { return  begin() + size(); }

    inline std_iterator rbegin () const { return std_iterator( m_seq.get(), m_last, m_first, -m_step ); }
    inline std_iterator   rend () const { return rbegin() + size(); }

    inline jcl_iterator   iter () const { return  begin(); }
    inline jcl_iterator  riter () const { return rbegin(); }

    // Legacy: _comparable
    inline bool operator== ( const self& that ) const
    {
        return m_seq   ==  that.m_seq
            && m_first ==  that.m_first
            && m_last  ==  that.m_last
            && m_step  ==  that.m_step;
    }

    // Assignment
    self& assign( data_t seq, uidx_t first, uidx_t last, sidx_t step = 1 );

    // Reverse the sequence
    inline self& reverse()
        { bit_swap(m_first,m_last); m_step = -m_step; return *this; }

    // Get current step
    inline sidx_t get_step() const
        { return m_step; }

    // Hide inherited subscripting
    inline ref_t operator[] ( uidx_t n ) const
        { return const_cast<ref_t>( data()[index(n)] ); }

protected:

    inline uidx_t index( uidx_t k ) const
        { return m_first + JCL_IDX(k,m_size) * m_step; }

    seq_t    m_seq;
    uidx_t  m_first, m_last, m_size;
    sidx_t  m_step;
};



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



template <class S, class T>
void sequence_wrapper<S,T>::clear()
{
    m_seq.clear();

    m_step  = 0;
    m_first = m_last = m_size = 0;
}

// ------------------------------------------------------------------------

template <class S, class T>
void sequence_wrapper<S,T>::swap( self& that )
{
    std::swap(
        stay(std::tie( m_seq, m_first, m_last, m_size, m_step )),
        stay(std::tie( that.m_seq, that.m_first, that.m_last, that.m_size, that.m_step ))
    );
}

// ------------------------------------------------------------------------

template <class S, class T>
array<T> sequence_wrapper<S,T>::to_array() const
{
    if ( m_step != 1 || !is_contiguous(data()) )
        throw std::runtime_error("[jcl.sequence_wrapper.to_array] Underlying sequence cannot be converted to an array.");

    return array<T>( &(data()[0]), size() );
}

// ------------------------------------------------------------------------

template <class S, class T>
auto sequence_wrapper<S,T>::assign( data_t seq, uidx_t first, uidx_t last, sidx_t step ) -> self&
{
    JCL_CHK_RV( step != 0, *this,
        "[jcl.sequence_wrapper.assign] Step should be non-zero." );

    if ( first < last && step < 0 ) bit_swap( first, last );
    if ( first > last && step > 0 ) step = -step;

    // Assign members
    m_seq.set(seq);
    m_first = first;
    m_last  = last;
    m_step  = step;

    if ( m_step > 0 )
    {
        JCL_DCHK( m_first <= m_last, "[jcl.sequence_wrapper.assign] This is a bug." );
        m_size = 1 + (m_last-m_first) / m_step;
    }
    else
    {
        JCL_DCHK( m_first >= m_last, "[jcl.sequence_wrapper.assign] This is a bug." );
        m_size = 1 + (m_first-m_last) / (-m_step);
    }

    m_last = index( m_size-1 );
    return *this;
}

JCL_NS_END_
