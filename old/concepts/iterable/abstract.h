
//==================================================
// @title        iterable.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <utility>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

/**
 * Iterable classes can be iterated either forward or backward.
 * Iteration is carried out either the standard way:
 *     for ( it = X.begin(); it != X.end(); ++it ) // ...
 * or the JCL way:
 *     for ( it = X.iter(); it; it.next() ) // ...
 */

template <class Value, class StandardIterator, class JCLIterator>
struct _iterable
{
	virtual uidx_t              size() const =0;

	// JCL iteration
	virtual JCLIterator        iter() const =0;
	virtual JCLIterator       riter() const =0;

	// Standard iteration
	virtual StandardIterator  begin() const =0;
	virtual StandardIterator    end() const =0;

	virtual StandardIterator rbegin() const =0;
	virtual StandardIterator   rend() const =0;
};

// ------------------------------------------------------------------------

/**
 * Helper function to either
 *    extract the iterator type from a JCL container, or
 *    wrap the input container into a generic JCL iterator (cf iterator_wrapper).
 */

// This finds the type of the iterator T().end()
template <class T>
using iterable_iterator_type =
	iterator_wrapper< decltype(std::declval<T>().end()) >;

// For a standard iterable container, wrap the iterators into an iterator_wrapper
template <class T>
inline constexpr iterable_iterator_type<T>
iterable_get_iterator( const T& ctn )
	{ return iterable_iterator_type<T>( ctn.begin(), ctn.end() ); }

// For an _iterable derived class, use the iter() method
template <class V, class S, class D>
inline constexpr D
iterable_get_iterator( const _iterable<V,S,D>& ctn )
	{ return ctn.iter(); }

JCL_NS_END_
