
//==================================================
// @title        comparable.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>

/**
 * Requires an iterable interface.
 *
 * Comparable classes provide element-wise comparison methods:
 *     lt(), gt(), leq(), geq(), eq(), neq()
 * as well as instance comparison overloads:
 *     operator== and operator!=
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class Value, class Forward, bool Enable = is_comparable<Value>::value >
struct _comparable {};

// ------------------------------------------------------------------------

#define JCL_COMPARABLE_OPERATOR( Name, Fun )                                          \
template <class U, class S, class D>                                                  \
bool _comparable_ ## Fun ( const _iterable<U,S,D>& u, std::false_type ) const         \
{                                                                                     \
	JCL_CHK_RV( u.size() == _cm_crtp().size(), false,                                \
		"[jcl._comparable] Wrong input size (received " JPux ", expected " JPux ")",  \
		u.size(), _cm_crtp().size() );                                                \
	return all( _cm_crtp().iter(), u.iter(), Fun<V,U>() );                            \
}                                                                                     \
                                                                                      \
template <class U>                                                                    \
inline bool _comparable_ ## Fun ( const U& u, std::true_type ) const                  \
	{ return all_s( _cm_crtp().iter(), u, Fun<V,U>() ); }                             \
                                                                                      \
template <class U>                                                                    \
inline bool Name ( const U& u ) const                                                 \
	{ return _comparable_ ## Fun ( u, std::is_arithmetic<U>() ); }

// ------------------------------------------------------------------------

template <class V, class F>
struct _comparable<V,F,true>
{
	inline       F& _cm_crtp()       { return static_cast<      F&>(*this); }
	inline const F& _cm_crtp() const { return static_cast<const F&>(*this); }

	// Element-wise more-/less-than comparison
	JCL_COMPARABLE_OPERATOR( lt , f_lt  );
	JCL_COMPARABLE_OPERATOR( leq, f_leq );
	JCL_COMPARABLE_OPERATOR( gt , f_gt  );
	JCL_COMPARABLE_OPERATOR( geq, f_geq );

	// Element-wise equality comparison
	JCL_COMPARABLE_OPERATOR( eq , f_eq  );
	JCL_COMPARABLE_OPERATOR( neq, f_neq );

	// Container comparison
	virtual bool operator== ( const F& that ) const =0;
	inline  bool operator!= ( const F& that ) const
		{ return !operator==(that); }
};

JCL_NS_END_
