
/**
 * Test whether a type implements the iter() method.
 */
template <class T>
struct has_iter
{
	typedef typename std::remove_const<T>::type test_type;

	template <class A>
	static constexpr bool test( A * pt,
		decltype(pt->iter()) * = nullptr
	{ return true; }

	template <class A>
	static constexpr bool test(...)
	{ return false; }

	static const bool value = test<test_type>(nullptr);
};

template <class T>
struct has_iter
	: public bool_type< has_iter_test<T>::value > {};
    