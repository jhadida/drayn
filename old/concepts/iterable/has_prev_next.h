
/**
 * Test whether a type implements the prev()/next() methods.
 */
template <class T>
struct has_prev_next_test
{
	typedef typename std::remove_const<T>::type test_type;

	template <class A>
	static constexpr bool test( A * pt,
		decltype(pt->prev()) * = nullptr,
		decltype(pt->next()) * = nullptr )
	{ return true; }

	template <class A>
	static constexpr bool test(...)
	{ return false; }

	static const bool value = test<test_type>(nullptr);
};

template <class T>
struct has_prev_next
	: public bool_type< has_prev_next_test<T>::value > {};
    