
/**
 * Test whether a type implements the begin()/end() methods.
 */
template <class T>
struct has_begin_end_test
{
	typedef typename std::remove_const<T>::type test_type;

	template <class A>
	static constexpr bool test( A * pt,
		decltype(pt->begin()) * = nullptr,
		decltype(pt->end()) * = nullptr )
	{ return true; }

	template <class A>
	static constexpr bool test(...)
	{ return false; }

	static const bool value = test<test_type>(nullptr);
};

template <class T>
struct has_begin_end
	: public bool_type< has_begin_end_test<T>::value > {};
    