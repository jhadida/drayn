
//==================================================
// @title        iterable_ostream.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iostream>
#include <type_traits>

/**
 * Print the contents of an iterable container.
 *
 * Note:
 * Standard iterable containers can also be printed using
 * std::copy(a.begin(), a.end(), std::ostream_iterator<T>(std::cout, " "));
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class V, class S, class D>
inline std::ostream& iterable_ostream_impl( std::ostream& os, const _iterable<V,S,D>& x, std::false_type )
{
	return os<< "<jcl._iterable with " << x.size() << " element(s)>\n";
}

template <class V, class S, class D>
std::ostream& iterable_ostream_impl( std::ostream& os, const _iterable<V,S,D>& x, std::true_type )
{
	os<< "[ ";
	const auto& nos = onstream<V>(os);
	for ( auto it = x.iter(); it; it.next() )
		nos << it.value() << " ";
	os<< "] (" << x.size() << ")\n";

	return os;
}

template <class V, class S, class D>
inline std::ostream& operator<< ( std::ostream& os, const _iterable<V,S,D>& x )
{
	return iterable_ostream_impl( os, x, is_numeric<V>() );
}

JCL_NS_END_
