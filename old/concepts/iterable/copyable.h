
//==================================================
// @title        copyable.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Copyable classes provide an element-wise method copy().
 * This method shall warn or raise an exception in case of size mismatch with the input.
 * Non-const overloads (eg with silent resizing) can also be defined if derived classes
 * allow it, but this concept does not require such definition.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class Value, class JCLIterator, bool Mutable>
struct _copyable
{
	virtual uidx_t      size() const =0;
	virtual JCLIterator  iter() const =0;
};

// ------------------------------------------------------------------------

template <class Value, class JCLIterator>
struct _copyable<Value, JCLIterator, true>
{
	virtual uidx_t      size() const =0;
	virtual JCLIterator  iter() const =0;

	template <class T>
	void copy( const T& that ) const
	{
		JCL_CHK_R( that.size() == size(),
			"[jcl._copyable] Input size mismatch (received " JPux ", expected " JPux ").",
			static_cast<uidx_t>(that.size()), size() );

		map( iterable_get_iterator(that), iter(), f_cast<Value>() );
	}
};

JCL_NS_END_
