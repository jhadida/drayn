#ifndef JCL_CONTAINERS_H_INCLUDED
#define JCL_CONTAINERS_H_INCLUDED

//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



#include "concepts/includes.h"
#include "pointer.h"
#include "shared.h"

#include "sequence/includes.h"
#include "multidim/includes.h"

#endif
