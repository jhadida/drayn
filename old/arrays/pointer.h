
//==================================================
// @title        pointer.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <utility>

/**
 * C-pointer wrapper with explicit step-size (allowing for "jumps" instead of contiguous steps).
 *
 * Note, from http://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B#cite_note-arrowptr-6
 * The return type of operator->() must be a type for which the -> operation can be applied, such as a pointer type.
 * If x is of type C where C overloads operator->(), x->y gets expanded to x.operator->()->y.
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



JCL_NS_START_

template <class T>
class pointer
    : public _random_access_iterator< T, pointer<T> >
{
public:

    CORE_TRAITS(T);
    using self          = pointer<T>;
    using is_mutable    = bool_negate< std::is_const<T> >;


    pointer()
        { clear(); }
    pointer( ptr_t p, uidx_t s = 1 ) // allow implicit conversion from pointer
        { assign(p,s); }

    template <class U>
    pointer( const pointer<U>& p ) // + when static_cast allows
        { assign( static_cast<ptr_t>(p.memptr()), p.get_step() ); }

    // Cast to pointer
    // inline operator ptr_t () const { return m_ptr; }

    // Legacy: _random_access_iterator
    void        clear      ();
    void        swap       ( self& that );
    sidx_t      operator-  ( const self& that ) const;
    inline bool comparable ( const self& that ) const { return m_step == that.m_step; }

    inline void increment( uidx_t n=1 ) { m_ptr += n*m_step; }
    inline void decrement( uidx_t n=1 ) { m_ptr -= n*m_step; }

    inline ref_t operator[] ( uidx_t n ) const
        { return m_ptr[n*m_step]; }

    // ------------------------------------------------------------------------

    // Validity
    inline bool    valid() const { return m_ptr; }
    inline operator bool() const { return m_ptr; }

    // Access members
    inline ptr_t  memptr() const
        { return m_ptr; }
    inline sidx_t get_step() const
        { return m_step; }
    inline void   set_step( sidx_t step ) 
        { assign(m_ptr,step); }

    // Assignment
    inline self& operator =( ptr_t p )
        { assign(p,1); return *this; }
    void assign( ptr_t p, sidx_t s = 1 );

private:

    ptr_t   m_ptr;
    uidx_t  m_step;
};



        /********************     **********     ********************/
        /********************     **********     ********************/



template <class T>
void pointer<T>::clear()
{
    m_ptr  = nullptr;
    m_step = 0;
}

// ------------------------------------------------------------------------

template <class T>
void pointer<T>::swap( self& that )
{
    std::swap( m_ptr , that.m_ptr  );
    std::swap( m_step, that.m_step );
}

// ------------------------------------------------------------------------

template <class T>
void pointer<T>::assign( ptr_t p, uidx_t s )
{
    JCL_CHK_THROW( p, "Null pointer assignment." )
    JCL_CHK_THROW( s, "Step should be positive." )

    m_ptr  = p;
    m_step = s;
}

// ------------------------------------------------------------------------

template <class T>
sidx_t pointer<T>::operator- ( const self& that ) const
{
    if ( !comparable(that) )
        throw std::invalid_argument( "[jcl.pointer.operator-] Input is not comparable." );

    return std::distance(that.m_ptr,m_ptr) / m_step;
}

JCL_NS_END_
