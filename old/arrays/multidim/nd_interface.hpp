
template <class T, class C>
void nd_interface<T,C>::clear()
{
	m_data           .clear();
	this->m_dims     .clear();
	this->m_strides  .clear();
}

// ------------------------------------------------------------------------

template <class T, class C>
void nd_interface<T,C>::assign( const Container& _data, const nd_dimensions& _dims, Layout _lay )
{
	JCL_CHK_R( _check_dims(_dims), "Dimensions check failed." );
	m_data = _data;
	reshape( _dims, _lay );
}

template <class T, class C>
void nd_interface<T,C>::assign( const Container& _data, const nd_dimensions& _dims, const nd_strides& _strides )
{
	JCL_CHK_R( _check_dims(_dims), "Dimensions check failed." );
	m_data = _data;
	reshape( _dims, _strides );
}

// ------------------------------------------------------------------------

template <class T, class C>
void nd_interface<T,C>::reshape( const nd_dimensions& _dims, Layout _lay )
{
	JCL_CHK_R( _check_dims(_dims), "Dimensions check failed." );
	JCL_CHK_R( _dims.numel() <= capacity(),
		"Dimensions are too large (numel " JPux ", capacity " JPux ").",
		_dims.numel(), capacity() );

	// Assign dimensions
	this->m_dims = _dims;

	// Compute default strides
	std::vector<uidx_t> _strides( ndims(), 1 );
	for ( uidx_t i = 1; i < ndims(); ++i )
		_strides[i] = _strides[i-1]*dim(i-1);

	// Apply layout convention
	if ( _lay == Layout::Rows )
	{
		_strides[1] = 1;
		_strides[0] = dim(1);
	}

	this->m_strides.assign( wrap(_strides) );
}

template <class T, class C>
void nd_interface<T,C>::reshape( const nd_dimensions& _dims, const nd_strides& _strides )
{
	JCL_CHK_R( _check_dims(_dims), "Dimensions check failed." );

	uidx_t max_index = alg_dot( _dims.dims(), _strides.strides() ) - alg_sum( _strides.strides() );
	JCL_CHK_R( max_index < capacity(),
		"Max index is too large (" JPux " >= capacity = " JPux ").", max_index, capacity() );

	// Assign dimensions and strides
	this->m_dims    = _dims;
	this->m_strides = _strides;
}

// ------------------------------------------------------------------------

template <class T, class C>
void nd_interface<T,C>::permute( const ndc_ucoord& perm )
{
	vector<uidx_t> arr( ndims(), 0 );

	// Make sure it is a permutation
	#ifdef JCL_DEBUG_MODE

		JCL_CHK_R( perm.size() == ndims(), "Wrong permutation size." );
		JCL_CHK_R( perm.lt(ndims()), "All elements should be less than " JPux ".", ndims() );
		for ( auto& p: perm )
			JCL_CHK_R( ++arr[p] == 1, "Dimension " JPux " is repeated twice.", p );

	#endif

	// Permute strides
	for ( uidx_t i = 0; i < ndims(); ++i )
		arr[i] = stride(perm[i]);
	this->m_strides.assign( arr );

	// Permute dimensions
	for ( uidx_t i = 0; i < ndims(); ++i )
		arr[i] = dim(perm[i]);
	this->m_dims.assign( arr );
}

// ------------------------------------------------------------------------

template <class T, class C>
sequence_wrapper<C,T> nd_interface<T,C>::subvector( const ndc_ucoord& _start, uidx_t _dim ) const
{
	sequence_wrapper<C,T> output;
	JCL_CHK_RV( _start.size() == ndims(), output,
		"Wrong input size (expected " JPux ", received " JPux ").", ndims(), _start.size() );

	uidx_t first = sub2ind( _start, this->m_dims, this->m_strides );
	uidx_t last  = first + (dim(_dim)-1 - JCL_IDX(_start[_dim],dim(_dim))) * stride(_dim);

	output.assign( m_data, first, last, stride(_dim) );
	return output;
}

// ------------------------------------------------------------------------

template <class T, class C>
template <class U>
U nd_interface<T,C>::subarray( const ndc_ucoord& _start, const nd_dimensions& _dims ) const
{
	U output;
	JCL_CHK_RV( _start.size() == ndims(), output,
		"Wrong start size (expected " JPux ", received " JPux ").", ndims(), _start.size() );
	JCL_CHK_RV( _dims.ndims() == ndims(), output,
		"Wrong dims size (expected " JPux ", received " JPux ").", ndims(), _dims.ndims() );


	uidx_t nz, nd, k;

	nd = ndims();
	nz = std::count( _dims.begin(), _dims.end(), 0 );
	nz = nd-nz;
	k  = 0;

	JCL_CHK_RV( nz, output, "Null dimensions." );
	std::vector<uidx_t> t_dims(nz), t_strides(nz);

	for ( uidx_t i = 0; i < nd; ++i ) if ( _dims[i] )
	{
		t_dims[k]    = _dims[i];
		t_strides[k] = stride(i);
			++k;
	}

	output.assign(
		m_data, nd_dimensions(wrap(t_dims)),
		nd_strides( wrap(t_strides), sub2ind(_start,this->m_dims,this->m_strides) )
	);
	return output;
}
