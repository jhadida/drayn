
//==================================================
// @title        indexing.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Bundle for indexing utilities.
 */

#include <algorithm>



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

#include "indexing/typedefs.h"
#include "indexing/dimensions.h"
#include "indexing/strides.h"

// ------------------------------------------------------------------------

/**
 * Column or row convention for indexing.
 * "Column" assumes a memory layout of concatenated columns (row-major).
 * "Row" assumes a memory layout of concatenated rows (column-major).
 */
enum class MemoryLayout
	: char { Columns=0, Rows=1 };

// ------------------------------------------------------------------------

/**
 * Returns the index in the underlying container of a nd_array object,
 * of the element at coordinates sub, given the dimensions and strides.
 */
void sub2ind( const ndc_ucoord& sub, const nd_dimensions& dims,
	const nd_strides& strides, uidx_t& ind );

uidx_t sub2ind( const ndc_ucoord& sub, const nd_dimensions& dims,
	const nd_strides& strides );


// ------------------------------------------------------------------------

/**
 * Computes the coordinates of an element at the sequential index ind
 * in a nd_array, given its dimensions and memory layout.
 *
 * The memory layout defaults to MemoryLayout::Columns, and please note
 * that this does not take the strides into account.
 */
void ind2sub( uidx_t ind, const nd_dimensions& dims,
	const nd_ucoord& sub, MemoryLayout lay = MemoryLayout::Columns );

JCL_NS_END_
