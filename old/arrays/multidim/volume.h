
//==================================================
// @title        volume.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class T, class C = array_shared<T>>
class volume
	: public nd_interface<T,C>
{
public:

	CORE_TRAITS(T);
	typedef volume<T,C>        self;
	typedef nd_interface<T,C>  parent;
	typedef nd_traits<T,C>     traits;
	typedef tag_volume         tag;

	typedef MemoryLayout                     Layout;
	typedef typename traits::container_type  Container;


	volume()
		{ this->clear(); }

	volume( const self& that )
		{ this->assign(that); }
	volume( self&& that )
		{ this->assign(std::move(that)); }

	volume( const Container& _data, const nd_dimensions& _dims, Layout _lay = Layout::Columns )
		{ this->assign(_data,_dims,_lay); }
	volume( const Container& _data, const nd_dimensions& _dims, const nd_strides& _strides )
		{ this->assign(_data,_dims,_strides); }


	inline uidx_t nrows  () const { return this->dim(0); }
	inline uidx_t ncols  () const { return this->dim(1); }
	inline uidx_t nslices() const { return this->dim(2); }


	// ------------------------------------------------------------------------


	using parent::assign;
	void swap( self& that );

	inline self& operator= ( const self& that ) { assign(that); return *this; }
	       void  assign    ( const self& that );

	inline self& operator= ( self&& that ) { assign(std::move(that)); return *this; }
	       void  assign    ( self&& that );


	// ------------------------------------------------------------------------

	// Extract slice
	inline matrix<T,C> slice( uidx_t k, uidx_t orth = 2 ) const
	{
		fixed<uidx_t,3> _start(0), _dims(this->m_dims.dims());

		_start [orth] = JCL_IDX(k,this->dim(orth));
		_dims  [orth] = 0;

		return this->template subarray< matrix<T,C> >( _start, nd_dimensions(_dims) );
	}

	// Subvolume
	inline self subvol( const ndc_ucoord& _start, const nd_dimensions& _dims ) const
		{ return this->template subarray<self>( _start, _dims ); }
	inline self subvol( const inilst<uidx_t>& _start, const nd_dimensions& _dims ) const
		{ return this->template subarray<self>( wrap_inilst(_start), _dims ); }

	// Faster access
	using parent::operator();
	ref_t operator() ( uidx_t i, uidx_t j, uidx_t k ) const
	{
		i = this->offset()
			+ JCL_IDX(i,this->dim(0)) * this->stride(0)
			+ JCL_IDX(j,this->dim(1)) * this->stride(1)
			+ JCL_IDX(k,this->dim(2)) * this->stride(2);
		return this->m_data[i];
	}

protected:

	inline bool _check_dims( const nd_dimensions& _dims ) const
		{ JCL_CHK_RF( _dims.ndims() == 3, "Ndims should be 3." ); return true; }
};

// ------------------------------------------------------------------------

template <class T, class C>
void volume<T,C>::swap( self& that )
{
	this->m_data    .swap( that.m_data );
	this->m_dims    .swap( that.m_dims );
	this->m_strides .swap( that.m_strides );
}

template <class T, class C>
void volume<T,C>::assign( const self& that )
{
	this->m_data    = that.m_data;
	this->m_dims    .copy( that.dims()    );
	this->m_strides .copy( that.strides() );
}

template <class T, class C>
void volume<T,C>::assign( self&& that )
{
	this->m_data    = that.m_data;
	this->m_dims    = that.dims();
	this->m_strides = that.strides();
}



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class T, class C>
std::ostream& volume_ostream_impl( std::ostream& os, const volume<T,C>& x, std::false_type )
{
	const uidx_t nr = x.nrows();
	const uidx_t nc = x.ncols();
	const uidx_t ns = x.nslices();

	return os<< "[jcl.volume instance] {size:" << nr<<"x"<<nc<<"x"<<ns << ", numel:" << nr*nc*ns << "}" << std::endl;
}

template <class T, class C>
std::ostream& volume_ostream_impl( std::ostream& os, const volume<T,C>& x, std::true_type )
{
	const uidx_t nr = x.nrows();
	const uidx_t nc = x.ncols();
	const uidx_t ns = x.nslices();

	os<< "[jcl.volume instance] {size: " << nr<<"x"<<nc<<"x"<<ns << ", numel: " << nr*nc*ns << "}" << std::endl;
	if ( x.size() )
	for ( uidx_t s=0; s<ns; ++s )
	{
		os<< "Slice " << s << ":\n";
		for ( uidx_t r=0; r<nr; ++r )
		{
			for ( uidx_t c=0; c<nc; ++c )
				onstream<T>(os)<< x(r,c,s) << " ";
			os<< std::endl;
		}
		os<< std::endl;
	}
	return os;
}

// ------------------------------------------------------------------------

template <class T, class C>
inline std::ostream& operator<<( std::ostream& os, const volume<T,C>& x )
{
	return volume_ostream_impl( os, x, is_numeric<T>() );
}

JCL_NS_END_
