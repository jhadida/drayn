
//==================================================
// @title        matrix.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class T, class C = array_shared<T>>
class matrix
	: public nd_interface<T,C>
{
public:

	CORE_TRAITS(T);
	typedef matrix<T,C>            self;
	typedef nd_interface<T,C>      parent;
	typedef nd_traits<T,C>         traits;
	typedef tag_matrix             tag;
	typedef sequence_wrapper<C,T>  wrapper_type;
	typedef wrapper_type           col_type;
	typedef wrapper_type           row_type;

	typedef MemoryLayout                     Layout;
	typedef typename traits::container_type  Container;


	matrix()
		{ this->clear(); }

	matrix( const self& that )
		{ this->assign(that); }
	matrix( self&& that )
		{ this->assign(std::move(that)); }

	matrix( const Container& _data, const nd_dimensions& _dims, Layout _lay = Layout::Columns )
		{ this->assign(_data,_dims,_lay); }
	matrix( const Container& _data, const nd_dimensions& _dims, const nd_strides& _strides )
		{ this->assign(_data,_dims,_strides); }


	inline uidx_t nrows() const { return this->dim(0); }
	inline uidx_t ncols() const { return this->dim(1); }


	// ------------------------------------------------------------------------


	using parent::assign;
	void swap( self& that );

	inline self& operator= ( const self& that ) { assign(that); return *this; }
	       void  assign    ( const self& that );

	inline self& operator= ( self&& that ) { assign(std::move(that)); return *this; }
	       void  assign    ( self&& that );


	// ------------------------------------------------------------------------

	// Extract row/columns
	inline row_type row( uidx_t i ) const
		{ return this->subvector( wrap_inilst<uidx_t>({ i, 0 }), 1 ); }

	inline col_type col( uidx_t j ) const
		{ return this->subvector( wrap_inilst<uidx_t>({ 0, j }), 0 ); }

	// Submatrix
	inline self submat( const ndc_ucoord& _start, const nd_dimensions& _dims )
		{ return this->template subarray<self>(_start,_dims); }
	inline self submat( const inilst<uidx_t>& _start, const nd_dimensions& _dims )
		{ return this->template subarray<self>( wrap_inilst(_start), _dims ); }

	// Faster access
	using parent::operator();
	ref_t operator() ( uidx_t i, uidx_t j ) const
	{
		i = this->offset()
			+ JCL_IDX(i,this->dim(0)) * this->stride(0)
			+ JCL_IDX(j,this->dim(1)) * this->stride(1);
		return this->m_data[i];
	}

protected:

	inline bool _check_dims( const nd_dimensions& _dims ) const
		{ JCL_CHK_RF( _dims.ndims() == 2, "Ndims should be 2." ); return true; }
};

// ------------------------------------------------------------------------

template <class T, class C>
void matrix<T,C>::swap( self& that )
{
	this->m_data    .swap( that.m_data );
	this->m_dims    .swap( that.m_dims );
	this->m_strides .swap( that.m_strides );
}

template <class T, class C>
void matrix<T,C>::assign( const self& that )
{
	this->m_data    = that.m_data;
	this->m_dims    .copy( that.dims()    );
	this->m_strides .copy( that.strides() );
}

template <class T, class C>
void matrix<T,C>::assign( self&& that )
{
	this->m_data    = that.m_data;
	this->m_dims    = that.dims();
	this->m_strides = that.strides();
}



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class T, class C>
std::ostream& matrix_ostream_impl( std::ostream& os, const matrix<T,C>& x, std::false_type )
{
	const uidx_t nr = x.nrows();
	const uidx_t nc = x.ncols();
	return os<< "[jcl.matrix instance] {size: " << nr<<"x"<<nc << ", numel: " << nr*nc << "}" << std::endl;
}

template <class T, class C>
std::ostream& matrix_ostream_impl( std::ostream& os, const matrix<T,C>& x, std::true_type )
{
	const uidx_t nr = x.nrows();
	const uidx_t nc = x.ncols();
	os<< "[jcl.matrix instance] {size: " << nr<<"x"<<nc << ", numel: " << nr*nc << "}" << std::endl;

	if ( x.size() )
	for ( uidx_t r=0; r<nr; ++r )
	{
		for ( uidx_t c=0; c<nc; ++c )
			onstream<T>(os)<< x(r,c) << " ";
		os<< std::endl;
	}
	return os<< std::endl;
}

// ------------------------------------------------------------------------

template <class T, class C>
inline std::ostream& operator<<( std::ostream& os, const matrix<T,C>& x )
{
	return matrix_ostream_impl( os, x, is_numeric<T>() );
}

JCL_NS_END_
