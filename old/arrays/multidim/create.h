
//==================================================
// @title        create.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class T, class C = array_shared<T> >
inline matrix<T,C> create_col( uidx_t n )
	{ return nd_array<T,C>( C( n ), wrap_inilst<uidx_t>({n,1}), MemoryLayout::Columns ); }

template <class T, class C = array_shared<T> >
inline matrix<T,C> create_row( uidx_t n )
	{ return nd_array<T,C>( C( n ), wrap_inilst<uidx_t>({1,n}), MemoryLayout::Rows ); }

template <class T, class C = array_shared<T> >
inline matrix<T,C> create_vector( uidx_t n )
	{ return create_col<T,C>(n); }

// ------------------------------------------------------------------------

template <class T, class C = array_shared<T> >
inline matrix<T,C> create_matrix( uidx_t nr, uidx_t nc, MemoryLayout lay = MemoryLayout::Columns )
	{ return matrix<T,C>( C( nr*nc ), wrap_inilst<uidx_t>({nr,nc}), lay ); }

template <class T, class C = array_shared<T> >
inline volume<T,C> create_volume( uidx_t nr, uidx_t nc, uidx_t ns, MemoryLayout lay = MemoryLayout::Columns )
	{ return volume<T,C>( C( nr*nc*ns ), wrap_inilst<uidx_t>({nr,nc,ns}), lay ); }

template <class T, class C = array_shared<T> >
inline nd_array<T,C> create_array( const nd_dimensions& dims, MemoryLayout lay = MemoryLayout::Columns )
	{ return nd_array<T,C>( C( dims.numel() ), dims, lay ); }

JCL_NS_END_
