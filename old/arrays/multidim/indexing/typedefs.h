
//==================================================
// @title        typedefs.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



typedef array<uidx_t>   nd_ucoord;
typedef array<sidx_t>   nd_icoord;
typedef array<double>  nd_rcoord;
typedef nd_ucoord      nd_subs;

typedef const_array<uidx_t>   ndc_ucoord;
typedef const_array<sidx_t>   ndc_icoord;
typedef const_array<double>  ndc_rcoord;
typedef ndc_ucoord           ndc_subs;
