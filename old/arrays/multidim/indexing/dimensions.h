
//==================================================
// @title        dimensions.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



class nd_dimensions
{
public:

	using self          = nd_dimensions;
	using dims_type     = vector<uidx_t>;
	using jcl_iterator  = typename dims_type::jcl_iterator;
	using std_iterator  = typename dims_type::std_iterator;


	// Ctors/dtor
	nd_dimensions()
		{ clear(); }
	nd_dimensions( const ndc_ucoord& _dims )
		{ assign(_dims); }
	nd_dimensions( const inilst<uidx_t>& _dims )
		{ assign(wrap_inilst(_dims)); }

	void clear();
	void swap( self& that );


	// Accessors
	inline uidx_t       ndims() const { return m_dims.size(); }
	inline uidx_t       numel() const { return m_numel; }
	inline ndc_ucoord  dims () const { return m_dims; }

	inline uidx_t dim        ( uidx_t k ) const { return (k < ndims())? m_dims[k] : 0; }
	inline uidx_t operator[] ( uidx_t k ) const { return m_dims[k]; }


	// ----------  =====  ----------

	// Iterate over dimensions
	inline std_iterator begin() const { return m_dims.begin(); }
	inline std_iterator end  () const { return m_dims.end(); }
	inline jcl_iterator iter () const { return m_dims.iter(); }

	// Assignments
	inline void  copy      ( const self& that )         { assign(that.m_dims); }
	inline self& operator= ( const inilst<uidx_t>& d ) { assign(wrap_inilst(d)); return *this; }
	inline self& operator= ( const ndc_ucoord& d )      { assign(d); return *this; }
	       void  assign    ( const ndc_ucoord& d );

	// Comparison
	inline bool operator== ( const self& that ) const { return m_dims.eq(that.m_dims); }
	inline bool operator!= ( const self& that ) const { return !operator==(that); }
	

	// ----------  =====  ----------
	
	// Index inbound testing
	template <class T> 
	inline bool is_in ( const T& sub ) const { return !_is_out_impl(sub); }
	template <class T> 
	inline bool is_out( const T& sub ) const { return  _is_out_impl(sub); }

	template <class T> 
	inline bool is_in ( const inilst<T>& sub ) const { return !_is_out_impl(wrap_inilst(sub)); }
	template <class T> 
	inline bool is_out( const inilst<T>& sub ) const { return  _is_out_impl(wrap_inilst(sub)); }
	
	
	// Increment/decrement subindex (indices should be (unsigned) INTEGERS)
	template <class T>
	void sub_increment( T& sub, uidx_t i=0 ) const;
	template <class T>
	void sub_decrement( T& sub, uidx_t i=0 ) const;


private:

	template <class T> 
	bool _is_out_impl( const T& sub ) const;


	dims_type  m_dims;
	uidx_t      m_numel;
};

// ------------------------------------------------------------------------

inline std::ostream& operator<<( std::ostream& os, const nd_dimensions& d )
	{ return os<< "[dimensions]: " << d.dims(); }



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class T> 
bool nd_dimensions::_is_out_impl( const T& sub ) const
{
	const uidx_t n = ndims();
	JCL_DCHK_RF( sub.size() == n, 
		"Wrong input size (received " JPux ", expected " JPux ").",
		static_cast<uidx_t>(sub.size()), n );

	sint_t s; 
	bool out = false; 
	
	for ( uidx_t k=0; !out && k < n; ++k )
	{
		s   = op_ifix(sub[k]);
		out = s < 0 || s >= static_cast<sint_t>(m_dims[k]);
	}

	return out;
}

// ------------------------------------------------------------------------

template <class T>
void nd_dimensions::sub_increment( T& sub, uidx_t i ) const
{
	JCL_DCHK_R( sub.size() == ndims(), 
		"Wrong input size (received " JPux ", expected " JPux ").",
		static_cast<uidx_t>(sub.size()), ndims() );

	for (; i < ndims() && (++sub[i] == m_dims[i]); ++i )
		sub[i] = 0;
}

template <class T>
void nd_dimensions::sub_decrement( T& sub, uidx_t i ) const
{
	JCL_DCHK_R( sub.size() == ndims(), 
		"Wrong input size (received " JPux ", expected " JPux ").",
		static_cast<uidx_t>(sub.size()), ndims() );

	for (; i < ndims() && (sub[i]-- == 0); ++i )
		sub[i] = m_dims[i]-1;
}
