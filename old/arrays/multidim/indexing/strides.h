
//==================================================
// @title        strides.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



class nd_strides
{
public:

	using self          = nd_strides;
	using strides_type  = vector<uidx_t>;
	using jcl_iterator  = typename strides_type::jcl_iterator;
	using std_iterator  = typename strides_type::std_iterator;


	nd_strides()
		{ clear(); }

	explicit
	nd_strides( const ndc_ucoord& _strides, uidx_t _offset = 0 )
		{ assign(_strides,_offset); }

	void clear();
	void swap( self& that );
	

	// Swap strides i and j
	inline void swap( uidx_t i, uidx_t j )
		{ bit_swap( m_strides.at(i), m_strides.at(j) ); }

	// Accessors
	inline uidx_t       ndims  ()  const { return m_strides.size(); }
	inline uidx_t       offset ()  const { return m_offset; }
	inline ndc_ucoord  strides()  const { return m_strides; }

	inline uidx_t stride     ( uidx_t k ) const { return (k < ndims())? m_strides[k] : 0; }
	inline uidx_t operator[] ( uidx_t k ) const { return m_strides[k]; }


	// Individual setters
	inline void set_strides( const ndc_ucoord& s ) { m_strides.copy(s); }
	inline void set_offset ( uidx_t o )           { m_offset = o; }

	// Comparison
	inline bool operator== ( const self& that ) const 
		{ return m_strides.eq(that.m_strides) && m_offset && that.m_offset; }
	inline bool operator!= ( const self& that ) const 
		{ return !operator==(that); }


	// Iterate over strides
	inline std_iterator begin() const { return m_strides.begin(); }
	inline std_iterator end  () const { return m_strides.end(); }
	inline jcl_iterator  iter () const { return m_strides.iter(); }

	// Assignments
	inline self& operator= ( const ndc_ucoord& s )      { assign(s); return *this; }
	inline self& operator= ( const inilst<uidx_t>& s ) { assign(wrap_inilst(s)); return *this; }
	inline void  copy      ( const self& that )         { assign(that.m_strides,that.m_offset); }
	void assign( const ndc_ucoord& _strides, uidx_t _offset = 0 );
	

private:

	strides_type  m_strides;
	uidx_t         m_offset;
};

// ------------------------------------------------------------------------

inline std::ostream& operator<<( std::ostream& os, const nd_strides& s )
	{ return os<< "[strides]: " << s.strides(); }

