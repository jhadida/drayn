
//==================================================
// @title        indexing.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

#include "indexing/dimensions.cpp"
#include "indexing/strides.cpp"

// ------------------------------------------------------------------------

void sub2ind( const array<const uidx_t>& sub, const nd_dimensions& dims,
	const nd_strides& strides, uidx_t& ind )
{
	JCL_DCHK_R( dims.ndims() == sub.size(),
		"Input size mismatch between dimensions and sub." );
	JCL_DCHK_R( dims.ndims() == strides.ndims(),
		"Input size mismatch between dimensions and strides." );

	uidx_t nd = dims.ndims();

	ind = strides.offset();
	for ( uidx_t i = 0; i < nd; ++i )
		ind += JCL_IDX(sub[i],dims[i]) * strides[i];
}

uidx_t sub2ind( const array<const uidx_t>& sub, const nd_dimensions& dims,
	const nd_strides& strides )
{
	uidx_t ind = 0; 
	sub2ind(sub,dims,strides,ind);
	return ind;
}

// ------------------------------------------------------------------------

void ind2sub( uidx_t ind, const nd_dimensions& dims,
	const array<uidx_t>& sub, MemoryLayout lay )
{
	JCL_DCHK_R( dims.ndims() == sub.size(),
		"Input size mismatch between dimensions and sub." );

	uidx_t nd      = dims.ndims();
	uidx_t cumprod = dims.numel();

	for ( int i = nd-1; i >= 0; --i )
	{
		cumprod /= dims[i];
		sub[i]   = ind / cumprod;
		ind      = ind % cumprod;
	}

	if ( lay == MemoryLayout::Rows )
		bit_swap( sub[0], sub[1] );
}

JCL_NS_END_
