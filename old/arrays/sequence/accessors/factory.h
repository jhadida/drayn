
//==================================================
// @title        factory.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



enum class AccessorId
	: char { Default=0, Cyclic, WrapAround, Bcond, ConstantBoundary };

template <class C>
struct accessor_factory
{
	typedef C                                container_type;
	typedef accessor<C>                      accessor_type;
	typedef std::shared_ptr<accessor_type>   output_type;
	typedef AccessorId                       id_type;


	template <class... Args>
	static output_type create( id_type id, Args&&... args )
	{
		output_type out;
		switch ( id )
		{
			case id_type::Cyclic:
			case id_type::WrapAround:
				out.reset( factory_create(
					factory_tag< accessor_cyclic<C> >(),
					std::forward<Args>(args)...
				) );
				break;

			case id_type::Bcond:
			case id_type::ConstantBoundary:
				out.reset( factory_create(
					factory_tag< accessor_bcond<C> >(),
					std::forward<Args>(args)...
				) );
				break;

			default:
				out.reset( factory_create(
					factory_tag< accessor_default<C> >(),
					std::forward<Args>(args)...
				) );
				break;
		}
		return out;
	}
};
