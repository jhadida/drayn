
//==================================================
// @title        default.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class Container>
class accessor_default
	: public accessor<Container>
{
public:

	FORWARD_TRAITS( Container );
	ACCESSOR_TRAITS
	typedef accessor_default<Container> self;


	accessor_default()
		{ this->clear(); }
	accessor_default( const Container& ctn )
		{ this->assign(ctn); }

	// Forward operator() to _value_of
	inline ref_t operator() ( const uidx& x ) const
		{ return _value_of(x); }
	inline ref_t operator() ( const iidx& x ) const
		{ return _value_of(x); }
	inline ref_t operator() ( const ridx& x ) const
		{ return _value_of(x); }

private:

	template <class T>
	inline ref_t _value_of( const T& x ) const
	{
		JCL_DCHK_RV( this->valid(), static_val<val_t>(),
			"[jcl.accessor_default] Accessor is not ready yet!" );
		uidx_t idx = op_clamp< sint_t >( op_iround(x), 0, this->size()-1 );
		return (*this->m_ctn)[idx];
	}
};
