
//==================================================
// @title        accessors.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <memory>



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

/**
 * Accessor traits for containers.
 */

#define ACCESSOR_TRAITS  \
typedef uidx_t  uidx;   \
typedef sidx_t  iidx;   \
typedef double   ridx;

// ----------  =====  ----------

// abstract base class
#include "abstract.h"

// concrete implementations
#include "default.h"
#include "cyclic.h"
#include "bcond.h"

// factory
#include "factory.h"

JCL_NS_END_
