
//==================================================
// @title        cyclic.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



/**
 * Accessor with wrap-arround bounary-condition.
 */

template <class Container>
class accessor_cyclic
	: public accessor<Container>
{
public:

	FORWARD_TRAITS( Container );
	ACCESSOR_TRAITS
	typedef accessor_cyclic<Container> self;


	accessor_cyclic()
		{ this->clear(); }
	accessor_cyclic( const Container& ctn )
		{ this->assign(ctn); }

	// Forward operator() to _value_of
	inline ref_t operator() ( const uidx& x ) const
		{ return _value_of(x); }
	inline ref_t operator() ( const iidx& x ) const
		{ return _value_of(x); }
	inline ref_t operator() ( const ridx& x ) const
		{ return _value_of(x); }

private:

	template <class T>
	inline ref_t _value_of( const T& x ) const
	{
		JCL_DCHK_RV( this->valid(), static_val<val_t>(),
			"[jcl.accessor_cyclic] Accessor is not ready yet!" );
		return (*this->m_ctn)[ static_cast<uidx_t>(op_mod(
			op_iround(x), static_cast<sint_t>(this->size())
		)) ];
	}

	inline ref_t _value_of( const uidx& x ) const
	{
		JCL_DCHK_RV( this->valid(), static_val<val_t>(),
			"Accessor is not ready yet!" );
		return (*this->m_ctn)[ x % this->size() ];
	}
};
