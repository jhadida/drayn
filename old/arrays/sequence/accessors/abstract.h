
//==================================================
// @title        abstract.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



/**
 * Abstract sequential accessor interface.
 */

template <class Container>
struct accessor
{
	FORWARD_TRAITS(Container);
	ACCESSOR_TRAITS

	const Container *m_ctn;

	// Assignment
	inline virtual void clear()
		{ m_ctn = nullptr; }
	inline virtual void assign( const Container& ctn )
		{ m_ctn = &ctn; }

	// Validity
	inline virtual bool valid() const
		{ return m_ctn; }
	inline virtual operator bool() const
		{ return valid(); }

	// Required implementations
	inline virtual uidx_t size() const
		{ return valid()? m_ctn->size() : 0; }

	virtual ref_t operator() ( const uidx& x ) const =0;
	virtual ref_t operator() ( const iidx& x ) const =0;
	virtual ref_t operator() ( const ridx& x ) const =0;
};
