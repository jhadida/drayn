
//==================================================
// @title        array_fixed.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <array>
#include <utility>

/**
 * Fixed-size (stack) array.
 * This implementation simply wraps around std::array.
 */


		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class T, uint16_t N>
class array_fixed :
	public array_fixed_traits<T,N>::sequential_parent,
	public array_fixed_traits<T,N>::iterable_parent,
	public array_fixed_traits<T,N>::arithmetic_parent,
	public array_fixed_traits<T,N>::comparable_parent,
	public array_fixed_traits<T,N>::copyable_parent
{
public:

	CORE_TRAITS(T);
	using self            = array_fixed<T,N>;
	using traits          = array_fixed_traits<T,N>;
	using tag             = tag_array_fixed;

	using container_type  = typename traits::container_type;
	using std_iterator    = typename traits::std_iterator;
	using jcl_iterator    = typename traits::jcl_iterator;
	using data_t          = typename traits::data_type;

	using subarray_type   = sequence_wrapper<container_type,val_t>;

	static_assert( traits::is_mutable::value,
		"Immutable fixed-size arrays are not allowed." );

	// ----------  =====  ----------


	array_fixed() {}
	array_fixed( const self& that )
		{ assign(that); }
	~array_fixed() {}

	// Value-initialize
	explicit
	array_fixed( const T& val )
		{ m_data.fill(val); }

	// Deep copy
	template <class U, class S, class D>
	array_fixed( const _iterable<U,S,D>& that )
		{ this->copy(that); }
	array_fixed( const std::array<T,N>& that )
		{ this->copy(that); }
	array_fixed( const inilst<T>& that )
		{ this->copy(that); }

	// Conversion to array
	template <class U>
	inline operator array<U>() const
		{ return array<U>( &m_data[0], size() ); }


	// Instance management
	inline void clear() {}
	inline self clone() const { return *this; }

	inline void swap( self& that )
		{ m_data.swap(m_data); }


	// Extract subarray
	inline subarray_type subarray( uidx_t first, uidx_t last, sidx_t step = 1 ) const
		{ return subarray_type( m_data, first, last, step ); }


	// Legacy: _sequential
	inline data_t   data  () const { return m_data; }
	inline uidx_t  size  () const { return N; }
	inline bool     valid () const { return true; }


	// Legacy: _iterable
	inline std_iterator  begin () const { return std_iterator(const_cast<ptr_t>( &m_data[0] )); }
	inline std_iterator    end () const { return begin()+size(); }

	inline std_iterator rbegin () const { auto p =   end(); p.reverse(); return ++p; }
	inline std_iterator   rend () const { auto p = begin(); p.reverse(); return ++p; }

	inline jcl_iterator   iter () const { return jcl_iterator(  begin(),  end() ); }
	inline jcl_iterator  riter () const { return jcl_iterator( rbegin(), rend() ); }

	// Legacy: _comparable
	inline bool operator== ( const self& that ) const
		{ return m_data == that.m_data; }

	// Assignment
	template <class U> inline self& operator= ( const U& that ) { assign(that); return *this; }
	template <class U> inline self& assign    ( const U& that ) { this->copy(that); return *this; }

protected:

	std::array<T,N> m_data;
};

JCL_NS_END_
