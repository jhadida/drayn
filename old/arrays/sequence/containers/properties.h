
//==================================================
// @title        properties.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



JCL_NS_START_

/**
 * Contiguity determines whether neighbour elements by index are stored 
 * next to each other in memory.
 * 
 * This question can only be answered at run-time.
 */

template <class Array>
inline bool is_contiguous( const Array& a, tag_array )
	{ return true; }

template <class Array>
inline bool is_contiguous( const Array& a, tag_array_fixed )
	{ return true; }

template <class Array>
inline bool is_contiguous( const Array& a, tag_array_shared )
	{ return true; }

template <class Array>
inline bool is_contiguous( const Array& a, tag_array_chunked )
	{ return a.n_chunks() == 1; }

// ----------  =====  ----------

template <class Array>
inline bool is_contiguous( const Array& a )
	{ return is_contiguous( a, typename Array::tag() ); }

// ------------------------------------------------------------------------

/**
 * Resizeability can be answered at compile-time.
 */

template <class Array, class Tag = typename Array::tag >
struct is_resizeable: bool_type<false> {};

template <class Array>
struct is_resizeable<Array,tag_array_shared>
	: bool_negate<std::is_same<
		typename Array::allocator_type::tag,
		allocator_noalloc<typename Array::val_t>
	>> {};

template <class Array>
struct is_resizeable<Array,tag_array_chunked>
	: bool_negate<std::is_same<
		typename Array::allocator_type::tag,
		allocator_noalloc<typename Array::val_t>
	>> {};

JCL_NS_END_

