
//==================================================
// @title        array_shared.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <algorithm>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class T, class A = allocator_new<T>>
class array_shared :
	public array_shared_traits<T,A>::sequential_parent,
	public array_shared_traits<T,A>::iterable_parent,
	public array_shared_traits<T,A>::arithmetic_parent,
	public array_shared_traits<T,A>::comparable_parent,
	public array_shared_traits<T,A>::copyable_parent
{
public:

	CORE_TRAITS(T);
	using self            = array_shared<T,A>;
	using traits          = array_shared_traits<T,A>;
	using tag             = tag_array_shared;

	using allocator_type  = A;
	using container_type  = typename traits::container_type;
	using std_iterator    = typename traits::std_iterator;
	using jcl_iterator    = typename traits::jcl_iterator;
	using data_t          = typename traits::data_type;

	using subarray_type   = sequence_wrapper<container_type,val_t>;

	static_assert( traits::is_mutable::value,
		"Immutable vectors are not allowed, use const_array instead." );

	static_assert( std::is_same<T,typename A::val_t>::value,
		"Type mismatch between value type and allocator." );

	// ----------  =====  ----------


	array_shared()
		{ clear(); }
	array_shared( const shared<T,A>& ptr )
		{ assign(ptr); }

	// Create
	explicit
	array_shared( uidx_t n )
		{ create(n); }
	array_shared( uidx_t n, const T& val )
		{ create(n,val); }

	// Deep-copy
	template <class U, class S, class D>
	explicit array_shared( const _iterable<U,S,D>& vals )
		{ copy(vals); }

	// Conversion to array
	template <class U>
	inline operator array<U>() const
		{ return array<U>( m_ptr.get(), size() ); }


	// Instance management methods
	inline void clear()
		{ m_ptr.clear(); }
	inline self clone() const
		{ return self(*this); }
	inline void swap( self& that )
		{ m_ptr.swap( that.m_ptr ); }

	template <class U>
	void copy( const U& that );


	// Resizing (create: discard current, resize: copy current)
	inline self& create( uidx_t n ) 
		{ m_ptr.create(n); return *this; }
	inline self& create( uidx_t n, const T& val ) 
		{ m_ptr.create(n,val); return *this; }
	self& resize( uidx_t n );


	// Extract subarray
	inline subarray_type subarray( uidx_t first, uidx_t last, sidx_t step = 1 ) const
		{ return subarray_type( m_ptr, first, last, step ); }


	// Legacy: _sequential
	inline data_t   data  () const { return m_ptr; }
	inline uidx_t  size  () const { return m_ptr.size(); }
	inline bool     valid () const { return m_ptr; }

	// Legacy: _iterable
	inline std_iterator  begin () const { return std_iterator( m_ptr.get() ); }
	inline std_iterator    end () const { return begin()+size(); }

	inline std_iterator rbegin () const { auto p =   end(); p.reverse(); return ++p; }
	inline std_iterator   rend () const { auto p = begin(); p.reverse(); return ++p; }

	inline jcl_iterator   iter () const { return jcl_iterator(  begin(),  end() ); }
	inline jcl_iterator  riter () const { return jcl_iterator( rbegin(), rend() ); }

	// Legacy: _comparable
	inline bool operator== ( const self& that ) const
		{ return m_ptr==that.m_ptr; }


	// Assignment
	inline self& operator= ( const shared<T,A>& ptr ) { assign(ptr); return *this; }
	inline self& assign    ( const shared<T,A>& ptr ) { m_ptr = ptr; return *this; }

protected:

	shared<T,A> m_ptr;
};



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class T, class A>
template <class U>
void array_shared<T,A>::copy( const U& that )
{
	create( that.size() );
	map( iterable_get_iterator(that), iter(), f_cast<T>() );
}

// ------------------------------------------------------------------------

template <class T, class A>
auto array_shared<T,A>::resize( uidx_t n ) -> self& // copy existing contents
{
	if ( n == 0 )
		{ clear(); return *this; }

	if ( n != size() )
	{
		// New allocation
		self that(n);

		// Copy what we can
		if ( !this->empty() )
			map( iter(), that.iter(), []( const T& val ){ return val; } );

		// Overwrite current array
		*this = that;
	}
	return *this;
}

JCL_NS_END_
