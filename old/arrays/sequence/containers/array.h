
//==================================================
// @title        array.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <stdexcept>
#include <utility>



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class T>
class array :
	public array_traits<T>::sequential_parent,
	public array_traits<T>::iterable_parent,
	public array_traits<T>::arithmetic_parent,
	public array_traits<T>::comparable_parent,
	public array_traits<T>::copyable_parent
{
public:

	CORE_TRAITS(T);
	using self            = array<T>;
	using traits          = array_traits<T>;
	using tag             = tag_array;

	using container_type  = typename traits::container_type;
	using std_iterator    = typename traits::std_iterator;
	using jcl_iterator    = typename traits::jcl_iterator;
	using data_t          = typename traits::data_type;

	using subarray_type   = sequence_wrapper<container_type,val_t>;

	// ----------  =====  ----------


	array()
		{ clear(); }
	array( ptr_t ptr, uidx_t len )
		{ assign(ptr,len); }

	// Accept implicit conversions when they compile
	template <class U>
	array( const array<U>& that )
		{ assign(that); }

	// Conversion to const variant
	inline operator array<const T> () const
		{ return array<const T>( data(), size() ); }


	// Instance management
	inline void clear() { m_ptr=nullptr; m_size=0; }
	inline self clone() const { return *this; }

	inline void swap( self& that )
		{ std::swap( m_ptr, that.m_ptr ); std::swap( m_size, that.m_size ); }


	// Extract subarray
	inline subarray_type subarray( uidx_t first, uidx_t last, sidx_t step = 1 ) const
		{ return subarray_type( m_ptr, first, last, step ); }


	// Legacy: _sequential
	inline data_t   data  () const { return m_ptr; }
	inline uidx_t    size  () const { return m_size; }
	inline bool     valid () const { return m_ptr; }

	// Legacy: _iterable
	inline std_iterator  begin () const { return std_iterator(m_ptr); }
	inline std_iterator    end () const { return begin()+size(); }

	inline std_iterator rbegin () const { auto p =   end(); p.reverse(); return ++p; }
	inline std_iterator   rend () const { auto p = begin(); p.reverse(); return ++p; }

	inline jcl_iterator   iter () const { return jcl_iterator(  begin(),  end() ); }
	inline jcl_iterator  riter () const { return jcl_iterator( rbegin(), rend() ); }

	// Legacy: _comparable
	inline bool operator== ( const self& that ) const
		{ return m_size==that.m_size && m_ptr==that.m_ptr; }


	// Assignment
	template <class U>
	inline self& assign( const array<U>& that )
		{ return assign( that.data(), that.size() ); }

	template <class U>
	inline self& operator= ( const array<U>& that )
		{ return assign(that); }

	inline self& assign( ptr_t ptr, uidx_t len )
		{ m_ptr = ptr; m_size = len; return *this; }

protected:

	ptr_t    m_ptr;
	uidx_t  m_size;
};

JCL_NS_END_
