
//==================================================
// @title        traits.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <deque>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

// Array without memory management.
// See: containers/sequence/array
struct tag_array {};
template <class T> class array;
template <class T> using array_traits =
	sequence_traits< T, T*, array<T>, pointer<T>, iterator_wrapper<pointer<T>,T> >;

// Array using shared memory.
// See: containers/sequence/array_shared
struct tag_array_shared {};
template <class T,class A> class array_shared;
template <class T,class A> using array_shared_traits =
	sequence_traits< T, shared<T,A>, array_shared<T,A>, pointer<T>, iterator_wrapper<pointer<T>,T> >;

// Array on stack memory.
// See: containers/sequence/array_fixed
struct tag_array_fixed {};
template <class T, uint16_t N> class array_fixed;
template <class T, uint16_t N> using array_fixed_traits =
	sequence_traits< T, std::array<T,N>, array_fixed<T,N>, pointer<T>, iterator_wrapper<pointer<T>,T> >;

// Array with non-contiguous block-memory storage.
// See: containers/sequence/array_chunked
struct tag_array_chunked {};
template <class T,class A> class array_chunked;
template <class T,class A> using array_chunked_traits =
	sequence_traits< T, std::deque<array_shared<T,A>>, array_chunked<T,A>,
		sequence_iterator<array_chunked<T,A>,T>,
		sequence_iterator<array_chunked<T,A>,T> >;

JCL_NS_END_
