
//==================================================
// @title        array_chunked.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <utility>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



JCL_NS_START_

template <class T, class A = allocator_new<T>>
class array_chunked :
	public array_chunked_traits<T,A>::sequential_parent,
	public array_chunked_traits<T,A>::iterable_parent,
	public array_chunked_traits<T,A>::arithmetic_parent,
	public array_chunked_traits<T,A>::comparable_parent
{
public:

	CORE_TRAITS(T);
	using self            = array_chunked<T,A>;
	using traits          = array_chunked_traits<T,A>;
	using tag             = tag_array_chunked;

	using allocator_type  = A;
	using container_type  = typename traits::container_type;
	using std_iterator    = typename traits::std_iterator;
	using jcl_iterator    = typename traits::jcl_iterator;

	using data_t          = typename traits::data_type;
	using subarray_type   = sequence_wrapper<self,T>;

	static_assert( traits::is_mutable::value,
		"Cannot create a variable size array of constant values." );

	static_assert( std::is_same<T,typename A::val_t>::value,
		"Type mismatch between value type and allocator." );

	// ----------  =====  ----------


	array_chunked()
		{ clear(); }

	// Copy-constructor
	array_chunked( const self& that )
		{ assign(that); }

	// Create
	array_chunked( uidx_t numel, uidx_t chunk )
		{ clear(); create(numel,chunk); }
	array_chunked( uidx_t numel, uidx_t chunk, cref_t val )
		{ clear(); create(numel,chunk,val); }

	// Deep-copy
	template <class U, class S, class D>
	array_chunked( const _iterable<U,S,D>& vals, uidx_t chunk )
		: array_chunked()
		{ create( vals.size(), chunk ); copy(vals); }


	// Instance management
	void clear();
	void swap( self& that );

	inline self clone() const
		{ return self( *this, m_chunk ); }

	template <class U>
	void copy( const U& that );

	// Resizing
	self& create( uidx_t numel, uidx_t chunk );
	self& create( uidx_t numel, uidx_t chunk, cref_t val );
	self& resize( uidx_t n );


	// Extract subarray
	inline subarray_type subarray( uidx_t first, uidx_t last, sidx_t step = 1 ) const
		{ return subarray_type( *this, first, last, step ); }


	// Legacy: _sequential
	inline data_t   data     () const { return m_data; }
	inline uidx_t  size     () const { return m_size; }
	inline uidx_t  n_chunks () const { return m_data.size(); }
	inline uidx_t  capacity () const { return m_chunk*n_chunks(); }
	inline bool     valid    () const { return m_chunk; }

	// Legacy: _iterable
	inline std_iterator  begin () const { return std_iterator(*this,0,m_size-1,1); }
	inline std_iterator    end () const { return  begin() + size(); }

	inline std_iterator rbegin () const { return std_iterator(*this,m_size-1,0,-1); }
	inline std_iterator   rend () const { return rbegin() + size(); }

	inline jcl_iterator   iter () const { return  begin(); }
	inline jcl_iterator  riter () const { return rbegin(); }

	// Legacy: _comparable
	inline bool operator== ( const self& that ) const
		{ return m_data == that.m_data && m_size == that.m_size; }

	// Assignment
	template <class U> inline self& operator= ( const U& that ) { assign(that); return *this; }
	template <class U> inline void  assign    ( const U& that ) { copy(that); }

	// Specialized assignment avoiding deep-copy
	self& assign( const self& that );

	// Hide inherited subscripting
	inline T& operator[] ( uidx_t n ) const
		{ return m_data[ JCL_IDX(n/m_chunk,m_data.size()) ][ n % m_chunk ]; }

protected:

	container_type  m_data;
	uidx_t         m_size, m_chunk;
};



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class T,class A>
void array_chunked<T,A>::clear()
{
	m_data  .clear();
	m_size  = 0;
	m_chunk = JCL_MEMORY_CHUNK_SIZE;
}

// ------------------------------------------------------------------------

template <class T,class A>
void array_chunked<T,A>::swap( self& that )
{
	m_data.swap( that.m_data );
	std::swap( m_size, that.m_size );
	std::swap( m_chunk, that.m_chunk );
}

// ------------------------------------------------------------------------

template <class T,class A>
template <class U>
void array_chunked<T,A>::copy( const U& that )
{
	create( that.size(), m_chunk );
	map( iterable_get_iterator(that), iter(), f_cast<T>() );
}

// ------------------------------------------------------------------------

template <class T,class A>
auto array_chunked<T,A>::assign( const self& that ) -> self&
{
	m_data  = that.m_data;
	m_size  = that.m_size;
	m_chunk = that.m_chunk;

	return *this;
}

// ------------------------------------------------------------------------

template <class T,class A>
auto array_chunked<T,A>::create( uidx_t numel, uidx_t chunk ) -> self&
{
	JCL_CHK_RV( chunk, *this, "Chunk size cannot be zero." );

	m_data.resize( (numel + chunk-1) / chunk );
	m_data.shrink_to_fit();

	for ( auto& c: m_data )
		c.create(chunk);

	m_size  = numel;
	m_chunk = chunk;

	return *this;
}

// ------------------------------------------------------------------------

template <class T,class A>
auto array_chunked<T,A>::create( uidx_t numel, uidx_t chunk, cref_t val ) -> self&
{
	JCL_CHK_RV( chunk, *this, "Chunk size cannot be zero." );

	m_data.resize( (numel + chunk-1) / chunk );
	m_data.shrink_to_fit();

	for ( auto& c: m_data )
		c.create(chunk,val);

	m_size  = numel;
	m_chunk = chunk;

	return *this;
}

// ------------------------------------------------------------------------

template <class T,class A>
auto array_chunked<T,A>::resize( uidx_t n ) -> self&
{
	if ( n != size() )
	{
		const uidx_t n_cur = m_data.size();
		const uidx_t n_new = (n + m_chunk-1)/m_chunk;

		if ( n_new < n_cur )       // remove chunks
			m_data.resize(n_new);
		else                       // or add new ones
		for ( uidx_t i = n_cur; i < n_new; ++i )
			m_data.emplace_back(m_chunk);

		// Update size
		m_size = n;
	}
	return *this;
}

JCL_NS_END_
