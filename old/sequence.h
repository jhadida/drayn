
//==================================================
// @title        sequence.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Containers must define:
 *      operator bool() const
 *      uidx_t size() const
 *      ref_t operator[] (uidx_t) const
 */

#include <utility>
#include <stdexcept>
#include <iostream>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Container, class Value = typename Container::value_type>
class sequence_iterator: 
    public _iterator<Value>
{
public:

    DRAYN_TRAITS(Value)

    sequence_iterator( const Container& ctn )
        : m_data(&ctn), m_ind(0)
        { m_size = ctn.size(); }

    inline operator bool() const { return m_ind < m_size; }
    inline ref_t value() const { return safeIndex(*m_data,m_ind); }
    inline void next() { m_ind += 1; }

protected:
    const Container *m_data;
    uidx_t m_ind, m_size;
};

// ------------------------------------------------------------------------

template <class Container, class Value = typename Container::value_type>
class sequence_std_iterator: 
    public _std_iterator< Value, sequence_std_iterator<Container,Value> >
{
public:

    DRAYN_TRAITS(Value)
    using self = sequence_std_iterator<Container,Value>;
    using parent = _std_iterator<Value,self>;

    sequence_std_iterator()
        : m_data(nullptr), m_size(0), m_ind(0) {}

    sequence_std_iterator( const Container& ctn )
        : m_data(&ctn), m_ind(0)
        { m_size = ctn.size(); }

    inline void swap( self& that ) {
        std::swap(
            stay(std::tie( m_data, m_ind, m_size )),
            stay(std::tie( that.m_data, that.m_ind, that.m_size ))
        );
    }

    void advance( sidx_t n ) {
        if ( n > static_cast<sidx_t>(m_size-m_ind) || -n > static_cast<sidx_t>(m_ind) )
            throw std::out_of_range("[drayn.sequence_std_iterator] Invalid increment.");
        m_ind += n;
    }

    using parent::operator-; // prevent shadowing of parent's operator
    sidx_t operator- ( const self& that ) const {
        if ( that.m_data != m_data )
            throw std::invalid_argument("[drayn.sequence_std_iterator] Non-comparable iterator.");
        return static_cast<sidx_t>(m_ind) - static_cast<sidx_t>(that.m_ind);
    }

    inline ref_t operator[] ( uidx_t n ) const 
        { return safeIndex(*m_data,m_ind); }

protected:

    const Container *m_data;
    uidx_t m_ind, m_size;
};

// ------------------------------------------------------------------------

template <class Container, class Value> class sequence; // forward definition

template <class Container, class Value = typename Container::value_type>
struct sequence_traits
{
    using container_type        = Container;
    using value_type            = Value;

    using self_type             = sequence<container_type,value_type>;
    using iterator_type         = sequence_iterator<container_type,value_type>;
    using std_iterator_type     = sequence_std_iterator<container_type,value_type>;
    
    using indexed_parent        = _indexed<value_type>;
    using iterable_parent       = _iterable<iterator_type>;
    using arithmetic_parent     = _arithmetic<value_type,self_type>;
    using comparable_parent     = _comparable<value_type,self_type>;
};

// ------------------------------------------------------------------------

template <class Container, class Value = typename Container::value_type>
class sequence: 
    public sequence_traits<Container,Value>::indexed_parent,
    public sequence_traits<Container,Value>::iterable_parent,
    public sequence_traits<Container,Value>::arithmetic_parent,
    public sequence_traits<Container,Value>::comparable_parent
{
public:

    using self = typename sequence_traits<Container,Value>::self_type;
    using value_type = typename sequence_traits<Container,Value>::value_type;
    using iterator_type = typename sequence_traits<Container,Value>::iterator_type;
    using std_iterator_type = typename sequence_traits<Container,Value>::std_iterator_type;

    DRAYN_TRAITS(value_type)

    template <typename... Args>
    explicit sequence( Args&& ...args ) 
        : m_data(Container(std::forward<Args>(args)...)) {}

    sequence( const Container& ctn )
        : m_data(ctn) {}

    sequence( const self& other )
        : m_data(other.m_data) {}

    virtual ~sequence() {} // -Wdelete-non-virtual-dtor

    inline const Container& data() const { return m_data; }
    inline operator const Container&() const { return m_data; }

    // proxy to container implementation
    inline bool valid() const { return m_data; }
    inline uidx_t size() const { return m_data.size(); }
    inline ref_t operator[] (uidx_t k) const { return safeIndex(m_data,k); }

    inline iterator_type iter() const 
        { return iterator_type(m_data); }

    inline std_iterator_type begin() const 
        { return std_iterator_type(m_data); }
    inline std_iterator_type end() const 
        { return std_iterator_type(m_data) += size(); }

protected:
    Container m_data;
};

DRAYN_NS_END
