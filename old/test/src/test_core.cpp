
#include <chrono>
#include <thread>
#include <vector>
#include <type_traits>



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



bool test_core_traits()
{
    JCL_CHK_RF( jcl::select() == 0, "[jcl.core.traits.select] FAIL 1" );
    JCL_CHK_RF( (jcl::select<false,false>() == 0), "[jcl.core.traits.select] FAIL 2" );
    JCL_CHK_RF( (jcl::select<true,false,false,true>() == 1), "[jcl.core.traits.select] FAIL 3" );
    JCL_CHK_RF( (jcl::select<false,false,true,false>() == 3), "[jcl.core.traits.select] FAIL 4" );

    JCL_CHK_RF( jcl::bool_type< 2 != 4 >::value, "[jcl.core.traits.bool_type] FAIL" );
    JCL_CHK_RF( jcl::bool_negate<std::false_type>::value, "[jcl.core.traits.bool_negate] FAIL" );

    return true;
}

// ------------------------------------------------------------------------

bool test_core_messages()
{
    jcl_println( "Testing %s.", "jcl_println" );
    JCL_WARN_  ( "This is a '%s' message.", "Warning" );
    JCL_INFO_  ( "This is a '%s' message.", "Info" );
    JCL_DEBUG_ ( "This is a '%s' message.", "Debug" );

    try {
        JCL_THROW( "This is a '%s' message.", "Debug" );
    } catch ( const std::exception& e ) {
        jcl_println( e.what() );
    }

    return true;
}

// ------------------------------------------------------------------------

bool test_core_patterns()
{
    jcl::static_val<double>() = 3.14;
    auto u = jcl::factory_create( jcl::factory_tag<unsigned>(), 42 );
    unsigned v = *u; delete u;

    JCL_CHK_RF( jcl::static_val<double>() == 3.14, "[jcl.core.pattern.singleton] FAIL" );
    JCL_CHK_RF( v == 42, "[jcl.core.pattern.factory] FAIL" );

    return true;
}

// ------------------------------------------------------------------------

// This will make the tests segfault if there is an error
bool test_core_allocators()
{
    typedef jcl::allocator_calloc<unsigned>            A1;
    typedef jcl::allocator_malloc<double>              A2;
    typedef jcl::allocator_new< std::complex<float> >  A3;

    auto p1 = A1::allocate( 23 );
    auto p2 = A2::allocate( 0 );
    auto p3 = A3::allocate( 1 );

    jcl::allocator_noalloc<float>::release( p3 );
    A2::release( p2, 0 );

    jcl::static_val< A1::deleter >()( p1, 23 );
    A3::release( p3, 1 );

    return true;
}

// ------------------------------------------------------------------------

bool test_core_randoms()
{
    auto U = jcl::uniform_gen();
    auto G = jcl::normal_gen( 2.0, 1.0 );
    auto E = jcl::exponential_gen( 0.5 );

    std::cout << "[jcl.core.randoms] Uniform distribution (0,1):" << std::endl;
    for ( uidx_t i = 0; i < 15; ++i ) jcl_print("%.2f ", U());
    std::cout << std::endl;

    std::cout << "[jcl.core.randoms] Normal distribution (2,1):" << std::endl;
    for ( uidx_t i = 0; i < 15; ++i ) jcl_print("%.2f ", G());
    std::cout << std::endl;

    std::cout << "[jcl.core.randoms] Exponential distribution (0.5):" << std::endl;
    for ( uidx_t i = 0; i < 15; ++i ) jcl_print("%.2f ", E());
    std::cout << std::endl;

    return true;
}

// ------------------------------------------------------------------------

typedef const unsigned    pubsub_data;
std::vector<unsigned>     pubsub_result;
jcl::events<pubsub_data>  pubsub_hub;

void pubsub_callback_fun( pubsub_data& val )
    { pubsub_result.at(0) = val; }

void pubsub_temporary( jcl::signal<pubsub_data>& c, pubsub_data& v )
{
    auto s = c.subscribe( []( pubsub_data& val ){ pubsub_callback_fun(val); } );
    c.publish(v);
}

struct pubsub_structure
{
    jcl::slot<pubsub_data> subscriber;

    pubsub_structure()
        : subscriber(pubsub_hub.channel(0).subscribe(
            [this] ( pubsub_data& val ){ this->member_callback(val); }
        )) {}

    void member_callback( pubsub_data& val )
        { pubsub_result.at(1) = val; }
};

bool test_core_pubsub()
{
    // allocate space for results
    pubsub_result.assign(2,0);

    // temporary subscriber
    pubsub_temporary( pubsub_hub.channel(0), 1 );
    JCL_CHK_RF( pubsub_result[0] == 1 && pubsub_result[1] == 0,
        "[jcl.core.pubsub] FAIL 1" );

    // previous subscriber should have been deleted when going out of scope
    pubsub_structure S;
    pubsub_result[0] = 0;

    pubsub_hub.channel(0).publish(2);
    JCL_CHK_RF( pubsub_result[0] == 0 && pubsub_result[1] == 2,
        "[jcl.core.pubsub] FAIL 2" );

    // try unsubsciption on-demand
    pubsub_hub.channel(0).unsubscribe( S.subscriber );
    pubsub_hub.channel(0).publish(4);
    JCL_CHK_RF( pubsub_result[0] == 0 && pubsub_result[1] == 2,
        "[jcl.core.pubsub] FAIL 3" );

    return true;
}

// ------------------------------------------------------------------------

bool test_core_timer()
{
    jcl::time_monitor monitor;

    monitor.start("example200");
    std::this_thread::sleep_for( std::chrono::milliseconds(200) );
    monitor.stop("example200");

    monitor.start("example700");
    std::this_thread::sleep_for( std::chrono::milliseconds(700) );
    monitor.stop("example700");

    monitor.report();

    return true;
}
