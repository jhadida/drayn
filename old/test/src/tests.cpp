
using namespace jcl::int_types;

// ------------------------------------------------------------------------

#include "test_core.cpp"
#include "test_core_numeric.cpp"

bool test_core()
{
    JCL_CHK_RF( test_core_traits(), "[jcl.core.traits] FAIL" );
    JCL_CHK_RF( test_core_patterns(), "[jcl.core.patterns] FAIL" );
    JCL_CHK_RF( test_core_allocators(), "[jcl.core.allocators] FAIL" );
    JCL_CHK_RF( test_core_pubsub(), "[jcl.core.pubsub] FAIL" );
    JCL_CHK_RF( test_core_numeric(), "[dr.core.numeric] FAIL" );

    // Uncomment these if you don't mind the output to console:
    //
    // JCL_CHK_RF( test_core_messages(), "[dr.core.messages] FAIL" );
    // JCL_CHK_RF( test_core_randoms(), "[dr.core.randoms] FAIL" );
    // JCL_CHK_RF( test_core_timer(), "[dr.core.timer] FAIL" );

    return true;
}



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



int main()
{
    JCL_CHK_RV( test_core(), 1, "[jcl.core] FAIL" );

    jcl_println("Everything went fine. Bye now :)");
}
