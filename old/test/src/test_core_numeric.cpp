
/**
 * TODO: write these tests...
 */

bool test_core_numeric_bit()
{
    JCL_CHK_RF( jcl::bit_count<int>( 0 ) == 0, "[jcl.core.numeric.bit.bit_count] FAIL 1" );
    JCL_CHK_RF( jcl::bit_count<uint16_t>( -1 ) == 16, "[jcl.core.numeric.bit.bit_count] FAIL 2" );
    JCL_CHK_RF( jcl::bit_count<unsigned>( 23 ) == 4, "[jcl.core.numeric.bit.bit_count] FAIL 3" );

    JCL_CHK_RF( jcl::bit_gcd<unsigned>( 54, 24 ) == 6, "[jcl.core.numeric.bit.bit_gcd] FAIL" );
    JCL_CHK_RF( jcl::bit_lcm<unsigned>( 4, 6 ) == 12, "[jcl.core.numeric.bit.bit_lcm] FAIL" );

    JCL_CHK_RF( jcl::bit_bin2gray<unsigned>(7) == 4, "[jcl.core.numeric.bit.bit_bin2gray] FAIL" );
    JCL_CHK_RF( jcl::bit_gray2bin<unsigned>(4) == 7, "[jcl.core.numeric.bit.bit_gray2bin] FAIL" );

    JCL_CHK_RF( jcl::bit_reverse<uint32_t>( 3, 5 ) == 24, "[jcl.core.numeric.bit.bit_reverse] FAIL" );

    unsigned a = 42, b = 23;
    jcl::bit_swap( a, b );
    JCL_CHK_RF( 2*a == b+4, "[jcl.core.numeric.bit.bit_swap] FAIL" );

    JCL_CHK_RF( jcl::bit_ispow2<int>(256), "[jcl.core.numeric.bit.bit_ispow2] FAIL" );
    JCL_CHK_RF( jcl::bit_log2<int>(1024) == 10, "[jcl.core.numeric.bit.bit_log2] FAIL" );
    JCL_CHK_RF( jcl::bit_nextpow2<int>(31) == 32, "[jcl.core.numeric.bit.bit_nextpow2] FAIL" );

    return true;
}

bool test_core_numeric_fun()
{
    return true;
}

bool test_core_numeric_op()
{
    return true;
}

bool test_core_numeric_pred()
{
    return true;
}

bool test_core_numeric()
{
    JCL_CHK_RF( test_core_numeric_bit(), "[jcl.core.numeric.bit] FAIL" );
    JCL_CHK_RF( test_core_numeric_fun(), "[jcl.core.numeric.fun] FAIL" );
    JCL_CHK_RF( test_core_numeric_op(), "[jcl.core.numeric.op] FAIL" );
    JCL_CHK_RF( test_core_numeric_pred(), "[jcl.core.numeric.pred] FAIL" );

    return true;
}
