#ifndef JCL_WRAPPER_H_INCLUDED
#define JCL_WRAPPER_H_INCLUDED

// Enable debug mode
#define JCL_DEBUG_MODE

// This flag activates infos
#define JCL_SHOW_INFO

#include "../src/jcl.h"

#endif
