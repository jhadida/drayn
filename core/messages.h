
//==================================================
// @title        messages.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <stdexcept>
#include <iostream>
#include <cstdarg>
#include <cstring>
#include <cstdio>
#include <string>



        /********************     **********     ********************/
        /********************     **********     ********************/



/**
 * Warning/error messages.
 */

static char drayn_msg_[DRAYN_MESSAGE_BUFFER]; // string used to print messages

#ifdef DRAYN_USING_MATLAB

	#define DRAYN_ERROR_( msg, args... ) { sprintf( drayn_msg_, "::ERROR:: " msg "\n", ##args ); mexErrMsgTxt(drayn_msg_); exit(EXIT_FAILURE); }
	#define DRAYN_WARN_( msg, args... ) { sprintf( drayn_msg_, "::WARNING:: " msg "\n", ##args ); mexWarnMsgTxt(drayn_msg_); }
	#define DRAYN_INFO_( msg, args... ) { sprintf( drayn_msg_, "::INFO:: " msg "\n", ##args ); mexWarnMsgTxt(drayn_msg_); }
	#define DRAYN_DEBUG_( msg, args... ) { sprintf( drayn_msg_, "::DEBUG:: " msg "\n", ##args ); mexWarnMsgTxt(drayn_msg_); }

	// Use this to force Matlab's output to flush.
	inline void mx_drawnow()
		{ mexCallMATLAB(0, NULL, 0, NULL, "drawnow"); }

#else

	#define DRAYN_ERROR_( msg, args... ) { fprintf( stderr, "::ERROR:: " msg "\n", ##args ); exit(EXIT_FAILURE); }
	#define DRAYN_WARN_( msg, args... ) { fprintf( stderr, "::WARNING:: " msg "\n", ##args ); }
	#define DRAYN_INFO_( msg, args... ) { printf( "::INFO:: " msg "\n", ##args ); }
	#define DRAYN_DEBUG_( msg, args... ) { printf( "::DEBUG:: " msg "\n", ##args ); }

#endif

// ------------------------------------------------------------------------

/**
 * Print statement
 */
#ifdef DRAYN_USING_MATLAB
	#define dr_print mexPrintf
#else
	#define dr_print ::printf
#endif

// #define dr_println( format, args... ) { dr_print( format "\n", ##args ); }

template <class... Args>
void dr_println( std::string fmt, Args&&... args )
{
	fmt += "\n"; 
	dr_print( fmt.c_str(), std::forward<Args>(args)... );
}

// ------------------------------------------------------------------------

/**
 * Errors cause the program to exit with EXIT_FAILURE
 */

// Print ERROR messages
#define DRAYN_ERROR( msg, args... ) { dr_print( "...# In %s (l.%d):\n", __FILE__, __LINE__ ); DRAYN_ERROR_(msg,##args) }

#define REJECT_ERROR( cdt, msg, args... ) { if(cdt) DRAYN_ERROR(msg,##args) }
#define ASSERT_ERROR( cdt, msg, args... ) REJECT_ERROR(!(cdt),msg,##args)


/**
 * Warning messages are always active
 */

// Print WARNING messages
#define DRAYN_WARN( msg, args... ) { dr_print( "...# In %s (l.%d):\n", __FILE__, __LINE__ ); DRAYN_WARN_(msg,##args) }

// Simple warn
#define REJECT( cdt, msg, args... ) { if(cdt) DRAYN_WARN(msg,##args) }
#define ASSERT( cdt, msg, args... ) REJECT(!(cdt),msg,##args)

// Warn and return
#define REJECT_R( cdt, msg, args... ) { if(cdt) {DRAYN_WARN(msg,##args) return;} }
#define ASSERT_R( cdt, msg, args... ) REJECT_R(!(cdt),msg,##args)

// Warn and return false
#define REJECT_RF( cdt, msg, args... ) { if(cdt) {DRAYN_WARN(msg,##args) return false;} }
#define ASSERT_RF( cdt, msg, args... ) REJECT_RF(!(cdt),msg,##args)

// Warn and return value
#define REJECT_RVAL( cdt, val, msg, args...  ) { if(cdt) {DRAYN_WARN(msg,##args) return (val);} }
#define ASSERT_RVAL( cdt, val, msg, args...  ) REJECT_RVAL(!(cdt),(val),msg,##args)

// Warn and goto
#define REJECT_GT( cdt, gt, msg, args...  ) { if(cdt) {DRAYN_WARN(msg,##args) goto gt;} }
#define ASSERT_GT( cdt, gt, msg, args...  ) REJECT_GT(!(cdt),gt,msg,##args)


/**
 * The default exception thrown is std::runtime_error
 */

#define DRAYN_THROW( msg, args... ) \
	{ sprintf( drayn_msg_, "::DRAYN:: " msg "\n", ##args ); throw std::runtime_error(drayn_msg_); }

#define REJECT_THROW( cdt, msg, args... ) { if (cdt) DRAYN_THROW(msg,##args) }
#define ASSERT_THROW( cdt, msg, args... ) REJECT_THROW(!(cdt),msg,##args)


/**
 * Activate DEBUG messages with the DRAYN_DEBUG_MODE flag.
 * Print a warning if the condition is true/false (REJECT/ASSERT).
 * Then return something, or not, or goto somwhere.
 */
#ifdef DRAYN_DEBUG_MODE

	// Print DEBUG messages
	#define DRAYN_DEBUG( msg, args... ) { dr_print( "...# In %s (l.%d):\n", __FILE__, __LINE__ ); DRAYN_DEBUG_(msg,##args) }

	// Simple warn
	#define DBG_REJECT( cdt, msg, args... ) { if(cdt) DRAYN_DEBUG(msg,##args) }
	#define DBG_ASSERT( cdt, msg, args... ) DBG_REJECT(!(cdt),msg,##args)

	// Warn and return
	#define DBG_REJECT_R( cdt, msg, args... ) { if(cdt) {DRAYN_DEBUG(msg,##args) return;} }
	#define DBG_ASSERT_R( cdt, msg, args... ) DBG_REJECT_R(!(cdt),msg,##args)

	// Warn and return false
	#define DBG_REJECT_RF( cdt, msg, args... ) { if(cdt) {DRAYN_DEBUG(msg,##args) return false;} }
	#define DBG_ASSERT_RF( cdt, msg, args... ) DBG_REJECT_RF(!(cdt),msg,##args)

	// Warn and return value
	#define DBG_REJECT_RVAL( cdt, val, msg, args... ) { if(cdt) {DRAYN_DEBUG(msg,##args) return (val);} }
	#define DBG_ASSERT_RVAL( cdt, val, msg, args... ) DBG_REJECT_RVAL(!(cdt),(val),msg,##args)

	// Warn and goto
	#define DBG_REJECT_GT( cdt, gt, msg, args... ) { if(cdt) {DRAYN_DEBUG(msg,##args) goto gt;} }
	#define DBG_ASSERT_GT( cdt, gt, msg, args... ) DBG_REJECT_GT(!(cdt),gt,msg,##args)

#else

	#define DRAYN_DEBUG( msg, args... )

	#define DBG_REJECT( cdt, msg, args... )
	#define DBG_ASSERT( cdt, msg, args... )

	// Warn and return
	#define DBG_REJECT_R( cdt, msg, args... )
	#define DBG_ASSERT_R( cdt, msg, args... )

	// Warn and return false
	#define DBG_REJECT_RF( cdt, msg, args... )
	#define DBG_ASSERT_RF( cdt, msg, args... )

	// Warn and return value
	#define DBG_REJECT_RVAL( cdt, val, msg, args... )
	#define DBG_ASSERT_RVAL( cdt, val, msg, args... )

	// Warn and goto
	#define DBG_REJECT_GT( cdt, gt, msg, args... )
	#define DBG_ASSERT_GT( cdt, gt, msg, args... )

#endif


/**
 * Activate INFO messages with the DRAYN_SHOW_INFO flag.
 */
#ifdef DRAYN_SHOW_INFO

	// Print INFO messages
	#define DRAYN_INFO( msg, args... ) { dr_print( "...# In %s (l.%d):\n", __FILE__, __LINE__ ); DRAYN_INFO_(msg,##args) }

	#define INFO_REJECT( cdt, msg, args... ) { if(cdt) DRAYN_INFO(msg,##args) }
	#define INFO_ASSERT( cdt, msg, args... ) INFO_REJECT(!(cdt),msg,##args)

#else

	#define DRAYN_INFO( msg, args... )

	#define INFO_REJECT( cdt, msg, args... )
	#define INFO_ASSERT( cdt, msg, args... )

#endif
