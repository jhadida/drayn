
//==================================================
// @title        constants.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <limits>



        /********************     **********     ********************/
        /********************     **********     ********************/


/**
 * Mathematical constants.
 */

#define C_E          2.7182818284590452354

#define C_LOG2E      1.4426950408889634074
#define C_LOG10E     0.43429448190325182765
#define C_LN2        0.69314718055994530942
#define C_LN10       2.302585092994045684

#define C_PI         3.1415926535897932385
#define C_1_PI       0.31830988618379067154 

#define C_SQRT2      1.4142135623730950488
#define C_1_SQRT2    0.7071067811865475244

#define C_SQRTPI     1.7724538509055160273
#define C_1_SQRTPI   0.56418958354775628695 

#define C_SQRT2PI    2.5066282746310005024
#define C_1_SQRT2PI  0.39894228040143267794 

// ------------------------------------------------------------------------

DRAYN_NS_START_

/**
 * Store useful numeric limits more concisely.
 */
template <class T>
struct c_num
{
	static const T max;
	static const T min;
	static const T eps;

	static const bool is_signed;
	static const bool is_integral;
};

template <class T> const T c_num<T>::max = std::numeric_limits<T>::max();
template <class T> const T c_num<T>::min = std::is_integral<T>::value ? T(0) : std::numeric_limits<T>::min();
template <class T> const T c_num<T>::eps = std::numeric_limits<T>::epsilon();

template <class T> const bool c_num<T>::is_signed   = std::numeric_limits<T>::is_signed;
template <class T> const bool c_num<T>::is_integral = std::numeric_limits<T>::is_integer;

// ------------------------------------------------------------------------

/**
 * Epsilon values are used for division/positiveness tests.
 */
template <class T>
struct c_eps
{
	static const T small;
	static const T infinitesimal;
};

template <class T> const T c_eps<T>::small = c_num<T>::eps;
template <class T> const T c_eps<T>::infinitesimal = c_num<T>::min;

DRAYN_NS_END_
