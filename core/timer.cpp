
//==================================================
// @title        timer.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

void time_format::clear()
{
	d = h = m = s = ms = us = 0;
}

void time_format::format_seconds( double sec )
{
	d  = op_ufloor(sec/86400);
		sec -= d*86400;
	h  = op_ufloor(sec/3600);
		sec -= h*3600;
	m  = op_ufloor(sec/60);
		sec -= m*60;
	s  = op_ufloor(sec);
		sec -= s;
	ms = op_ufloor(sec*1000);
		sec -= ms/1000.0;
	us = op_ufloor(sec*1e6);
}

const char* time_format::to_string()
{
	if ( d > 0 )
		sprintf( m_repr, "%u days and %02u:%02u:%02u", d, h, m, s );
	else if ( h > 0 )
		sprintf( m_repr, "%u h %02u min %02u sec", h, m, s );
	else if ( m > 0 )
		sprintf( m_repr, "%u min %02u sec", m, s );
	else if ( s > 0 )
		sprintf( m_repr, "%u.%03u sec", s, ms );
	else
		sprintf( m_repr, "%03u.%03u ms", ms, us );

	return m_repr;
}

// ------------------------------------------------------------------------

double timer::timeleft( double fraction_done ) const
{
	double seconds = runtime();
	return seconds / fraction_done - seconds;
}

const char* timer::timeleft_str( double fraction_done )
{
	m_format.format_seconds( timeleft(fraction_done) );
	return m_format.to_string();
}

// ------------------------------------------------------------------------

void stopwatch::clear()
{
	REJECT( m_running, "[dr.stopwatch] The timer is still running!" );
	reset();
}

void stopwatch::reset()
{
	m_running = false;
	m_runtime = 0.0;
}

void stopwatch::start()
{ if ( !m_running ) {

	m_timer.reset();
	m_running = true;
} }

void stopwatch::stop()
{ if ( m_running ) {

	m_running = false;
	m_runtime += m_timer.runtime();
} }

const char* stopwatch::runtime_str()
{
	m_format.format_seconds( m_runtime );
	return m_format.to_string();
}

// ------------------------------------------------------------------------

void time_monitor::clear()
{
	for ( auto& tim : m_timers )
		tim.second.reset();
	m_timers.clear();
}

void time_monitor::start( const std::string& key )
{
	if ( key.size() > DRAYN_TIMER_KEY_MAXSIZE )
	{
		DRAYN_WARN( "Label '%s' (%lu) is too long! "
			"[dr.time_monitor] Either choose a shorter name, or increase DRAYN_TIMER_KEY_MAXSIZE.",
			key.c_str(), key.size() );
		m_timers[ key.substr(0,DRAYN_TIMER_KEY_MAXSIZE) ].start();
	}
	else
		m_timers[key].start();
}

void time_monitor::stop( const std::string& key )
{
	m_timers[key].stop();
}

void time_monitor::report()
{
	// Reverse map to sort by decreasing runtime
	report_type report;
	char rbuf[DRAYN_TIMER_REPORT_BUFFER];

	for ( auto& tim : m_timers )
	{
		REJECT( tim.second.running(), "Timer '%s' is still running!", tim.first.c_str() );
		sprintf( rbuf, "\t[%s]: %s", tim.first.c_str(), tim.second.runtime_str() );
		report[ tim.second.runtime() ] = rbuf;
	}

	// Print report
	std::cout<< "[TIMER REPORT] (sorted descending):" << std::endl;
	for ( auto& rep : report )
		std::cout<< rep.second << std::endl;
}

DRAYN_NS_END_
