
//==================================================
// @title        timer.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <ctime>
#include <cstring>
#include <chrono>

#include <map>
#include <string>
#include <iostream>
#include <functional>
#include <unordered_map>

#define DRAYN_TIMER_REPORT_BUFFER (DRAYN_TIMER_KEY_MAXSIZE + 10 + DRAYN_TIMER_FORMAT_BUFFER)



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * String representation of a duration in seconds.
 * Currently up to micro-second precision.
 */
class time_format
{
	char m_repr[DRAYN_TIMER_FORMAT_BUFFER];

public:

	unsigned d, h, m, s, ms, us;

	time_format()
		{ clear(); }
	time_format( double sec )
		{ format_seconds(sec); }

	// Reset duration
	void clear();

	// Set duration
	void format_seconds( double sec );

	// Get string representation
	const char* to_string();

};

// ------------------------------------------------------------------------

/**
 * Simple stop-watch that estimates the timeleft from a fraction of work done between 0 and 1.
 */
class timer
{
	using seconds = std::chrono::duration< double, std::ratio<1,1> >;
	using steady_clock = std::chrono::steady_clock;

	steady_clock::time_point m_start;
	time_format m_format;

public:

	timer()
		{ reset(); }

	// Reset the timer
	inline void reset()
		{ m_start = steady_clock::now(); }

	// Runtime in seconds
	inline double runtime() const
		{ return std::chrono::duration_cast<seconds>( steady_clock::now() - m_start ).count(); }

	// Evaluate timeleft in seconds, assuming linear progress
	double timeleft( double fraction_done ) const;
	const char* timeleft_str( double fraction_done );

};

// ------------------------------------------------------------------------

/**
 * Used internally by time_monitor.
 */
class stopwatch
{
	bool m_running;
	double m_runtime;
	timer m_timer;
	time_format m_format;

public:

	stopwatch()
		{ reset(); }

	// Clear the timer
	void clear();

	// Timer controls
	void reset();
	void start();
	void stop();

	inline bool running() const
		{ return m_running; }
	inline const double& runtime() const
		{ return m_runtime; }
	const char* runtime_str();

};

// ------------------------------------------------------------------------

/**
 * Monitors can be used to time implementations and generate reports.
 * The report is very basic for now (execution time by label, sorted descending).
 *
 * Example:
 *
 * dr::time_monitor TM;
 *
 * // Create a label
 * TM.start('mylabel');
 * 	//... do something time-consuming
 * TM.stop('mylabel');
 *
 * //... create as many as you want
 *
 * // Print the report at the end, which sorts the time-consumptions in descending order
 * TM.report();
 */
class time_monitor
{
public:

	typedef std::unordered_map< std::string, stopwatch > hash_table;
	typedef std::map< double, std::string, std::greater_equal<double> > report_type;


	time_monitor() {}

	// Clear all timers
	void clear();

	// Start/stop labeled timers
	void start( const std::string& key );
	void stop( const std::string& key );

	// Print output
	void report();

private:

	hash_table m_timers;
};

DRAYN_NS_END_
