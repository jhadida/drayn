
//==================================================
// @title        singleton.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T> 
struct singleton 
	{ static T instance; };

template <class T> 
T singleton<T>::instance = T();

// ------------------------------------------------------------------------

template <class T>
constexpr T& static_val()
	{ return singleton<T>::instance; }

template <class T>
constexpr T* static_ptr()
	{ return &singleton<T>::instance; }

DRAYN_NS_END_
