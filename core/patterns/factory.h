
//==================================================
// @title        factory.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <type_traits>

/**
 * The factory pattern define helpful functions to create factories for any type of object.
 * The factory_tag<T> merely acts as a vector for type T.
 * The function factory_create accepts a variable number of inputs, and returns either
 *  - a new instance of T constructed with these inputs, if T provides a suitable constructor,
 *  - a null pointer otherwise.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// Tags to identify types created by the factory
template <class T>
struct factory_tag {};

// ------------------------------------------------------------------------

// Implement creation methods using tag-dispatch
template <class T, class... Args>
inline T* factory_create( factory_tag<T> tag, Args&&... args )
	{ return factory_create_impl( tag, std::is_constructible<T,Args...>(), std::forward<Args>(args)... ); }

template <class T, class... Args>
inline T* factory_create_impl( factory_tag<T>, std::true_type, Args&&... args )
	{ return new T( std::forward<Args>(args)... ); }

template <class T, class... Args>
inline T* factory_create_impl( factory_tag<T>, std::false_type, Args&&... args )
		{ return nullptr; }

DRAYN_NS_END_
