
//==================================================
// @title        macros.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



/**
 * Min/max related
 */
#define Drayn_MIN(a,b) ((a)<(b)?(a):(b))
#define Drayn_MAX(a,b) ((a)>(b)?(a):(b))
#define Drayn_CLAMP(a,lb,ub) Drayn_MAX( Drayn_MIN(a,ub), lb )

#define Drayn_MIN3(a,b,c) Drayn_MIN(a,Drayn_MIN(b,c))
#define Drayn_MAX3(a,b,c) Drayn_MAX(a,Drayn_MAX(b,c))

/**
 * Index related
 */
#ifdef DRAYN_PROTECT_INDICES
	#define Drayn_IDX(k,s) (k % s)
#else
 	#define Drayn_IDX(k,s) k
#endif

/**
 * Operation chaining
 */
#define OP1( x, Op ) (x)
#define OP2( x, Op ) ((x) Op OP1( x, Op ))
#define OP3( x, Op ) ((x) Op OP2( x, Op ))
#define OP4( x, Op ) ((x) Op OP3( x, Op ))
#define OP5( x, Op ) ((x) Op OP4( x, Op ))
#define OP6( x, Op ) ((x) Op OP5( x, Op ))
#define OP7( x, Op ) ((x) Op OP6( x, Op ))
#define OP8( x, Op ) ((x) Op OP7( x, Op ))
#define OP9( x, Op ) ((x) Op OP8( x, Op ))
