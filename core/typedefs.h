
//==================================================
// @title        typedefs.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cstdint>
#include <cinttypes>
#include <initializer_list>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// Short alias for initializer lists
template <class T>
using inilst = std::initializer_list<T>;

// An empty type
struct void_type {};

// ------------------------------------------------------------------------

/**
 * Orders of magnitude:
 *
 * 2^64: 1.84 e 19   uint64_t, aka unsigned long
 * 2^32: 4.29 e  9   uint32_t, aka unsigned
 * 2^16: 6.55 e  5   uint16_t, aka ushort
 * 2^ 8:  256         uint8_t, aka uchar
 */

// By default, all words are 64 bits
#ifdef DRAYN_USE_32BITS_WORD

	namespace int_types {
		typedef uint32_t  uword_t;
		typedef int32_t   iword_t;
	}

	#define DPuw "%" PRIu32
	#define DPiw "%" PRId32

#else

	namespace int_types {
		typedef uint64_t  uword_t;
		typedef int64_t   iword_t;
	}

	#define DPuw "%" PRIu64
	#define DPiw "%" PRId64

#endif

// By default, all indices are 32 bits
#ifdef DRAYN_USE_64BITS_SIZE

	// Size types
	namespace int_types {
		typedef uint64_t  usize_t;
		typedef int64_t   isize_t;
	}

	// #define DPus "%zu" // for std::size_t
	#define DPus "%" PRIu64
	#define DPis "%" PRId64

#else

	// Size types
	namespace int_types {
		typedef uint32_t  usize_t;
		typedef int32_t   isize_t;
	}

	// #define DPus "%zu" // for std::size_t
	#define DPus "%" PRIu32
	#define DPis "%" PRId32

#endif

// Use the previous types within the Drayn namespace
using namespace int_types;

DRAYN_NS_END_
