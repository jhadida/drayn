
//==================================================
// @title        traits.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cstddef>
#include <utility>
#include <complex>
#include <iterator>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// Inverse of std::move
template <class T>
constexpr T& stay(T&& t) { return t; }

// ------------------------------------------------------------------------

/**
 * Return the index of the first template type that is true.
 * If none is true, or there is not input, return 0.
 */
template<bool B0=false, bool... Bs>
constexpr unsigned type_selector( unsigned I = 1 )
{
    return B0 ? I : type_selector<Bs...>(I+1);
}

template<>
constexpr unsigned type_selector<false>( unsigned I )
{
    return 0;
}

// ------------------------------------------------------------------------

// Forwad core typedefs from input Class
#define FORWARD_TRAITS(Class)           \
typedef typename Class::val_t    val_t; \
typedef typename Class::ref_t    ref_t; \
typedef typename Class::ptr_t    ptr_t; \
typedef typename Class::cval_t  cval_t; \
typedef typename Class::cref_t  cref_t; \
typedef typename Class::cptr_t  cptr_t;

// ------------------------------------------------------------------------

/**
 * Core type traits.
 */
template <class T>
struct core_traits
{
	typedef T  val_t;
	typedef T& ref_t;
	typedef T* ptr_t;

	typedef const T  cval_t;
	typedef const T& cref_t;
	typedef const T* cptr_t;
};

#define CORE_TRAITS(Type) FORWARD_TRAITS( core_traits<Type> )

// ----------  =====  ----------

/**
 * Core traits from standard iterator traits.
 */
template <class T>
struct iterator_traits
{
	typedef typename std::iterator_traits<T>::value_type  val_t;
	typedef typename std::iterator_traits<T>::reference   ref_t;
	typedef typename std::iterator_traits<T>::pointer     ptr_t;

	typedef const val_t   cval_t;
	typedef const val_t&  cref_t;
	typedef const val_t*  cptr_t;
};

#define ITERATOR_TRAITS(Type) FORWARD_TRAITS( iterator_traits<Type> )

// ------------------------------------------------------------------------

/**
 * Boolean-related static predicates.
 */
template <bool B>
struct bool_type
	: public std::integral_constant <bool,B> {};

template <class T>
struct bool_negate
	: public std::integral_constant <bool, !T::value> {};

template <class T, class U>
struct bool_equal
	: public std::integral_constant <bool, !(T::value ^ U::value) > {};

// ------------------------------------------------------------------------

/**
 * Test whether a type is complex.
 */
template <class T>
struct is_complex: bool_type<false> {};

template <class T>
struct is_complex< std::complex<T> >: bool_type<true> {};

/**
 * Test whether a type is numeric.
 */
template<class T>
struct is_numeric
   : public std::integral_constant < bool,
	   std::is_integral<T>::value || std::is_floating_point<T>::value > {};

// ------------------------------------------------------------------------

/**
 * Arithmetic and comparable predicate types.
 *
 * Used to enable the interface provided by the corresponding concepts
 * (cf containers/concepts). If you want an external class to enable
 * these interfaces too, you need to specialize the predicate using:
 *
 * template <>
 * struct dr::is_comparable<MyClass>
 * 	: std::true_type {};
 *
 * Note that the arithmetic and comparable concepts both require an
 * iterable interface (see the concepts).
 */
template<class T>
struct is_arithmetic
	: public std::integral_constant < bool,
    	is_numeric<T>::value > {};

template<class T>
struct is_comparable
	: public std::integral_constant < bool,
    	is_arithmetic<T>::value > {};

// ------------------------------------------------------------------------

/**
 * Test whether a type implements the begin()/end() methods.
 */
template <class T>
struct has_begin_end_test
{
	typedef typename std::remove_const<T>::type test_type;

	template <class A>
	static constexpr bool test( A * pt,
		decltype(pt->begin()) * = nullptr,
		decltype(pt->end()) * = nullptr )
	{ return true; }

	template <class A>
	static constexpr bool test(...)
	{ return false; }

	static const bool value = test<test_type>(nullptr);
};

template <class T>
struct has_begin_end
	: public bool_type< has_begin_end_test<T>::value > {};

DRAYN_NS_END_
