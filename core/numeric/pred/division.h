
//==================================================
// @title        division.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T>
inline bool _safe_division( T a, T b, std::true_type const )
{
	return b != T(0);
}

template <class T>
bool _safe_division( T a, T b, std::false_type const )
{
	static const T M = c_num<T>::max;
	static const T m = c_num<T>::min;

	a = op_abs(a); b = op_abs(b);
	return (b < T(1)) ? (a <= b*M) : (a >= b*m);
}

template < class T, class U, 
	class V = typename std::common_type<T,U>::type >
inline bool is_division_safe( T a, U b )
{
	return _safe_division( static_cast<V>(a), static_cast<V>(b), 
		std::is_integral<V>() );
}
