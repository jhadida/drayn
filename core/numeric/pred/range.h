
//==================================================
// @title        range.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T, char left, char right>
struct in_range
{
	T lower, upper;

	in_range( const T &l, const T &u )
		: lower(l), upper(u) {}

	inline bool operator() ( const T &x ) const
		{ return false; }
};

// ------------------------------------------------------------------------

#define DRAYN_RANGE_PREDICATE( lchar, rchar, Test )    \
template <class T>                                     \
struct in_range<T,lchar,rchar>                         \
{                                                      \
	T lower, upper;                                    \
	in_range( const T& l, const T& u )                 \
		: lower(l), upper(u) {}                        \
	inline bool operator() ( const T& x ) const        \
		{ return (Test); }                             \
};

DRAYN_RANGE_PREDICATE( '[', ']', (x >= lower) && (x <= upper) );
DRAYN_RANGE_PREDICATE( '[', ')', (x >= lower) && (x <  upper) );
DRAYN_RANGE_PREDICATE( '(', ']', (x >  lower) && (x <= upper) );
DRAYN_RANGE_PREDICATE( '(', ')', (x >  lower) && (x <  upper) );
