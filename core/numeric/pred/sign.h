
//==================================================
// @title        sign.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#define DRAYN_SIGN_PREDICATE( Name, Test )        \
template <class T>                                \
struct Name                                       \
{                                                 \
	inline bool operator() ( const T &x ) const   \
		{ return (Test); }                        \
};                                                \
template <class T>                                \
inline bool is_ ## Name ( const T& x )            \
	{ return (Test); }

DRAYN_SIGN_PREDICATE( non_negative, x >= T(0) );
DRAYN_SIGN_PREDICATE( non_positive, x <= T(0) );
DRAYN_SIGN_PREDICATE( negative, x < T(0) );
DRAYN_SIGN_PREDICATE( positive, x > T(0) );
