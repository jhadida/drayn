
//==================================================
// @title        rem.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T>
inline T _rem( T a, T b, std::true_type ) // integral type
{
	return a % b;
}

template <class T>
inline T _rem( T a, T b, std::false_type ) // non-integral type
{
	return a - b*op_fix(op_quotient(a,b));
}

// ------------------------------------------------------------------------

/**
 * Same as Matlab's rem function.
 */
template < class T, class U, 
	class V = typename std::common_type<T,U>::type >
inline V op_rem( T a, U b )
{
	return _rem( static_cast<V>(a), static_cast<V>(b), std::is_integral<V>() );
}