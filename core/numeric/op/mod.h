
//==================================================
// @title        mod.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T>
inline T _mod( T a, T b, std::true_type ) // integral type
{
	return (a%b + b)%b;
}

template <class T>
inline T _mod( T a, T b, std::false_type ) // non-integral type
{
	return a - b*std::floor(op_quotient(a,b));
}

// ------------------------------------------------------------------------

/**
 * The "mathematical" modulus function.
 */
template < class T, class U, 
	class V = typename std::common_type<T,U>::type >
inline V op_mod( T a, U b )
{
	return _mod( static_cast<V>(a), static_cast<V>(b), std::is_integral<V>() );
}