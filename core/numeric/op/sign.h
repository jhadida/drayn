
//==================================================
// @title        sign.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T> 
inline constexpr int _sign( T x, std::false_type const ) // non-signed type
{
	return x > 0; // 1 or 0
}

template <class T> 
inline constexpr int _sign( T x, std::true_type const ) // signed type
{
	return (x > 0) - (x < 0); // 1, 0, -1
}

// ------------------------------------------------------------------------

template <class T> 
inline constexpr int op_sign( T x ) 
	{ return _sign( x, std::is_signed<T>() ); }