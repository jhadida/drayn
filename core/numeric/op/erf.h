
//==================================================
// @title        erf.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Gauss error function, its complement, and inverse.
 * The standard implementation seems to be the best one.
 */

double _erf_abramowitz_and_stegun( double x );
double _erf_numerical_recipes( double x );

inline double op_erf( double x )
	{ return std::erf(x); }
inline double op_erfc( double x )
	{ return 1.0 - op_erf(x); }

// ------------------------------------------------------------------------

double _erfinv_stackoverflow( double y );
double _erfinv_wikipedia( double y );

inline double op_erfinv( double y )
	{ return _erfinv_wikipedia(y); }
