
//==================================================
// @title        binomial.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Binomial coefficient "n choose k"
template <class Unsigned>
Unsigned op_binomial( Unsigned n, Unsigned k )
{
	Unsigned num, com, res = 1;

	for ( Unsigned d = 1; d <= k; ++d )
	{
		com  = bit_gcd(res, d);
		res /= com;
		num  = (n - k + d) / (d / com);

		ASSERT_RVAL( res <= c_num<Unsigned>::max / num, 1,
			"[dr.op_binomial] Overflow error." );

		res *= num;
	}

	return res;
}
