
//==================================================
// @title        erf.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Adapted from http://www.johndcook.com/blog/stand_alone_code/
 */
double _erf_abramowitz_and_stegun( double x )
{
	static const double
		a1 =  0.254829592,
		a2 = -0.284496736,
		a3 =  1.421413741,
		a4 = -1.453152027,
		a5 =  1.061405429,
		p  =  0.3275911;

	// Save the sign of x
	int sign = op_sign(x);

	// A&S formula 7.1.26
	double t = 1.0 / (1.0 + sign*p*x);
	double y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);

	return sign*y;
}

// ------------------------------------------------------------------------

/**
 * From wikipedia:
 * https://en.wikipedia.org/wiki/Error_function#Numerical_approximation
 */
double _erf_numerical_recipes( double x )
{
	int sign = op_sign(x);

	double t = 1.0 / (1.0 + sign*0.5*x);
	double y = t*exp(-x*x-1.26551223+t*(1.00002368+t*(0.37409196+t*(0.09678418+
		t*(-0.18628806+t*(0.27886807+t*(-1.13520398+t*(1.48851587+
		t*(-0.82215223+t*0.17087277)))))))));

	return sign*(1.0 - y);
}

// ------------------------------------------------------------------------

/**
 * From StackOverflow:
 * http://stackoverflow.com/questions/5971830/need-code-for-inverse-error-function
 */
double _erfinv_stackoverflow( double y )
{
	static const double a[] = { 0.886226899, -1.645349621,  0.914624893, -0.140543331};
	static const double b[] = {-2.118377725,  1.442710462, -0.329097515,  0.012229801};
	static const double c[] = {-1.970840454, -1.624906493,  3.429567803,  1.641345311};
	static const double d[] = { 3.543889200,  1.637067800};

	int sign = op_sign(y);
	double x, z = sign*y;

	if ( z >= 1.0-c_num<double>::eps )
		return sign*c_num<double>::max;

	if ( z >= -0.7 )
	{
		z = sqrt( -log( (1.0 - z)/2.0 ) );
		x = sign * (((c[3]*z+c[2])*z+c[1])*z+c[0])/((d[1]*z+d[0])*z+1.0);
	}
	else
	{
		z = y*y;
		x = y*(((a[3]*z+a[2])*z+a[1])*z+a[0])/((((b[3]*z+b[3])*z+b[1])*z+b[0])*z+1.0);
	}

	// Refine to full accuracy
	x -= (op_erf(x) - y) / (2.0*C_1_SQRTPI * exp(-x*x));
	x -= (op_erf(x) - y) / (2.0*C_1_SQRTPI * exp(-x*x));

	return x;
}

double _erfinv_wikipedia( double y )
{
	static const double a = 8*(C_PI - 3.0) /(4.0-C_PI) /(3.0*C_PI);
	static const double b = 2.0 / (C_PI*a);

	int sign = op_sign(y);

	if ( y*y >= 1.0-c_num<double>::eps )
		return sign*c_num<double>::max;

	double l = log(1.0 - y*y);
	double x = b + l/2.0;

	x = sqrt( x*x - l/a ) - x;
	return sign * sqrt(x);
}
