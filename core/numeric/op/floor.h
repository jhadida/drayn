
//==================================================
// @title        floor.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T,class U>
inline U _floor_cast( T x, std::true_type ) // integral type
	{ return static_cast<U>(x); }

template <class T,class U>
inline U _floor_cast( T x, std::false_type ) // non-integral type
	{ return static_cast<U>(std::floor(x)); }

// ------------------------------------------------------------------------

// No cast
template <class T>
inline constexpr T op_floor( T x )
	{ return std::floor(x); }

// Cast as unsigned
template <class T>
inline uword_t op_ufloor( T x )
	{ return _floor_cast<T,uword_t>( x, std::is_integral<T>() ); }

// Cast as int
template <class T>
inline iword_t op_ifloor( T x )
	{ return _floor_cast<T,iword_t>( x, std::is_integral<T>() ); }