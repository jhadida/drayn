
//==================================================
// @title        owen.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================


inline double _owen_znorm1( double z )
  { return 0.5 * op_erf( z * C_1_SQRT2 ); }

inline double _owen_znorm2( double z )
  { return 0.5 * op_erfc( z * C_1_SQRT2 ); }

// ------------------------------------------------------------------------

double _owen_tfun ( double h, double a, double ah );
double op_owen_t  ( double h, double a  );
double op_owen_q  ( double h, double ah );
