
//==================================================
// @title        ceil.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T,class U>
inline U _ceil_cast( T x, std::true_type ) // integral type
	{ return static_cast<U>(x); }

template <class T,class U>
inline U _ceil_cast( T x, std::false_type ) // non-integral type
	{ return static_cast<U>(std::ceil(x)); }

// ------------------------------------------------------------------------

// No cast
template <class T>
inline constexpr T op_ceil( T x )
	{ return std::ceil(x); }

// Cast as unsigned
template <class T>
inline uword_t op_uceil( T x )
	{ return _ceil_cast<T,uword_t>( x, std::is_integral<T>() ); }

// Cast as int
template <class T>
inline iword_t op_iceil( T x )
	{ return _ceil_cast<T,iword_t>( x, std::is_integral<T>() ); }