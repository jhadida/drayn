
//==================================================
// @title        quotient.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T>
inline T _quotient( T a, T b, std::true_type const ) // integral type
{
	DBG_ASSERT( b != 0, "[dr._quotient] Division by zero." );
	return (b != 0) ? a/b : 0;
}

template <class T>
T _quotient( T a, T b, std::false_type const ) // non-integral type
{
	static const T Z = T(0);
	static const T M = c_num<T>::max;
	static const T m = c_num<T>::min;

	// a combines both signs, b becomes positive
	int s = (b >= Z) - (b < Z); 
	a *= s;
	b *= s;

	if ( b < 1 )
	{
		DBG_ASSERT( op_abs(a) < b*M, "[dr._quotient] Division overflow." );
		return (op_abs(a) < b*M) ? a/b : op_sign(a)*M;
	}
	else
	{
		DBG_ASSERT( op_abs(a) >= b*m, "[dr._quotient] Division underflow." );
		return (op_abs(a) >= b*m) ? a/b : op_sign(a)*m;
	}
}

// ------------------------------------------------------------------------

template < class T, class U,
	class V = typename std::common_type<T,U>::type >
inline V op_quotient( T a, U b )
	{ return _quotient( static_cast<V>(a), static_cast<V>(b), std::is_integral<V>() ); }
