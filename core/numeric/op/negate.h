
//==================================================
// @title        negate.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T> 
inline constexpr T _negate( T x, std::false_type ) // non-signed type
	{ return x; }

template <class T> 
inline constexpr T _negate( T x, std::true_type ) // signed type
	{ return -x; }

// ------------------------------------------------------------------------

template <class T> 
inline constexpr T op_negate( T x ) 
	{ return _negate( x, std::is_signed<T>() ); }
