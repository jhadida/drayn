
//==================================================
// @title        rad.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Conversion from/to radians/degrees
inline constexpr double op_rad2deg ( double r ) { return r*180*C_1_PI; }
inline constexpr double op_deg2rad ( double d ) { return d*C_PI / 180; }
