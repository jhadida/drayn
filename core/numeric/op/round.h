
//==================================================
// @title        round.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T,class U>
inline U _round_cast( T x, std::true_type ) // integral type
	{ return static_cast<U>(x); }

template <class T,class U>
inline U _round_cast( T x, std::false_type ) // non-integral type
	{ return static_cast<U>(std::round(x)); }

// ------------------------------------------------------------------------

// No cast
template <class T>
inline constexpr T op_round( T x )
	{ return std::round(x); }

// Cast as unsigned
template <class T>
inline uword_t op_uround( T x )
	{ return _round_cast<T,uword_t>( x, std::is_integral<T>() ); }

// Cast as int
template <class T>
inline iword_t op_iround( T x )
	{ return _round_cast<T,iword_t>( x, std::is_integral<T>() ); }