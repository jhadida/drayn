
//==================================================
// @title        abs.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T> 
inline constexpr T _abs( T x, std::false_type const ) // non-integral type
{
	return std::abs(x);
}

template <class T> 
inline constexpr T _abs( T x, std::true_type const ) // integral type
{
	return x < 0 ? -x : x;
}

// ------------------------------------------------------------------------

/**
 * Absolute value.
 */
template <class T> 
inline constexpr T op_abs( T x ) 
{
	return _abs( x, std::is_integral<T>() );
}

