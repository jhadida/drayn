
//==================================================
// @title        clamp.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Force a number in a range.
 */
template <class T> 
inline T op_clamp( T a, T lower_bound, T upper_bound )
{
	return a >= upper_bound 
		? upper_bound 
		: ( a < lower_bound? lower_bound : a );
}