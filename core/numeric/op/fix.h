
//==================================================
// @title        fix.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T>
inline T _fix( T x, std::true_type ) // signed type
	{ return x >= T(0) ? std::floor(x) : std::ceil(x); }

template <class T>
inline T _fix( T x, std::false_type ) // unsigned type
	{ return std::floor(x); }

// ------------------------------------------------------------------------

template <class T>
inline T op_fix( T x )
	{ return _fix<T>( x, std::is_signed<T>() ); }

// Cast as unsigned
template <class T>
inline uword_t op_ufix( T x )
	{ return static_cast<uword_t>(op_fix(x)); }

// Cast as int
template <class T>
inline iword_t op_ifix( T x )
	{ return static_cast<iword_t>(op_fix(x)); }
