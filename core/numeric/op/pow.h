
//==================================================
// @title        pow.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class Integer>
double _pow( double x, Integer a, std::true_type )
{
	if ( a < 0 )
		return op_quotient( 1.0, _pow( x, -a, std::true_type() ) );

	double result = 1.0;
	while (a)
	{
		if (a & 1)
			result *= x;
		a >>= 1;
		x  *= x;
	}

	return result;
}

template <class T>
inline double _pow( double x, T a, std::false_type )
	{ return std::pow( x, static_cast<double>(a) ); }

// ------------------------------------------------------------------------

template <class T, class U>
inline double op_pow( T x, U a )
	{ return _pow( static_cast<double>(x), a, std::is_integral<U>() ); }
