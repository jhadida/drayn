
//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cmath>
#include <complex>
#include <cstdint>
#include <cinttypes>

#include <iostream>
#include <algorithm>
#include <functional>
#include <type_traits>
#include <iomanip>
#include <ios>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

#include "ostream.h"

// ------------------------------------------------------------------------
// Bitwise operations

#include "bit/count.h"
#include "bit/gcd.h"
#include "bit/lcm.h"
#include "bit/gray.h"
#include "bit/print.h"
#include "bit/reverse.h"
#include "bit/swap.h"
#include "bit/two.h"

// ------------------------------------------------------------------------
// Math functions

#include "op/negate.h"
#include "op/sign.h"
#include "op/abs.h"
#include "op/floor.h"
#include "op/ceil.h"
#include "op/round.h"
#include "op/fix.h"
#include "op/clamp.h"
#include "op/binomial.h"
#include "op/quotient.h"
#include "op/pow.h"
#include "op/mod.h"
#include "op/rem.h"
#include "op/erf.h"
#include "op/owen.h"
#include "op/rad.h"

// ------------------------------------------------------------------------
// Function objects

#include "fun/arithmetic.h"
#include "fun/comparison.h"
#include "fun/fp_comparison.h"
#include "fun/cast.h"
#include "fun/scaling.h"
#include "fun/normal.h"

// ------------------------------------------------------------------------
// Predicates

#include "pred/sign.h"
#include "pred/division.h"
#include "pred/range.h"

DRAYN_NS_END_
