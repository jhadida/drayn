
//==================================================
// @title        arithmetic.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#define DRAYN_ARITHMETIC_FUNCTION( Name, Op )                  \
template < class In1, class In2 = In1,                         \
	class C = typename std::common_type<In1,In2>::type >       \
struct Name                                                    \
{                                                              \
	inline C operator() ( const In1& a, const In2& b ) const   \
		{ return static_cast<C>(a Op b); }                     \
};

DRAYN_ARITHMETIC_FUNCTION( f_add, + ); // addition
DRAYN_ARITHMETIC_FUNCTION( f_sub, - ); // subtraction
DRAYN_ARITHMETIC_FUNCTION( f_div, / ); // division
DRAYN_ARITHMETIC_FUNCTION( f_mul, * ); // multiplication

// ------------------------------------------------------------------------

template < class In1, class In2 = In1,
	class C = typename std::common_type<In1,In2>::type >
struct f_quo
{
	inline C operator() ( const In1& a, const In2& b ) const
		{ return op_quotient( static_cast<C>(a), static_cast<C>(b) ); }
};

template <class T>
struct f_inv
{
	inline T operator() ( const T& x ) const
		{ return op_quotient( T(1), x ); }
};

template <class T>
struct f_neg
{
	inline T operator() ( const T& x ) const
		{ return op_negate(x); }
};

template <class T>
struct f_abs
{
	inline T operator() ( const T& x ) const
		{ return op_abs(x); }
};
