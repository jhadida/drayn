
//==================================================
// @title        normal.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * One-dimensional Gaussian functions.
 */

struct f_normal
{
	f_scaling<double> scaling;

	f_normal( double m = 0.0, double s = 1.0 )
		: scaling(m,s) {}


	// Setters
	inline void set_mu    ( double m ) { scaling.mu    = m; }
	inline void set_sigma ( double s ) { scaling.sigma = s; }


	// Probability density
	inline double pdf( double x )
	{ 
		double v = scaling.scale(x); 
		return C_1_SQRT2PI * exp( - 0.5*v*v ) / scaling.sigma; 
	}

	inline double pdf_inv_pos( double y ) { return scaling.mu + pdf_inv_dev(y); }
	inline double pdf_inv_neg( double y ) { return scaling.mu - pdf_inv_dev(y); }
	       double pdf_inv_dev( double y );


	// Cumulative density
	inline double cdf( double x )
		{ return 0.5 * (1.0 + op_erf( scaling.scale(x)*C_1_SQRT2 )); }

	double cdf_inv( double y );
};
