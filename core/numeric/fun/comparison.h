
//==================================================
// @title        comparison.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#define DRAYN_COMPARISON_FUNCTION( Name, Op )                     \
template < class In1, class In2 = In1,                            \
	class C = typename std::common_type<In1,In2>::type >          \
struct Name                                                       \
{                                                                 \
	inline bool operator() ( const In1& a, const In2& b ) const   \
		{ return static_cast<C>(a) Op static_cast<C>(b); }        \
};

DRAYN_COMPARISON_FUNCTION( f_lt , <  ); // lesser than
DRAYN_COMPARISON_FUNCTION( f_leq, <= ); // lesser or equal
DRAYN_COMPARISON_FUNCTION( f_gt , >  ); // greater than
DRAYN_COMPARISON_FUNCTION( f_geq, >= ); // greater or equal
DRAYN_COMPARISON_FUNCTION( f_eq , == ); // equal
DRAYN_COMPARISON_FUNCTION( f_neq, != ); // not equal
