
//==================================================
// @title        cast.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

template <class T>
struct f_cast
{
	inline T operator() ( const T& x ) const
		{ return x; }
	template <class U>
	inline T operator() ( const U& x ) const 
		{ return static_cast<T>(x); }
};