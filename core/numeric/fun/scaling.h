
//==================================================
// @title        scaling.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// x -> (x-mu) / sigma
template <class Real = double>
struct f_scaling
{
	Real mu, sigma;

	f_scaling( Real m = Real(0), Real s = Real(1) )
		: mu(m), sigma(s) {}

	inline Real   scale( const Real& x ) const { return (x-mu) / sigma; }
	inline Real unscale( const Real& y ) const { return mu + sigma*y; }
};
