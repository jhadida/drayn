
//==================================================
// @title        fp_relational.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Floating point comparison
 */

template < class In1, class In2 = In1, 
	class C = typename std::common_type<In1,In2>::type >
struct fp_equal_strict
{
	inline bool operator() ( const In1& a, const In2& b )
	{
		return op_abs(a - b) 
			<= std::max(
				c_num<C>::min * std::min( op_abs(a), op_abs(b) ),
				c_num<C>::eps
			);
	}
};

template < class In1, class In2 = In1, 
	class C = typename std::common_type<In1,In2>::type >
struct fp_equal_loose
{
	inline bool operator() ( const In1& a, const In2& b )
	{
		return op_abs(a - b) 
			<= std::max(
				c_num<C>::min * std::max( op_abs(a), op_abs(b) ),
				c_num<C>::eps
			);
	}
};

// ------------------------------------------------------------------------

template < class In1, class In2 = In1, 
	class C = typename std::common_type<In1,In2>::type >
struct fp_greater
{
	inline bool operator() ( const In1& a, const In2& b )
	{
		return (a - b) >= c_num<C>::min * std::max( op_abs(a), op_abs(b) );
	}
};

template < class In1, class In2 = In1, 
	class C = typename std::common_type<In1,In2>::type >
struct fp_lesser
{
	inline bool operator() ( const In1& a, const In2& b )
	{
		return (b - a) >= c_num<C>::min * std::max( op_abs(a), op_abs(b) );
	}
};

// ------------------------------------------------------------------------

template < class In1, class In2 = In1, 
	class C = typename std::common_type<In1,In2>::type >
struct fp_close
{
	C eps;

	fp_close( C e = c_num<C>::eps )
		: eps(e) {}

	inline bool operator() ( const In1& a, const In2& b )
	{
		return op_abs(a - b) <= eps;
	}
};
