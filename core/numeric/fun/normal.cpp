
//==================================================
// @title        normal.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

double f_normal::pdf_inv_dev( double y )
{
	y = C_SQRT2PI * scaling.sigma*y;

	DBG_ASSERT_RVAL( y <= 1.0, scaling.mu,
		"[dr.f_normal] Bad input: denormalized probability should be less than 1." );
	DBG_ASSERT_RVAL( y > c_num<double>::eps, c_num<double>::max,
		"[dr.f_normal] Overflow: probability is too small." );

	return scaling.sigma * sqrt( -2.0 * log(Drayn_CLAMP( y, c_num<double>::eps, 1.0 )) );
}

// ------------------------------------------------------------------------

double f_normal::cdf_inv( double y )
{
	DBG_ASSERT_RVAL( y > c_num<double>::eps, -c_num<double>::max,
		"[dr.f_normal] Underflow: input is too close to 0." );
	DBG_ASSERT_RVAL( y < 1.0-c_num<double>::eps, c_num<double>::max,
		"[dr.f_normal] Overflow: input is too close to 1." );

	y = Drayn_CLAMP( y, c_num<double>::eps, 1.0-c_num<double>::eps );
	return scaling.unscale( C_SQRT2*op_erfinv( 2.0*y - 1.0 ) );
}
