
//==================================================
// @title        ostream.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



/**
 * Output stream format for numeric types.
 * TODO: also format complex numbers.
 */

template <
	class T,
	unsigned Selector = type_selector< std::is_integral<T>::value, std::is_floating_point<T>::value >()
>
struct numeric_ostream_width {};

template <class T>
struct numeric_ostream_width<T,1> // integral specialisation
{
	static unsigned value;
	static void set( unsigned w ) { value = w; }
};

template <class T>
struct numeric_ostream_width<T,2> // floating-point specialisation
{
	static unsigned value;
	static void set( unsigned w ) { value = w; }
};

struct numeric_ostream_precision
{
	static unsigned value;
	static void set( unsigned p ) { value = p; }
};

// ------------------------------------------------------------------------

template <class T> unsigned numeric_ostream_width<T,1>::value = DRAYN_OUTPUT_INTEGER_WIDTH;
template <class T> unsigned numeric_ostream_width<T,2>::value = DRAYN_OUTPUT_FLOAT_WIDTH;

// ------------------------------------------------------------------------

template <
	class T,
	class = typename std::enable_if< is_numeric<T>::value >::type
>
inline std::ostream& numeric_ostream( std::ostream& os )
{
	return os<< std::fixed << std::setw( numeric_ostream_width<T>::value )
		<< std::setprecision(numeric_ostream_precision::value);
}
