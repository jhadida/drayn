
//==================================================
// @title        lcm.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Least common multiple
template <class Unsigned>
Unsigned bit_lcm( Unsigned a, Unsigned b )
{
	Unsigned t = bit_gcd(a,b);
	return t? (a/t * b) : 0;
}
