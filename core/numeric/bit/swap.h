
//==================================================
// @title        swap.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// XOR swap of two integers
template <class Integer>
inline void bit_swap( Integer& a, Integer& b )
{
	a ^= b;
	b ^= a;
	a ^= b;
}
