
//==================================================
// @title        print.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Print an integer in binary form
template <class Integer>
void bit_print( Integer n )
{
	static const size_t b = 8*sizeof(Integer);
	static char buf[b+1]; buf[b] = '\0';

	std::cout<< "[bitprint] " << n;
	for (size_t m = b; m; n >>= 1, --m)
		buf[m-1] = (n & 1) + '0';
	std::cout<< " = " << buf;
}
