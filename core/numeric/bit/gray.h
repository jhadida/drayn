
//==================================================
// @title        gray.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Gray codes transforms.
 */

template <class Integer>
inline Integer bit_bin2gray( Integer n )
{
	return (n >> 1) ^ n;
}

template <class Integer>
Integer bit_gray2bin( Integer n )
{
	Integer m = n; while ( m >>= 1 ) { n = n ^ m; }
	return n;
}
