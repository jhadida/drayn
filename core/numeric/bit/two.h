
//==================================================
// @title        two.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Related to the number 2.
 */

template <class Integer>
unsigned bit_log2( Integer n )
{
	unsigned l = 0; while ( n > 1 ) { n >>= 1; ++l; }
	return l;
}

inline uint64_t bit_pow2( unsigned p )
{
	return static_cast<uint64_t>(1) << p;
}
// ------------------------------------------------------------------------

template <class Integer>
inline bool bit_ispow2( Integer n )
{
	return (n > 0) && ((n & (n-1)) == 0);
}

template <class Integer>
inline uint64_t bit_nextpow2( Integer n )
{
	return bit_pow2(bit_log2(n) + !bit_ispow2(n));
}
