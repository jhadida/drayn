
//==================================================
// @title        reverse.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Reverse the bits of a given integer
template <class Integer>
Integer bit_reverse( Integer n, unsigned nbits )
{
	Integer r = n;
	Integer l, m;

	// Compute shift and bitmask
	l = (Integer) (nbits - 1);
	m = (1 << nbits) - 1;

	// Permute
	while ( n >>= 1 )
	{
		r <<= 1;
		r |= n & 1;
		--l;
	}

	// Final shift and masking
	r <<= l;
	return r & m;
}
