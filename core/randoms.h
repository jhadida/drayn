
//==================================================
// @title        randoms.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <random>
#include <functional>
#include <algorithm>
#include <utility>
#include <chrono>

/**
 * WARNING
 * Deterministic random seed generation will only work WITHIN A SINGLE THREAD.
 * It does not make sense to require a deterministic sequence to be conserved
 * across multiple runs of a multi-threaded app, because the order of execution
 * of the threads is undefined.
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START_

//-------------------------------------------------------------
// Random seed generators
//-------------------------------------------------------------

// wide minimal standard random engine
typedef std::linear_congruential_engine<size_t, 48271, 0, 2147483647> wminst_rand;

// Epoch-based random seed generator
struct random_seed_generator
{
	static inline size_t get()
		{ return std::chrono::system_clock::now().time_since_epoch().count(); }
};

// Deterministic seed generator
struct deterministic_seed_generator
{
	static wminst_rand engine;

	static inline size_t get()
		{ return engine(); }

	static inline void seed( size_t s )
		{ engine.seed(s); }
};

// Fixed seed "generator"
struct fixed_seed_generator
{
	static size_t seed;
	static inline size_t get()
		{ return seed; }
};

//-------------------------------------------------------------
// Type definitions
//-------------------------------------------------------------

// By default, Drayn uses a 64bit Mersenne-Twister random engine
typedef std::mt19937_64 default_random_engine;

// Uncomment the corresponding flags in drayn.h or define the following via compiler flag
#if    defined(DRAYN_DETERMINISTIC_RANDOM_SEED)
	typedef deterministic_seed_generator
#elif  defined(DRAYN_FIXED_RANDOM_SEED)
	typedef fixed_seed_generator
#else
	typedef random_seed_generator
#endif
	default_seed_generator;

//-------------------------------------------------------------
// Bind any distribution to any engine
//-------------------------------------------------------------

// Type of a random distribution bound to a random generator
template <class _dist>
using generator_type = std::function< typename _dist::result_type () >;

// Generic distribution binder
template <class _dist, class _seed, class _engine, class... Args>
inline generator_type<_dist> create_random_generator( Args&&... args )
{
	return std::bind(
		_dist   ( std::forward<Args>(args)... ),
		_engine ( _seed::get() )
	);
}

// Simplified distribution binder with manual seed as first argument
template <class _dist, class... Args>
inline generator_type<_dist> create_random_generator_with_seed( size_t seed, Args&&... args )
{
	return std::bind( _dist( std::forward<Args>(args)... ), default_random_engine( seed ) );
}



		/********************     **********     ********************/
		/********************     **********     ********************/



//-------------------------------------------------------------
// Shorthand functions to create common generators
//-------------------------------------------------------------

template <
	class _iterator,  // random-access iterator
	class _seed   = default_seed_generator,
	class _engine = default_random_engine
>
inline void shuffle( _iterator begin, _iterator end )
	{ std::shuffle( begin, end, _engine( _seed::get() ) ); }

// ------------------------------------------------------------------------

template <
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::bernoulli_distribution
>
inline generator_type<_dist> bernoulli_gen( double p )
	{ return create_random_generator<_dist,_seed,_engine>(p); }


template <
	class _int    = int,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::binomial_distribution<_int>
>
inline generator_type<_dist> binomial_gen( _int t, double p = 0.5 )
	{ return create_random_generator<_dist,_seed,_engine>( t, p ); }


template <
	class _int    = int,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::uniform_int_distribution<_int>
>
inline generator_type<_dist> uniform_int_gen( _int a, _int b )
	{ return create_random_generator<_dist,_seed,_engine>( a, b ); }


template <
	class _int    = int,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::geometric_distribution<_int>
>
inline generator_type<_dist> geometric_gen( double p = 0.5 )
	{ return create_random_generator<_dist,_seed,_engine>( p ); }


template <
	class _int    = int,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::poisson_distribution<_int>
>
inline generator_type<_dist> poisson_gen( double mu = 1.0 )
	{ return create_random_generator<_dist,_seed,_engine>( mu ); }

// ------------------------------------------------------------------------

template <
	class _real   = double,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::uniform_real_distribution<_real>
>
inline generator_type<_dist> uniform_gen( _real a = _real(0.0), _real b = _real(1.0) )
	{ return create_random_generator<_dist,_seed,_engine>( a, b ); }


template <
	class _real   = double,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::normal_distribution<_real>
>
inline generator_type<_dist> normal_gen( _real mu = _real(0.0), _real sigma = _real(1.0) )
	{ return create_random_generator<_dist,_seed,_engine>( mu, sigma ); }


template <
	class _real   = double,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::exponential_distribution<_real>
>
inline generator_type<_dist> exponential_gen( _real lambda = _real(1.0) )
	{ return create_random_generator<_dist,_seed,_engine>( lambda ); }


template <
	class _real   = double,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::lognormal_distribution<_real>
>
inline generator_type<_dist> lognormal_gen( _real mu = _real(0.0), _real sigma = _real(1.0) )
	{ return create_random_generator<_dist,_seed,_engine>( mu, sigma ); }


template <
	class _real   = double,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::gamma_distribution<_real>
>
inline generator_type<_dist> gamma_gen( _real alpha = _real(1.0), _real beta = _real(1.0) )
	{ return create_random_generator<_dist,_seed,_engine>( alpha, beta ); }

// ------------------------------------------------------------------------

template <
	class _real   = double,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::student_t_distribution<_real>
>
inline generator_type<_dist> student_t_gen( _real n = _real(1.0) )
	{ return create_random_generator<_dist,_seed,_engine>( n ); }


template <
	class _real   = double,
	class _seed   = default_seed_generator,
	class _engine = default_random_engine,
	class _dist   = std::chi_squared_distribution<_real>
>
inline generator_type<_dist> chi_squared_gen( _real n = _real(1.0) )
	{ return create_random_generator<_dist,_seed,_engine>( n ); }

DRAYN_NS_END_
