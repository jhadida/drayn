
//==================================================
// @title        allocators.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <type_traits>
#include <algorithm>
#include <stdexcept>
#include <cstdlib>
#include <cstring>
#include <new>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START_

/**
 * Static default-constructor / destructor.
 * This is used to initialize the allocated memory using the C functions malloc and calloc.
 * It should not be used directly unless you know what you're doing.
 */
template <
	class T,
	usize_t Selector = type_selector< std::is_pointer<T>::value, std::is_arithmetic<T>::value >()
>
struct constructor
{
	CORE_TRAITS(T);

	static_assert( !std::is_const<T>::value,
		"[dr.constructor] Cannot construct constant value-types." );

	static void construct( ptr_t p, usize_t n = 1 )
	{
		for ( usize_t i = 0; i < n; ++i, ++p )
			new (p) T();
	}

	static void destroy( ptr_t p, usize_t n = 1 )
	{
		for ( usize_t i = 0; i < n; ++i, ++p )
			p->~T();
	}
};

	template <class T>
	struct constructor<T,1> // pointer specialisation
	{
		CORE_TRAITS(T);

		// Note: this does not affect non-constant pointers to const values
		static_assert( !std::is_const<T>::value,
			"[dr.constructor] Cannot construct constant pointers." );

		static void construct( ptr_t p, usize_t n = 1 )
		{
			#ifdef DRAYN_DEBUG
			std::fill_n( p, n, nullptr );
			#endif
		}

		static void destroy( ptr_t p, usize_t n = 1 )
		{
			#ifdef DRAYN_DEBUG
			std::fill_n( p, n, nullptr );
			#endif
		}
	};

	template <class T>
	struct constructor<T,2> // arithmetic specialisation
	{
		CORE_TRAITS(T);

		// Note: this does not affect non-constant pointers to const values
		static_assert( !std::is_const<T>::value,
			"[dr.constructor] Cannot construct constant numbers." );

		static void construct( ptr_t p, usize_t n = 1 )
		{
			// Set to 0 in debug mode
			#ifdef DRAYN_DEBUG
			memset( p, 0, n*sizeof(T) );
			#endif
		}

		static void destroy( ptr_t p, usize_t n = 1 )
			{ /* nothing to do */ }
	};

// ------------------------------------------------------------------------

struct tag_allocator_noalloc {};
struct tag_allocator_new {};
struct tag_allocator_malloc {};
struct tag_allocator_calloc {};

// ------------------------------------------------------------------------

/**
 * This is useful for unmanaged (external) memory handling.
 * This allocator doesn't do anything, the release method accepts any input and
 * the size (second input) is optional.
 */
template <class T>
struct allocator_noalloc
{
	CORE_TRAITS(T);
	typedef allocator_noalloc<T> self;
	typedef tag_allocator_noalloc tag;

	static ptr_t allocate( usize_t n=0 )
		{ return nullptr; }

	template <class U>
	static void release( U ptr, usize_t n=0 )
		{ /* do nothing */ }

	struct deleter
	{
		template <class U>
		void operator() ( U ptr, usize_t n=0 ) const {}
	};
};

// ------------------------------------------------------------------------

/**
 * Allocator using new/delete.
 */
template <class T>
struct allocator_new
{
	CORE_TRAITS(T);
	typedef allocator_new<T> self;
	typedef tag_allocator_new tag;

	static_assert( !std::is_const<T>::value,
		"[dr.allocator_new] Cannot allocate constant value-types." );

	static ptr_t allocate( usize_t n )
	{
		ptr_t ptr = nullptr;

		if ( n > 1 )
			ptr = new (std::nothrow) T[n];
		else if ( n == 1 )
			ptr = new (std::nothrow) T();

		return ptr;
	}

	static void release( ptr_t ptr, usize_t n )
	{
		DBG_REJECT( ptr && (n==0), "[dr.allocator_new] Got a non-null pointer with null size." );
		if ( ptr && n )
		{
			// constructor<T>::destroy( ptr, n ); // called by delete
			if (n > 1)
				delete[] ptr;
			else
				delete ptr;
		}
	}

	struct deleter
	{
		inline void operator() ( ptr_t ptr, usize_t n ) const
			{ self::release(ptr,n); }
	};
};

// ------------------------------------------------------------------------

/**
 * Allocator using malloc.
 */
template <class T>
struct allocator_malloc
{
	CORE_TRAITS(T);
	typedef allocator_malloc<T> self;
	typedef tag_allocator_malloc tag;

	static_assert( !std::is_const<T>::value,
		"[dr.allocator_malloc] Cannot allocate constant value-types." );

	static ptr_t allocate( usize_t n )
	{
		ptr_t ptr = nullptr;

		if ( n > 0 && (ptr = (ptr_t) std::malloc( n*sizeof(T) )) )
			constructor<T>::construct( ptr, n );

		return ptr;
	}

	static void release( ptr_t ptr, usize_t n )
	{
		DBG_REJECT( ptr && (n==0), "[dr.allocator_malloc] Got a non-null pointer with null size." );
		if ( ptr && n )
		{
			constructor<T>::destroy( ptr, n );
			std::free( (void*) ptr );
		}
	}

	struct deleter
	{
		inline void operator() ( ptr_t ptr, usize_t n ) const
			{ self::release(ptr,n); }
	};
};

// ------------------------------------------------------------------------

/**
 * Allocator using calloc.
 */
template <class T>
struct allocator_calloc
{
	CORE_TRAITS(T);
	typedef allocator_calloc<T> self;
	typedef tag_allocator_calloc tag;

	static_assert( !std::is_const<T>::value,
		"[dr.allocator_calloc] Cannot allocate constant value-types." );

	static ptr_t allocate( usize_t n )
	{
		ptr_t ptr = nullptr;

		if ( n > 0 && (ptr = (ptr_t) std::calloc( n, sizeof(T) )) )
			constructor<T>::construct( ptr, n );

		return ptr;
	}

	static void release( ptr_t ptr, usize_t n )
	{
		DBG_REJECT( ptr && (n==0), "[dr.allocator_calloc] Got a non-null pointer with null size." );
		if ( ptr && n )
		{
			constructor<T>::destroy( ptr, n );
			std::free( (void*) ptr );
		}
	}

	struct deleter
	{
		inline void operator() ( ptr_t ptr, usize_t n ) const
			{ self::release(ptr,n); }
	};
};

DRAYN_NS_END_
