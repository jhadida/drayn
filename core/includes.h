#ifndef DRAYN_CORE_H_INCLUDED
#define DRAYN_CORE_H_INCLUDED

//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "typedefs.h"
#include "traits.h"
#include "macros.h"
#include "messages.h"

// Design patterns
#include "patterns/singleton.h"
#include "patterns/factory.h"

// Standalone
#include "allocators.h"
#include "constants.h"
#include "randoms.h"
#include "pubsub.h"
#include "timer.h"

// Numeric routines
#include "numeric/includes.h"

#endif
