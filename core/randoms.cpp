
//==================================================
// @title        randoms.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

#ifdef DRAYN_DETERMINISTIC_RANDOM_SEED
	wminst_rand deterministic_seed_generator::engine( DRAYN_DETERMINISTIC_RANDOM_SEED );
#else
	wminst_rand deterministic_seed_generator::engine(0);
#endif

#ifdef DRAYN_FIXED_RANDOM_SEED
	size_t fixed_seed_generator::seed = DRAYN_FIXED_RANDOM_SEED;
#else
	size_t fixed_seed_generator::seed = 0;
#endif

DRAYN_NS_END_
