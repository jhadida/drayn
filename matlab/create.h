
//==================================================
// @title        create.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Template type T should never be const (allocator will throw a static error).
 * Boolean flag M should only be set to false for Mex outputs (memory leak otherwise).
 *
 * By default, arrays created with these methods are temporary runtime arrays;
 * allocated memory will automatically be released at the end of the Mex file.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T,bool M = true>
inline mx_vector<T,M> mx_create_vector( usize_t n )
	{ return mx_vector<T,M>( allocator_matlab_numeric<T>::allocate_vector(n) ); }

template <class T,bool M = true>
inline mx_vector<T,M> mx_create_col( usize_t n )
	{ return mx_vector<T,M>( allocator_matlab_numeric<T>::allocate_col(n) ); }

template <class T,bool M = true>
inline mx_vector<T,M> mx_create_row( usize_t n )
	{ return mx_vector<T,M>( allocator_matlab_numeric<T>::allocate_row(n) ); }

// ------------------------------------------------------------------------

template <class T,bool M = true>
inline mx_matrix<T,M> mx_create_matrix( usize_t r, usize_t c )
	{ return mx_matrix<T,M>( allocator_matlab_numeric<T>::allocate_matrix(r,c) ); }

template <class T,bool M = true>
inline mx_volume<T,M> mx_create_volume( usize_t r, usize_t c, usize_t p )
	{ return mx_volume<T,M>( allocator_matlab_numeric<T>::allocate_volume(r,c,p) ); }

// TODO: this is dangerous if ptr jumps, pass nd_dimensions instead
template <class T,bool M = true>
inline mx_array<T,M> mx_create_array( const array<const usize_t>& size )
	{ return mx_array<T,M>( allocator_matlab_numeric<T>::allocate_array(size) ); }

DRAYN_NS_END_
