
//==================================================
// @title        export.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "mex.h"
#include <string>

/**
 * These methods are used mainly by mx_arrays.
 * They should NOT be used with temporary runtime Mex arrays (dynamically
 * allocated, non-output), unless you know what you are doing.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T>
inline mxArray* mx_export_scalar( const T& val )
	{ return mxCreateDoubleScalar( static_cast<double>(val) ); }

inline mxArray* mx_export_logical( bool val )
	{ return mxCreateLogicalScalar(val); }

inline mxArray* mx_export_string( const std::string& val )
	{ return mxCreateString( val.c_str() ); }

// ------------------------------------------------------------------------

template <class Vec>
mxArray* mx_export_vector( const Vec& v, usize_t n, bool column = false )
{
	usize_t nrows = column ? n : 1;
	usize_t ncols = column ? 1 : n;

	mxArray *mexv = mxCreateDoubleMatrix( nrows, ncols, mxREAL );
	double  *mptr = mxGetPr(mexv);

	for ( usize_t i = 0; i < n; ++i )
		mptr[i] = static_cast<double>(v[i]);

	return mexv;
}

template <class Mat>
mxArray* mx_export_matrix( const Mat& M, usize_t nrows, usize_t ncols )
{
	mxArray *mexm = mxCreateDoubleMatrix( nrows, ncols, mxREAL );
	double  *mptr = mxGetPr(mexm);

	for ( usize_t c = 0; c < ncols; ++c )
	for ( usize_t r = 0; r < nrows; ++r )
		mptr[ r + c*nrows ] = static_cast<double>(M(r,c));

	return mexm;
}

template <class Vol>
mxArray* mx_export_volume( const Vol& V, usize_t nrows, usize_t ncols, usize_t nslices )
{
	mwSize dims[3];
		dims[0] = nrows;
		dims[1] = ncols;
		dims[2] = nslices;

	mxArray *mexv = mxCreateNumericArray( 3, (const mwSize*) dims, mxDOUBLE_CLASS, mxREAL );
	double  *mptr = mxGetPr(mexv);

	for ( usize_t s = 0; s < nslices; ++s )
	for ( usize_t c = 0; c < ncols; ++c )
	for ( usize_t r = 0; r < nrows; ++r )
		mptr[ r + c*nrows + s*nrows*ncols ] = static_cast<double>(V(r,c,s));

	return mexv;
}

DRAYN_NS_END_
