#ifndef DRAYN_MATLAB_H_INCLUDED
#define DRAYN_MATLAB_H_INCLUDED
#ifdef  DRAYN_USING_MATLAB

//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "typedefs.h"
#include "allocator.h"
#include "traits.h"
#include "output.h"

// ----------  =====  ----------

/**
 * NOTE:
 * Comment the following include if you dont/cant include the libut library.
 * If uncomented, make sure you include the flag "-lut" to your gcc/clang command.
 */
#include "interrupt.h"

// ----------  =====  ----------

#include "wrappers/header.h"
#include "extract.h"
#include "export.h"

// Abstract classes
#include "abstract/array.h"
#include "abstract/mappable.h"

// Wrappers
#include "wrappers/vector.h"
#include "wrappers/matrix.h"
#include "wrappers/volume.h"
#include "wrappers/array.h"
#include "wrappers/struct.h"
#include "wrappers/cell.h"
#include "wrappers/MAT.h"

#include "create.h"

#endif
#endif
