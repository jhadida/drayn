
//==================================================
// @title        interrupt.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#ifdef __cplusplus
    extern "C" bool utIsInterruptPending();
#else
    extern bool utIsInterruptPending();
#endif



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

inline bool mx_check_interruption( unsigned every_X_call = 1 )
{
	static unsigned counter = 0;

	counter = (counter+1) % Drayn_MAX(every_X_call,1);
	return (counter == 0) && utIsInterruptPending();
}

// ------------------------------------------------------------------------

inline void mx_throw_on_interruption( const char *msg = "Interruption detected.", unsigned every_X_call = 1 )
{
	if ( mx_check_interruption(every_X_call) )
		throw std::runtime_error(msg);
}

DRAYN_NS_END_
