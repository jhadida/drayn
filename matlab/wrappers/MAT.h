
//==================================================
// @title        MAT.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

class mx_MAT
	: public mx_mappable
{
public:

	typedef mx_MAT       self;
	typedef mx_mappable  parent;

	mx_MAT()
		: m_file(nullptr)
		{ clear(); }
	mx_MAT( const char *name )
		: m_file(nullptr)
		{ open(name); }
	~mx_MAT()
		{ clear(); }

	void clear();
	bool open( const char *name );

	inline bool valid() const { return m_file; }
	inline const MATFile* mx() const { return m_file; }

protected:

	MATFile *m_file;
};

DRAYN_NS_END_
