
//==================================================
// @title        header.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

void mx_header::clear()
{
	type_id = mxUNKNOWN_CLASS;
	numel = element_size = 0;
	size.clear();
}

// ------------------------------------------------------------------------

auto mx_header::assign( const mxArray *pm ) -> self&
{
	ASSERT_RVAL( pm, *this, "Null pointer in input." );

	element_size = mxGetElementSize(pm);
	type_id      = mxGetClassID(pm);
	numel        = mxGetNumberOfElements(pm);

	const usize_t nd = mxGetNumberOfDimensions(pm);
	const usize_t *s = (const usize_t*) mxGetDimensions(pm);
	size.assign( s, s+nd );

	return *this;
}

DRAYN_NS_END_
