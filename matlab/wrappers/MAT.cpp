
//==================================================
// @title        MAT.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

void mx_MAT::clear()
{
	if ( m_file )
		matClose(m_file);

	m_file = nullptr;
	parent::clear();
}

// ------------------------------------------------------------------------

bool mx_MAT::open( const char *name )
{
	clear();
	ASSERT_RF( name, "Null pointer." );

	MATFile *mf = matOpen( name, "r" );
	ASSERT_RF( mf != NULL, "Error opening file '%s'.", name );

	int nf = 0;
	const char **fnames = (const char**) matGetDir( mf, &nf );
	REJECT_RF( nf == 0, "Empty MAT file." );

	m_file = mf;
	this->m_fields.resize(nf);

	for ( int f = 0; f < nf; ++f )
	{
		this->m_fields[f] = fnames[f];
		this->m_fmap[ this->m_fields[f] ] = matGetVariable( mf, fnames[f] );
	}

	return true;
}

DRAYN_NS_END_
