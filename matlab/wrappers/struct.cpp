
//==================================================
// @title        struct.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

void mx_struct::clear()
{
	m_mstruct = nullptr;
	parent::clear();
}

// ------------------------------------------------------------------------

bool mx_struct::wrap( const mxArray* ms, mwIndex index )
{
	clear();
	ASSERT_RF( ms, "Null pointer." );
	ASSERT_RF( mxIsStruct(ms), "Not a struct." );

	const usize_t nf = mxGetNumberOfFields(ms);
	INFO_REJECT( nf == 0, "Empty struct." );

	m_mstruct = ms;
	this->m_fields.resize(nf);

	for ( usize_t f = 0; f < nf; ++f )
	{
		this->m_fields[f] = mxGetFieldNameByNumber(ms,f);
		this->m_fmap[ this->m_fields[f] ] = mxGetFieldByNumber(ms,index,f);
	}

	return true;
}

// ------------------------------------------------------------------------

int mx_set_field( mxArray *mxs, usize_t index, const char *field, mxArray *value )
{
	ASSERT_RVAL( mxIsStruct(mxs), -1, "Input is not a struct." );

	// try to find the corresponding field
	int fnum = mxGetFieldNumber( mxs, field );

	// the field exists, check if there is something there
	if ( fnum >= 0 )
	{
		mxArray *fval = mxGetFieldByNumber( mxs, index, fnum );

		// there is something, so delete it first
		if ( fval ) mxDestroyArray( fval );
	}
	else // the field doesn't exist, so create it
	{
		fnum = mxAddField( mxs, field );
	}

	// set the value now
	mxSetField( mxs, index, field, value );

	return fnum;
}

DRAYN_NS_END_
