
//==================================================
// @title        header.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <vector>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * This class stores the typical information associated with Mex data types.
 * The method assign(), or alternatively the assigment operator or constructor
 * extract and copy these information into the instance.
 */

struct mx_header
{
	// Matlab ClassID
	mxClassID type_id;

	// Number of elements
	usize_t numel;

	// Size in bytes of one element
	usize_t element_size;

	// Dimensions of the array container
	std::vector<usize_t> size;

	// ------------------------------------------------------------------------

	typedef mx_header self;

	mx_header()
		{ clear(); }
	mx_header( const mxArray *pm )
		{ assign(pm); }

	void clear();

	inline usize_t ndims() const
		{ return size.size(); }

	       self&  assign    ( const mxArray *pm );
	inline self&  operator= ( const mxArray *pm ) { assign(pm); return *this; }
};

DRAYN_NS_END_
