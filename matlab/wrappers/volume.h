
//==================================================
// @title        volume.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * This ties a Mex array with a Drayn nd_array together.
 * Use the get() method to get the Drayn array,
 * and the mx() method to get the pointer to mxArray.
 *
 * See create.h and extract.h for practical features using
 * this data structure.
 */
template <class T, bool managed = false>
class mx_volume
	: public mx_array_base< T, mx::vol<T>, managed >
{
public:

	CORE_TRAITS(T);

	typedef mx_volume<T,managed>          self;
	typedef mx_traits<T,managed>          traits;
	typedef typename traits::mx_ptr_type  mxptr_t;

	// ----------  =====  ----------


	mx_volume() {}
	mx_volume( mxptr_t *pm ) { assign(pm); }

	// proxy methods
	inline usize_t nrows  () const { return this->m_dr.dim(0); }
	inline usize_t ncols  () const { return this->m_dr.dim(1); }
	inline usize_t nslices() const { return this->m_dr.dim(2); }

	inline ref_t operator() ( usize_t i, usize_t j, usize_t k ) const
		{ return this->m_dr(i,j,k); }


	void swap( self& other )
	{
		this->m_mx.swap( other.m_mx );
		this->m_dr.swap( other.m_dr );
	}

	bool assign( mxptr_t *pm )
	{
		this->m_mx.reset( pm, typename traits::deleter_type() );
		return mx_extract_volume( pm, this->m_dr );
	}
	
	inline mx::mat<T> slice( usize_t k, usize_t orth = 2 ) const
		{ return this->m_dr.slice(k,orth); }

};

DRAYN_NS_END_
