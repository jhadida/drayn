
//==================================================
// @title        array.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * This ties a Mex array with a Drayn nd_array together.
 * Use the get() method to get the Drayn array,
 * and the mx() method to get the pointer to mxArray.
 *
 * See create.h and extract.h for practical features using
 * this data structure.
 */
template <class T, bool managed = false>
class mx_array
	: public mx_array_base< T, mx::arr<T>, managed >
{
public:

	CORE_TRAITS(T);

	typedef mx_array<T,managed>           self;
	typedef mx_traits<T,managed>          traits;
	typedef typename traits::mx_ptr_type  mxptr_t;

	// ----------  =====  ----------


	mx_array() {}
	mx_array( mxptr_t *pm ) { assign(pm); }

	inline usize_t size( usize_t k ) const
		{ return this->m_dr.dim(k); }
	inline usize_t numel() const
		{ return this->m_dr.numel(); }

	void swap( self& other )
	{
		this->m_mx.swap( other.m_mx );
		this->m_dr.swap( other.m_dr );
	}

	bool assign( mxptr_t *pm )
	{
		this->m_mx.reset( pm, typename traits::deleter_type() );
		return mx_extract_array( pm, this->m_dr );
	}
};

DRAYN_NS_END_
