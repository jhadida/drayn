
//==================================================
// @title        struct.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * This provides a read-only wrapper for Mex input structures.
 * The method wrap() returns false if the input is not a Mex structure,
 * and you can specify the index in the case of structure-arrays.
 *
 * The operator[] is overloaded for convenience, so that instances behave
 * like standard maps. You can use the method has_field to check whether
 * a specific field exists, and has_fields({}) (initializer list of strings)
 * to check for several fields at once.
 */
class mx_struct
	: public mx_mappable
{
public:

	typedef mx_struct    self;
	typedef mx_mappable  parent;

	mx_struct()
		{ clear(); }
	mx_struct( const mxArray* ms, mwIndex index = 0 )
		{ wrap(ms,index); }


	void clear();
	bool wrap( const mxArray* ms, mwIndex index = 0 );

	inline bool valid() const { return m_mstruct; }
	inline const mxArray* mx() const { return m_mstruct; }

protected:

	const mxArray *m_mstruct;
};

// ------------------------------------------------------------------------

// Create a struct-matrix with specified size and fieldnames
inline mxArray* mx_create_struct( const char *fields[], usize_t nfields, usize_t nrows=1, usize_t ncols=1 )
	{ return mxCreateStructMatrix( nrows, ncols, nfields, (const char**) fields ); }

inline mxArray* mx_create_struct( inilst<const char*> fields, usize_t nrows=1, usize_t ncols=1 )
	{ return mxCreateStructMatrix( nrows, ncols, fields.size(), const_cast<const char**>(fields.begin()) ); }

// Set field in structure
int mx_set_field( mxArray *mxs, usize_t index, const char *field, mxArray *value );

inline int mx_set_field( mxArray *mxs, const char *field, mxArray *value )
	{ return mx_set_field( mxs, 0, field, value ); }

DRAYN_NS_END_
