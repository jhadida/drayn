
//==================================================
// @title        matrix.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * This ties a Mex array with a Drayn nd_array together.
 * Use the get() method to get the Drayn array,
 * and the mx() method to get the pointer to mxArray.
 *
 * See create.h and extract.h for practical features using
 * this data structure.
 */
template <class T, bool managed = false>
class mx_matrix
	: public mx_array_base< T, mx::mat<T>, managed >
{
public:

	CORE_TRAITS(T);

	typedef mx_matrix<T,managed>          self;
	typedef mx_traits<T,managed>          traits;
	typedef mx::mat<T>                    mat_type;
	typedef typename mat_type::col_type   col_type;
	typedef typename mat_type::row_type   row_type;
	typedef typename traits::mx_ptr_type  mxptr_t;

	// ----------  =====  ----------


	mx_matrix() {}
	mx_matrix( mxptr_t *pm ) { assign(pm); }

	// proxy methods
	inline usize_t nrows  () const { return this->m_dr.dim(0); }
	inline usize_t ncols  () const { return this->m_dr.dim(1); }

	inline ref_t operator() ( usize_t i, usize_t j ) const
		{ return this->m_dr(i,j); }


	void swap( self& other )
	{
		this->m_mx.swap( other.m_mx );
		this->m_dr.swap( other.m_dr );
	}

	bool assign( mxptr_t *pm )
	{
		this->m_mx.reset( pm, typename traits::deleter_type() );
		return mx_extract_matrix( pm, this->m_dr );
	}

	inline row_type row( usize_t k ) const { return this->m_dr.row(k); }
	inline col_type col( usize_t k ) const { return this->m_dr.col(k); }

};

DRAYN_NS_END_
