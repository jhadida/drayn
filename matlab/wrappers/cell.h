
//==================================================
// @title        cell.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * Read only wrapper for Mex cell arrays.
 * This discards the dimensions of the cell entirely, ie single subscript indexing only.
 */
class mx_cell
{
public:

	typedef mx_cell	self;

	mx_cell()
		{ clear(); }
	mx_cell( const mxArray *mc )
		{ wrap(mc); }

	inline void clear()
		{ m_mcell = nullptr; }
	inline bool wrap( const mxArray *mc )
	{
		ASSERT_RF( mc && mxIsCell(mc), "[dr.matlab.mx_cell] Not a cell." );
		m_mcell = mc; return true;
	}

	// Dimensions and validity
	inline usize_t   numel () const { return mxGetNumberOfElements(m_mcell); }
	inline bool      empty () const { return numel() == 0; }
	inline bool      valid () const { return m_mcell && mxIsCell(m_mcell); }
	inline operator  bool  () const { return valid() && !empty(); }

	// Linear subscript access
	inline mxArray* operator[] ( usize_t k ) const
		{ return mxGetCell( m_mcell, k ); }

private:
	
	const mxArray *m_mcell;
};

DRAYN_NS_END_
