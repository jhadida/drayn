
//==================================================
// @title        vector.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * This ties a Mex array with a Drayn nd_array together.
 * Use the get() method to get the Drayn array, 
 * and the mx() method to get the pointer to mxArray.
 *
 * See create.h and extract.h for practical features using 
 * this data structure.
 */
template <class T, bool managed = false>
class mx_vector
	: public mx_array_base< T, mx::vec<T>, managed >
{
public:

	CORE_TRAITS(T);

	typedef mx_vector<T,managed>          self;
	typedef mx_traits<T,managed>          traits;
	typedef typename traits::mx_ptr_type  mxptr_t;

	// ----------  =====  ----------
	

	mx_vector() {}
	mx_vector( mxptr_t *pm ) { assign(pm); }

	// proxy methods
	inline usize_t length() const 
		{ return this->m_dr.size(); }

	inline ref_t operator[] ( usize_t k ) const
		{ return this->m_dr[k]; }


	void swap( self& other )
	{
		this->m_mx.swap( other.m_mx );
		this->m_dr.swap( other.m_dr );
	}

	bool assign( mxptr_t *pm )
	{
		this->m_mx.reset( pm, typename traits::deleter_type() );
		return mx_extract_vector( pm, this->m_dr );
	}

};

DRAYN_NS_END_
