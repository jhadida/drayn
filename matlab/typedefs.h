
//==================================================
// @title        typedefs.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cstdint>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * C++11 to Matlab types mapping.
 */

template <class T>
struct mx_type
{
	static const char *name;
	static const mxClassID id;
};

// Explicit instantiation DECLARATION of mx_type

#define DRAYN_DECLARE_MX_TYPE(TYPE)                       \
template <> const char*      mx_type<TYPE>::name       ;  \
template <> const mxClassID  mx_type<TYPE>::id         ;  \
template <> const char*      mx_type<const TYPE>::name ;  \
template <> const mxClassID  mx_type<const TYPE>::id   ;


DRAYN_DECLARE_MX_TYPE( bool     )
DRAYN_DECLARE_MX_TYPE( int8_t   )
DRAYN_DECLARE_MX_TYPE( uint8_t  )
DRAYN_DECLARE_MX_TYPE( int16_t  )
DRAYN_DECLARE_MX_TYPE( uint16_t )
DRAYN_DECLARE_MX_TYPE( int32_t 	)
DRAYN_DECLARE_MX_TYPE( uint32_t )
DRAYN_DECLARE_MX_TYPE( int64_t 	)
DRAYN_DECLARE_MX_TYPE( uint64_t )
DRAYN_DECLARE_MX_TYPE( float    )
DRAYN_DECLARE_MX_TYPE( double   )

// ------------------------------------------------------------------------

/**
 * Matlab to C++11 types mapping.
 */

template <int C>
struct cpp_type
{
	typedef void type;
};

template <> struct cpp_type<mxLOGICAL_CLASS> { typedef bool type; };
template <> struct cpp_type<   mxINT8_CLASS> { typedef int8_t type; };
template <> struct cpp_type<  mxUINT8_CLASS> { typedef uint8_t type; };
template <> struct cpp_type<  mxINT16_CLASS> { typedef int16_t type; };
template <> struct cpp_type< mxUINT16_CLASS> { typedef uint16_t type; };
template <> struct cpp_type<  mxINT32_CLASS> { typedef int32_t type; };
template <> struct cpp_type< mxUINT32_CLASS> { typedef uint32_t type; };
template <> struct cpp_type<  mxINT64_CLASS> { typedef int64_t type; };
template <> struct cpp_type< mxUINT64_CLASS> { typedef uint64_t type; };
template <> struct cpp_type< mxSINGLE_CLASS> { typedef float type; };
template <> struct cpp_type< mxDOUBLE_CLASS> { typedef double type; };

// ------------------------------------------------------------------------

/**
 * Aliases for Mex input/output
 */
namespace mx {

	// Generic types for interfacing Mex and Drayn
	template <class T> using vec = array    < T >;
	template <class T> using mat = matrix   < T, array<T> >;
	template <class T> using vol = volume   < T, array<T> >;
	template <class T> using arr = nd_array < T, array<T> >;

	// Drayn handles for output Mex allocations
	template <class T> using vec_out = vec<T>;
	template <class T> using mat_out = mat<T>;
	template <class T> using vol_out = vol<T>;
	template <class T> using arr_out = arr<T>;

	// Drayn handles for Mex inputs
	template <class T> using vec_in  = array    < const T >;
	template <class T> using mat_in  = matrix   < const T, array<const T> >;
	template <class T> using vol_in  = volume   < const T, array<const T> >;
	template <class T> using arr_in  = nd_array < const T, array<const T> >;
}

DRAYN_NS_END_
