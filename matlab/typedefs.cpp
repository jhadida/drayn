
//==================================================
// @title        typedefs.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

//-------------------------------------------------------------
// Explicit instantiation DEFINITIONS of mx_type
//-------------------------------------------------------------

#define DRAYN_DEFINE_MX_TYPE(TYPE,NAME,ID)                      \
template <> const char*      mx_type<TYPE>::name       = NAME;  \
template <> const mxClassID  mx_type<TYPE>::id         = ID;    \
template <> const char*      mx_type<const TYPE>::name = NAME;  \
template <> const mxClassID  mx_type<const TYPE>::id   = ID;


DRAYN_DEFINE_MX_TYPE( bool,     "logical", mxLOGICAL_CLASS )
DRAYN_DEFINE_MX_TYPE( int8_t,   "int8",    mxINT8_CLASS    )
DRAYN_DEFINE_MX_TYPE( uint8_t,  "uint8",   mxUINT8_CLASS   )
DRAYN_DEFINE_MX_TYPE( int16_t,  "int16",   mxINT16_CLASS   )
DRAYN_DEFINE_MX_TYPE( uint16_t, "uint16",  mxUINT16_CLASS  )
DRAYN_DEFINE_MX_TYPE( int32_t,  "int32",   mxINT32_CLASS   )
DRAYN_DEFINE_MX_TYPE( uint32_t, "uint32",  mxUINT32_CLASS  )
DRAYN_DEFINE_MX_TYPE( int64_t,  "int64",   mxINT64_CLASS   )
DRAYN_DEFINE_MX_TYPE( uint64_t, "uint64",  mxUINT64_CLASS  )
DRAYN_DEFINE_MX_TYPE( float,    "single",  mxSINGLE_CLASS  )
DRAYN_DEFINE_MX_TYPE( double,   "double",  mxDOUBLE_CLASS  )

// ------------------------------------------------------------------------

// Additional overloads for stubborn compilers
#ifdef __APPLE__
	DRAYN_DEFINE_MX_TYPE( unsigned long, "uint64", mxUINT64_CLASS )
#endif

DRAYN_NS_END_
