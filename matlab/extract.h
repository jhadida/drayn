
//==================================================
// @title        extract.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "mex.h"
#include <string>

/**
 * These methods are used mainly by mx_arrays.
 * They should NOT be used with temporary runtime Mex arrays (dynamically
 * allocated, non-output), unless you know what you are doing.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T>
bool mx_extract_scalar( const mxArray *data, T& val )
{
	ASSERT_RF( data, "Null pointer in input." );
	ASSERT_RF( (mxIsNumeric(data) || mxIsLogical(data)) && (mxGetNumberOfElements(data) == 1),
		"Input should be a numeric scalar." );
	INFO_ASSERT( mxGetClassID(data) == mx_type<T>::id,
		"Input/output types mismatch (in: %s, out: %s).",
		mxGetClassName(data), mx_type<T>::name );

	val = static_cast<T>(mxGetScalar(data));
	return true;
}

bool mx_extract_string( const mxArray *data, std::string& val );

// ------------------------------------------------------------------------

// JH Oct 18: using in[k] for invalid k causes segfaults in Mex

// DEPRECATED: kept for convenience only, but will be removed in future versions
// DO NOT USE WITH MEX INPUTS!
template <class T, class U>
bool mx_extract_scalar( const mxArray *data, T& val, const U& default_val )
{
	if ( data )
		return mx_extract_scalar( data, val );

	val = default_val;
	return true;
}

// bool mx_extract_string( const mxArray *data, std::string& val, const std::string& default_val );

// ------------------------------------------------------------------------

template <class T>
bool mx_extract_vector( const mxArray *data, array<T>& vec )
{
	ASSERT_RF( data, "Null pointer in input." );
	ASSERT_RF( mxGetClassID(data) == mx_type<T>::id,
		"Input/output types mismatch (in: %s, out: %s), abort extraction.",
		mxGetClassName(data), mx_type<T>::name );

	ASSERT_RF( mxGetNumberOfDimensions(data) == 2 &&
		(mxGetN(data) == 1 || mxGetM(data) == 1), "Input mxArray is not a vector." );

	vec.assign( static_cast<T*>(mxGetData(data)), mxGetNumberOfElements(data) );
	return true;
}

template <class T>
bool mx_extract_row( const mxArray *data, array<T>& vec )
{
	ASSERT_RF( data, "Null pointer in input." );
	ASSERT_RF( mxGetM(data) == 1, "Input mxArray is not a row." );

	return mx_extract_vector(data,vec);
}

template <class T>
bool mx_extract_col( const mxArray *data, array<T>& vec )
{
	ASSERT_RF( data, "Null pointer in input." );
	ASSERT_RF( mxGetN(data) == 1, "Input mxArray is not a column." );

	return mx_extract_vector(data,vec);
}

// ------------------------------------------------------------------------

template <class Out, class T = typename Out::val_t >
bool mx_extract_array( const mxArray *data, Out& arr )
{
	ASSERT_RF( data, "Null pointer in input." );
	ASSERT_RF( mxGetClassID(data) == mx_type<T>::id,
		"Input/output types mismatch (in: %s, out: %s), abort extraction.",
		mxGetClassName(data), mx_type<T>::name );

	mx_header header(data);
	array<T> container( static_cast<T*>(mxGetData(data)), mxGetNumberOfElements(data) );
	arr.assign( container, nd_dimensions(wrap(header.size)), MemoryLayout::Columns );
	return true;
}

template <class Out>
bool mx_extract_matrix( const mxArray *data, Out& mat )
{
	ASSERT_RF( data, "Null pointer in input." );
	ASSERT_RF( mxGetNumberOfDimensions(data) == 2, "Input mxArray is not a matrix." );

	return mx_extract_array(data,mat);
}

template <class Out>
bool mx_extract_volume( const mxArray *data, Out& vol )
{
	ASSERT_RF( data, "Null pointer in input." );
	ASSERT_RF( mxGetNumberOfDimensions(data) == 3, "Input mxArray is not a volume." );

	return mx_extract_array(data,vol);
}

DRAYN_NS_END_
