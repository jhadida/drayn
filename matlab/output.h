
//==================================================
// @title        output.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <streambuf>
#include <iostream>
#include <cstdio>
#include <memory>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_


/**
 * These classes allow to redirect the standard output std::cout to the Matlab console.
 * They essentially replace the stream buffer of std::cout with a custom buffer using
 * Drayn output functions.
 *
 * To enable the redirection _temporarily_, you just need to instanciate an object of
 * type dr::mx_cout_redirection at the beginning of the scope in your code.
 *
 * If you want to redirect _permanently_ (until the end of execution), you can call
 * dr::mx_cout_redirect() at the beginning of your main. Note that there is no way
 * to restore the standard output after that.
 */

class mx_output_dr_print
	: public std::streambuf
{
protected:

	virtual inline std::streamsize xsputn( const char* s, std::streamsize n )
		{ dr_print("%.*s", n, s); return n; }

	virtual inline int overflow( int c = EOF )
		{ if (c != EOF) dr_print("%.1s", &c); return 1; }
};

// ------------------------------------------------------------------------

template <class B = mx_output_dr_print>
class mx_cout_redirection
{
public:

	typedef B buffer_type;

	mx_cout_redirection()
		{ enable(); }
	~mx_cout_redirection()
		{ disable(); }

protected:

	std::streambuf *m_backup;
	buffer_type m_buf;

	inline void enable()
		{ m_backup = std::cout.rdbuf( &m_buf ); }
	inline void disable()
		{ std::cout.rdbuf( m_backup ); }
};

// ------------------------------------------------------------------------

inline void mx_cout_redirect( bool do_it = true )
{
	static std::unique_ptr<mx_cout_redirection<mx_output_dr_print>> r;

	if ( do_it && !r )
		r.reset( new mx_cout_redirection<mx_output_dr_print>() );

	if ( !do_it )
		r.reset();
}

DRAYN_NS_END_
