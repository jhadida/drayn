
//==================================================
// @title        extract.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

bool mx_extract_string( const mxArray *data, std::string& val )
{
	ASSERT_RF( data, "Null pointer in input." );
	ASSERT_RF( mxIsChar(data), "Input mxArray is not a string." );

	val.resize( mxGetNumberOfElements(data) );
	return mxGetString( data, &val[0], val.size()+1 ) == 0;
}

// bool mx_extract_string( const mxArray *data, std::string& val, const std::string& default_val )
// {
// 	if ( data )
// 		return mx_extract_string( data, val );

// 	val = default_val;
// 	return true;
// }

DRAYN_NS_END_
