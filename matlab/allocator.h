
//==================================================
// @title        allocator.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cstddef>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// Custom tag
struct tag_allocator_matlab_numeric {};

/**
 * Allocator for Mex arrays using Matlab's Mex library.
 * Note that there is no generic allocation function; the type of allocation
 * (matrix, vector, multidimensional array) must be known at compile time (
 * or seleted at runtime). By default, "vectors" are assumed to be rows.
 */
template <class T>
struct allocator_matlab_numeric
{
	CORE_TRAITS(T);
	typedef allocator_matlab_numeric<T> self;
	typedef tag_allocator_matlab_numeric tag;

	static_assert( !std::is_const<T>::value,
		"allocator_matlab_numeric cannot be initialized with constant value-types." );



	static mxArray* allocate_vector( usize_t n ) // default to row-layout
	{
		return allocate_row(n);
	}

	static mxArray* allocate_row( usize_t n )
	{
		ASSERT_RVAL( n > 0, NULL, "Size must be greater than 0." );
		return mxCreateNumericMatrix( 1, (mwSize) n, mx_type<T>::id, mxREAL );
	}

	static mxArray* allocate_col( usize_t n )
	{
		ASSERT_RVAL( n > 0, NULL, "Size must be greater than 0." );
		return mxCreateNumericMatrix( (mwSize) n, 1, mx_type<T>::id, mxREAL );
	}

	// ------------------------------------------------------------------------

	static mxArray* allocate_matrix( usize_t r, usize_t c )
	{
		ASSERT_RVAL( r*c > 0, NULL, "All dimensions must be greater than 0." );
		return mxCreateNumericMatrix( (mwSize) r, (mwSize) c, mx_type<T>::id, mxREAL );
	}

	static mxArray* allocate_volume( usize_t r, usize_t c, usize_t s )
	{
		ASSERT_RVAL( r*c*s > 0, NULL, "All dimensions must be greater than 0." );
		return allocate_array( wrap_inilst<usize_t>({r,c,s}) );
	}

	// TODO: this is dangerous if ptr jumps, pass nd_dimension instead
	static mxArray* allocate_array( const nd_dimensions& size )
	{
		ASSERT_RVAL( size.ndims() > 0, NULL, "Number of dimensions must be greater than 0." );
		ASSERT_RVAL( size.dims().gt(0), NULL, "All dimensions must be greater than 0." );
		return mxCreateNumericArray( (mwSize) size.ndims(), (const mwSize*) &(size.dims()[0]), mx_type<T>::id, mxREAL );
	}

	// ------------------------------------------------------------------------

	static void release( mxArray *ptr )
	{
		if (ptr) mxDestroyArray(ptr);
	}

	struct deleter
	{
		inline void operator() ( mxArray *ptr, usize_t n=0 ) const
			{ self::release(ptr); }
	};
};

DRAYN_NS_END_
