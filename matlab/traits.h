
//==================================================
// @title        traits.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <memory>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * Define traits for extraction.
 *
 * Valid couples are:
 * - T and managed: dynamically allocated non-output Mex array
 * - T and not managed: dynamically allocated Mex output
 * - const T and not managed: Mex input
 */
template <class T, bool managed = false>
struct mx_traits
{
	static_assert( !(managed && std::is_const<T>::value),
		"Only non-const mxArrays can be managed." );

	// Assign a suitable allocator depending on memory management
	typedef typename std::conditional< managed,
		allocator_matlab_numeric<T>, allocator_noalloc<T> >::type alloc_type;

	// Associated deleter for memory management
	typedef typename alloc_type::deleter deleter_type;

	// Const value types cannot come from non-const mxArrays
	typedef typename std::conditional< std::is_const<T>::value,
		const mxArray, mxArray >::type mx_ptr_type;

	// Types used by mx_array
	typedef std::shared_ptr<mx_ptr_type> shared_type; // contains a mxArray*
};

DRAYN_NS_END_
