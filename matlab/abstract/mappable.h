
//==================================================
// @title        mappable.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <deque>
#include <string>
#include <unordered_map>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * This provides a read-only wrapper for Mex mappable data (struct and MAT files).
 *
 * The operator[] is overloaded for convenience, so that instances behave
 * like standard maps. You can use the method has_field to check whether
 * a specific field exists, and has_fields({}) (initializer list of strings)
 * to check for several fields at once.
 */
class mx_mappable
{
public:

	typedef std::unordered_map< std::string, mxArray* >  fieldmap_type;
	typedef std::deque< std::string >                    fields_type;

	// ----------  =====  ----------

	virtual void clear();
	virtual bool valid() const =0;

	// Dimensions / validity
	inline bool       empty() const { return n_fields() == 0; }
	inline usize_t     size() const { return n_fields(); }
	inline usize_t n_fields() const { return m_fields.size(); }
	inline operator    bool() const { return valid(); }

	// Check if field exists
	       bool has_any    ( const inilst<const char*>& names ) const;
	       bool has_fields ( const inilst<const char*>& names ) const;
	inline bool has_field  ( const std::string& name )          const
		{ return m_fmap.find(name) != m_fmap.end(); }

	// Access by index
	inline const std::string& field_name  ( usize_t n ) const { return m_fields.at(n); }
	inline mxArray*           field_value ( usize_t n ) const { return field_value(field_name(n)); }

 	// Access by name (overload necessary to avoid ambiguity)
 	inline mxArray* operator[] ( const std::string& name ) const { return field_value(name); }
	inline mxArray* operator[] ( const char* name )        const { return field_value(name); }

	inline mxArray* field_value( const std::string& name ) const
		{ return has_field(name)? m_fmap.find(name)->second : nullptr; }


protected:

	fieldmap_type  m_fmap;
	fields_type    m_fields;
};

DRAYN_NS_END_
