
//==================================================
// @title        mappable.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

void mx_mappable::clear()
{
	m_fields .clear();
	m_fmap   .clear();
}

// ------------------------------------------------------------------------

bool mx_mappable::has_fields( const inilst<const char*>& names ) const
{
	for ( auto& name: names )
		if ( !has_field(name) )
		{
			DRAYN_INFO( "[dr.matlab.mx_mappable] Field '%s' doesn't exist.", name );
			return false;
		}
	return true;
}

bool mx_mappable::has_any( const inilst<const char*>& names ) const
{
	for ( auto& name: names )
		if ( has_field(name) )
			return true;

	return false;
}

DRAYN_NS_END_
