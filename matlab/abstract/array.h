
//==================================================
// @title        array.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <utility>
#include <type_traits>
#include <memory>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * This ties a Mex array together with a Drayn array handle.
 * Use the get() method to get the Drayn handle, 
 * and the mx () method to get the pointer to mxArray.
 *
 * See create.h and extract.h for practical features using 
 * this data structure.
 */
template <class Value, class Handle, bool managed>
class mx_array_base
{
public:

	CORE_TRAITS( Value );

	typedef typename mx_traits<Value,managed>::mx_ptr_type   mxptr_t;
	typedef typename mx_traits<Value,managed>::shared_type   shared_t;
	typedef typename mx_traits<Value,managed>::deleter_type  deleter_t;
	typedef Handle                                           handle_t;

	inline void clear()
		{ m_mx.reset(); m_dr.clear(); }

	// Validity
	inline bool     valid() const { return m_mx && m_dr.valid(); }
	inline bool     empty() const { return m_dr.empty(); }
	inline operator bool () const { return valid() && !empty(); }

	// Assignment
	virtual bool assign ( mxptr_t *pm ) =0;
	inline  bool wrap   ( mxptr_t *pm ) { return assign(pm); }

	// Accessors
	inline       handle_t&  get()       { return m_dr; }
	inline const handle_t&  get() const { return m_dr; }
	inline mxptr_t*         mx () const { return m_mx.get(); }

	// Header information about underlying Mex object
	inline mx_header header() const { return mx_header(mx()); }

protected:

	shared_t m_mx;
	handle_t m_dr;
};

DRAYN_NS_END_
