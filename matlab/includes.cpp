
//==================================================
// @title        includes.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



#ifdef DRAYN_USING_MATLAB

#include "typedefs.cpp"
#include "extract.cpp"

#include "abstract/mappable.cpp"
#include "wrappers/header.cpp"
#include "wrappers/struct.cpp"
#include "wrappers/MAT.cpp"

#endif
