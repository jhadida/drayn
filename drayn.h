#ifndef DRAYN_H_INCLUDED
#define DRAYN_H_INCLUDED

//==================================================
// @title        drayn.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#define DRAYN_NS_START_ namespace dr {
#define DRAYN_NS_END_   } // Drayn namespace

// ----------  =====  ----------

#ifndef DRAYN_MESSAGE_BUFFER
#define DRAYN_MESSAGE_BUFFER 4095
#endif
#ifndef DRAYN_TIMER_FORMAT_BUFFER
#define DRAYN_TIMER_FORMAT_BUFFER 255
#endif
#ifndef DRAYN_TIMER_KEY_MAXSIZE
#define DRAYN_TIMER_KEY_MAXSIZE 255
#endif
#ifndef DRAYN_OUTPUT_INTEGER_WIDTH
#define DRAYN_OUTPUT_INTEGER_WIDTH 4
#endif
#ifndef DRAYN_OUTPUT_FLOAT_WIDTH
#define DRAYN_OUTPUT_FLOAT_WIDTH 9
#endif
#ifndef DRAYN_OUTPUT_FLOAT_PRECISION
#define DRAYN_OUTPUT_FLOAT_PRECISION 3
#endif
#ifndef DRAYN_MEMORY_CHUNK_SIZE
#define DRAYN_MEMORY_CHUNK_SIZE 100
#endif

// Used with random generators
// #define DRAYN_DETERMINISTIC_RANDOM_SEED  123
// #define DRAYN_FIXED_RANDOM_SEED          123

/**
 * OTHER AVAILABLE FLAGS:
 *
 * DRAYN_DEBUG_MODE      : activate debug tests and messages
 * DRAYN_SHOW_INFO       : activate info tests and messages
 * DRAYN_PROTECT_INDICES : protects against index overflows (slower)
 *
 * DRAYN_USE_64BITS_SIZE : force 64 bits indices (default: 32 bits)
 * DRAYN_USE_32BITS_WORD : force 32 bits words (default: 64 bits)
 *
 * DRAYN_USING_ARMADILLO : enable the Armadillo interface
 * DRAYN_USING_MATLAB    : enable the Matlab/Mex interface
 */

// ------------------------------------------------------------------------

/**
 * IMPORTANT NOTE:
 * When compiling with Matlab, make sure you link to libut.so by adding the
 * flag "-lut" to the gcc/clang command.
 */
#ifdef DRAYN_USING_MATLAB
	#include "mex.h"
	#include "mat.h"
	#include "matrix.h"
#endif

// ------------------------------------------------------------------------

#include "core/includes.h"
#include "containers/includes.h"
#include "algorithms/includes.h"

// External interfaces
#include "matlab/includes.h"
#include "arma/includes.h"

#endif
