[![License: MPL 2.0](https://img.shields.io/badge/License-MPL%202.0-brightgreen.svg)](https://opensource.org/licenses/MPL-2.0)

# drayn

General-purpose C++11 library.
Main features:

- wrappers for random number generation
- high resolution timers
- events handlers, publish/subscribe
- numerical functions / predicates
- custom memory allocators
- flexible sequential and multi-dimensional containers (contiguous or not, shared, or on stack)
- basic arithmetics and iteration capability for all containers
- basic algorithms (stats, metrics)
- wrappers for compatibility with Armadillo (sources included)
- wrappers for compatibility with Matlab Mex
