#ifndef DRAYN_EXTERNAL_H_INCLUDED
#define DRAYN_EXTERNAL_H_INCLUDED

//==================================================
// @title        external
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#ifdef DRAYN_USE_ARMA
    #include "arma.h"
#endif
#ifdef DRAYN_USE_JMX
    #include "matlab.h"
#endif

#endif
