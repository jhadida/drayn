
//==================================================
// @title        jmx.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include "jmx.h"

/*
 * Import array containers from the JMX library.
 * See: https://jhadida.gitlab.io/deck/#/jmx/index
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class T>
class JMX_Wrapper: public _sequence< JMX_Wrapper<T>, T >
{
public:

    DRAYN_TRAITS(T)
    using value_type = T;
    using memory_type = jmx::AbstractMemory<T>;

    JMX_Wrapper()
        : m_mem(nullptr) {}

    template <class C>
    JMX_Wrapper( const C& ctn )
        : m_mem(nullptr)
        { assign( ctn ); }

    inline void clear() { m_mem = nullptr; }

    template <class C>
    inline void assign( const C& ctn ) { m_mem = &ctn.mem; }

    inline bool valid() const { return m_mem; }
    inline uidx_t size() const { return valid() ? m_mem->size : 0; }
    inline ref_t operator[] ( uidx_t k ) const { return (*m_mem)[k]; }

private:

    const memory_type *m_mem;
};

// ------------------------------------------------------------------------

template <class M>
using _jmx_wrapper = JMX_Wrapper< typename M::value_type >;

template <class T, class M>
_jmx_wrapper<M> jmx_import( const jmx::Vector<T,M>& x ) {
    return _jmx_wrapper<M>(x);
}

template <class T, class M>
matrix< _jmx_wrapper<M> > jmx_import( const jmx::Matrix<T,M>& x ) {
    return matrix< _jmx_wrapper<M> >( _jmx_wrapper<M>(x), x.nrows(), x.ncols() );
}

template <class T, class M>
volume< _jmx_wrapper<M> > jmx_import( const jmx::Volume<T,M>& x ) {
    return volume< _jmx_wrapper<M> >( _jmx_wrapper<M>(x), x.nrows(), x.ncols(), x.nslabs() );
}

DRAYN_NS_END
