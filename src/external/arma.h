
//==================================================
// @title        arma.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include "armadillo"
#include <type_traits>

/*
 * Issues:
 *
 *  Locking container size (i.e. setting mem_state=2) also declares the 
 *  memory as auxiliary, which means it needs to be managed. However,
 *  memory management in arma is complicated, because of preallocation
 *  (no dynamic allocation below a certain size).
 *
 *  As a result, all imported containers are "fragile"; if the original
 *  container changes size or goes out of scope, we are left with a 
 *  dangling reference.
 *
 *  The value-type of arma container is called elem_type, and it cannot 
 *  be const. Calling memptr() on a const instance yields a pointer to 
 *  const.
 *  
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Copied from Cube_bones.hpp
// 
// mem_state = 0: normal container which manages its own memory
// mem_state = 1: use auxiliary memory until a size change
// mem_state = 2: use auxiliary memory and don't allow the number of elements to be changed
// mem_state = 3: fixed size (eg. via template based size specification)

// ------------------------------------------------------------------------

template <class T>
pvec<T> arma_import( const arma::Row<T>& x ) {
    return pvec<T>( const_cast<T*>(x.memptr()), x.n_elem );
}

template <class T>
pvec<T> arma_import( const arma::Col<T>& x ) {
    return pvec<T>( const_cast<T*>(x.memptr()), x.n_elem );
}

template <class T>
pmat<T> arma_import( const arma::Mat<T>& x ) {
    return pmat<T>(
        pvec<T>( const_cast<T*>(x.memptr()), x.n_elem ),
        x.n_rows, x.n_cols, layout::Cols
    );
}

template <class T>
pvol<T> arma_import( const arma::Cube<T>& x ) {
    return pvol<T>(
        pvec<T>( const_cast<T*>(x.memptr()), x.n_elem ),
        x.n_rows, x.n_cols, x.n_slices, layout::Cols
    );
}

// ------------------------------------------------------------------------

template <class T, class A>
bool _arma_contiguous( const shared<T,A>& x ) { return true; }

template <class T>
bool _arma_contiguous( const pointer<T>& x ) { return x.step()==1; }

template <class C>
bool _arma_contiguous( const C& x ) { 
    const auto& idx = x.index();
    const uidx_t D = x.ndims();

    uidx_t k = 0;
    for ( uidx_t n=1; k<D && idx.step(k)==n; n*=idx.size(k++) ) {}

    return k==D && _arma_contiguous(x.data()); 
}

// ------------------------------------------------------------------------

template <class T> using _arma_val = typename std::remove_const<T>::type;
template <class T> using _arma_ptr = typename std::add_pointer<_arma_val<T>>::type;

template <class T, class C>
constexpr _arma_ptr<T> _arma_cast( C *x ) 
    { return const_cast<_arma_ptr<T>>(x); }

template <class T> using _arma_row = arma::Row< _arma_val<T> >;
template <class T> using _arma_col = arma::Row< _arma_val<T> >;
template <class T> using _arma_mat = arma::Mat< _arma_val<T> >;
template <class T> using _arma_vol = arma::Cube< _arma_val<T> >;

template <class T>
_arma_row<T> arma_row( const pointer<T>& x, bool copy=false, bool lock=true ) {
    DRAYN_ASSERT_ERR( _arma_contiguous(x), "Input pointer is not contiguous." )
    return _arma_row<T>( _arma_cast<T>(x.memptr()), x.size(), copy, lock );
}

template <class T, class A>
_arma_row<T> arma_row( const shared<T,A>& x, bool copy=false, bool lock=true ) {
    DRAYN_ASSERT_ERR( _arma_contiguous(x), "Input pointer is not contiguous." )
    return _arma_row<T>( _arma_cast<T>(x.memptr()), x.size(), copy, lock );
}

template <class T>
_arma_col<T> arma_col( const pointer<T>& x, bool copy=false, bool lock=true ) {
    DRAYN_ASSERT_ERR( _arma_contiguous(x), "Input pointer is not contiguous." )
    return _arma_col<T>( _arma_cast<T>(x.memptr()), x.size(), copy, lock );
}

template <class T, class A>
_arma_col<T> arma_col( const shared<T,A>& x, bool copy=false, bool lock=true ) {
    DRAYN_ASSERT_ERR( _arma_contiguous(x), "Input pointer is not contiguous." )
    return _arma_col<T>( _arma_cast<T>(x.memptr()), x.size(), copy, lock );
}

template <class T, class C>
_arma_mat<T> arma_mat( const matrix<C,T>& M, bool copy=false, bool lock=true ) {
    DRAYN_ASSERT_ERR( _arma_contiguous(M), "Input matrix is not contiguous." )
    return _arma_mat<T>( _arma_cast<T>(M.data().memptr()), M.nrows(), M.ncols(), copy, lock );
}

template <class T, class C>
_arma_vol<T> arma_vol( const volume<C,T>& V, bool copy=false, bool lock=true ) {
    DRAYN_ASSERT_ERR( _arma_contiguous(V), "Input volume is not contiguous." )
    return _arma_vol<T>( _arma_cast<T>(V.data().memptr()), V.nrows(), V.ncols(), copy, lock );
}

DRAYN_NS_END
