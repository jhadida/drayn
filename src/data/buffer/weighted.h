
//==================================================
// @title        weighted.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iostream>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

/**
 * Circular buffer implementation.
 * Buffer capacity corresponds to initial weights length.
 * Call method next() to get the next state. 
 * Buffer initially grows from empty to full capacity (variable m_length), then overwrites oldest states.
 * Use methods chansum / statesum to convolve weights with channels.
 */

template <class T>
class buf_weighted: public _indexed<T>
{
public:

    DRAYN_TRAITS(T)
    using self = buf_weighted<T>;
    using value_type = T;
    using state_type = typename smat<T>::unsafe_type;

    buf_weighted() { clear(); }

    template <class C>
    buf_weighted( const C& w, uidx_t nc )
        { init(w,nc); }

    // cleanup instance
    void clear() {
        m_last = m_length = 0;
        m_weight.clear();
    }

    // initialise from any container
    template <class C>
    self& init( const C& w, uidx_t nc ) {
        DRAYN_ASSERT_ERR( w.size() > 0, "Bad input weights." )
        DRAYN_ASSERT_ERR( nc > 0, "Null number of states." )
        DRAYN_ASSERT( is_close<T>( alg_sum(w), 1 ), "Window is not normalised." )
        m_weight.copy(w);
        m_data = make_mat<T>(w.size(),nc);
        m_last = m_length = 0;
        return *this;
    }

    // access data
    inline ref_t value( uidx_t r, uidx_t c ) const
        { return m_data( (m_last+1+r) % m_length, c ); }
    inline ref_t weight( uidx_t k ) const 
        { return m_weight[k]; }

    inline ref_t operator[] (uidx_t k) const 
        { return weight(k); }
    inline ref_t operator() (uidx_t r, uidx_t c) const 
        { return value(r,c); }

    // indexed interface
    inline uidx_t size() const { return m_weight.size(); }
    inline uidx_t nchan() const { return m_data.ncols(); }

    inline bool valid() const 
        { return m_length > 0 
            && m_length == m_data.nrows() 
            && m_length == m_weight.size(); }

    // circular buffer
    state_type next() {
        state_type s;
        if ( m_length < size() ) {
            s = m_data.unsafe_row(m_length++);
        } else {
            s = m_data.unsafe_row(m_last);
            m_last = (m_last + 1) % m_length;
        }
        return s;
    }

    // compute weighted sum
    template <class C>
    void statesum( const C& vec ) const {
        DRAYN_ASSERT_ERR( vec.size() == nchan(), "Bad input size." )        
        
        for ( uidx_t c=0; c < nchan(); c++ )
            vec[c] = chansum(c);
    }

    T chansum( uidx_t c ) const {
        DRAYN_ASSERT_ERR( valid(), "Container is not properly initialised." )
        DRAYN_ASSERT_ERR( c < nchan(), "Bad channel index." )

        T ret = 0;
        for ( uidx_t r=0; r < size(); r++ )
            ret += m_weight[r] * value(r,c); 

        return ret;
    }

protected:

    uidx_t m_last, m_length;
    shared<T> m_weight;
    smat<T> m_data;
};

// ------------------------------------------------------------------------

template <class T>
std::ostream& operator<< ( std::ostream& os, const buf_weighted<T>& buf ) 
{
    DRAYN_ASSERT_ERR( buf.valid(), "Invalid buffer in input." )
    const auto on = onstream<T>();
    const uidx_t nr = buf.size();
    const uidx_t nc = buf.nchan();
    const uidx_t p = op_ndigits(nr);

    os << "[WeightedBuffer " << nr << "x" << nc << "]:" << std::endl;
    for ( uidx_t k=0; k < nr; ++k ) {
        os << "\n  [" << std::setw(p) << k << "]\t";
        on(os) << buf.weight(k) << "\t";
        for ( uidx_t c=0; c < nc; ++c ) 
            on(os) << buf.value(k,c) << ", ";
        os << "\b\b ";
    }
    return os << std::endl;
}

DRAYN_NS_END
