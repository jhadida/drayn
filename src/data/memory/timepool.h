
//==================================================
// @title        timepool.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>
#include <deque>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Growing container for time-series data

// NOTE: 
// use lag(true) to discount current point from size()
// this can be useful for interpolation

template <class Value, class Time=double>
class timepool: public _timeseries<Value,Time>
{
public:

    static_assert( !std::is_const<Value>::value && !std::is_const<Time>::value, 
        "Const template types are prohibited for time-pools." );

    using self = timepool<Value,Time>;

    using time_type = Time;
    using value_type = Value;

    using time_sub = shared<time_type>;
    using data_sub = shared<value_type>;

    using block_type = timeseries<data_sub,time_sub>;
    using state_type = typename block_type::ustate_type;
    using data_mat = typename block_type::data_ctn;

    using time_ctn = std::deque<time_sub>;
    using data_ctn = std::deque<data_mat>;
    
    // ----------  =====  ----------
    
    // public event
    using signal_type = signal<self>;
    using slot_type = slot<self>;

    mutable signal_type BlockAdded;
    mutable signal_type BlockRemoved;
    
    // ----------  =====  ----------
    
    timepool() 
        { clear(); }
    timepool( uidx_t nc, uidx_t bs, bool fixed=false )
        { init(nc,bs,fixed); }

    void clear() {
        m_nchan = m_bsize = m_index  = 0;
        m_fixed = m_lag = false;
        m_time.clear();
        m_data.clear();
    }

    self& init( uidx_t nc, uidx_t bs, bool fixed=false ) {
        DRAYN_ASSERT_ERR( nc*bs > 0, "Null size." )
        m_nchan = nc;
        m_bsize = bs;
        m_index = 0;
        m_fixed = fixed;

        m_time.clear();
        m_data.clear();
        return append();
    }

    // ----------  =====  ----------

    inline bool is_fixed_step() const { return m_fixed; }
    inline uidx_t nchan() const { return m_nchan; }
    inline uidx_t ntime() const { 
        if ( m_lag ) return m_index;
        return capacity() ? (m_index+1) : 0; 
    }

    // override parent's bool() operator
    inline operator bool() const { return nblock() > 0 && nchan() > 0; }
    
    inline uidx_t capacity() const { return m_bsize * m_time.size(); }
    inline uidx_t nblock() const { return m_time.size(); }
    inline uidx_t bsize() const { return m_bsize; }
    inline uidx_t index() const { return m_index; }
    
    inline uidx_t ind2block( uidx_t k ) const { return k/m_bsize; }

    // ----------  =====  ----------
    
    // access by state index
    inline time_type& time( uidx_t i ) const 
        { return m_time[i/m_bsize][i%m_bsize]; }

    inline value_type& value( uidx_t i, uidx_t j ) const  
        { return m_data[i/m_bsize](i%m_bsize,j); }

    inline state_type state( uidx_t i ) const 
        { return m_data[i/m_bsize].unsafe_row(i%m_bsize); }

    inline block_type block( uidx_t i ) const 
        { return block_type( m_time[i], m_data[i] ); }

    // no-index means current state
    inline time_type& time() const { return time(m_index); }
    inline value_type& value( uidx_t j ) const { return value(m_index,j); }
    inline state_type state() const { return state(m_index); }
    inline block_type block() const { return block(m_index); }

    // assign current state
    template <class C>
    inline void assign( uidx_t i, const C& x ) const { state(i).copy(x); }

    template <class C>
    inline void assign( const C& x ) const { state().copy(x); }

    // ----------  =====  ----------
    
    inline void lag( bool L ) { m_lag = L; }
    inline void fixed( bool f ) { m_fixed = f; } // allow dynamic changes
    inline void increment() { if ( ++m_index >= capacity() ) append(); }
    inline void decrement() { if ( m_index > 0 ) --m_index; }

    self& pop() { 
        DRAYN_ASSERT_ERR( nblock() > 1, "Cannot pop an active block." ) 
        m_time.pop_front();
        m_data.pop_front();
        m_index -= m_bsize;

        DRAYN_DEBUG( "[drayn.timepool.pop] Block removed: %d block(s), capacity %d.", nblock(), capacity() )
        BlockRemoved.publish(*this);
        return *this;
    }

protected:

    self& append() {
        m_time.emplace_back(m_bsize);
        m_data.emplace_back(data_sub(m_bsize*m_nchan), m_bsize, m_nchan, layout::Rows);
        
        DRAYN_DEBUG( "[drayn.timepool.append] Block added: %d block(s), capacity %d.", nblock(), capacity() )
        BlockAdded.publish(*this);
        return *this;
    }

    bool m_fixed, m_lag;
    uidx_t m_nchan, m_bsize, m_index;
    time_ctn m_time;
    data_ctn m_data;
};

DRAYN_NS_END
