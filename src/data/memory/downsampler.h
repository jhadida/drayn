
//==================================================
// @title        downsampler.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START


template <class Value, class Time=double>
class downsampler
{
public:

    using self = downsampler<Value,Time>;

    using pool_type = timepool<Value,Time>;
    using time_type = Time;
    using value_type = Value;
    using slot_type = typename pool_type::slot_type;
    
    // ----------  =====  ----------
    
    downsampler() { clear(); }

    template <class W>
    downsampler( pool_type& src, time_type tini, time_type step, 
        const W& win, bool consume=false, time_type lag=0, uidx_t bs=100 ) 
        { init(src,tini,step,consume,lag,bs); }

    void clear() {
        m_tini = m_step = m_lag = 0;
        m_consume = false;
        m_slot.reset();
        m_pool.clear();
    }

    inline operator bool() const 
        { return m_slot && m_slot->valid() && m_step > 0; }

    inline const pool_type& pool() const 
        { return m_pool; }
    
    void init( pool_type& src, time_type tini, time_type step, 
        bool consume=false, time_type lag=0, uidx_t bs=100 );

    const pool_type& finalise( pool_type& in );

protected:

    void update( pool_type& in );

    inline time_type time( uidx_t i ) const 
        { return fma(i,m_step,m_tini); }

    void assign( const pool_type& in, time_type t ) {
        m_pool.time() = t;
        interp_pchip_state( in, t, m_pool.state(), in.is_fixed_step() );
        m_pool.increment();
    }

    bool m_consume;
    buf_weighted<V> m_buf;
    
    pool_type m_pool;
    slot_type m_slot;
    time_type m_tini, m_step, m_lag;
};

DRAYN_NS_END
