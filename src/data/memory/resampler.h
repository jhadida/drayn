
//==================================================
// @title        resampler.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Value, class Time=double>
class resampler
{
public:

    using self = resampler<Value,Time>;

    using pool_type = timepool<Value,Time>;
    using time_type = Time;
    using value_type = Value;
    using slot_type = typename pool_type::slot_type;
    
    // ----------  =====  ----------
    
    resampler() { clear(); }

    resampler( pool_type& src, time_type tini, time_type step, 
        bool consume=false, time_type lag=0, uidx_t bs=100 ) 
        { init(src,tini,step,consume,lag,bs); }

    void clear() {
        m_tini = m_step = m_lag = 0;
        m_consume = false;
        m_slot.reset();
        m_pool.clear();
    }

    inline operator bool() const 
        { return m_slot && m_slot->valid() && m_step > 0; }

    inline const pool_type& pool() const 
        { return m_pool; }
    
    void init( pool_type& src, time_type tini, time_type step, 
        bool consume=false, time_type lag=0, uidx_t bs=100 );

    const pool_type& finalise( pool_type& in );

protected:

    void update( pool_type& in );

    inline time_type time( uidx_t i ) const 
        { return fma(i,m_step,m_tini); }

    void assign( const pool_type& in, time_type t ) {
        m_pool.time() = t;
        interp_pchip_state( in, t, m_pool.state(), in.is_fixed_step() );
        m_pool.increment();
    }

    bool m_consume;
    pool_type m_pool;
    slot_type m_slot;
    time_type m_tini, m_step, m_lag;
};



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



template <class V, class T>
void resampler<V,T>::init( 
    pool_type& src, time_type tini, time_type step, 
    bool consume, time_type lag, uidx_t bs ) 
{
    DRAYN_ASSERT_ERR( lag >= 0, "Lag should be non-negative." )
    DRAYN_ASSERT_ERR( step > c_num<time_type>::eps, "Step is too small." )

    m_lag = lag;
    m_step = step;
    m_consume = consume;

    // initialise pool
    m_pool.init( src.nchan(), bs, true );
    m_pool.lag(true);
    update(src);

    // subscribe to new block events
    m_slot = src.BlockAdded.subscribe(
        [ this ]( pool_type& tp ){ this->update(tp); }
    );
}

// ------------------------------------------------------------------------

template <class V, class T>
void resampler<V,T>::update( pool_type& in )
{
    if ( in.empty() ) return;
    
    DRAYN_DEBUG( "[drayn.resampler:update] Input timpool: %d timepoint(s), %d block(s), timeframe [%g,%g].",
        in.ntime(), in.nblock(), in.tstart(), in.tend() )

    for ( uidx_t n = m_pool.index(); !in.empty() && time(n)+m_lag < in.tend(); n++ ) 
        assign( in, time(n) );

    if ( m_consume && !m_pool.empty() ) 
    for ( uidx_t b = in.ind2block( upper_bound(in,m_pool.tend()) ); b > 1; b-- ) 
        in.pop();
}

// ------------------------------------------------------------------------

template <class V, class T>
auto resampler<V,T>::finalise( pool_type& in ) -> const pool_type&
{
    if ( in.empty() ) return m_pool;

    DRAYN_DEBUG( "[drayn.resampler:finalise] Input timpool: %d timepoint(s), %d block(s), timeframe [%g,%g].",
        in.ntime(), in.nblock(), in.tstart(), in.tend() )

    for ( uidx_t n = m_pool.index(); !in.empty() && time(n) <= in.tend(); n++ ) 
        assign( in, time(n) );

    return m_pool;
}

DRAYN_NS_END
