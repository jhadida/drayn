
//==================================================
// @title        resample.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Resampling to specified rate using PCHIP interpolation.

template <class TS, 
    class V=typename TS::mvalue_type, 
    class T=typename TS::mtime_type >
ts_shr<V,T> resample_ts( const TS& in, T step, T burn=0 )
{
    DRAYN_ASSERT_ERR( 0 <= burn && burn <= in.tspan(), "Bad burn-in." )
    auto out = make_ts<V,T>( in.nchan(), in.tstart()+burn, in.tend(), step );
    
    const uidx_t nt = out.ntime();
    for ( uidx_t i=0; i < nt; i++ )
        interp_pchip_state( in, out[i], out.state_unsafe(i), in.is_fixed_step() );

    return out;
}

template <class TC, 
    class V=typename TC::mvalue_type, 
    class T=typename TC::mtime_type >
tc_shr<V,T> resample_tc( const TC& in, T step, T burn=0 )
{
    DRAYN_ASSERT_ERR( 0 <= burn && burn <= in.tspan(), "Bad burn-in." )
    DRAYN_ASSERT_ERR( in.nchan()==1, "Input is not a timecourse." )
    auto out = make_tc<V,T>( in.tstart()+burn, in.tend(), step );
    
    const uidx_t nt = out.ntime();
    for ( uidx_t i=0; i < nt; i++ )
        out(i) = in.pinterp(out[i]);

    return out;
}

DRAYN_NS_END
