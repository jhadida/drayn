
//==================================================
// @title        timeseries.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Vctn, class Tctn,
    class Value=typename Vctn::value_type,
    class Time=typename Tctn::value_type>
class timeseries: public _timeseries<Value,Time>
{
public:

    using self = timeseries< Vctn, Tctn, Value, Time >;

    using time_type = Time;
    using value_type = Value;

    using mtime_type = typename std::remove_const<Time>::type;
    using mvalue_type = typename std::remove_const<Value>::type;

    using time_ctn = Tctn;
    using data_ctn = matrix< Vctn, value_type >;

    using state_type = typename data_ctn::vector_type;
    using ustate_type = typename data_ctn::unsafe_type;

    using chan_type = timecourse< state_type, time_ctn, value_type, time_type >;
    using uchan_type = timecourse< ustate_type, time_ctn, value_type, time_type >;
    
    // ----------  =====  ----------

    timeseries(): m_fixed(false) {}

    timeseries( const time_ctn& t, const data_ctn& d )
        { assign(t,d); }
    timeseries( const time_ctn& t, const data_ctn& d, bool fixed )
        { assign(t,d,fixed); }


    self& assign( const time_ctn& t, const data_ctn& d ) {
        bool fixed = check_fixed_step<mtime_type>(t);
        DRAYN_IASSERT( fixed,
            "[drayn.timeseries] Time-series has a variable time-step, "
            "consider resampling to accelerate interpolation."
        );
        return assign( t, d, fixed );
    }

    self& assign( const time_ctn& t, const data_ctn& d, bool fixed ) {
        const uidx_t nt = t.size();
        const uidx_t nd = d.nrows();
        DRAYN_ASSERT_ERR( nt*nd > 0 && nd==nt, "Input size mismatch." )

        m_time = t;
        m_data = d;
        m_fixed = fixed;

        return *this;
    }
    
    inline chan_type channel( uidx_t i ) const 
        { return chan_type( m_time, m_data.col(i), m_fixed ); }
    inline uchan_type channel_unsafe( uidx_t i ) const 
        { return uchan_type( m_time, m_data.unsafe_col(i), m_fixed ); }

    inline state_type state( uidx_t i ) const 
        { return m_data.row(i); }
    inline ustate_type state_unsafe( uidx_t i ) const 
        { return m_data.unsafe_row(i); }

    inline const time_ctn& time() const { return m_time; }
    inline const data_ctn& data() const { return m_data; }

    // ----------  =====  ----------

    inline bool is_fixed_step() const { return m_fixed; }
    inline uidx_t ntime() const { return m_time.size(); }
    inline uidx_t nchan() const { return m_data.ncols(); }

    inline time_type& time( uidx_t i ) const { return m_time[i]; }
    inline value_type& value( uidx_t i, uidx_t j ) const  { return m_data(i,j); }
    
    // ----------  =====  ----------
    
    template <class C>
    inline void pinterp_state( mtime_type t, const C& x ) const
        { interp_pchip_state( *this, t, x, is_fixed_step() ); }

    template <class C>
    inline void linterp_state( mtime_type t, const C& x ) const
        { interp_linear_state( *this, t, x, is_fixed_step() ); }

    template <class C>
    inline void nearest_state( mtime_type t, const C& x ) const
        { ::drayn::nearest_state( *this, t, x, is_fixed_step() ); }

protected:

    bool m_fixed;
    time_ctn m_time;
    data_ctn m_data;
};

DRAYN_NS_END
