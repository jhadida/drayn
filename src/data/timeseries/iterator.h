
//==================================================
// @title        iterator.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Value, class Time> struct _timeseries; // forward declaration

template <class Value, class Time>
class timeseries_iterator: public _indexed<Value>
{
public:

    using self = timeseries_iterator<Value,Time>;
    using time_type = Time;
    using value_type = Value;
    DRAYN_TRAITS(value_type)

    using ts_type = _timeseries<Value,Time>;
    using state_type = wrapper<self>;
    
    // ----------  =====  ----------
    
    timeseries_iterator( const ts_type& ts )
        : m_ts(&ts), m_ind(0), m_state(this) {}

    inline bool valid() const { return m_ts; }
    inline uidx_t size() const { return m_ts->nchan(); }

    inline uidx_t niter() const { return m_ts->ntime(); }
    inline uidx_t index() const { return m_ind; }
    inline operator bool() const { return valid() && m_ind < niter(); }

    inline time_type& time() const { return (*m_ts)[m_ind]; }
    inline value_type& chan(uidx_t j) const { return (*m_ts)(m_ind,j); }
    inline ref_t operator[] (uidx_t j) const { return chan(j); }
    
    inline const state_type& state() const { return m_state; }

    // ----------  =====  ----------
    
    self& advance(sidx_t n) {
        if ( n > static_cast<sidx_t>(niter()-m_ind) || -n > static_cast<sidx_t>(m_ind) )
            throw std::out_of_range("[drayn.timeseries_iterator] Invalid increment.");
        m_ind += n;
        return *this;
    }

    self& index(uidx_t i) {
        checkIndex( i, niter() );
        m_ind = i;
        return *this;
    }

    inline self& next() { return advance( 1); }
    inline self& prev() { return advance(-1); }

protected:

    const state_type m_state;
    const ts_type *m_ts;
    uidx_t m_ind;
};

DRAYN_NS_END
