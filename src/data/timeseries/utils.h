
//==================================================
// @title        utils.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// common types
template <class V, class T = double> using ts_shr = timeseries< shared<V>, shared<T> >;
template <class V, class T = double> using ts_ptr = timeseries< pointer<V>, pointer<T> >;

template <class V, class T = double> using tc_shr = timecourse< shared<V>, shared<T> >;
template <class V, class T = double> using tc_ptr = timecourse< pointer<V>, pointer<T> >;

// ------------------------------------------------------------------------

template <class V, class T = double>
ts_shr<V,T> make_ts( uidx_t nchan, T tstart, T tend, T step )
{
    auto t = linstep<T>( tstart, tend, step );
    auto d = make_mat<V>( t.size(), nchan, layout::Cols );
    return ts_shr<V,T>(t,d,true);
}

template <class V, class T = double>
tc_shr<V,T> make_tc( T tstart, T tend, T step )
{
    auto t = linstep<T>( tstart, tend, step );
    auto d = svec<V>( t.size() );
    return tc_shr<V,T>(t,d,true);
}

// ------------------------------------------------------------------------

template <class V, class T = double>
ts_shr<V,T> make_ts( uidx_t nchan, T len, T fs )
    { return make_ts<V,T>( nchan, 0, len, 1/fs ); }

template <class V, class T = double>
tc_shr<V,T> make_tc( T len, T fs )
    { return make_tc<V,T>( 0, len, 1/fs ); }

DRAYN_NS_END
