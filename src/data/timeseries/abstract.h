
//==================================================
// @title        abstract.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>
#include <iostream>
#include <iomanip>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// utility function to determine whether input sequence has fixed step
template <class T, class S>
bool check_fixed_step( const S& seq, T fac = T(1e5) )
{
    const uidx_t n = seq.size();
    if ( n <= 1 ) return true;

    const T h = seq[1]-seq[0];
    for ( uidx_t i=2; i < n; ++i )
        if ( op_abs(h - seq[i]+seq[i-1]) > h/fac )
            return false;

    return true;
}

// ------------------------------------------------------------------------

template <class Value, class Time>
struct _timeseries
{
    using time_type = Time;
    using value_type = Value;
    using mtime_type = typename std::remove_const<Time>::type;
    using mvalue_type = typename std::remove_const<Value>::type;
    using iterator_type = timeseries_iterator<Value,Time>;

    virtual bool is_fixed_step() const =0;

    // dimensions
    virtual uidx_t ntime() const =0;
    virtual uidx_t nchan() const =0;
    
    // data access
    virtual time_type& time( uidx_t i ) const =0;
    virtual value_type& value( uidx_t i, uidx_t j ) const =0;

    // dependent
    inline time_type& operator[] ( uidx_t i ) const { return time(i); }
    inline value_type& operator() ( uidx_t i, uidx_t j ) const { return value(i,j); }

    inline uidx_t size() const { return ntime(); }
    inline uidx_t ndims() const { return nchan(); }
    inline uidx_t numel() const { return ntime()*nchan(); }

    inline bool empty() const { return numel()==0; }
    virtual operator bool() const { return ntime() > 1 && nchan() > 0; }

    inline mtime_type tstart() const { 
        DRAYN_ASSERT_ERR( ntime() > 0, "Empty time-series." )
        return time(0); 
    }
    inline mtime_type tend() const { 
        DRAYN_ASSERT_ERR( ntime() > 0, "Empty time-series." )
        return time(ntime()-1); 
    }

    inline mtime_type tspan() const { return tend() - tstart(); }
    inline mtime_type tstep() const { return tspan() / (ntime()-1); }

    // interpolation
    inline mvalue_type pinterp( mtime_type t, uidx_t j ) const 
        { return interp_pchip_value( *this, t, j, is_fixed_step() ); }

    inline mvalue_type linterp( mtime_type t, uidx_t j ) const 
        { return interp_linear_value( *this, t, j, is_fixed_step() ); }

    inline mvalue_type nearest( mtime_type t, uidx_t j ) const 
        { return nearest_value( *this, t, j, is_fixed_step() ); }

    // state iteration
    inline iterator_type iter() const
        { return iterator_type(*this); }
};

// ------------------------------------------------------------------------

template <class D, class T>
std::ostream& operator<< ( std::ostream &os, const _timeseries<D,T>& ts ) 
{
    const auto ond = onstream<D>();
    const auto ont = onstream<T>();

    const uidx_t nt = ts.ntime();
    const uidx_t nc = ts.nchan();
    const uidx_t p = op_ndigits(nt);

    os << "[Timeseries " << nt << "x" << nc << "]:" << std::endl;
    for ( uidx_t k=0; k < nt; ++k ) {
        os << "\n  [" << std::setw(p) << k << "]\t";
        ont(os) << ts[k] << "\t";
        for ( uidx_t c=0; c < nc; ++c ) 
            ond(os) << ts(k,c) << ", ";
        os << "\b\b ";
    }

    return os << std::endl;
}

DRAYN_NS_END
