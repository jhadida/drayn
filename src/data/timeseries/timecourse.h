
//==================================================
// @title        timecourse.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Vctn, class Tctn,
    class Value=typename Vctn::value_type, 
    class Time=typename Tctn::value_type>
class timecourse: public _timeseries<Value,Time>
{
public:

    using self = timecourse< Vctn, Tctn, Value, Time >;
    using parent = _timeseries<Value,Time>;

    using time_type = Time;
    using value_type = Value;

    using mtime_type = typename std::remove_const<Time>::type;
    using mvalue_type = typename std::remove_const<Value>::type;

    using time_ctn = Tctn;
    using data_ctn = Vctn;
    
    // ----------  =====  ----------

    timecourse(): m_fixed(false) {}

    timecourse( const time_ctn& t, const data_ctn& d )
        { assign(t,d); }
    timecourse( const time_ctn& t, const data_ctn& d, bool fixed )
        { assign(t,d,fixed); }

    
    self& assign( const time_ctn& t, const data_ctn& d ) {
        bool fixed = check_fixed_step<mtime_type>(t);
        DRAYN_IASSERT( fixed,
            "[drayn.timecourse] Timecourse has a variable time-step, "
            "consider resampling to accelerate interpolation."
        );
        return assign( t, d, fixed );
    }

    self& assign( const time_ctn& t, const data_ctn& d, bool fixed ) {
        const uidx_t nt = t.size();
        const uidx_t nd = d.size();
        DRAYN_ASSERT_ERR( nt*nd > 0 && nd==nt, "Input size mismatch." )

        m_time = t;
        m_data = d;
        m_fixed = fixed;

        return *this;
    }

    inline const time_ctn& time() const { return m_time; }
    inline const data_ctn& data() const { return m_data; }

    // ----------  =====  ----------

    inline bool is_fixed_step() const { return m_fixed; }
    inline uidx_t ntime() const { return m_time.size(); }
    inline uidx_t nchan() const { return 1; }

    inline time_type& time( uidx_t i ) const { return m_time[i]; }
    inline value_type& value( uidx_t i ) const  { return m_data[i]; }
    inline value_type& operator() ( uidx_t i ) const { return m_data[i]; }
    
    // ----------  =====  ----------
    
    // deal with overloads for single-index variants
    using parent::pinterp;
    using parent::linterp;
    using parent::nearest;

    inline value_type& value(uidx_t i, uidx_t) const { return value(i); }
    inline value_type& operator() (uidx_t i, uidx_t) const { return value(i); }

    inline value_type pinterp( mtime_type t ) const { return this->pinterp(t,0); }
    inline value_type linterp( mtime_type t ) const { return this->linterp(t,0); }
    inline value_type nearest( mtime_type t ) const { return this->nearest(t,0); }

protected:

    bool m_fixed;
    time_ctn m_time;
    data_ctn m_data;
};

DRAYN_NS_END
