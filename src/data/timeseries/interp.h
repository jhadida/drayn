
//==================================================
// @title        interp.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class T, class TS, class S>
void nearest_state( const TS& ts, T query, const S& state, bool fixed_step )
{
    const uidx_t k = fixed_step? nearest_fixed(ts,query) : nearest_ascending(ts,query);
    const uidx_t n = state.size();
    for ( uidx_t i = 0; i < n; ++i )
        state[i] = ts(k,i);
}

template <class T, class TS, class V = typename TS::value_type>
V nearest_value( const TS& ts, T query, uidx_t chan, bool fixed_step )
{
    const uidx_t k = fixed_step? nearest_fixed(ts,query) : nearest_ascending(ts,query);
    return ts(k,chan);
}

// ------------------------------------------------------------------------

template <class T, class TS, class S>
void interp_linear_state( const TS& ts, T query, const S& state, bool fixed_step )
{
    LinearInterpolant lin;

    if ( fixed_step )
        interp_linear_fixed( ts, query, lin );
    else
        interp_linear_ascending( ts, query, lin );

    const uidx_t n = state.size();
    for ( uidx_t i = 0; i < n; ++i )
        state[i] =
            lin.wleft  * ts( lin.left,  i ) +
            lin.wright * ts( lin.right, i ) ;
}

template <class T, class TS, class V = typename TS::mvalue_type>
V interp_linear_value( const TS& ts, T query, uidx_t chan, bool fixed_step )
{
    LinearInterpolant lin;

    if ( fixed_step )
        interp_linear_fixed( ts, query, lin );
    else
        interp_linear_ascending( ts, query, lin );

    return static_cast<V>(
        lin.wleft  * ts( lin.left,  chan ) +
        lin.wright * ts( lin.right, chan ) );
}

// ------------------------------------------------------------------------

template <class TS, class V = typename TS::mvalue_type>
V interp_pchip_slope( const TS& ts, uidx_t i, uidx_t j )
{
    const uidx_t nt = ts.ntime();
    checkIndex( i, nt );
    checkIndex( j, ts.nchan() );

    if (i == 0)
        return pchip_slope_end(
            ts[1]-ts[0], ts[2]-ts[1],
            ts(1,j)-ts(0,j), ts(2,j)-ts(1,j)
        );
    else if (i == nt-1)
        return pchip_slope_end(
            ts[nt-1]-ts[nt-2], ts[nt-2]-ts[nt-3],
            ts(nt-1,j)-ts(nt-2,j), ts(nt-2,j)-ts(nt-3,j)
        );
    else
        return pchip_slope(
            ts[i]-ts[i-1], ts[i+1]-ts[i],
            ts(i,j)-ts(i-1,j), ts(i+1,j)-ts(i,j)
        );
}

template <class TS, class V = typename TS::mvalue_type>
V interp_pchip( const TS& ts, uidx_t chan, const LinearInterpolant& lin )
{
    V dl, dr;

    switch ( ts.ntime() )
    {
        case 0:
            return 0.0;
        case 1:
            return ts( lin.left, chan );
        case 2:
            return static_cast<V>(
                lin.wleft  * ts( lin.left,  chan ) +
                lin.wright * ts( lin.right, chan ) );
        default:

            dl = interp_pchip_slope( ts, lin.left,  chan );
            dr = interp_pchip_slope( ts, lin.right, chan );

            return pchip_value<V>( ts[lin.right]-ts[lin.left], lin.wright, 
                ts(lin.left,chan), ts(lin.right,chan), dl, dr );
    }
}

// ------------------------------------------------------------------------

template <class T, class TS, class S>
void interp_pchip_state( const TS& ts, T query, const S& state, bool fixed_step )
{
    LinearInterpolant lin;

    if ( fixed_step )
        interp_linear_fixed( ts, query, lin );
    else
        interp_linear_ascending( ts, query, lin );

    const uidx_t n = state.size();
    for ( uidx_t i = 0; i < n; ++i )
        state[i] = interp_pchip( ts, i, lin );
}

template <class T, class TS, class V = typename TS::mvalue_type>
V interp_pchip_value( const TS& ts, T query, uidx_t chan, bool fixed_step )
{
    LinearInterpolant lin;

    if ( fixed_step )
        interp_linear_fixed( ts, query, lin );
    else
        interp_linear_ascending( ts, query, lin );

    return interp_pchip( ts, chan, lin );
}

DRAYN_NS_END
