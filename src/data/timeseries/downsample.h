
//==================================================
// @title        downsample.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Downsampling to specified rate using a moving weighted-average.

template <class TS, class W, 
    class V=typename TS::mvalue_type, 
    class T=typename TS::mtime_type >
ts_shr<V,T> downsample_ts( const TS& in, T step, const W& w, T burn=0 )
{
    DRAYN_ASSERT_ERR( 0 <= burn && burn <= in.tspan(), "Bad burn-in." )
    DRAYN_ASSERT_ERR( w.size() > 1, "Not enough weights." )
    DRAYN_ASSERT( step > in.tstep(), "Downsampling to a higher rate?!" )

    const auto out = make_ts<V,T>( in.nchan(), in.tstart(), in.tend(), step );
    const bool fixed = in.is_fixed_step();

    // const uidx_t nbefore = in.ntime(); // -Wunused-variable
    const uidx_t nafter = out.ntime();
    DRAYN_ASSERT_ERR( nafter > 1, "Step is too large." )

    // timecourse for the filter
    const uidx_t nw = w.size();
    const uidx_t ws = nw / 2;
    const auto t = range<T>( in.tstart(), in.tend(), 2*step/(nw-1) );

    // get first/last point by copy/interpolation
    out.state_unsafe(0).copy( in.state(0) );
    interp_pchip_state( in, out.tend(), out.state_unsafe(nafter-1), fixed );

    // initialise buffer
    auto buf = buf_weighted<T>( w, in.nchan() );

    uidx_t tn = 0;
    for (; tn < (nw+1)/2; tn++ )
        interp_pchip_state( in, t[tn], buf.next(), fixed );

    // use buffer to estimate internal states
    for ( uidx_t i=1; (i < nafter-1) && (tn+ws < t.size()); i++ ) {
        for ( uidx_t j=0; j < ws; j++, tn++ )
            interp_pchip_state( in, t[tn], buf.next(), fixed );

        buf.statesum( out.state_unsafe(i) );
    }

    return out;
}

template <class TC, class W, 
    class V=typename TC::mvalue_type, 
    class T=typename TC::mtime_type>
tc_shr<V,T> downsample_tc( const TC& in, T step, const W& w, T burn=0 )
{
    DRAYN_ASSERT_ERR( 0 <= burn && burn <= in.tspan(), "Bad burn-in." )
    DRAYN_ASSERT_ERR( w.size() > 1, "Not enough weights." )
    DRAYN_ASSERT( step > in.tstep(), "Downsampling to a higher rate?!" )

    const auto out = make_tc<V,T>( in.tstart(), in.tend(), step );
    const bool fixed = in.is_fixed_step();

    // const uidx_t nbefore = in.ntime(); // -Wunused-variable
    const uidx_t nafter = out.ntime();
    DRAYN_ASSERT_ERR( nafter > 1, "Step is too large." )

    // timecourse for the filter
    const uidx_t nw = w.size();
    const uidx_t ws = nw / 2;
    const auto t = range<T>( in.tstart(), in.tend(), 2*step/(nw-1) );

    // get first/last point by copy/interpolation
    out(0) = in(0);
    out(nafter-1) = in.pinterp(out.tend());

    // initialise buffer
    auto buf = buf_weighted<T>(w,1);

    uidx_t tn = 0;
    for (; tn < (nw+1)/2; tn++ )
        interp_pchip_state( in, t[tn], buf.next(), fixed );

    // use buffer to estimate internal states
    for ( uidx_t i=1; (i < nafter-1) && (tn+ws < t.size()); i++ ) {
        for ( uidx_t j=0; j < ws; j++, tn++ )
            interp_pchip_state( in, t[tn], buf.next(), fixed );
        
        out(i) = buf.chansum(0);
    }

    return out;
}

DRAYN_NS_END
