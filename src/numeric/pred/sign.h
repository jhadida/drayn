
//==================================================
// @title        sign.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#define DRAYN_SIGN_PREDICATE( Name, Test )   \
template <class T>                         \
struct Name                                \
{                                          \
    inline bool operator() ( T x ) const   \
        { return (Test); }                 \
};                                         \
template <class T>                         \
inline bool is_ ## Name ( T x )            \
    { return (Test); }

DRAYN_SIGN_PREDICATE( non_negative, x >= T(0) );
DRAYN_SIGN_PREDICATE( non_positive, x <= T(0) );
DRAYN_SIGN_PREDICATE( negative, x < T(0) );
DRAYN_SIGN_PREDICATE( positive, x > T(0) );
