
//==================================================
// @title        range.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T, char left, char right>
struct in_range
{
    T lower, upper;

    in_range( T l, T u )
        : lower(l), upper(u) {}

    inline bool operator() ( T x ) const
        { return false; }
};

// ------------------------------------------------------------------------

#define DRAYN_RANGE_PREDICATE( lchar, rchar, Test )      \
template <class T>                                     \
struct in_range<T,lchar,rchar>                         \
{                                                      \
    T lower, upper;                                    \
    in_range( T l, T u )                               \
        : lower(l), upper(u) {}                        \
    inline bool operator() ( T x ) const               \
        { return (Test); }                             \
};

DRAYN_RANGE_PREDICATE( '[', ']', (x >= lower) && (x <= upper) );
DRAYN_RANGE_PREDICATE( '[', ')', (x >= lower) && (x <  upper) );
DRAYN_RANGE_PREDICATE( '(', ']', (x >  lower) && (x <= upper) );
DRAYN_RANGE_PREDICATE( '(', ')', (x >  lower) && (x <  upper) );
