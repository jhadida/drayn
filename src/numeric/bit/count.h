
//==================================================
// @title        count.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// Count the number of bits set
template <class Integer>
unsigned bit_count( Integer n )
{
    unsigned c;
    for (c = 0; n; ++c)
        n &= n - 1;
    return c;
}
