
//==================================================
// @title        swap.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// XOR swap of two integers
template <class Integer>
inline void bit_swap( Integer& a, Integer& b )
{
    a ^= b;
    b ^= a;
    a ^= b;
}
