
//==================================================
// @title        gcd.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// Greatest common divisor
template <class Unsigned>
Unsigned bit_gcd( Unsigned a, Unsigned b )
{
    while ( b )
    {
        b ^= a;
        a ^= b;
        b  = (b^a) % a;
    }
    return a;
}
