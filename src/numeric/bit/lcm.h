
//==================================================
// @title        lcm.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// Least common multiple
template <class Unsigned>
Unsigned bit_lcm( Unsigned a, Unsigned b )
{
    Unsigned t = bit_gcd(a,b);
    return t? (a/t * b) : 0;
}
