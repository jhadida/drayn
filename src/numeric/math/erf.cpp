
//==================================================
// @title        erf.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * From StackOverflow (more precise):
 * https://stackoverflow.com/questions/5971830/need-code-for-inverse-error-function
 */
double _erfinv_stackoverflow( double y )
{
    const double a[] = { 0.886226899, -1.645349621,  0.914624893, -0.140543331};
    const double b[] = {-2.118377725,  1.442710462, -0.329097515,  0.012229801};
    const double c[] = {-1.970840454, -1.624906493,  3.429567803,  1.641345311};
    const double d[] = { 3.543889200,  1.637067800};
    const double isqpi = 1.0/sqrt(atan(1));

    int sign = op_sign(y);
    double x, z = sign*y;

    if ( z >= 0.25 )
    {
        z = sqrt( -log( (1.0 - z)/2.0 ) );
        x = sign * (((c[3]*z+c[2])*z+c[1])*z+c[0])/((d[1]*z+d[0])*z+1.0);
    }
    else
    {
        z = y*y;
        x = y*(((a[3]*z+a[2])*z+a[1])*z+a[0])/((((b[3]*z+b[3])*z+b[1])*z+b[0])*z+1.0);
    }

    // Refine to full accuracy
    x -= (sp_erf(x) - y) / (isqpi * exp(-x*x));
    x -= (sp_erf(x) - y) / (isqpi * exp(-x*x));

    return x;
}

// faster but no difference with _erfinv_stackoverflow?
double _erfinv_custom( double y )
{
    const double a[] = {-1.970840454, -1.624906493,  3.429567803,  1.641345311};
    const double b[] = { 3.543889200,  1.637067800};
    const double isqpi = 1.0/sqrt(atan(1));

    int sign = op_sign(y);
    double x, z = sign*y;

    z = sqrt( -log( (1.0 - z)/2.0 ) );
    x = sign * (((a[3]*z+a[2])*z+a[1])*z+a[0])/((b[1]*z+b[0])*z+1.0);

    // Refine to full accuracy
    x -= (sp_erf(x) - y) / (isqpi * exp(-x*x));
    x -= (sp_erf(x) - y) / (isqpi * exp(-x*x));

    return x;
}

/**
 * From Wikipedia:
 * https://www.wikiwand.com/en/Error_function#/Approximation_with_elementary_functions
 */
double _erfinv_wikipedia( double y )
{
    const double pi = math::Pi;
    const double a = 0.75 * (4.0-pi)/(pi-3.0);
    const int sign = op_sign(y);

    double l = log(1.0 - y*y)/2.0;
    double x = a+l;
    
    x = sqrt( x*x - a*l*pi ) - x;
    return sign * sqrt(x);
}
