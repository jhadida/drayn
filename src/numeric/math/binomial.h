
//==================================================
// @title        binomial.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// Binomial coefficient "n choose k"
template <class U>
U sp_nchoosek( U n, U k )
{
    static_assert( std::is_integral<U>::value && std::is_unsigned<U>::value, 
        "Template type should be unsigned integral." );

    U num, com, res = 1;

    for ( U d = 1; d <= k; ++d )
    {
        com  = bit_gcd(res, d);
        res /= com;
        num  = (n - k + d) / (d / com);

        DRAYN_ASSERT_RV( res <= c_num<U>::max / num, 1,
            "[drayn.sp_nchoosek] Overflow error." );

        res *= num;
    }

    return res;
}
