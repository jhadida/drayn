
//==================================================
// @title        erf.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// Gauss error function
inline double sp_erf( double x ) { return std::erf(x); }
inline double sp_erfc( double x ) { return 1.0 - sp_erf(x); }

double _erfinv_stackoverflow( double y );
double _erfinv_wikipedia( double y );
double _erfinv_custom( double y );

inline double sp_erfinv( double y ) { return _erfinv_custom(y); }
