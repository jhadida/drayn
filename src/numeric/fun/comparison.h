
//==================================================
// @title        comparison.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#define DRAYN_COMPARISON_FUNCTION( Name, Op )   \
struct Name                                     \
{                                               \
    template <class T>                          \
    inline bool operator() ( T a, T b ) const   \
        { return a Op b; }                      \
};

DRAYN_COMPARISON_FUNCTION( f_lt , <  ); // lesser than
DRAYN_COMPARISON_FUNCTION( f_leq, <= ); // lesser or equal
DRAYN_COMPARISON_FUNCTION( f_gt , >  ); // greater than
DRAYN_COMPARISON_FUNCTION( f_geq, >= ); // greater or equal
DRAYN_COMPARISON_FUNCTION( f_eq , == ); // equal
DRAYN_COMPARISON_FUNCTION( f_neq, != ); // not equal
