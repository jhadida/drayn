
//==================================================
// @title        cast.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T>
struct f_cast
{
    template <class U>
    inline T operator() ( const U& x ) const 
        { return static_cast<T>(x); }

    // no cast needed
    inline T operator() ( const T& x ) const
        { return x; }
};
