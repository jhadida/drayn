
//==================================================
// @title        arithmetic.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#define DRAYN_ARITHMETIC_FUNCTION( Name, Op )   \
template <class Out>                            \
struct Name                                     \
{                                               \
    template <class T>                          \
    inline Out operator() ( T a, T b ) const    \
        { return static_cast<Out>(a Op b); }    \
};

DRAYN_ARITHMETIC_FUNCTION( f_add, + ); // addition
DRAYN_ARITHMETIC_FUNCTION( f_sub, - ); // subtraction
DRAYN_ARITHMETIC_FUNCTION( f_div, / ); // division
DRAYN_ARITHMETIC_FUNCTION( f_mul, * ); // multiplication

// ------------------------------------------------------------------------

template <class Out>
struct f_inv
{
    template <class T>
    inline T operator() ( T x ) const
        { return static_cast<Out>( Out(1) / x ); }
};

template <class T>
struct f_abs
{
    inline T operator() ( T x ) const
        { return op_abs(x); }
};
