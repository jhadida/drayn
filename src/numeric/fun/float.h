
//==================================================
// @title        float.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// Floating point comparison
template <class T>
struct fp_equal_strict
{
    inline bool operator() ( T a, T b ) {
        return op_abs(a - b) 
            <= std::max(
                c_num<T>::min * std::min( op_abs(a), op_abs(b) ),
                c_num<T>::eps
            );
    }
};

template <class T>
struct fp_equal_loose
{
    inline bool operator() ( T a, T b ) {
        return op_abs(a - b) 
            <= std::max(
                c_num<T>::min * std::max( op_abs(a), op_abs(b) ),
                c_num<T>::eps
            );
    }
};

template <class T>
struct fp_greater
{
    inline bool operator() ( T a, T b ) {
        return (a - b) >= c_num<T>::min * std::max( op_abs(a), op_abs(b) );
    }
};

template <class T>
struct fp_lesser
{
    inline bool operator() ( T a, T b ) {
        return (b - a) >= c_num<T>::min * std::max( op_abs(a), op_abs(b) );
    }
};

template <class T>
struct fp_close
{
    T eps;

    fp_close( T e = c_num<T>::eps )
        : eps(e) {}

    inline bool operator() ( T a, T b ) {
        return op_abs(a - b) <= eps;
    }
};

// ------------------------------------------------------------------------

template <class T>
bool is_close( T a, T b, T t = c_num<T>::eps ) {
    return op_abs(a-b) <= t;
}
