
//==================================================
// @title        round.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T,class U>
inline U _round_cast( T x, std::true_type ) // integral type
    { return static_cast<U>(x); }

template <class T,class U>
inline U _round_cast( T x, std::false_type ) // non-integral type
    { return static_cast<U>(std::round(x)); }

// ------------------------------------------------------------------------

// No cast
template <class T>
inline constexpr T op_round( T x )
    { return std::round(x); }

// Cast as unsigned
template <class T>
inline uint_t op_uround( T x )
    { return _round_cast<T,uint_t>( x, std::is_integral<T>() ); }

// Cast as int
template <class T>
inline sint_t op_iround( T x )
    { return _round_cast<T,sint_t>( x, std::is_integral<T>() ); }
