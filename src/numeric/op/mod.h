
//==================================================
// @title        mod.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T>
inline T _mod( T a, T b, std::true_type ) // integral type
{
    return (a%b + b)%b;
}

template <class T>
inline T _mod( T a, T b, std::false_type ) // non-integral type
{
    return a - b*std::floor(a/b);
}

// ------------------------------------------------------------------------

// The "mathematical" modulus function.
template <class T>
inline T op_mod( T a, T b )
    { return _mod( a, b, std::is_integral<T>() ); }
