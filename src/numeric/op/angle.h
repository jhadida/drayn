
//==================================================
// @title        angle.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Conversion between degrees and radians.
 */

inline double op_rad2deg ( double r ) { return r*180 / math::Pi; }
inline double op_deg2rad ( double d ) { return d*math::Pi / 180; }

/**
 * Signed minimum angle between two angles.
 */

template <class T>
inline T op_degdiff( T a, T b )
    { return static_cast<T>(op_mod( b-a - 180.0, 360.0 ) + 180.0); }

template <class T>
inline T op_raddiff( T a, T b )
    { return static_cast<T>(op_mod( b-a - math::Pi, math::Tau ) + math::Pi); }
