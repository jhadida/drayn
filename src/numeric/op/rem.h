
//==================================================
// @title        rem.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T>
inline T _rem( T a, T b, std::true_type ) // integral type
{
    return a % b;
}

template <class T>
inline T _rem( T a, T b, std::false_type ) // non-integral type
{
    return a - b*op_fix(a/b);
}

// ------------------------------------------------------------------------

/**
 * Same as Matlab's rem function.
 */

template <class T>
inline T op_rem( T a, T b )
    { return _rem( a, b, std::is_integral<T>() ); }
