
//==================================================
// @title        clamp.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// force a number in a range
template <class T> 
inline T op_clamp( T a, T lb, T ub )
    { return a >= ub ? ub : ( a < lb ? lb : a ); }
