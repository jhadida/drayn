
//==================================================
// @title        abs.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T>
inline T _abs( T a, std::true_type ) // integral type
    { return a < 0 ? -a : a; }

template <class T>
inline T _abs( T a, std::false_type ) // non-integral type
    { return std::abs(a); }

// ------------------------------------------------------------------------

// The "mathematical" absulus function.
template <class T>
inline T op_abs( T a )
    { return _abs( a, std::is_integral<T>() ); }
