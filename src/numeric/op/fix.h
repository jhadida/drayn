
//==================================================
// @title        fix.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T>
inline T _fix( T x, std::true_type ) // signed type
    { return x >= T(0) ? std::floor(x) : std::ceil(x); }

template <class T>
inline T _fix( T x, std::false_type ) // unsigned type
    { return std::floor(x); }

// ------------------------------------------------------------------------

template <class T>
inline T op_fix( T x )
    { return _fix<T>( x, std::is_signed<T>() ); }

// Cast as unsigned
template <class T>
inline uint_t op_ufix( T x )
    { return static_cast<uint_t>(op_fix(x)); }

// Cast as int
template <class T>
inline sint_t op_ifix( T x )
    { return static_cast<sint_t>(op_fix(x)); }
