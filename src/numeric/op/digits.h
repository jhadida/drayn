
//==================================================
// @title        digits.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// number of digits in input integer
template <class T>
uint_t op_ndigits( T x ) {
    static_assert( std::is_integral<T>::value, "[drayn.op_ndigits] Only accepts integer types." );
    return op_abs(x) > 9? std::floor(1+log10(op_abs(x))) : 1;
}

// order of magnitude
template <class T>
sint_t op_exponent( T x ) {
    return op_ifloor(1 + log10(op_abs(x)));
}
