
//==================================================
// @title        numeric.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>
#include <complex>
#include <cstdint>
#include <cinttypes>

#include <algorithm>
#include <functional>
#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



// NOTE: previously used to find consensus between types, removed for clarity:
// template < class T, class U, 
//      class V = typename std::common_type<T,U>::type >

DRAYN_NS_START

// ------------------------------------------------------------------------
// Predicates

#include "pred/sign.h"
#include "pred/division.h"
#include "pred/range.h"

// ------------------------------------------------------------------------
// Bitwise operations

#include "bit/count.h"
#include "bit/gcd.h"
#include "bit/lcm.h"
#include "bit/gray.h"
#include "bit/print.h"
#include "bit/reverse.h"
#include "bit/swap.h"
#include "bit/two.h"

// ------------------------------------------------------------------------
// Usual operations

#include "op/abs.h"
#include "op/sign.h"
#include "op/floor.h"
#include "op/ceil.h"
#include "op/round.h"
#include "op/fix.h"
#include "op/clamp.h"
#include "op/pow.h"
#include "op/mod.h"
#include "op/rem.h"
#include "op/angle.h"
#include "op/digits.h"

// ------------------------------------------------------------------------
// Math funxtions

#include "math/erf.h"
#include "math/binomial.h"

// ------------------------------------------------------------------------
// Function objects

#include "fun/arithmetic.h"
#include "fun/comparison.h"
#include "fun/float.h"
#include "fun/cast.h"
#include "fun/scaling.h"

DRAYN_NS_END
