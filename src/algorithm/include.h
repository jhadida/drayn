#ifndef DRAYN_ALGORITHM_H_INCLUDED
#define DRAYN_ALGORITHM_H_INCLUDED

//==================================================
// @title        algorithn
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include "bound.h"
#include "order.h"

#include "math/extrema.h"
#include "math/norm.h"
#include "math/distance.h"
#include "math/metric.h"
#include "math/average.h"
#include "math/interval.h"
#include "math/integral.h"

#include "interp/nearest.h"
#include "interp/linear.h"
#include "interp/pchip.h"

#include "window.h"

#endif
