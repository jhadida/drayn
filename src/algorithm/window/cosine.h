
//==================================================
// @title        coswin.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T>
svec<T> win_cosine( uidx_t n, win type )
{
    DRAYN_ASSERT_ERR( n > 1, "Window size must be greater than 1." )
    const T tau = T(math::Tau);

    // for flattop
    const double 
        a0 = 0.21557895,
        a1 = 0.41663158,
        a2 = 0.277263158,
        a3 = 0.083578947,
        a4 = 0.006947368;

    svec<T> out(n,0);
    switch (type)
    {
        case win::Blackman:
            _coswin_calc( out, [tau]( double w ){ 
                return 0.42 - 0.5*cos(tau*w) + 0.08*cos(2*tau*w); 
            });
            out[0] = 0;
            break;
        case win::Flattop:
            _coswin_calc( out, [a0,a1,a2,a3,a4,tau]( double w ){
                return a0 - a1*cos(tau*w) + a2*cos(2*tau*w) - a3*cos(3*tau*w) + a4*cos(4*tau*w); 
            });
            break;
        case win::Hamming:
            _coswin_calc( out, [tau]( double w ){ return 0.54 - 0.46*cos(tau*w); } );
            break;
        case win::Hann:
            _coswin_calc( out, [tau]( double w ){ return 0.5 - 0.5*cos(tau*w); } );
            break;
        default:
            throw std::invalid_argument("Invalid cosine window type.");
    }

    _coswin_mirror(out);
    return out;
}

// ------------------------------------------------------------------------

template <class T, class F>
void _coswin_calc( const svec<T>& x, F fun )
{
    const uidx_t n = x.size();
    const uidx_t m = (n+1)/2;

    for ( uidx_t i=0; i < m; i++ ) 
        x[i] = static_cast<T>(fun( static_cast<double>(i)/(n-1) ));
}

template <class T>
void _coswin_mirror( const svec<T>& x )
{
    const uidx_t n = x.size();
    const uidx_t m = (n+1)/2;

    for ( uidx_t i=0; i < m; i++ )
        x[n-1-i] = x[i];
}
