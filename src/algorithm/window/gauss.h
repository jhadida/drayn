
//==================================================
// @title        gauss.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T>
svec<T> win_gauss( uidx_t n, double a = 2.5 )
{
    DRAYN_ASSERT_ERR( n > 1, "Window size must be greater than 1." )

    svec<T> out(n);
    const double L = (n-1)/2.0;

    double t;
    for ( uidx_t i=0; i < n; ++i ) {
        t = a*(i-L)/L;
        out[i] = exp(- t*t/2);
    }
    
    return out;
}
