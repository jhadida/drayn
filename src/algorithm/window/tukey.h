
//==================================================
// @title        tukey.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

template <class T>
svec<T> win_tukey( uidx_t n, double r = 0.5 ) 
{
    DRAYN_ASSERT_ERR( n > 1, "Window size must be greater than 1." )
    
    // when ratio is not in (0,1)
    if ( r <= 0.0 ) return svec<T>(n,1); // equivalent to boxcar
    if ( r >= 1.0 ) return win_cosine<T>(n,win::Hann); // equivalent to Hann

    svec<T> out(n,1);
    r = 0.5*r*(n-1);
    
    const uidx_t t = op_ufloor(r);
    for ( uidx_t i=0; i <= t; ++i ) {
        out[i] = static_cast<T>((1+cos( math::Pi * (i-r)/r ))/2);
        out[n-1-i] = out[i];
    }

    return out;
}
