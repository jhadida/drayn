
//==================================================
// @title        metric.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>
#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class T>
struct Metric
{
    virtual double norm( const T& ) const =0;
    virtual double distance( const T&, const T& ) const =0; 
};

// ------------------------------------------------------------------------

template <class T>
struct ScalarMetric
    : public Metric<T>
{
    typedef typename std::conditional< is_complex<T>::value,
        std::complex<double>, double >::type  ctype;

    inline double norm( const T& x ) const 
        { return op_abs(x); }
    
    inline double distance( const T& a, const T& b ) const 
        { return _distance_impl(a,b); }

    inline double _distance_impl( ctype a, ctype b ) const 
        { return op_abs(a-b); }
};

// ------------------------------------------------------------------------

template <unsigned p, class T>
struct LpMetric
    : public Metric<T>
{
    inline double norm( const T& x ) const { return norm_p<p>(x); };
    inline double distance( const T& a, const T& b ) const { return distance_p<p>(a,b); };
};

// ------------------------------------------------------------------------

template <class T>
struct LpMetric<1,T>
    : public Metric<T>
{
    inline double norm( const T& x ) const { return norm_1(x); };
    inline double distance( const T& a, const T& b ) const { return distance_1(a,b); };
};

template <class T> using L1Metric = LpMetric<1,T>;
template <class T> using ManhattanMetric = LpMetric<1,T>;

// ------------------------------------------------------------------------

template <class T>
struct LpMetric<2,T>
    : public Metric<T>
{
    inline double norm( const T& x ) const { return norm_2(x); };
    inline double distance( const T& a, const T& b ) const { return distance_2(a,b); };
};

template <class T> using L2Metric = LpMetric<2,T>;
template <class T> using EuclideanMetric = LpMetric<2,T>;

// ------------------------------------------------------------------------

template <class T>
struct LpMetric<0,T>
    : public Metric<T>
{
    inline double norm( const T& x ) const { return norm_0(x); };
    inline double distance( const T& a, const T& b ) const { return distance_0(a,b); };
};

template <class T> using MinAbsMetric = LpMetric<0,T>;

// ------------------------------------------------------------------------

template <class T>
struct LInfMetric
    : public Metric<T>
{
    inline double norm( const T& x ) const { return norm_inf(x); };
    inline double distance( const T& a, const T& b ) const { return distance_inf(a,b); };
};

template <class T> using MaxAbsMetric = LInfMetric<T>;

// ------------------------------------------------------------------------

template <class T>
struct MadMetric
    : public Metric<T>
{
    inline double norm( const T& x ) const { return norm_1(x); };
    inline double distance( const T& a, const T& b ) const 
        { return distance_mad(a,b); };
};

// ------------------------------------------------------------------------

template <class T>
struct RmsdMetric
    : public Metric<T>
{
    inline double norm( const T& x ) const { return norm_2(x); };
    inline double distance( const T& a, const T& b ) const 
        { return distance_rmsd(a,b); };
};

// ------------------------------------------------------------------------

template <class T>
struct CosineMetric
    : public Metric<T>
{
    inline double norm( const T& x ) const { return norm_2(x); };
    inline double distance( const T& a, const T& b ) const 
        { return alg_dot(a,b) / norm(a) / norm(b); };
};

DRAYN_NS_END
