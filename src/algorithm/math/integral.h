
//==================================================
// @title        integral.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Sx, class Sy>
double trapz( const Sx& x, const Sy& y )
{
    const uidx_t n = x.size();
    DRAYN_ASSERT_ERR( y.size() == n, "Input size mismatch." )
    
    double out = 0.0;
    for ( uidx_t k=1; k < n; k++ ) 
        out += 0.5*(x[k]-x[k-1])*(y[k]+y[k-1]);

    return out;
}

DRAYN_NS_END
