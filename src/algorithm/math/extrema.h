
//==================================================
// @title        extrema.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// find min/max elements in iterable
#include <cstdlib>
#include <algorithm>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class S, class T>
void extrema( const S& data, T& m, T& M )
{
    auto r = std::minmax_element( data.begin(), data.end() );
    m = static_cast<T>( *(r.first)  ); 
    M = static_cast<T>( *(r.second) );
}

// ------------------------------------------------------------------------

template <class S>
double maximum_difference( const S& data )
{
    double min, max;
    extrema( data, min, max );
    return max-min;
}

// ------------------------------------------------------------------------

template <class S, class T = typename S::value_type> 
inline T& minimum( const S& data )
    { return *std::min_element( data.begin(), data.end() ); }
template <class S, class T = typename S::value_type> 
inline T& maximum( const S& data )
    { return *std::max_element( data.begin(), data.end() ); }

// ------------------------------------------------------------------------

template <class S, class T = typename S::value_type> 
T minimum_abs( const S& data )
{
    return reduce( data, op_abs(data[0]), 
        []( const T& m, const T& x ){ return std::min( m, op_abs(x) ); } );
}

template <class S, class T = typename S::value_type> 
T maximum_abs( const S& data )
{
    return reduce( data, op_abs(data[0]), 
        []( const T& m, const T& x ){ return std::max( m, op_abs(x) ); } );
}

DRAYN_NS_END
