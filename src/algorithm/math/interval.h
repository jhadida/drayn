
//==================================================
// @title        interval.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iostream>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Val, class Real = double>
struct interval
{
    Val lo, up;

    interval() {}
    interval( Val a, Val b )
        { assign(a,b); }
    interval( const inilst<Val>& lst )
        { assign(lst); }

    inline operator bool() const
        { return delta() > c_num<Real>::eps; }

    inline void assign( Val a, Val b ) { lo = a; up = b; }
    inline void assign( const inilst<Val>& lst ) {
        ASSERT_R( lst.size() == 2, "Two values required." );
        lo = lst.begin()[0];
        up = lst.begin()[1];
    }

    inline Real delta() const { return static_cast<Real>(up-lo); }
    inline Real midpoint() const { return static_cast<Real>(lo+up) / 2; }

    inline Real ratio   ( Real x ) const { return ratio_ab(x); }
    inline Real ratio_ab( Real x ) const { return static_cast<Real>(x-lo)/delta(); }
    inline Real ratio_ba( Real x ) const { return static_cast<Real>(up-x)/delta(); }
};

// ------------------------------------------------------------------------

template <class V, class R>
inline std::ostream& operator<<( std::ostream& os, const interval<V,R>& I )
{
    const auto on = onstream<V>();
    return os << "[Interval] (";
        on(os) << I.lo << ", ";
        on(os) << I.up << ") " << std::endl; 
}

DRAYN_NS_END
