
//==================================================
// @title        average.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class S>
inline double avg_arithmetic( const S& data )
    { return alg_sum(data) / data.size(); }

template <class S>
inline double avg_geometric( const S& data, bool use_log = true )
{
    if ( use_log )
        return exp( reduce( 
            data, 0.0, f_add<double>(), 
            []( double x ){ return log(op_abs(x)); } 
        ) / data.size() );
    else
        return std::pow( op_abs(alg_prod(data)), 1.0/data.size() );
}

// ------------------------------------------------------------------------

template <class S>
inline double avg_1( const S& data )
    { return sum_1(data) / data.size(); }

template <class S>
inline double avg_2( const S& data )
    { return sum_2(data) / data.size(); }

template <class S>
inline double avg_p( const S& data, unsigned p )
    { return sum_p(data,p) / data.size(); }

template <class S>
inline double avg( const S& data )
    { return avg_arithmetic(data); }

template <class S>
inline double rms( const S& data )
    { return sqrt(avg_2(data)); }

DRAYN_NS_END
