
//==================================================
// @title        nearest.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <stdexcept>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class T, class C>
uidx_t nearest_ascending( const C& x, T q ) {
    const uidx_t n = x.size();
    if ( n == 0 ) throw std::invalid_argument("Empty container.");

    const uidx_t k = lower_bound<T>(x,q);
    if ( k < n-1 ) return n + ((x[n+1] - q) <= (q - x[n]));
    else return n-1;
}

template <class T, class C>
uidx_t nearest_fixed( const C& x, T q ) {
    const uidx_t n = x.size();
    if ( n == 0 ) throw std::invalid_argument("Empty container.");

    auto r = std::round( (q-x[0]) / (x[1]-x[0]) );
    return static_cast<uidx_t>(op_clamp<double>( r, 1, n-1 ));
}

DRAYN_NS_END
