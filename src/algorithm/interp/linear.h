
//==================================================
// @title        linear.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iostream>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

struct LinearInterpolant
{
    uidx_t left, right;
    double wleft, wright;
};

// Output stream overload
inline std::ostream& operator<<( std::ostream& os, const LinearInterpolant& L )
{
    return os << "[LinInterp] { " <<
        "L: (" << L.left  << "," << L.wleft  << "), " <<
        "R: (" << L.right << "," << L.wright << ") }" << std::endl;
}

// ------------------------------------------------------------------------

/**
 * Linear interpolation on a sorted (ascending) array of values.
 * The result contains the indices left and right such that:
 * values[left] <= q <= values[right]
 *
 * Undefined behaviour if values[left] == values[right].
 */
template <class T, class C>
void interp_linear_ascending( const C& x, T q, LinearInterpolant& L )
{
    const uidx_t n = x.size();
    interval<T> weights;

    if ( n <= 1 )
    {
        L.left = L.right = 0;

        // NOTE: needs to be consistent with the following case right == 0
        L.wright = static_cast<double>((n == 0) || (q < x[0]));
    }
    else
    {
        L.right = upper_bound( x, q );
        if ( L.right == 0 )
        {
            L.left = 0;
            L.wright = 1.0;
            DRAYN_INFO("Interpolation before first value.")
        }
        else
        if ( L.right == n )
        {
            L.left = --L.right;
            L.wright = 0.0;
            DRAYN_IREJECT( q > x[n-1], "Interpolation past last value." )
        }
        else
        {
            L.left = L.right-1;

            weights.assign( x[L.left], x[L.right] );
            DRAYN_ASSERT_ERR( weights, "Interpolants are too close to each other." )
            L.wright = weights.ratio( q );
            L.wright = op_clamp( L.wright, 0.0, 1.0 );
        }
    }
    L.wleft = 1.0 - L.wright;
}

// ------------------------------------------------------------------------

// same as previously, but for fixed-step progressions
template <class T, class C>
void interp_linear_fixed( const C& x, T q, LinearInterpolant& L )
{
    const uidx_t n = x.size();
    const sidx_t nmax = n - (n > 0);

    double delta; 
    sidx_t left;

    if ( n <= 1 )
    {
        L.left = L.right = 0;

        // NOTE: needs to be consistent with the following case left < 0
        L.wright = static_cast<double>((n == 0) || (q < x[0]));
    }
    else
    {
        delta = x[1]-x[0];
        left  = op_ifloor( (q-x[0]) / delta );

        if ( left < 0 )
        {
            L.left = L.right = 0;
            L.wright = 1.0;
            DRAYN_INFO("Interpolation before first value.")
        }
        else
        if ( left >= nmax )
        {
            L.left = L.right = n-1;
            L.wright = 0.0;
            DRAYN_IREJECT( left > nmax, "Interpolation past last value." )
        }
        else
        {
            L.left   = left;
            L.right  = left+1;
            L.wright = ( q - x[left] ) / delta;
            L.wright = std::min( L.wright, 1.0 );
        }
    }
    L.wleft = 1.0 - L.wright;
}

DRAYN_NS_END
