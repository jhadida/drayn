
//==================================================
// @title        pchip.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

/**
 * For all inner points:
 *
 * d(k) = pchip_slope( t(k)-t(k-1), t(k+1)-t(k), v(k)-v(k-1), v(k+1)-v(k) );
 */
template <class T, class V>
V pchip_slope( T h1, T h2, V D1, V D2 )
{
    // const V eps = c_num<V>::eps;
    // if ( (op_abs(D1) < eps) || (op_abs(D2) < eps) || (D1*D2 < 0) )

    if ( D1*D2 <= 0 ) return V(0);

    const T w1 = 2*h2 + h1;
    const T w2 = h2 + 2*h1;

    return static_cast<V>( (w1+w2) / ( h1*w1/D1 + h2*w2/D2 ) );
}

/**
 * For the first and last point:
 *
 * d(0) = pchip_slope_end( t(1)-t(0), t(2)-t(1), v(1)-v(0), v(2)-v(1) )
 * d(n-1) = pchip_slope_end( t(n-1)-t(n-2), t(n-2)-t(n-3), v(n-1)-v(n-2), v(n-2)-v(n-3) )
 */
template <class T, class V>
V pchip_slope_end( T h1, T h2, V D1, V D2 )
{
    const V d = ( (2*h1+h2)*D1/h1 - h1*D2/h2 ) / (h1+h2);
    if ( d*D1 < 0 )
        return V(0);
    else if ( (D1*D2 < 0) && (op_abs(d) > op_abs(3*D1/h1)) )
        return static_cast<V>(3*D1/h1);
    else
        return d;
}

// ------------------------------------------------------------------------

template <class T>
T pchip_value( T h, T w, T vl, T vr, T dl, T dr )
{
    if ( h*h == 0 ) return (vl+vr)/2;

    const T s = h*w;
    const T c = ( 3*(vr-vl)/h - 2*dl - dr ) / h;
    const T b = ( dl - 2*(vr-vl)/h + dr ) / (h*h);

    return vl + s*( dl + s*(c + s*b) );
}

DRAYN_NS_END
