
//==================================================
// @title        bound.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Replicate the standard algorithms, but using sequential access only (no random-access iterators).
// Memo: upper-bound is exclusive, lower-bound is inclusive

// finds first index k such that query < val[k]
// 
//      if k == 0,          then query < val[0]
//      if k == size(),     then val[size()-1] <= query
// 
template <class T, class C>
uidx_t upper_bound( const C& val, const T query )
{
    uidx_t len = val.size();
    uidx_t first = 0;
    uidx_t step  = 1;

    while ( len > 0 )
    {
        step = len/2;
        if ( query >= static_cast<T>(val[first+step]) ) {
            first += step+1;
            len -= step+1;
        }
        else len = step;
    }

    return first;
}

// finds first index k such that query >= val[k]
// 
//      if k == size(),     then val[size()-1] < query
//      if k == 0,          then query <= val[0]
// 
template <class T, class C>
uidx_t lower_bound( const C& val, const T query )
{
    uidx_t len = val.size();
    uidx_t first = 0;
    uidx_t step  = 1;

    while ( len > 0 )
    {
        step = len/2;
        if ( query < static_cast<T>(val[first+step]) ) {
            first += step+1;
            len -= step+1;
        }
        else len = step;
    }

    return first;
}

DRAYN_NS_END
