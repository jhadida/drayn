
//==================================================
// @title        order.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <algorithm>
#include <functional>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class S>
svec<uidx_t> rank( const S& data )
{
    auto r = xrange(data.size());
    std::sort( r.begin(), r.end(), [ &data ]( uidx_t i, uidx_t j ) -> bool { return data[i] < data[j]; } );
    return r;
}

// ------------------------------------------------------------------------

template <class S>
bool is_ascending( const S& seq, bool strict = false )
{
    const uidx_t n = seq.size();
    if (strict) {
        for ( uidx_t i=1; i < n; ++i ) 
            if ( seq[i] <= seq[i-1] ) 
                return false;
    }
    else {
        for ( uidx_t i=1; i < n; ++i ) 
            if ( seq[i] < seq[i-1] ) 
                return false;
    }

    return true;
}

// ------------------------------------------------------------------------

template <class S>
bool is_descending( const S& seq, bool strict = false )
{
    const uidx_t n = seq.size();
    if (strict) {
        for ( uidx_t i=1; i < n; ++i ) 
            if ( seq[i] >= seq[i-1] ) 
                return false;
    }
    else {
        for ( uidx_t i=1; i < n; ++i ) 
            if ( seq[i] > seq[i-1] ) 
                return false;
    }

    return true;
}

DRAYN_NS_END
