
//==================================================
// @title        window.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>
#include <stdexcept>
#include <functional>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

enum class win
{ 
    Blackman,
    Flattop,
    Gauss,
    Hamming,
    Hann,
    Tukey
};

#include "window/cosine.h"
#include "window/tukey.h"
#include "window/gauss.h"

// ------------------------------------------------------------------------

template <class T>
svec<T> window( win type, uidx_t n )
{
    switch (type)
    {
        case win::Blackman:
            return win_cosine<T>(n,type);
        case win::Flattop:
            return win_cosine<T>(n,type);
        case win::Gauss:
            return win_gauss<T>(n);
        case win::Hamming:
            return win_cosine<T>(n,type);
        case win::Hann:
            return win_cosine<T>(n,type);
        case win::Tukey:
            return win_tukey<T>(n);
        default:
            throw std::invalid_argument("Unknown window type.");
    }
}

template <class T>
svec<T> window_norm( win type, uidx_t n )
{
    auto out = window<T>(type,n);
    return out /= sum_1(out);
}

DRAYN_NS_END
