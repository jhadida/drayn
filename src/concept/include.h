#ifndef DRAYN_CONCEPT_H_INCLUDED
#define DRAYN_CONCEPT_H_INCLUDED

//==================================================
// @title        concept
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include "iterator.h"
#include "iterable.h"

#include "functional.h"
#include "algebra.h"

#include "arithmetic.h"
#include "comparable.h"
#include "copyable.h"

#include "indexed.h"
#include "sequence.h"

#endif
