
//==================================================
// @title        sequence.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Container, class Value>
struct sequence_traits
{
    using iterator_type         = indexed_iterator<Value>;
    using std_iterator_type     = indexed_std_iterator<Value>;
    
    using indexed_parent        = _indexed<Value>;
    using iterable_parent       = _iterable<iterator_type>;
    using arithmetic_parent     = _arithmetic<Value,Container>;
    using comparable_parent     = _comparable<Value,Container>;
};

// ------------------------------------------------------------------------

template <class C, class V>
struct _sequence: 
    public sequence_traits<C,V>::indexed_parent,
    public sequence_traits<C,V>::iterable_parent,
    public sequence_traits<C,V>::arithmetic_parent,
    public sequence_traits<C,V>::comparable_parent
{
    DRAYN_TRAITS(V)

    using value_type = V;
    using mvalue_type = typename std::remove_const<V>::type;
    using iterator_type = typename sequence_traits<C,V>::iterator_type;
    using std_iterator_type = typename sequence_traits<C,V>::std_iterator_type;

    // to be implemented
    virtual bool valid() const =0;
    virtual uidx_t size() const =0;
    virtual ref_t operator[] (uidx_t) const =0;

    // iteration methods
    inline iterator_type iter() const 
        { return iterator_type(this); }

    inline std_iterator_type begin() const 
        { return std_iterator_type(this); }
    inline std_iterator_type end() const 
        { return std_iterator_type(this) += this->size(); }
};

DRAYN_NS_END
