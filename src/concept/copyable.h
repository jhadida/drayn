
//==================================================
// @title        copyable.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Copyable classes provide an element-wise method copy().
 * This method shall warn or raise an exception in case of size mismatch with the input.
 * Non-const overloads (eg with silent resizing) can also be defined if derived classes
 * allow it, but this concept does not require such definition.
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START


template <class V, bool Enable = is_mutable<V>::value >
struct _copyable {};

// ------------------------------------------------------------------------

template <class V>
struct _copyable<V,true>
{
    DRAYN_TRAITS(V)
    virtual uidx_t size() const =0;
    virtual ref_t operator[] (uidx_t) const =0;

    template <class C>
    void copy( const C& other ) const
    {
        DRAYN_ASSERT_R( other.size() == size(),
            "[drayn._copyable] Input size mismatch" );

        for ( uidx_t k=0; k < other.size(); k++ )
            (*this)[k] = other[k];
    }
};

DRAYN_NS_END
