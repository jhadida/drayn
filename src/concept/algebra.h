
//==================================================
// @title        iterable_algebra.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>

/**
 * Simple element-wise arithmetic operations for iterable classes.
 * 
 * The first section regroups element-wise operations with scalar output (dot, sum, product).
 * The second section regroups in-place element-wise operations (scalar inversion, cumulative sum/product).
 */

//  alg_dot( in1, in2 )
//  alg_sum( in )
//  alg_prod( in )
//  alg_cumsum( in )
//  alg_cumprod( in )
//  alg_invert( in )



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Dot product
template <class I1, class I2, class U = _iter_comtype<I1,I2> >
inline U alg_dot( const I1& in1, const I2& in2 ) 
    { return inner( in1, in2, U(0) ); }

// Sum of elements
template <class I, class U = _iter_valtype<I> >
inline U alg_sum( const I& in )
    { return reduce( in, U(0), f_add<U>() ); }

// Product of elements
template <class I, class U = _iter_valtype<I> >
inline U alg_prod( const I& in )
    { return reduce( in, U(1), f_mul<U>() ); }

// ----------  =====  ----------

// In-place inversion
template <class I>
inline void alg_invert( const I& in )
    { map( in, in, f_inv< _iter_valtype<I> >() ); }

// In-place cumulative sum
template <class I>
inline void alg_cumsum( const I& in ) 
    { cumul( in, f_add< _iter_valtype<I> >() ); }

// In-place cumulative product
template <class I>
inline void alg_cumprod( const I& in ) 
    { cumul( in, f_mul< _iter_valtype<I> >() ); }

DRAYN_NS_END
