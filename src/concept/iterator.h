
//==================================================
// @title        iterator.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iterator>

/**
 * Iterators implement methods next() and value(), and overload 
 * the boolean operator to check for remaining iterations.
 * 
 * The standard iterations in C++ normally look like:
 *      for ( auto it = x.begin(); it != x.end(); ++it )
 * and are usually shortened as:
 *      for ( auto &v: x )
 * 
 * This is annoying for index-based iterations, because the comparison 
 * operators (== and !=) require CRTP on the base iterator. 
 * 
 * Instead, we consider Python-like iterations:
 *      for ( auto it = x.iter(); it; it.next() )
 * 
 * This is slightly more verbose than range-based iterations, but
 * eliminates a lot of cumbersome code in the ABCs, and makes the
 * interfaces much cleaner.
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Value>
struct _iterator
{
    using value_type = Value;
    DRAYN_TRAITS(value_type)

    // to be implemented
    virtual operator bool() const =0;
    virtual ref_t value() const =0;
    virtual void next() =0;
    
    // R-value
    inline ref_t operator*  () const { return  value(); }
    inline ptr_t operator-> () const { return &value(); }
    inline ptr_t operator&  () const { return &value(); }
};

// ------------------------------------------------------------------------

template <class Value, class Forward>
struct _std_iterator:
    public std::iterator< std::random_access_iterator_tag, Value >
{
    DRAYN_TRAITS(Value)
    using self = Forward;

    // CRTP
    inline self& _it_crtp()
        { return static_cast<self&>(*this); }
    inline const self& _it_crtp() const 
        { return static_cast<const self&>(*this); }
    
    // ----------  =====  ----------
    
    virtual void swap( self& that ) =0;
    virtual void advance( sidx_t n ) =0;
    virtual sidx_t operator- ( const self& that ) const =0;
    virtual ref_t operator[] ( uidx_t n ) const =0;
    
    // ----------  =====  ----------

    inline ref_t value      () const { return operator[](0); }
    inline ref_t operator*  () const { return  value(); }
    inline ptr_t operator-> () const { return &value(); }
    inline ptr_t operator&  () const { return &value(); }
    
    inline bool operator== ( const self& that ) const { return operator-(that) == 0; }
    inline bool operator!= ( const self& that ) const { return !operator==(that); }

    inline bool operator< ( const self& that ) const { return operator-(that) < 0; }
    inline bool operator> ( const self& that ) const { return operator-(that) > 0; }

    inline bool operator<= ( const self& that ) const { return !operator>(that); }
    inline bool operator>= ( const self& that ) const { return !operator<(that); }

    inline self& operator++ () { this->advance( 1); return _it_crtp(); }
    inline self& operator-- () { this->advance(-1); return _it_crtp(); }

    inline self operator++ (int) { self t(_it_crtp()); this->advance( 1); return t; }
    inline self operator-- (int) { self t(_it_crtp()); this->advance(-1); return t; }

    inline self& operator+= ( sidx_t n ) { advance( n); return _it_crtp(); }
    inline self& operator-= ( sidx_t n ) { advance(-n); return _it_crtp(); }
    
    inline self operator+ ( sidx_t n ) const { self t(_it_crtp()); t.advance( n); return t; }
    inline self operator- ( sidx_t n ) const { self t(_it_crtp()); t.advance(-n); return t; }
};

DRAYN_NS_END
