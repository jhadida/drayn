
//==================================================
// @title        arithmetic.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>

/**
 * Requires an iterable interface with mutable value-type. 
 *
 * Called _arithmetic for short, but this is really a concept for
 * "arithmetic-compound-assignable" classes. Such classes provide
 * operator overloads for in-place element-wise arithmetic-compound
 * operations:
 *     +=, -=, *= and /=
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class V, class F,
    bool Enable = is_mutable<V>::value && is_arithmetic<V>::value >
struct _arithmetic {};

// ------------------------------------------------------------------------

#define DRAYN_ARITHMETIC_OPERATOR( Op, Fun )                                \
template <class I>                                                          \
void _arithmetic_ ## Fun ( const I& u, std::false_type ) const              \
{                                                                           \
    DRAYN_ASSERT_R( u.size() == _ar_crtp().size(),                          \
        "[drayn._arithmetic] Input size mismatch" )                         \
    combine( _ar_crtp(), u, _ar_crtp(), Fun<V>() );                         \
}                                                                           \
                                                                            \
template <class U>                                                          \
inline void _arithmetic_ ## Fun ( const U& u, std::true_type ) const        \
    { combine_s( _ar_crtp(), u, _ar_crtp(), Fun<V>() ); }                   \
                                                                            \
template <class U>                                                          \
inline const F& operator Op ( const U& u ) const                            \
    { _arithmetic_ ## Fun ( u, is_arithmetic<U>() ); return _ar_crtp(); }   \
                                                                            \
template <class U>                                                          \
inline F& operator Op ( const U& u )                                        \
    { _arithmetic_ ## Fun ( u, is_arithmetic<U>() ); return _ar_crtp(); }

// ------------------------------------------------------------------------

template <class V, class F>
struct _arithmetic<V,F,true>
{
    inline       F& _ar_crtp()       { return static_cast<      F&>(*this); }
    inline const F& _ar_crtp() const { return static_cast<const F&>(*this); }

    // ----------  =====  ----------

    DRAYN_ARITHMETIC_OPERATOR( +=, f_add );
    DRAYN_ARITHMETIC_OPERATOR( -=, f_sub );
    DRAYN_ARITHMETIC_OPERATOR( *=, f_mul );
    DRAYN_ARITHMETIC_OPERATOR( /=, f_div );
};

DRAYN_NS_END
