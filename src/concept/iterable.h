
//==================================================
// @title        iterable.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iostream>
#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Iterator>
struct _iterable
{
    using iterator_type = Iterator;
    using value_type = typename iterator_type::value_type;
    DRAYN_TRAITS(value_type)

    virtual uidx_t size() const =0;
    virtual Iterator iter() const =0;
};

// ------------------------------------------------------------------------

template <class I>
using _iter_valtype = typename std::remove_const< typename I::value_type >::type;

template <class I1, class I2>
using _iter_comtype = typename std::common_type< _iter_valtype<I1>, _iter_valtype<I2> >::type;

// ------------------------------------------------------------------------

template <class I, class V = typename I::value_type >
std::ostream& operator<< ( std::ostream& os, const _iterable<I>& it ) 
{
    const auto on = onstream<V>();

    os << "[Iterable " << it.size() << "]: ";
    for ( auto x = it.iter(); x; x.next() )
        on(os) << *x << ", ";

    return os << "\b\b " << std::endl;
}

DRAYN_NS_END
