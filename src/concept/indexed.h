
//==================================================
// @title        indexed.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <utility>
#include <stdexcept>
#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class Value>
struct _indexed: public _copyable<Value>
{
    DRAYN_TRAITS(Value)
    using value_type = Value;

    // to be implemented
    virtual bool valid() const =0;
    virtual uidx_t size() const =0;
    virtual ref_t operator[] (uidx_t) const =0;

    // basic features
    inline bool empty() const { return size() == 0; }
    inline operator bool() const { return valid() && !empty(); }

    ref_t at( uidx_t n ) const {
        if ( n >= size() ) throw std::out_of_range("[drayn._indexed] Index out of range.");
        return operator[](n);
    }

    inline ref_t front() const { return at(0); }
    inline ref_t back() const { return at(size()-1); }
};

// ------------------------------------------------------------------------

#ifdef DRAYN_SAFE_INDEXING

    template <class C, class V = typename C::value_type>
    inline constexpr V& safeIndex( const C& data, uidx_t k ) { return data.at(k); }

    inline void checkIndex( uidx_t k, uidx_t n ) {
        if ( k >= n ) throw std::out_of_range("[drayn.checkIndex] Index out of range.");
    }

#else 

    template <class C, class V = typename C::value_type>
    inline constexpr V& safeIndex( const C& data, uidx_t k ) { return data[k]; }

    inline void checkIndex( uidx_t k, uidx_t n ) {}

#endif

// ------------------------------------------------------------------------

template <class Value>
class indexed_iterator: public _iterator<Value>
{
public:

    DRAYN_TRAITS(Value)
    using ctn_type = _indexed<Value>;
    using value_type = Value;

    indexed_iterator()
        : m_ctn(nullptr), m_ind(0), m_size(0) {}

    indexed_iterator( const ctn_type *ctn )
        : m_ctn(ctn), m_ind(0)
        { m_size = ctn->size(); }

    inline operator bool() const { return m_ind < m_size; }
    inline ref_t value() const { return safeIndex(*m_ctn,m_ind); }
    inline void next() { m_ind += 1; }

protected:

    const ctn_type *m_ctn;
    uidx_t m_ind, m_size;
};

template <class C, class V = typename C::value_type>
inline constexpr indexed_iterator<V> iterate( const C& ctn )
    { return indexed_iterator<V>( &ctn ); }

// ------------------------------------------------------------------------

template <class Value>
class indexed_std_iterator: public _std_iterator< Value, indexed_std_iterator<Value> >
{
public:

    DRAYN_TRAITS(Value)
    using self = indexed_std_iterator<Value>;
    using parent = _std_iterator<Value,self>;
    using ctn_type = _indexed<Value>;
    using value_type = Value;

    indexed_std_iterator()
        : m_ctn(nullptr), m_size(0), m_ind(0) {}

    indexed_std_iterator( const ctn_type *ctn )
        : m_ctn(ctn), m_ind(0)
        { m_size = ctn->size(); }

    inline void swap( self& that ) {
        std::swap(
            stay(std::tie( m_ctn, m_ind, m_size )),
            stay(std::tie( that.m_ctn, that.m_ind, that.m_size ))
        );
    }

    void advance( sidx_t n ) {
        if ( n > static_cast<sidx_t>(m_size-m_ind) || -n > static_cast<sidx_t>(m_ind) )
            throw std::out_of_range("[drayn.indexed_std_iterator] Invalid increment.");
        m_ind += n;
    }

    using parent::operator-; // prevent shadowing of parent's operator
    sidx_t operator- ( const self& that ) const {
        if ( that.m_ctn != m_ctn )
            throw std::invalid_argument("[drayn.indexed_std_iterator] Non-comparable iterator.");
        return static_cast<sidx_t>(m_ind) - static_cast<sidx_t>(that.m_ind);
    }

    inline ref_t operator[] ( uidx_t n ) const 
        { return safeIndex(*m_ctn,m_ind); }

protected:

    const ctn_type *m_ctn;
    uidx_t m_ind, m_size;
};

template <class C, class V = typename C::value_type>
inline constexpr indexed_std_iterator<V> iterate_std( const C& ctn )
    { return indexed_std_iterator<V>( &ctn ); }

DRAYN_NS_END
