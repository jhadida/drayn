
//==================================================
// @title        iterable_functional.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

/**
 * Different functional patterns of iteration on either one or two iterators.
 * This is essentially turning the imperative code using iterable classes into a weak 
 * functional language geared towards element-wise processing.
 *
 * Some functions are provided for convenience only, eg the function "fill" below is 
 * equivalent to:
 *     apply( in, [val]( T& x ){ x=val; } )
 */

// apply( in, uop )                     in[] = uop( in[] )
// fill( in, val )                      in[] = val
// map( in, out, uop )                  out[] = uop( in[] )
// copy( in, out )                      out[] = in[]
// combine( in1, in2, out, bop )        out[] = bop( in1[], in2[] )
// combine_s( in, val, out, bop )       out[] = bop( in[], val )
// reduce( in, start, bop )             start = bop( start, in[] )
// reduce( in, start, bop, uop )        start = bop( start, uop(in[]) )
// cumul( in, bop )                     in[+1] = bop( in[+1], in[+0] )
// inner( in1, in2, start, bop )        start += bop( in1[], in2[] )
// inner( in1, in2, start )             start += in1[] * in2[]
// inner_s( in, val, start, bop )       start += bop( in[], val )
// inner_s( in, val, start )            start += in[] * val
// all( in, test )                      when ( !test( in[] ) ) return false;
// all( in1, in2, test )                when ( !test( in1[], in2[] ) ) return false;
// all_s( in, val, test )               when ( !test( in[], val ) ) return false;
// any( in, test )                      when ( test( in[] ) ) return true;
// any( in1, in2, test )                when ( test( in1[], in2[] ) ) return true;
// any_s( in, val, test )               when ( test( in[], val ) ) return true;




        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Apply function to elements
template <class I, class UnaryOperator>
inline void apply( I in, UnaryOperator op )
{
    for ( auto in_=in.iter(); in_; in_.next() ) op( *in_ );
}

// Fill with value 
template <class I, class T>
inline void fill( I in, const T& val )
{
    for ( auto in_=in.iter(); in_; in_.next() ) *in_ = val;
}

// Fill A from output of unary function applied to elements of B
template <class I, class O, class UnaryOperator>
void map( I in, O out, UnaryOperator op )
{
    auto in_ = in.iter();
    auto out_ = out.iter();
    for (; in_ && out_; in_.next(), out_.next() ) *out_ = op( *in_ );
}

template <class I, class O>
void copy( I in, O out )
{
    auto in_ = in.iter();
    auto out_ = out.iter();
    for (; in_ && out_; in_.next(), out_.next() ) *out_ = *in_;
}

// ------------------------------------------------------------------------

// Fill A from output of binary function applied to B and C
template <class I1, class I2, class O, class BinaryOperator>
void combine( I1 in1, I2 in2, O out, BinaryOperator op )
{
    auto in1_ = in1.iter();
    auto in2_ = in2.iter();
    auto out_ = out.iter();
    for (; in1_ && in2_ && out_; in1_.next(), in2_.next(), out_.next() ) *out_ = op( *in1_, *in2_ );
}

template <class I, class T, class O, class BinaryOperator>
void combine_s( I in, T val, O out, BinaryOperator op )
{
    auto in_ = in.iter();
    auto out_ = out.iter();
    for (; in_ && out_; in_.next(), out_.next() ) *out_ = op( *in_, val );
}

// ------------------------------------------------------------------------

// Recursively set scalar A to result of binary function applied to A and element of B
template <class I, class T, class BinaryOperator>
inline T reduce( I in, T start, BinaryOperator bop )
{
    for ( auto in_=in.iter(); in_; in_.next() ) start = bop( start, *in_ );
    return start;
}

template <class I, class T, class BinaryOperator, class UnaryOperator>
inline T reduce( I in, T start, BinaryOperator bop, UnaryOperator uop )
{
    for ( auto in_=in.iter(); in_; in_.next() ) start = bop( start, uop( *in_ ) );
    return start;
}

// ------------------------------------------------------------------------

// Recursively and cumulatively apply operation to elements of iterable A
template <class I, class BinaryOperator>
void cumul( I in, BinaryOperator bop )
{
    // 1-step delayed iterator
    auto in_ = in.iter();
    auto lag_ = in_;

    if ( in_ ) for ( in_.next(); in_; in_.next(), lag_.next() )
        *in_ = bop( *in_, *lag_ );
}

// ------------------------------------------------------------------------

// Same but with two input arrays
template <class I1, class I2, class T, class BinaryOperator>
T inner( I1 in1, I2 in2, T start, BinaryOperator bop )
{
    auto in1_ = in1.iter();
    auto in2_ = in2.iter();
    for (; in1_ && in2_; in1_.next(), in2_.next() ) start += bop( *in1_, *in2_ );
    return start;
}

template <class I1, class I2, class T>
inline T inner( I1 in1, I2 in2, T start )
{
    auto in1_ = in1.iter();
    auto in2_ = in2.iter();
    for (; in1_ && in2_; in1_.next(), in2_.next() ) start += (*in1_) * (*in2_);
    return start;
}

template <class I, class U, class T, class BinaryOperator>
inline T inner_s( I in, U val, T start, BinaryOperator bop )
{
    for ( auto in_=in.iter(); in_; in_.next() ) start += bop( *in_, val );
    return start;
}

template <class I, class U, class T>
inline T inner_s( I in, U val, T start )
{
    for ( auto in_=in.iter(); in_; in_.next() ) start += (*in_) * val;
    return start;
}

// ------------------------------------------------------------------------

// Test if all elements of an iterator verify a predicate
template <class I, class Predicate>
inline bool all( I in, Predicate test )
{
    for ( auto in_=in.iter(); in_; in_.next() ) if ( !test( *in_ ) ) return false;
    return true;
}

template <class I1, class I2, class Predicate>
bool all( I1 in1, I2 in2, Predicate test )
{
    auto in1_ = in1.iter();
    auto in2_ = in2.iter();
    for (; in1_ && in2_; in1_.next(), in2_.next() ) if ( !test( *in1_, *in2_ ) ) return false;
    return true;
}

template <class I, class T, class Predicate>
inline bool all_s( I in, const T& val, Predicate test )
{
    for ( auto in_=in.iter(); in_; in_.next() ) if ( !test( *in_, val ) ) return false;
    return true;
}

// ------------------------------------------------------------------------

// Test if any element of an iterator verifies a predicate
template <class I, class Predicate>
inline bool any( I in, Predicate test )
{
    for ( auto in_=in.iter(); in_; in_.next() ) if ( test( *in_ ) ) return true;
    return false;
}

template <class I1, class I2, class Predicate>
bool any( I1 in1, I2 in2, Predicate test )
{
    auto in1_ = in1.iter();
    auto in2_ = in2.iter();
    for (; in1_ && in2_; in1_.next(), in2_.next() ) if ( test( *in1_, *in2_ ) ) return true;
    return false;
}

template <class I, class T, class Predicate>
inline bool any_s( I in, const T& val, Predicate test )
{
    for ( auto in_=in.iter(); in_; in_.next() ) if ( test( *in_, val ) ) return true;
    return false;
}

DRAYN_NS_END
