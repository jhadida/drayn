
//==================================================
// @title        logging.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <stdexcept>
#include <cstdio>



        /********************     **********     ********************/
        /********************     **********     ********************/



//-------------------------------------------------------------
//  MESSAGE PRINTING
//-------------------------------------------------------------

// Warning/error messages

#define DRAYN_DECORATE_(txt) "[>= " txt " =<] "
#define DRAYN_TRACEBACK_ drayn_print( "### '%s' (l.%d):", __FILE__, __LINE__ )

#define DRAYN_ERROR( msg, args... ) { DRAYN_TRACEBACK_; throw std::runtime_error(drayn_format( DRAYN_DECORATE_("ERROR") msg, ##args )); }
#define DRAYN_WARN( msg, args... )  { DRAYN_TRACEBACK_; drayn_printerr( DRAYN_DECORATE_("WARNING") msg, ##args ); }
#define DRAYN_INFO( msg, args... )  { DRAYN_TRACEBACK_; drayn_print( DRAYN_DECORATE_("INFO") msg, ##args ); }
#define DRAYN_DEBUG( msg, args... ) { DRAYN_TRACEBACK_; drayn_print( DRAYN_DECORATE_("DEBUG") msg, ##args ); }


//-------------------------------------------------------------
// ASSERTIONS
//-------------------------------------------------------------

/**
 * Errors cause the program to exit with EXIT_FAILURE
 */

// Print ERROR messages
#define DRAYN_REJECT_ERR( cdt, msg, args... ) { if(cdt) DRAYN_ERROR(msg,##args) }
#define DRAYN_ASSERT_ERR( cdt, msg, args... ) DRAYN_REJECT_ERR(!(cdt),msg,##args)

/**
 * Warning messages are always active
 */

// Simple warn
#define DRAYN_REJECT( cdt, msg, args... ) { if(cdt) DRAYN_WARN(msg,##args) }
#define DRAYN_ASSERT( cdt, msg, args... ) DRAYN_REJECT(!(cdt),msg,##args)

// Warn and return
#define DRAYN_REJECT_R( cdt, msg, args... ) { if(cdt) {DRAYN_WARN(msg,##args) return;} }
#define DRAYN_ASSERT_R( cdt, msg, args... ) DRAYN_REJECT_R(!(cdt),msg,##args)

// Warn and return false
#define DRAYN_REJECT_RF( cdt, msg, args... ) { if(cdt) {DRAYN_WARN(msg,##args) return false;} }
#define DRAYN_ASSERT_RF( cdt, msg, args... ) DRAYN_REJECT_RF(!(cdt),msg,##args)

// Warn and return value
#define DRAYN_REJECT_RV( cdt, val, msg, args...  ) { if(cdt) {DRAYN_WARN(msg,##args) return (val);} }
#define DRAYN_ASSERT_RV( cdt, val, msg, args...  ) DRAYN_REJECT_RV(!(cdt),(val),msg,##args)

// Warn and goto
#define DRAYN_REJECT_GT( cdt, gt, msg, args...  ) { if(cdt) {DRAYN_WARN(msg,##args) goto gt;} }
#define DRAYN_ASSERT_GT( cdt, gt, msg, args...  ) DRAYN_REJECT_GT(!(cdt),gt,msg,##args)


//-------------------------------------------------------------
// DEBUGGING
//-------------------------------------------------------------

/**
 * Activate DEBUG messages with the DRAYN_SHOW_DEBUG flag.
 * Print a warning if the condition is true/false (DRAYN_REJECT/DRAYN_ASSERT).
 * Then return something, or not, or goto somwhere.
 */

#ifdef DRAYN_SHOW_DEBUG

    // force info
    #ifndef DRAYN_SHOW_INFO
    #define DRAYN_SHOW_INFO
    #endif

    // force safe indexing
    #ifndef DRAYN_SAFE_INDEXING
    #define DRAYN_SAFE_INDEXING
    #endif
    

    #define DRAYN_DREJECT_ERR( cdt, msg, args... ) { if(cdt) DRAYN_ERROR(msg,##args) }
    #define DRAYN_DASSERT_ERR( cdt, msg, args... ) DRAYN_DREJECT_ERR(!(cdt),msg,##args)

    // Simple warn
    #define DRAYN_DREJECT( cdt, msg, args... ) { if(cdt) DRAYN_DEBUG(msg,##args) }
    #define DRAYN_DASSERT( cdt, msg, args... ) DRAYN_DREJECT(!(cdt),msg,##args)

    // Warn and return
    #define DRAYN_DREJECT_R( cdt, msg, args... ) { if(cdt) {DRAYN_DEBUG(msg,##args) return;} }
    #define DRAYN_DASSERT_R( cdt, msg, args... ) DRAYN_DREJECT_R(!(cdt),msg,##args)

    // Warn and return false
    #define DRAYN_DREJECT_RF( cdt, msg, args... ) { if(cdt) {DRAYN_DEBUG(msg,##args) return false;} }
    #define DRAYN_DASSERT_RF( cdt, msg, args... ) DRAYN_DREJECT_RF(!(cdt),msg,##args)

    // Warn and return value
    #define DRAYN_DREJECT_RV( cdt, val, msg, args... ) { if(cdt) {DRAYN_DEBUG(msg,##args) return (val);} }
    #define DRAYN_DASSERT_RV( cdt, val, msg, args... ) DRAYN_DREJECT_RV(!(cdt),(val),msg,##args)

    // Warn and goto
    #define DRAYN_DREJECT_GT( cdt, gt, msg, args... ) { if(cdt) {DRAYN_DEBUG(msg,##args) goto gt;} }
    #define DRAYN_DASSERT_GT( cdt, gt, msg, args... ) DRAYN_DREJECT_GT(!(cdt),gt,msg,##args)

#else

    #undef  DRAYN_DEBUG
    #define DRAYN_DEBUG( msg, args... )

    #define DRAYN_DREJECT_ERR( cdt, msg, args... )
    #define DRAYN_DASSERT_ERR( cdt, msg, args... )

    #define DRAYN_DREJECT( cdt, msg, args... )
    #define DRAYN_DASSERT( cdt, msg, args... )

    // Warn and return
    #define DRAYN_DREJECT_R( cdt, msg, args... )
    #define DRAYN_DASSERT_R( cdt, msg, args... )

    // Warn and return false
    #define DRAYN_DREJECT_RF( cdt, msg, args... )
    #define DRAYN_DASSERT_RF( cdt, msg, args... )

    // Warn and return value
    #define DRAYN_DREJECT_RV( cdt, val, msg, args... )
    #define DRAYN_DASSERT_RV( cdt, val, msg, args... )

    // Warn and goto
    #define DRAYN_DREJECT_GT( cdt, gt, msg, args... )
    #define DRAYN_DASSERT_GT( cdt, gt, msg, args... )

#endif


//-------------------------------------------------------------
// INFORMATION
//-------------------------------------------------------------

/**
 * Activate INFO messages with DRAYN_SHOW_INFO
 */

#ifdef DRAYN_SHOW_INFO

    #define DRAYN_IREJECT_ERR( cdt, msg, args... ) { if(cdt) DRAYN_ERROR(msg,##args) }
    #define DRAYN_IASSERT_ERR( cdt, msg, args... ) DRAYN_IREJECT_ERR(!(cdt),msg,##args)

    // Simple warn
    #define DRAYN_IREJECT( cdt, msg, args... ) { if(cdt) DRAYN_INFO(msg,##args) }
    #define DRAYN_IASSERT( cdt, msg, args... ) DRAYN_IREJECT(!(cdt),msg,##args)

#else

    #undef  DRAYN_INFO
    #define DRAYN_INFO( msg, args... )

    #define DRAYN_IREJECT_ERR( cdt, msg, args... )
    #define DRAYN_IASSERT_ERR( cdt, msg, args... )

    #define DRAYN_IREJECT( cdt, msg, args... )
    #define DRAYN_IASSERT( cdt, msg, args... )

#endif
