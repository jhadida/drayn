
//==================================================
// @title        randoms.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

#ifdef DRAYN_RAND_DETERMINISTIC_SEED
    wminst_rand deterministic_seed_generator::engine( DRAYN_RAND_DETERMINISTIC_SEED );
#else
    wminst_rand deterministic_seed_generator::engine(0);
#endif

#ifdef DRAYN_RAND_FIXED_SEED
    size_t fixed_seed_generator::seed = DRAYN_RAND_FIXED_SEED;
#else
    size_t fixed_seed_generator::seed = 0;
#endif

DRAYN_NS_END
