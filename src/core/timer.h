
//==================================================
// @title        timer.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <ctime>
#include <chrono>

#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <functional>
#include <unordered_map>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

/**
 * String representation of a duration in seconds.
 * Currently up to micro-second precision.
 */
struct time_format
{
    unsigned d, h, m, s, ms, us;

    time_format()
        { clear(); }
    time_format( double sec )
        { format_seconds(sec); }

    // Reset duration
    void clear();

    // Set duration
    void format_seconds( double sec );

    // Get string representation
    const char* to_string() const;
};

inline const char* sec2str( double sec ) 
    { return time_format(sec).to_string(); }

// ------------------------------------------------------------------------

// A stop-watch estimates timeleft from a fraction between 0 and 1.
class timer
{
protected:

    using seconds = std::chrono::duration< double, std::ratio<1,1> >;
    using steady_clock = std::chrono::steady_clock;

    steady_clock::time_point m_start;

public:

    timer() { reset(); }

    // Reset the timer
    inline void reset()
        { m_start = steady_clock::now(); }

    // Runtime in seconds
    inline double runtime() const
        { return std::chrono::duration_cast<seconds>( steady_clock::now() - m_start ).count(); }

    // Evaluate timeleft in seconds, assuming linear progress
    inline double timeleft( double frac ) const 
        { double sec = runtime(); return sec/frac - sec; }

    inline const char* timeleft_str( double frac ) const
        { return sec2str(timeleft(frac)); }
};

// ------------------------------------------------------------------------

// Used internally by time_monitor
class stopwatch
{
protected:

    bool m_running;
    double m_runtime;
    timer m_timer;

public:

    stopwatch()
        { reset(); }

    // Clear the timer
    void clear();

    // Timer controls
    void reset();
    void start();
    void stop();

    inline bool running() const
        { return m_running; }
    inline double runtime() const
        { return m_runtime; }
    inline const char* runtime_str() const
        { return sec2str(m_runtime); }
};

// ------------------------------------------------------------------------

/**
 * Monitors can be used to time implementations and generate reports.
 * The report is very basic for now (execution time by label, sorted descending).
 *
 * Example:
 *
 * drayn::time_monitor TM;
 *
 * TM.start('mylabel'); // Create a label
 * TM.stop('mylabel');  // ... do something time-consuming
 *
 * TM.report();         // print time-consumptions in descending order
 */
class time_monitor
{
public:

    using hash_table = std::unordered_map< std::string, stopwatch >;
    using report_type = std::map< double, std::string, std::greater_equal<double> >;

    time_monitor() {}

    // Clear all timers
    inline void clear() { m_timers.clear(); }

    // Start/stop labeled timers
    inline void start( const std::string& key ) { m_timers[key].start(); }
    inline void stop( const std::string& key ) { m_timers[key].stop(); }

    // Print output
    void report() const;

protected:
    hash_table m_timers;
};

DRAYN_NS_END
