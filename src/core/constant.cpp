
//==================================================
// @title        constant.cpp
// @author       Jonathan Hadida
// @contact      Jhadida87 [at] gmail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

const double math::Pi = 4*atan(1.0);
const double math::Tau = 8*atan(1.0);

DRAYN_NS_END
