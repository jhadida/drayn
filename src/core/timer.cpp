
//==================================================
// @title        timer.cpp
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

void time_format::clear()
{
    d = h = m = s = ms = us = 0;
}

void time_format::format_seconds( double sec )
{
    d  = std::floor(sec/86400);
        sec -= d*86400;
    h  = std::floor(sec/3600);
        sec -= h*3600;
    m  = std::floor(sec/60);
        sec -= m*60;
    s  = std::floor(sec);
        sec -= s;
    ms = std::floor(sec*1000);
        sec -= ms/1000.0;
    us = std::floor(sec*1e6);
}

const char* time_format::to_string() const
{
    if ( d > 0 )
        return drayn_format( "%u days and %02u:%02u:%02u", d, h, m, s );
    else if ( h > 0 )
        return drayn_format( "%u h %02u min %02u sec", h, m, s );
    else if ( m > 0 )
        return drayn_format( "%u min %02u sec", m, s );
    else if ( s > 0 )
        return drayn_format( "%u.%03u sec", s, ms );
    else
        return drayn_format( "%03u.%03u ms", ms, us );
}

// ------------------------------------------------------------------------

void stopwatch::clear()
{
    DRAYN_REJECT( m_running, "[drayn.stopwatch] The timer is still running!" );
    reset();
}

void stopwatch::reset()
{
    m_running = false;
    m_runtime = 0.0;
}

void stopwatch::start()
{ if ( !m_running ) {

    m_timer.reset();
    m_running = true;
} }

void stopwatch::stop()
{ if ( m_running ) {

    m_running = false;
    m_runtime += m_timer.runtime();
} }

// ------------------------------------------------------------------------

void time_monitor::report() const
{
    // first pass to determine key length
    size_t klen = 0;
    for ( auto& t: m_timers ) {
        DRAYN_REJECT( t.second.running(), "Timer '%s' is still running!", t.first.c_str() )
        if ( t.first.size() > klen ) klen = t.first.size();
    }

    // second pass to build the report and sort durations
    std::stringstream ss;
    report_type out;

    for ( auto& t: m_timers ) {
        ss.str("");
        ss << "\t[" << std::setw(klen) << t.first << "]: " << t.second.runtime_str();
        out[ t.second.runtime() ] = ss.str();
    }

    // final pass to print report
    std::cout<< "[Timer report]:" << std::endl;
    for ( auto& r : out )
        std::cout<< r.second << std::endl;
}

DRAYN_NS_END
