
//==================================================
// @title        onstream.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

// Numeric output stream
// TODO: also format complex numbers

#include <functional>
#include <iostream>
#include <iomanip>
#include <ios>



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



DRAYN_NS_START

template < 
    class T, 
    int Selector = select< 
        std::is_integral<T>::value, 
        std::is_floating_point<T>::value 
    >() 
>
struct onstream_width {};

template <class T>
struct onstream_width<T,1> // integral specialisation
{
    static unsigned value;
    static void set( unsigned w ) { value = w; }
};

template <class T>
struct onstream_width<T,2> // floating-point specialisation
{
    static unsigned value;
    static void set( unsigned w ) { value = w; }
};

struct onstream_precision
{
    static unsigned value;
    static void set( unsigned p ) { value = p; }
};

// ------------------------------------------------------------------------

template <class T> unsigned onstream_width<T,1>::value = DRAYN_OUTPUT_INTEGER_WIDTH;
template <class T> unsigned onstream_width<T,2>::value = DRAYN_OUTPUT_FLOAT_WIDTH;

// ------------------------------------------------------------------------

template <class T, bool isNum = is_numeric<T>::value>
struct onstream
{
    inline std::ostream& operator() ( std::ostream& os ) const { return os; }
};

template <class T>
struct onstream<T,true>
{
    inline std::ostream& operator() ( std::ostream& os ) const {
        return os<< std::fixed << std::setw( onstream_width<T>::value )
            << std::setprecision(onstream_precision::value);
    }
};

DRAYN_NS_END
