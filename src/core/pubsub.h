
//==================================================
// @title        pubsub.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <memory>
#include <string>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

/**
 * Relays are intermediates between a Channel (see below) and a callback function.
 * Specifically the method 'call' passes event data coming from the channel to the callback function.
 *
 * Use the methods 'enable'/'disable' to bind the relay to a given callback function.
 * Make sure the callback function stays alive as long as the relay is used (eg in the case of member functions).
 */
template <class Data>
struct ps_relay
{
    using self = ps_relay<Data>;
    using data_type = Data;
    using callback_type = std::function<void(Data&)>;

    callback_type callback;
    
    // ----------  =====  ----------
    
    ps_relay() {}
    ps_relay( callback_type C ) { enable(C); }

    ~ps_relay(){ disable(); }

    inline bool valid() const { return static_cast<bool>(callback); }
    inline operator bool() const { return valid(); }

    inline void call( Data& data )
        { if ( callback ) callback(data); }

    inline self& enable( callback_type C ) { callback = C; return *this; }
    inline self& disable() { callback_type().swap(callback); return *this; }

    inline self& operator= ( callback_type C ) 
        { enable(C); return *this; }
};

// ------------------------------------------------------------------------

/**
 * Channels implement the publish/subscribe methods for event-related message passing.
 *
 * Subscription methods return a 'subscriber', which is a shared pointer to a Relay (see above).
 * Specifically, a Relay is created internally by the Channel, and the shared pointer interface is
 * used to keep track of external uses of this relay (using use_count).
 * When all shared copies are destroyed, ie when the only remaining instance of a relay is the original
 * one created in the Channel, the relay is destroyed along with the subscription.
 * Note that you can subscribe to a channel without specifying a callback function, in which case the
 * subscriber is disabled until you specify one.
 */
template <class Data>
class ps_channel
{
public:

    using data_type = Data;
    using relay_type = ps_relay<Data>;

    using subscriber_type = std::shared_ptr<relay_type>;
    using callback_type = typename relay_type::callback_type;
    
    // ----------  =====  ----------
    
    ps_channel(){}

    void publish( Data& data ) 
    {
        auto it = m_subs.begin();
        while ( it != m_subs.end() )
            if ( it->use_count() == 1 )
                it = m_subs.erase(it);
            else
                (*it++)->call(data);
    }

    inline subscriber_type subscribe()
        { return *( m_subs.emplace(new relay_type()) .first ); }

    inline subscriber_type subscribe( callback_type c )
        { return *( m_subs.emplace(new relay_type(c)) .first ); }

    void unsubscribe( const subscriber_type& s ) {
        auto it = m_subs.find(s);
        if ( it != m_subs.end() )
            m_subs.erase(it);
    }

    inline uidx_t subscriber_count() const
        { return m_subs.size(); }


protected:

    struct _relay_hasher
    {
        inline size_t operator() ( const subscriber_type& s ) const {
            static std::hash<void*> Hash;
            return Hash( (void*) s.get() );
        }
    };

    std::unordered_set<subscriber_type,_relay_hasher> m_subs;
};

// ------------------------------------------------------------------------

/**
 * A hub is basically a HashTable of Channels.
 * This allows to separate different event-systems from each other.
 */
template <class Data>
class ps_hub
{
public:

    using data_type = Data;
    using channel_type = ps_channel<Data>;
    
    // ----------  =====  ----------
    
    inline channel_type& channel( size_t id )
        { return m_channels[id]; }

    inline channel_type& channel( const std::string& name ) {
        static std::hash< std::string > Hash;
        return m_channels[Hash(name)];
    }

    inline bool has_channel( size_t id ) const
        { return m_channels.find(id) != m_channels.end(); }

protected:

    std::unordered_map<size_t,channel_type> m_channels;
};

// ------------------------------------------------------------------------

// Aliases
template <class D>
using ps_subscriber = typename ps_channel<D>::subscriber_type;

template <class D> using slot   = ps_subscriber<D>;
template <class D> using signal = ps_channel<D>;
template <class D> using events = ps_hub<D>;

DRAYN_NS_END
