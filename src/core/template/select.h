
/**
 * Return the index of the first template type that is true.
 * If none is true, or there is no input, return 0.
 * 
 * See: https://stackoverflow.com/a/32886575/472610
 */
template<bool B0=false, bool... Bs>
constexpr unsigned select( unsigned I = 1 )
{
    return B0 ? I : select<Bs...>(I+1);
}

template<>
constexpr unsigned select<false>( unsigned I )
{
    return 0;
}
