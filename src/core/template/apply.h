
//==================================================
// @title        apply.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <tuple>
#include <cstddef>
#include <utility>
#include <type_traits>

/*
 * Call function with variable args packed in tuple:
 *     apply( F, Tuple )
 *
 * For calling methods, use lambda closures.
 *
 * See: https://stackoverflow.com/a/12650100/472610
 */


        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START  

template <unsigned N>
struct _apply_obj {
    template <typename F, typename T, typename... A>
    static inline auto apply( F&& f, T&& t, A&& ...a ) 
        -> decltype(_apply_obj<N-1>::apply(
            std::forward<F>(f), std::forward<T>(t), 
            std::get<N-1>(std::forward<T>(t)), std::forward<A>(a)...
        ))
    {
        return _apply_obj<N-1>::apply(
            std::forward<F>(f), std::forward<T>(t), 
            std::get<N-1>(std::forward<T>(t)), std::forward<A>(a)... );
    }
};

template <>
struct _apply_obj<0> {
    template <typename F, typename T, typename... A>
    static inline auto apply( F&& f, T&& t, A&& ...a ) 
        -> decltype(std::forward<F>(f)( std::forward<A>(a)... ))
    {
        return std::forward<F>(f)( std::forward<A>(a)... );
    }
};

template <typename F, typename T>
inline auto apply( F&& f, T&& t )
    -> decltype(_apply_obj<
        std::tuple_size< typename std::decay<T>::type >::value
    >::apply( std::forward<F>(f), std::forward<T>(t) ))
{
    return _apply_obj<
        std::tuple_size< typename std::decay<T>::type >::value
    >::apply( std::forward<F>(f), std::forward<T>(t) );
}

DRAYN_NS_END
