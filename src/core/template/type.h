
// see: https://stackoverflow.com/a/22592618/472610
template<template<class> class T, class U>
struct isDerivedFrom
{
private:
    template<class V>
    static decltype(static_cast<const T<V>&>(std::declval<U>()), std::true_type{})
    test(const T<V>&);

    static std::false_type test(...);
public:
    static constexpr bool value = decltype(isDerivedFrom::test(std::declval<U>()))::value;
};

// Inverse of std::move
template <class T>
constexpr T& stay(T&& t) 
    { return t; }
