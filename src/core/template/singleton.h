
template <class T> 
struct singleton 
    { static thread_local T instance; };

template <class T> 
thread_local T singleton<T>::instance = T();

// ----------  =====  ----------

template <class T>
constexpr T& static_val()
    { return singleton<T>::instance; }

template <class T>
constexpr T* static_ptr()
    { return &singleton<T>::instance; }
    