
//==================================================
// @title        template.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <tuple>
#include <cstddef>
#include <utility>
#include <complex>
#include <iterator>
#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// variadic template
#include "select.h"
// #include "apply.h" // funky stuff

// design patterns
// #include "singleton.h" // NOTE: seems to be causing issues with LSBM
#include "factory.h"

// type traits
#include "boolean.h"
#include "value.h"
#include "type.h"

DRAYN_NS_END
