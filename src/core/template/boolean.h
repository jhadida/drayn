
template <bool B>
struct bool_type
    : public std::integral_constant <bool,B> {};

template <class T>
struct bool_negate
    : public std::integral_constant <bool, !T::value> {};

template <class T, class U>
struct bool_equal
    : public std::integral_constant <bool, !(T::value ^ U::value) > {};
