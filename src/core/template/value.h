
template <class T>
struct is_complex: bool_type<false> {};

template <class T>
struct is_complex< std::complex<T> >: bool_type<true> {};

// ------------------------------------------------------------------------

template<class T>
struct is_numeric
   : public std::integral_constant < bool,
       std::is_integral<T>::value || std::is_floating_point<T>::value > {};

template <class T>
struct is_mutable
   : public std::integral_constant < bool,
      !std::is_const<T>::value > {};

// ------------------------------------------------------------------------
 
template<class T>
struct is_arithmetic
    : public std::integral_constant < bool,
        is_numeric<T>::value > {};

template<class T>
struct is_comparable
    : public std::integral_constant < bool,
        is_arithmetic<T>::value > {};
       