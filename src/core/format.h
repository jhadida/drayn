
//==================================================
// @title        format.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cstring>
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <utility>



        /********************     **********     ********************/
        /********************     **********     ********************/



template <class... Args>
const char* drayn_format( const char *fmt, Args&&... args ) {
    static std::vector<char> buf;
    const size_t n = snprintf( nullptr, 0, fmt, std::forward<Args>(args)... ) + 1; // extra space for '\0'
    if ( n > buf.size() ) buf.resize(n);
    snprintf( &buf[0], n, fmt, std::forward<Args>(args)... );
    return &buf[0];
}

inline const char* drayn_format( const char *fmt ) { // quick version
    return fmt;
}

template <class... Args>
inline void drayn_print( const char *fmt, Args&& ...args ) {
    std::cout << drayn_format( fmt, std::forward<Args>(args)... ) << std::endl;
}

template <class... Args>
inline void drayn_printerr( const char *fmt, Args&& ...args ) {
    std::cerr << drayn_format( fmt, std::forward<Args>(args)... ) << std::endl;
}
