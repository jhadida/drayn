
//==================================================
// @title        traits.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iterator>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Forwad core typedefs from input Class
#define DRAYN_FW_TRAITS(Class)          \
typedef typename Class::val_t    val_t; \
typedef typename Class::ref_t    ref_t; \
typedef typename Class::ptr_t    ptr_t; \
typedef typename Class::cval_t  cval_t; \
typedef typename Class::cref_t  cref_t; \
typedef typename Class::cptr_t  cptr_t;

// ------------------------------------------------------------------------

/**
 * Core type traits.
 */
template <class T>
struct core_traits
{
    typedef T  val_t;
    typedef T& ref_t;
    typedef T* ptr_t;

    typedef const T  cval_t;
    typedef const T& cref_t;
    typedef const T* cptr_t;
};

#define DRAYN_TRAITS(Type) DRAYN_FW_TRAITS( core_traits<Type> )

/**
 * Core traits from standard iterator traits.
 */
template <class T>
struct iterator_traits
{
    typedef typename std::iterator_traits<T>::value_type  val_t;
    typedef typename std::iterator_traits<T>::reference   ref_t;
    typedef typename std::iterator_traits<T>::pointer     ptr_t;

    typedef const val_t   cval_t;
    typedef const val_t&  cref_t;
    typedef const val_t*  cptr_t;
};

#define DRAYN_IT_TRAITS(Type) DRAYN_FW_TRAITS( iterator_traits<Type> )

DRAYN_NS_END
