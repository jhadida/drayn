
//==================================================
// @title        typedefs.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cstdint>
#include <cinttypes>
#include <initializer_list>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Short alias for initializer lists
template <class T>
using inilst = std::initializer_list<T>;

// An empty type
struct void_type {};

// ------------------------------------------------------------------------

/**
 * Orders of magnitude:
 *
 * 2^64: 1.84 e 19   uint64_t, aka unsigned long
 * 2^32: 4.29 e  9   uint32_t, aka unsigned
 * 2^16: 6.55 e  5   uint16_t, aka ushort
 * 2^ 8:  256         uint8_t, aka uchar
 */

// By default, all numbers are 64 bits
#ifdef DRAYN_USE_32BITS_NUMBER

    namespace integer {
        typedef uint32_t  uint_t;
        typedef int32_t   sint_t;
    }

    #define uint_f "%" PRIu32
    #define sint_f "%" PRId32

#else

    namespace integer {
        typedef uint64_t  uint_t;
        typedef int64_t   sint_t;
    }

    #define uint_f "%" PRIu64
    #define sint_f "%" PRId64

#endif

// By default, all indices are 32 bits
#ifdef DRAYN_USE_32BITS_INDEX

    // Size types
    namespace integer {
        typedef uint32_t  uidx_t;
        typedef int32_t   sidx_t;
    }

    // #define uidx_f "%zu" // for std::size_t
    #define uidx_f "%" PRIu32
    #define sidx_f "%" PRId32

#else

    // Size types
    namespace integer {
        typedef uint64_t  uidx_t;
        typedef int64_t   sidx_t;
    }

    // #define uidx_f "%zu" // for std::size_t
    #define uidx_f "%" PRIu64
    #define sidx_f "%" PRId64

#endif

// Use the previous types within the DRAYN namespace
using namespace integer;

DRAYN_NS_END
