
//==================================================
// @title        constants.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>
#include <limits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// mathematical & physical constants

struct math 
{
    static const double Pi;
    static const double Tau;
};

struct phys
{

};

// ------------------------------------------------------------------------

// store useful numeric limits more concisely
template <class T>
struct c_num
{
    static const T max;
    static const T min;
    static const T low;

    static const T eps;
    static const T inf;

    static const bool is_signed;
    static const bool is_integral;
};

template <class T> const T c_num<T>::max = std::numeric_limits<T>::max();
template <class T> const T c_num<T>::min = std::is_integral<T>::value ? T(0) : std::numeric_limits<T>::min();
template <class T> const T c_num<T>::low = std::numeric_limits<T>::lowest();

template <class T> const T c_num<T>::eps = std::numeric_limits<T>::epsilon();
template <class T> const T c_num<T>::inf = std::numeric_limits<T>::infinity();

template <class T> const bool c_num<T>::is_signed   = std::numeric_limits<T>::is_signed;
template <class T> const bool c_num<T>::is_integral = std::numeric_limits<T>::is_integer;

// ------------------------------------------------------------------------

// epsilon values are used for division/positiveness tests
template <class T>
struct c_eps
{
    static const T small;
    static const T tiny;
};

template <class T> const T c_eps<T>::small = c_num<T>::eps;
template <class T> const T c_eps<T>::tiny = c_num<T>::min;

DRAYN_NS_END
