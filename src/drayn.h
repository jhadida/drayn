#ifndef DRAYN_H_INCLUDED
#define DRAYN_H_INCLUDED

//==================================================
// @title        drayn.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



//-------------------------------------------------------------
// CONFIGURATION
//-------------------------------------------------------------

#ifndef DRAYN_OUTPUT_INTEGER_WIDTH
#define DRAYN_OUTPUT_INTEGER_WIDTH 4
#endif
#ifndef DRAYN_OUTPUT_FLOAT_WIDTH
#define DRAYN_OUTPUT_FLOAT_WIDTH 9
#endif
#ifndef DRAYN_OUTPUT_FLOAT_PRECISION
#define DRAYN_OUTPUT_FLOAT_PRECISION 3
#endif

// Used with random generators
// #define DRAYN_RAND_DETERMINISTIC_SEED  123
// #define DRAYN_RAND_FIXED_SEED          123

// External interfaces
// #define DRAYN_USE_ARMA
// #define DRAYN_USE_JMX

/**
 * OTHER AVAILABLE FLAGS:
 *
 * DRAYN_SHOW_DEBUG           : activate debug tests and messages
 * DRAYN_SHOW_INFO            : activate info tests and messages
 *
 * DRAYN_USE_32BITS_INDEX     : force 32 bits indices (default: 64 bits)
 * DRAYN_USE_32BITS_NUMBER    : force 32 bits numbers (default: 64 bits)
 *
 * DRAYN_SAFE_INDEXING        : checks for index overflow where possible
 *                              makes code slower, use for debug only
 */

//-------------------------------------------------------------
// MAIN INCLUDES
//-------------------------------------------------------------

#define DRAYN_NS_START namespace drayn {
#define DRAYN_NS_END   } // DRAYN namespace

// ------------------------------------------------------------------------

#include "core/include.h"
#include "numeric/include.h"
#include "concept/include.h"
#include "container/include.h"
#include "algorithm/include.h"
#include "data/include.h"
#include "external/include.h"

#endif
