
//==================================================
// @title        wrapper.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>

/**
 * Wrap an external container as a Drayn sequence.
 * The container class only needs to define: 
 *      uidx_t size() const
 *      ref_t operator[] (uidx_t)
 *
 * Use as:
 *      std::vector<unsigned> v(42,0);
 *       ...
 *      auto w = drayn::wrap(v);
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class C, class V = typename C::value_type>
class wrapper: public _sequence< wrapper<C,V>, V >
{
public:

    DRAYN_TRAITS(V)
    using self = wrapper<C,V>;
    using ctn_type = typename std::add_const<C>::type;
    // using value_type = V;

    wrapper( ctn_type *ctn = nullptr ) 
        : m_ctn(ctn) {}

    inline void clear() { m_ctn = nullptr; }
    inline self& wrap( ctn_type *ctn ) { m_ctn = ctn; }

    // use underlying pointer
    inline ctn_type& get() const { return *m_ctn; }
    inline operator ctn_type&() const { return *m_ctn; }

    // indexed interface
    inline bool valid() const { return m_ctn; }
    inline uidx_t size() const { return m_ctn->size(); }
    inline ref_t operator[] ( uidx_t n ) const 
        { return const_cast<ref_t>((*m_ctn)[n]); } // for std containers

    // comparison
    inline bool operator== ( const self& that ) const 
        { return m_ctn == that.m_ctn; }
    inline bool operator!= ( const self& that ) const 
        { return !operator==(that); }

protected:

    ctn_type *m_ctn;
};

// ------------------------------------------------------------------------

template <class C, class V = typename C::value_type>
inline constexpr wrapper<C,V> wrap( const C& ctn )
    { return wrapper<C,V>(&ctn); }

DRAYN_NS_END
