
//==================================================
// @title        sliced.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <algorithm>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class T>
class sliced: public _sequence< sliced<T>, T >
{
public:

    DRAYN_TRAITS(T)
    using self = sliced<T>;
    using ctn_type = _indexed<T>;
    using ind_type = range<sidx_t>;
    // using value_type = T;

    sliced() 
        { clear(); }

    sliced( const ctn_type *ctn, sidx_t lo, sidx_t up, sidx_t step=1 ) 
        { assign(ctn,lo,up,step); }

    inline void clear() { 
        m_ctn = nullptr; 
        m_ind.clear();
    }

    inline self& assign( const ctn_type *ctn, sidx_t lo, sidx_t up, sidx_t step=1 ) { 
        m_ind.assign(lo,up,step);
        DRAYN_ASSERT_ERR( *ctn, "Bad input container." )
        DRAYN_ASSERT_ERR( 
            std::max( m_ind.front(), m_ind.back() ) < ctn->size(), 
            "Slice indices out of bounds." )
        m_ctn = ctn; 
        return *this;
    }

    // indexed interface
    inline bool valid() const { return m_ctn && m_ind; }
    inline uidx_t size() const { return m_ind.size(); }
    inline ref_t operator[] ( uidx_t n ) const 
        { return (*m_ctn)[m_ind[n]]; }

protected:

    ind_type m_ind;
    const ctn_type *m_ctn;
};

// ------------------------------------------------------------------------

template <class C, class V = typename C::value_type>
inline constexpr sliced<V> slice( const C& ctn, sidx_t lo, sidx_t up, sidx_t step=1 )
    { return sliced<V>(&ctn,lo,up,step); }

DRAYN_NS_END
