
//==================================================
// @title        range.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <type_traits>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class T = sidx_t>
class range: public _sequence< range<T>, const T >
{
public:

    static_assert( !std::is_const<T>::value, "Value-type cannot be const." );

    DRAYN_TRAITS(T)
    using self = range<T>;
    // using value_type = const T;

    range() { clear(); }
    explicit range( val_t up ) { assign(0,up,1); }
    range( val_t lo, val_t up, val_t step=1 ) { assign(lo,up,step); }

    void clear() { 
        m_lo = m_up = m_step = 0; 
        m_size = 0; 
    }

    void assign( val_t lo, val_t up, val_t step=1 ) { 
        m_lo=lo; 
        m_up=up; 
        m_step=step; 
        set_size(); 
    }

    inline bool valid() const 
        { return m_step != 0; }

    inline uidx_t size() const 
        { return m_size; }

    inline cref_t operator[] (uidx_t n) const 
        { return m_value = m_lo + m_step*n; }

    inline self reverse() const { 
        DRAYN_ASSERT_ERR( std::is_signed<T>::value, "Value-type should be signed." );
        return self( m_up, m_lo, -m_step ); 
    }

protected:

    void set_size()
    {
        DRAYN_ASSERT_ERR( m_step != 0, "Null step." )
        if (m_step > 0)
            DRAYN_ASSERT_ERR( m_up >= m_lo, "Bad bounds." )
        else
            DRAYN_ASSERT_ERR( m_lo >= m_up, "Bad bounds." )
        
        m_size = 1 + (m_up-m_lo) / m_step; // +1 to reach the upper-bound
        m_up = operator[](m_size-1);       // correct upper-bound for reversal
    }

    mutable val_t m_value; 
    val_t m_lo, m_up, m_step;
    uidx_t m_size;
};

// ------------------------------------------------------------------------

template <class T>
std::ostream& operator<< ( std::ostream &os, const range<T>& r ) 
{
    const auto on = onstream<T>();

    os << "[Range " << r.size() << "]: ";
    for ( auto x = r.iter(); x; x.next() )
        on(os) << *x << ", ";

    return os << "\b\b " << std::endl;
}

DRAYN_NS_END
