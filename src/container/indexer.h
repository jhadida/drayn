
//==================================================
// @title        indexer.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cstdint>
#include <algorithm>
#include <iostream>
#include <vector>

/*
 * This mixes indexing and layout information, and only implements sub2ind.
 *
 * It is possible to implement ind2sub as follows:
 *      subtract offset 
 *      sort strides descending
 *      recursively divmod by the largest stride
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

enum class layout
    : char { Columns=0, Cols=0, Rows=1 };

// ----------  =====  ----------

// some implementations in include.cpp
class indexer 
{
public:

    using size_type = std::vector<uidx_t>;
    
    // ----------  =====  ----------
    
    indexer() { clear(); }

    template <class T>
    explicit
    indexer( const T& size, layout cv = layout::Columns )
        : m_offset(0)
        { assign(size,cv); }

    template <class T, class U>
    indexer( const T& size, const U& step )
        : m_offset(0)
        { assign(size,step); }

    // with initialiser lists
    indexer( const inilst<uidx_t>& size, layout cv = layout::Columns )
        : m_offset(0)
        { assign(ini2ptr(size),cv); }

    indexer( const inilst<uidx_t>& size, const inilst<uidx_t>& step )
        : m_offset(0)
        { assign(ini2ptr(size),ini2ptr(step)); }


    void clear();

    // get/set offset
    inline uidx_t offset() const { return m_offset; }
    inline indexer& offset( uidx_t off ) { m_offset=off; return *this; }

    // access dimensions
    inline uidx_t ndims() const { return m_size.size(); }
    inline uidx_t numel() const { return m_numel; }
    inline operator bool() const { return m_numel; }

    inline const size_type& size() const { return m_size; }
    inline uidx_t size(uidx_t k) const { return m_size[k]; }

    inline const size_type& step() const { return m_step; }
    inline uidx_t step(uidx_t k) const { return m_step[k]; }

    // assignment accepts any container that defines size() and operator[]
    template <class T>
    indexer& assign( const T& size, layout cv = layout::Columns );

    template <class T, class U>
    indexer& assign( const T& size, const U& step );

    // convert coordinates to a container index
    template <class T>
    uidx_t sub2ind( const T& sub ) const;

    inline uidx_t sub2ind( const inilst<uidx_t>& sub ) const 
        { return sub2ind(ini2ptr(sub)); }

    // return a indexer of ndims-1
    indexer slice( uidx_t dim, uidx_t n ) const;

    // swap two dimensions
    indexer& swap( uidx_t d1, uidx_t d2 );

    // permute dimensions
    template <class T>
    indexer& permute( const T& perm );

    inline indexer& permute( const inilst<uidx_t>& p ) 
        { return permute(ini2ptr(p)); }

protected:

    uidx_t m_offset, m_numel;
    size_type m_size, m_step;
};

std::ostream& operator<< ( std::ostream &os, const indexer& ind ); // display



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



template <class T>
uidx_t indexer::sub2ind( const T& sub ) const
{
    const uidx_t D = ndims();
    DRAYN_ASSERT_ERR( sub.size()==D, "Input size mismatch." )

    uidx_t ind = m_offset;
    for ( uidx_t k=0; k < D; ++k )
        ind += sub[k]*m_step[k];
    return ind;
}

// ------------------------------------------------------------------------

template <class T>
indexer& indexer::assign( const T& size, layout cv )
{
    const uidx_t D = size.size();
    DRAYN_ASSERT_ERR( D > 0, "Empty size." )

    m_size.resize(D);
    m_step.resize(D);
    
    m_numel = 1;
    for ( uidx_t k=0; k < D; ++k ) {
        m_size[k] = size[k];
        m_step[k] = m_numel;
        m_numel *= m_size[k];
    }

    if ( D > 1 && cv == layout::Rows ) {
        m_step[0] = m_size[1];
        m_step[1] = 1;
    }

    DRAYN_ASSERT_ERR( m_numel > 0, "Null dimension." )
    return *this;
}

// ------------------------------------------------------------------------

template <class T, class U>
indexer& indexer::assign( const T& size, const U& step )
{
    const uidx_t D = size.size();
    DRAYN_ASSERT_ERR( D > 0, "Empty size." )
    DRAYN_ASSERT_ERR( step.size()==D, "Size mismatch between inputs." )

    m_size.resize(D);
    m_step.resize(D);
    
    m_numel = 1;
    for ( uidx_t k=0; k < D; ++k ) {
        m_size[k] = size[k];
        m_step[k] = step[k];
        m_numel *= m_size[k];
    }

    DRAYN_ASSERT_ERR( m_numel > 0, "Null dimension." )
    return *this;
}

// ------------------------------------------------------------------------

template <class T>
indexer& indexer::permute( const T& perm )
{
    const uidx_t D = ndims();
    DRAYN_ASSERT_ERR( perm.size() == D, "Bad input size." )

    // check that input is a permutation
    shared<uidx_t> temp(D,0);
    for ( uidx_t k=0; k < D; ++k )
        temp[k] = m_size[perm[k]];

    DRAYN_ASSERT_ERR( temp.neq(uidx_t(0)), "Not a permutation." )

    // permute size and step
    for ( uidx_t k=0; k < D; ++k ) {
        m_size[k] = temp[k];
        temp[k] = m_step[perm[k]];
    }
    for ( uidx_t k=0; k < D; ++k )
        m_step[k] = temp[k];

    return *this;
}

DRAYN_NS_END
