
//==================================================
// @title        utils.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <cmath>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// aliases
template <class T> using pvec = pointer<T>;
template <class T> using pmat = matrix< pointer<T> >;
template <class T> using pvol = volume< pointer<T> >;
template <class T> using parr = array< pointer<T> >;

template <class T, class A = allocator_new<T>> using svec = shared<T,A>;
template <class T, class A = allocator_new<T>> using smat = matrix< shared<T,A> >;
template <class T, class A = allocator_new<T>> using svol = volume< shared<T,A> >;
template <class T, class A = allocator_new<T>> using sarr = array< shared<T,A> >;

// ------------------------------------------------------------------------
//  VECTOR
// ------------------------------------------------------------------------

template <class T> // kept for consistency
constexpr svec<T> make_vec( uidx_t n )
    { return shared<T>(n); }

// common initialisers
template <class T = double>
inline shared<T> zeros( uidx_t n ) { return shared<T>(n,T(0)); }

template <class T = double>
inline shared<T> ones( uidx_t n ) { return shared<T>(n,T(1)); }

// random vectors
template <class T = int>
shared<T> randi( uidx_t n, T lo, T up ) {
    auto U = uniform_int_gen<T>(lo,up);
    shared<T> x(n);
    for ( uidx_t i=0; i < n; i++ ) x[i] = U();
    return x;
}

template <class T = double>
shared<T> rand( uidx_t n, T lo=0, T up=1 ) {
    auto U = uniform_gen<T>(lo,up);
    shared<T> x(n);
    for ( uidx_t i=0; i < n; i++ ) x[i] = U();
    return x;
}

template <class T = double>
shared<T> randn( uidx_t n, T mu=0, T sig=1 ) {
    auto G = normal_gen<T>(mu,sig);
    shared<T> x(n);
    for ( uidx_t i=0; i < n; i++ ) x[i] = G();
    return x;
}

// ------------------------------------------------------------------------

// N = x.size(), x = { first + k*step, k=0..N }
template <class T, class C>
void vecstep( C& x, T first, T step = T(1) )
{
    const uidx_t n = x.size();
    for ( uidx_t i = 0; i < n; ++i )
        x[i] = fma(i,step,first);
}

// N = x.size(), x = { first + k*(last-first)/N, k=0..N }
template <class T, class C>
void vecrange( C& x, T first, T last )
{
    const uidx_t n = x.size();
    DRAYN_ASSERT_ERR( n > 1, "Input should not be scalar." );
    for ( uidx_t i = 0; i < n; ++i )
        x[i] = first + (i*(last-first))/(n-1);
}

template <class T = uidx_t>
inline svec<T> xrange( uidx_t n )
{
    svec<T> out(n);
    vecstep<T>(out,0); 
    return out;
}

template <class T>
inline svec<T> linspace( T first, T last, uidx_t n )
{
    svec<T> out(n);
    vecrange<T>( out, first, last );
    return out;
}

template <class T>
inline svec<T> linstep( T first, T last, T step )
{
    DRAYN_ASSERT_ERR( op_abs(step) > c_num<T>::eps, "Null step." )
    DRAYN_ASSERT_ERR( (last-first)/step > 0, "Empty range (or bad step sign)." )

    const uidx_t n = op_ufloor( (last-first)/step );
    svec<T> out(n);
    vecstep<T>(out,first,step);
    return out;
}



// ------------------------------------------------------------------------
// MATRIX
// ------------------------------------------------------------------------

template <class T>
constexpr smat<T> make_mat( uidx_t nr, uidx_t nc, layout cv = layout::Cols )
    { return smat<T>( shared<T>(nr*nc), nr, nc, cv ); }

// common initialisers
template <class T = double>
inline smat<T> zeros( uidx_t nr, uidx_t nc, layout cv = layout::Cols ) 
    { return smat<T>( shared<T>(nr*nc,T(0)), nr, nc, cv ); }

template <class T = double>
inline smat<T> ones( uidx_t nr, uidx_t nc, layout cv = layout::Cols ) 
    { return smat<T>( shared<T>(nr*nc,T(1)), nr, nc, cv ); }

template <class T = double>
smat<T> eye( uidx_t n, layout cv = layout::Cols ) { 
    auto m = zeros<T>(n,n,cv);
    for ( uidx_t i=0; i < n; i++ ) m(i,i) = T(1);
    return m; 
}

// random matrices
template <class T = int>
inline smat<T> mrandi( uidx_t nr, uidx_t nc, T lo, T up, layout cv = layout::Cols )
    { return smat<T>( randi<T>( nr*nc, lo, up ), nr, nc, cv ); }

template <class T = double>
inline smat<T> mrand( uidx_t nr, uidx_t nc, T lo=0, T up=1, layout cv = layout::Cols )
    { return smat<T>( rand<T>( nr*nc, lo, up ), nr, nc, cv ); }

template <class T = double>
inline smat<T> mrandn( uidx_t nr, uidx_t nc, T mu=0, T sig=1, layout cv = layout::Cols )
    { return smat<T>( randn<T>( nr*nc, mu, sig ), nr, nc, cv ); }



// ------------------------------------------------------------------------
// VOLUME
// ------------------------------------------------------------------------

template <class T>
constexpr svol<T> make_vol( uidx_t nr, uidx_t nc, uidx_t ns, layout cv = layout::Cols )
    { return svol<T>( shared<T>(nr*nc*ns), nr, nc, ns, cv ); }

// common initialisers
template <class T = double>
inline svol<T> zeros( uidx_t nr, uidx_t nc, uidx_t ns, layout cv = layout::Cols ) 
    { return svol<T>( shared<T>(nr*nc*ns,T(0)), nr, nc, ns, cv ); }

template <class T = double>
inline svol<T> ones( uidx_t nr, uidx_t nc, uidx_t ns, layout cv = layout::Cols ) 
    { return svol<T>( shared<T>(nr*nc*ns,T(1)), nr, nc, ns, cv ); }

// random volumes
template <class T = int>
inline svol<T> vrandi( uidx_t nr, uidx_t nc, uidx_t ns, T lo, T up, layout cv = layout::Cols )
    { return svol<T>( randi<T>( nr*nc*ns, lo, up ), nr, nc, ns, cv ); }

template <class T = double>
inline svol<T> vrand( uidx_t nr, uidx_t nc, uidx_t ns, T lo=0, T up=1, layout cv = layout::Cols )
    { return svol<T>( rand<T>( nr*nc*ns, lo, up ), nr, nc, ns, cv ); }

template <class T = double>
inline svol<T> vrandn( uidx_t nr, uidx_t nc, uidx_t ns, T mu=0, T sig=1, layout cv = layout::Cols )
    { return svol<T>( randn<T>( nr*nc*ns, mu, sig ), nr, nc, ns, cv ); }

DRAYN_NS_END
