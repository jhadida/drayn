
//==================================================
// @title        container
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

void indexer::clear()
{
    m_offset = m_numel = 0;
    m_size.clear();
    m_step.clear();
}

// ------------------------------------------------------------------------

indexer& indexer::swap( uidx_t d1, uidx_t d2 ) 
{
    const uidx_t D = ndims();
    DRAYN_ASSERT_ERR( d1 != d2 && d1 < D && d2 < D, "Bad inputs." )

    std::swap( m_size[d1], m_size[d2] );
    std::swap( m_step[d1], m_step[d2] );

    return *this;
}

// ------------------------------------------------------------------------

indexer indexer::slice( uidx_t dim, uidx_t n ) const 
{
    const uidx_t D = ndims();
    DRAYN_ASSERT_ERR( D > 1, "Cannot slice 1-d indexer." )
    DRAYN_ASSERT_ERR( dim < D && n < m_size[dim], "Bad inputs." )

    // copy size/step without dimension dim
    uidx_t c = 0;
    size_type size(D-1), step(D-1);
    for ( uidx_t k=0; k < D; ++k ) {
        if ( k == dim ) continue;
        size[c] = m_size[k];
        step[c] = m_step[k];
        c += 1;
    }

    // create output indexer
    indexer out( size, step );
    out.offset( n*m_step[dim] );
    return out;
}

// ------------------------------------------------------------------------

std::ostream& operator<< ( std::ostream &os, const indexer& ind ) 
{
    const auto on = onstream<uidx_t>();

    os << "[Indexer " << ind.numel() << "]:" << std::endl;
    os << "\tndim: " << ind.ndims() << std::endl;

    os << "\tsize: ";
        for ( auto& s: ind.size() ) on(os) << s << ", ";
        os << "\b\b " << std::endl;
    
    os << "\tstep: ";
        for ( auto& s: ind.step() ) on(os) << s << ", ";
        os << "\b\b " << std::endl;

    return os;
}

DRAYN_NS_END
