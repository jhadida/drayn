
//==================================================
// @title        array.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Container should inherit _indexed.
template <class Container, class Value = typename Container::value_type>
class array 
{
public:

    DRAYN_TRAITS(Value);

    using size_type = indexer::size_type;
    using slice_type = array<Container,Value>;
    using volume_type = volume<Container,Value>;
    using matrix_type = matrix<Container,Value>;
    
    // ----------  =====  ----------
    
    array() {}
    array( const Container& ctn, const indexer& ind )
        : m_data(ctn), m_index(ind) {}

    inline const Container& data() const { return m_data; }
    inline const indexer& index() const { return m_index; }

    // proxy to container
    inline operator bool() const { return m_data; }
    inline ref_t operator[] (uidx_t k) const 
        { return const_cast<ref_t>(m_data[k]); }

    // proxy to indexer
    inline const size_type& size() const { return m_index.size(); }
    inline uidx_t size(uidx_t k) const { return m_index.size(k); }
    inline uidx_t ndims() const { return m_index.ndims(); }
    inline uidx_t numel() const { return m_index.numel(); }

    inline uidx_t nrows() const { return m_index.size(0); }
    inline uidx_t ncols() const { return m_index.size(1); }
    inline uidx_t nslab() const { return m_index.size(2); }

    // data access methods
    template <class T>
    inline ref_t operator() ( const T& sub ) const 
        { return m_data[m_index.sub2ind(sub)]; }

    inline ref_t operator() ( const inilst<uidx_t>& sub ) const 
        { return m_data[m_index.sub2ind(sub)]; }

    // slice along a dimension
    inline slice_type slice( uidx_t dim, uidx_t n ) const
        { return slice_type( m_data, m_index.slice(dim,n) ); }

    // conversion to specialised arrays
    volume_type to_volume() const {
        DRAYN_ASSERT_ERR( ndims()==3, "Array is not 3-dimensional." )
        return volume_type( m_data, m_index );
    }

    matrix_type to_matrix() const {
        DRAYN_ASSERT_ERR( ndims()==2, "Array is not 2-dimensional." )
        return matrix_type( m_data, m_index );
    }

protected:

    Container m_data;
    indexer m_index;
};

DRAYN_NS_END
