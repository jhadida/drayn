
//==================================================
// @title        shared.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <algorithm>
#include <type_traits>

/**
 * Custom shared-pointer class with allocation methods.
 *
 * Note:
 * This is only suitable if the value type is default-constructible,
 * otherwise use std::shared_ptr.
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class T, class Allocator = allocator_new<T> >
class shared: public _sequence< shared<T>, T >
{
public:

    static_assert( !std::is_const<T>::value, "Value-type cannot be const." );

    DRAYN_TRAITS(T);
    using self = shared<T,Allocator>;
    using alloc = Allocator;
    // using value_type = T;

    // ----------  =====  ----------

    shared()
        : m_ref_count(nullptr)
        { clear(); }

    shared( const self& that )
        : m_ref_count(nullptr)
        { assign(that); }

    // TODO: possibly remove this ctor to avoid conflict with size+value initialiser below
    shared( ptr_t ptr, uidx_t size ) 
        : m_ref_count(nullptr)
        { assign(ptr,size); }

    explicit
    shared( uidx_t n )
        : m_ref_count(nullptr)
        { clear(); resize(n); }
    shared( uidx_t n, cref_t val )
        : m_ref_count(nullptr)
        { clear(); resize(n,val); }
    
    shared( const inilst<val_t>& ini )
        : m_ref_count(nullptr)
        { clear(); copy(pointer<const val_t>( ini.begin(), ini.size() )); }


    virtual ~shared() { clear(); } // -Wdelete-non-virtual-dtor

    void clear();

    // use underlying pointer
    inline ptr_t memptr() const { return m_ptr; }

    // conversion to pointer
    inline operator pointer<T>() const { return pointer<T>(m_ptr,m_size); }

    // indexed interface
    inline bool valid() const { return m_ref_count; }
    inline uidx_t size() const { return m_size; }
    inline ref_t operator[] ( uidx_t n ) const { return m_ptr[n]; }

    // assignment
    inline self& operator= ( const self& that )
        { assign(that); return *this; }
    inline self& operator= ( const inilst<val_t>& ini )
        { return copy(pointer<const val_t>( ini.begin(), ini.size() )); }

    self& assign( const self& that );
    self& assign( ptr_t ptr, uidx_t size );

    // allocation
    self& resize( uidx_t n );
    self& resize( uidx_t n, cref_t val );

    // copy external container
    template <class C>
    self& copy( const C& ctn );

    // reference counting
    inline uidx_t ref_count() const { return *m_ref_count; }
    inline bool unique() const { return ref_count() == 1; }

    // comparison
    inline bool operator== ( const self& that ) const 
        { return m_ref_count == that.m_ref_count; }
    inline bool operator!= ( const self& that ) const 
        { return !operator==(that); }

protected:

    ptr_t    m_ptr;
    uidx_t  *m_ref_count;
    uidx_t   m_size;
};



    //--------------------     ==========     --------------------//
    //--------------------     **********     --------------------//



template <class T, class A>
void shared<T,A>::clear()
{
    // Free memory or decrease references count
    if ( valid() )
    {
        if ( unique() )
        {
            alloc::release( m_ptr, m_size );
            delete m_ref_count;
        }
        else
            --(*m_ref_count);
    }

    // Reset members
    m_ptr       = nullptr;
    m_ref_count = nullptr;
    m_size      = 0;
}

// ------------------------------------------------------------------------

template <class T, class A>
auto shared<T,A>::resize( uidx_t n ) -> self&
{
    if ( n == 0 ) 
        clear();
    else if ( size() != n )
        assign( alloc::allocate(n), n );

    return *this;
}

template <class T, class A>
auto shared<T,A>::resize( uidx_t n, cref_t val ) -> self&
{
    resize(n); 
    std::fill_n( memptr(), n, val ); 
    
    return *this;
}

// ------------------------------------------------------------------------

template <class T, class A>
auto shared<T,A>::assign( const self& that ) -> self&
{
    if ( &that != this )
    {
        clear();

        // Copy input
        m_ref_count = that.m_ref_count;
        m_size      = that.m_size;
        m_ptr       = that.m_ptr;

        if ( valid() )
            ++(*m_ref_count);
    }
    return *this;
}

template <class T, class A>
auto shared<T,A>::assign( ptr_t ptr, uidx_t size ) -> self&
{
    clear();
    DRAYN_ASSERT_RV( ptr, *this, "[drayn.shared.assign] Null pointer in input." );

    // Create new references count
    m_ref_count = new uidx_t(1);

    // Assign members
    m_ptr  = ptr;
    m_size = size;

    return *this;
}

// ------------------------------------------------------------------------

template <class T, class A>
template <class C>
auto shared<T,A>::copy( const C& ctn ) -> self&
{
    const uidx_t n = ctn.size();
    resize(n);
    for ( uidx_t k=0; k < n; ++k )
        m_ptr[k] = static_cast<val_t>(ctn[k]);

    return *this;
}

DRAYN_NS_END
