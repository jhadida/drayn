
//==================================================
// @title        pointer.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <vector>
#include <array>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

template <class T>
class pointer: public _sequence< pointer<T>, T >
{
public:

    DRAYN_TRAITS(T)
    using self = pointer<T>;
    // using value_type = T;

    pointer()
        { clear(); }
    pointer( ptr_t ptr, uidx_t size, uidx_t step=1 )
        { assign(ptr,size,step); }


    inline void clear() { assign(nullptr,0,1); }

    self& assign( ptr_t ptr, uidx_t size, uidx_t step=1 ) {
        m_ptr = ptr; 
        m_size = size;
        m_step = step;
        return *this;
    }

    // indexed interface
    inline bool valid() const { return m_ptr && (m_step > 0); }
    inline uidx_t size() const { return m_size; }
    inline uidx_t step() const { return m_step; }
    inline ref_t operator[] ( uidx_t n ) const { return m_ptr[n*m_step]; }

    // use underlying pointer
    inline ptr_t memptr() const { return m_ptr; }
    inline ref_t operator* () const { return *memptr(); }
    inline ptr_t operator-> () const { return memptr(); }
    inline operator ptr_t() const { return m_ptr; }

    inline self& operator+= ( sidx_t k ) { m_ptr += k; return *this; }
    inline self& operator-= ( sidx_t k ) { m_ptr -= k; return *this; }

    inline self& operator++ () { return operator+=(1); }
    inline self& operator-- () { return operator-=(1); }

    inline self operator+ ( sidx_t k ) const { return self( m_ptr+k, m_size, m_step ); }
    inline self operator- ( sidx_t k ) const { return self( m_ptr-k, m_size, m_step ); }

    inline self operator++ (int) { return self( m_ptr++, m_size, m_step ); }
    inline self operator-- (int) { return self( m_ptr--, m_size, m_step ); }

    // comparison
    inline bool operator== ( const self& that ) const 
        { return m_ptr == that.m_ptr 
            && m_size == that.m_size 
            && m_step == that.m_step; }
    inline bool operator!= ( const self& that ) const 
        { return !operator==(that); }

protected:

    ptr_t m_ptr;
    uidx_t m_size, m_step;
};

// ------------------------------------------------------------------------

// These specialisations yield temporary "shallow copies".
// They are efficient, but may lead to dangling references
// if used improperly.
// 
// DO NOT USE THE OUTPUT AFTER THE INPUT HAS EXPIRED

template <class T> // needs a special name...
inline constexpr pointer<const T> ini2ptr( const inilst<T>& lst ) 
    { return pointer<const T>( lst.begin(), lst.size() ); }

template <class T>
inline constexpr pointer<T> vec2ptr( std::vector<T>& vec )
    { return pointer<T>( vec.data(), vec.size() ); }

template <class T>
inline constexpr pointer<const T> vec2ptr( const std::vector<T>& vec )
    { return pointer<const T>( vec.data(), vec.size() ); }

template <class T, unsigned N>
inline constexpr pointer<T> arr2ptr( std::array<T,N>& arr )
    { return pointer<T>( arr.data(), N ); }

template <class T, unsigned N>
inline constexpr pointer<const T> arr2ptr( const std::array<T,N>& arr )
    { return pointer<const T>( arr.data(), N ); }

DRAYN_NS_END
