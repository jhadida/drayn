
//==================================================
// @title        volume.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iostream>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Container should inherit _indexed.
template <class Container, class Value = typename Container::value_type>
class volume 
{
public:

    DRAYN_TRAITS(Value);

    using size_type = indexer::size_type;
    using matrix_type = matrix<Container,Value>;
    
    // ----------  =====  ----------
    
    volume() {}
    volume( const Container& ctn, uidx_t nrows, uidx_t ncols, uidx_t nslab, layout cv = layout::Columns )
        : m_data(ctn), m_index({nrows,ncols,nslab},cv) {}
    volume( const Container& ctn, const indexer& ind )
        : m_data(ctn), m_index(ind) { DRAYN_ASSERT_ERR(ind.ndims()==3, "Bad indexer size.") }


    inline const Container& data() const { return m_data; }
    inline const indexer& index() const { return m_index; }

    // proxy to container
    inline operator bool() const { return m_data; }
    inline ref_t operator[] (uidx_t k) const 
        { return const_cast<ref_t>(m_data[k]); }

    // proxy to indexer
    inline const size_type& size() const { return m_index.size(); }
    inline uidx_t size(uidx_t k) const { return m_index.size(k); }
    inline uidx_t ndims() const { return 3; }
    inline uidx_t numel() const { return m_index.numel(); }

    inline uidx_t nrows() const { return m_index.size(0); }
    inline uidx_t ncols() const { return m_index.size(1); }
    inline uidx_t nslab() const { return m_index.size(2); }

    inline uidx_t nslabs() const { return nslab(); }
    inline uidx_t nslice() const { return nslab(); }
    inline uidx_t nslices() const { return nslab(); }

    inline bool chksize( uidx_t nr, uidx_t nc, uidx_t ns ) const 
        { return nr==nrows() && nc==ncols() && ns==nslab(); }

    // data access methods
    inline ref_t operator() ( uidx_t r, uidx_t c, uidx_t s ) const 
        { return const_cast<ref_t>(m_data[ m_index.offset() 
            + r*m_index.step(0) 
            + c*m_index.step(1) 
            + s*m_index.step(2) ]); }

    inline matrix_type slice( uidx_t dim, uidx_t n ) const
        { return matrix_type( m_data, m_index.slice(dim,n) ); }

protected:

    Container m_data;
    indexer m_index;
};

// ------------------------------------------------------------------------

template <class C, class V>
std::ostream& operator<< ( std::ostream& os, const volume<C,V>& vol ) 
{
    const auto on = onstream<V>();
    const uidx_t nr = vol.nrows();
    const uidx_t nc = vol.ncols();
    const uidx_t ns = vol.nslab();

    os << "[Volume " << nr << "x" << nc << "x" << ns << "]:";
    for ( uidx_t s=0; s < ns; ++s ) {
        os << std::endl << "---------- slab " << s;
        for ( uidx_t r=0; r < nr; ++r ) {
            os << std::endl << "\t";
            for ( uidx_t c=0; c < nc; ++c ) 
                on(os) << vol(r,c,s) << ", ";
            os << "\b\b ";
        }
    }

    return os << std::endl;
}

DRAYN_NS_END
