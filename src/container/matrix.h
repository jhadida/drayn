
//==================================================
// @title        matrix.h
// @author       Jonathan Hadida
// @contact      jhadida87 at gm-ail
//==================================================

#include <iostream>



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START

// Container should inherit _indexed.
template <class Container, class Value = typename Container::value_type>
class matrix 
{
public:

    DRAYN_TRAITS(Value);

    using self = matrix<Container,Value>;
    using size_type = indexer::size_type;
    using vector_type = sliced<Value>;
    using unsafe_type = pointer<val_t>;
    
    // ----------  =====  ----------
    
    matrix() {}
    matrix( const Container& ctn, uidx_t nrows, uidx_t ncols, layout cv = layout::Columns )
        : m_data(ctn), m_index({nrows,ncols},cv) {}
    matrix( const Container& ctn, const indexer& ind )
        : m_data(ctn), m_index(ind) { DRAYN_ASSERT_ERR(ind.ndims()==2, "Bad indexer size.") }


    inline const Container& data() const { return m_data; }
    inline const indexer& index() const { return m_index; }

    // proxy to container
    inline operator bool() const { return m_data; }
    inline ref_t operator[] (uidx_t k) const 
        { return const_cast<ref_t>(m_data[k]); }

    // proxy to indexer
    inline const size_type& size() const { return m_index.size(); }
    inline uidx_t size(uidx_t k) const { return m_index.size(k); }
    inline uidx_t ndims() const { return 2; }
    inline uidx_t numel() const { return m_index.numel(); }

    inline uidx_t nrows() const { return m_index.size(0); }
    inline uidx_t ncols() const { return m_index.size(1); }

    inline bool chksize( uidx_t nr, uidx_t nc ) const 
        { return nr==nrows() && nc==ncols(); } 

    // data access methods
    inline ref_t operator() ( uidx_t r, uidx_t c ) const 
        { return const_cast<ref_t>(m_data[ m_index.offset() 
            + r*m_index.step(0) 
            + c*m_index.step(1) ]); }

    vector_type row( uidx_t r ) const { 
        uidx_t off = m_index.offset() + r*m_index.step(0);
        uidx_t step = m_index.step(1);
        return vector_type( &m_data, off, off+(size(1)-1)*step, step );
    }

    vector_type col( uidx_t c ) const { 
        uidx_t off = m_index.offset() + c*m_index.step(1);
        uidx_t step = m_index.step(0);
        return vector_type( &m_data, off, off+(size(0)-1)*step, step );
    }
    
    // WARNING: the following methods may cause SEGFAULTS!
    // 
    // They are quicker than col() and row(), but only work if:
    //  - the underlying container is allocated in memory (e.g. not range)
    //  - successive indices are stored contiguously (e.g. not pointer with step > 1)
    // 
    inline unsafe_type unsafe_row( uidx_t r ) const
        { return unsafe_type( &(operator()(r,0)), m_index.size(1), m_index.step(1) ); }
    inline unsafe_type unsafe_col( uidx_t c ) const
        { return unsafe_type( &(operator()(0,c)), m_index.size(0), m_index.step(0) ); }

    // transposition
    inline self& t() { m_index.swap(0,1); return *this; } // in-place
    inline self transpose() const {
        indexer ind(m_index);
        return self( m_data, ind.swap(0,1) );
    }

protected:

    Container m_data;
    indexer m_index;
};

// ------------------------------------------------------------------------

template <class C, class V>
std::ostream& operator<< ( std::ostream& os, const matrix<C,V>& mat ) 
{
    const auto on = onstream<V>();
    const uidx_t nr = mat.nrows();
    const uidx_t nc = mat.ncols();

    os << "[Matrix " << nr << "x" << nc << "]:";
    for ( uidx_t r=0; r < nr; ++r ) {
        os << std::endl << "\t";
        for ( uidx_t c=0; c < nc; ++c ) 
            on(os) << mat(r,c) << ", ";
        os << "\b\b ";
    }

    return os << std::endl;
}

DRAYN_NS_END
