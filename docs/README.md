
# The Drayn C++11 library

Drayn is a modern C++ library intended for core scientific programming.
In brief, the main contributions are flexible multi-dimensional array containers with smart memory allocators and, building upon language features introduced in C++11, random number generation wrappers, high-resolution timers and publish/subscribe event management tools.
Here are a few points which should convince you to give it a try:

 - it is entirely standard-C++11 compliant, which means it should compile without warning on any machine that has an up-to-date compiler;
 - it has **no required dependency**, so you don't have to worry about installing unsupported packages on your cluster or dependency-clashes between library versions;
 - it interfaces with Matlab, so you can build the bulk of your work in a free widely-used industry-friendly language like C++, and connect to Matlab down the line for educational usage or analysis prototyping;
 - it interfaces with Armadillo (sources included), which means you have access to all of the BLAS/LAPACK magic if you need, and it won't affect runtime performance;
 - it is released free of charge under the Mozilla Public License v2.0, so even businesses can use it alongside private code, linked statically or dynamically, as long as they share with everyone any improvement from the public version they were allowed to use in the first place.

## Why another library?

There are tons of libraries C++ projects can use, ranging from essential libraries like Boost, to specialised libraries like rendering engines. Drayn is not trying to compete with any other library I know, but simply matured from my own experience coding with C++ for data-analysis in Biomedical engineering and Life Sciences. Although it was a priority for me to design the code sensibly from a software perspective, I am not a software engineer and certain parts might evolve in the future as more people with different perspectives get involved in that project. I am looking forward to such improvements, but I am quite confident at this point that these would not affect the current structure much.

## Why "Drayn"?

"Drayn" is almost a circular permutation of "N-d array".
