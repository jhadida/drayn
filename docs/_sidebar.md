
* Getting started

  * [Installation](install.md)
  * [Library overview](overview.md)

* Core library

  * [Memory management](core/memory.md)
  * [Types & patterns](core/program.md)
  * [Random generators](core/random.md)
  * [Events](core/events.md)
  * [Timer](core/timer.md)
  * [Messages & assertions](core/messages.md)
  * [Numerics](core/numerics.md)

* Data containers

  * [Overview](data/overview.md)
  * [Concepts](data/concepts.md)
  * [1-D Arrays](data/sequential.md)
  * [N-D Arrays](data/multidim.md)

* Basic algorithms

  * [Maths](algo/maths.md)
  * [Sorting](algo/sort.md)
  * [Statistics](algo/stats.md)
  * [Interpolation](algo/interp.md)

* Interface with Matlab

  * [Overview](matlab/overview.md)
  * [Type wrappers](matlab/wrappers.md)
  * [Reading inputs](matlab/inputs.md)
  * [Creating outputs](matlab/outputs.md)
  * [Print & interrupt](matlab/console.md)

* Interface with Armadillo

  * [Overview](arma/overview.md)
  * [Type wrappers](arma/wrappers.md)

* Contribution

  * [How-to](contribute.md)
  * [To-do list](todo.md)