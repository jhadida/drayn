
#include "drayn.h"
namespace dr = drayn;

int main() {

    std::vector<double> v = { 2.0, 13.0, -5.0, 2.1 }; // ranks: [2 0 3 1]

    // sort an initialier list
    auto r = dr::rank(dr::vec2ptr(v));
    ASSERT_RF( r.size() == 4, "[dr.algorithm.rank] FAIL 1" );
    ASSERT_RF( r[0]==2 && r[1]==0 && r[2]==3 && r[3]==1, "[dr.algorithm.rank] FAIL 2" );

    // sort a vector
    auto a = dr::ini2ptr<double>( {8.7, 3.2, 7.6, 4.3, 2.1, 5.4, 6.5, 1.0} );
    auto s = dr::rank(a);
    ASSERT_RF( s.size() == a.size(), "[dr.algorithm.rank] FAIL 3" );
    ASSERT_RF( s[0]==7 && s[1]==4 && s[2]==1 && s[3]==3 && s[4]==5 && s[5]==6 && s[6]==2, "[dr.algorithm.rank] FAIL 4" );

    // apply ranking
    dr::svec<double> u(v.size());
    dr::map( r, u, [&v]( usize_t i ){ return v[i]; } );
    ASSERT_RF( dr::is_ascending(u), "[dr.algorithm.rank] FAIL 5" );

    // test ternary insertion
    auto w     = dr::xrange(4);
    auto urand = dr::uniform_gen<double>( -1.0, 5.0 );

    for ( usize_t i = 0; i < 100; ++i )
	{
		double val = urand();

		usize_t x = dr::ternary_insert(w,w.size(),val);
		usize_t y = std::upper_bound(w.begin(),w.end(),val) - w.begin();

		ASSERT_RF( x == y, "[dr.algorithm.rank] FAIL 6" );
	}

    // time_algorithm_ternary();
    return true;
}