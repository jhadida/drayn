#ifndef DRAYN_TEST_WRAPPER_H_INCLUDED
#define DRAYN_TEST_WRAPPER_H_INCLUDED

//-------------------------------------------------------------
// Drayn flags
//-------------------------------------------------------------

// Enable debug mode
#define DRAYN_DEBUG_MODE

// This flag protects one dimensional indexing
#define DRAYN_PROTECT_INDICES

// This flag activates infos
// #define DRAYN_SHOW_INFO

// Activate this flag if using Matlab
// #define DRAYN_USING_MATLAB

// ------------------------------------------------------------------------

#include "../drayn.h"

#endif
