#include <string>
#include <iostream>

#include "drayn.h"
namespace dr = drayn; // alias 

// can also be a qualified POD type (e.g. const unsigned)
struct EventData { 
    std::string msg;
    EventData(const char *m): msg(m) {}
    void print() const { std::cout << msg << std::endl; }
};

struct Emitter {
    dr::signal<const EventData> signal;
    Emitter() {}
    void hip() { signal.publish("Hurray!"); }
};

// Note: s is called a "subscriber", and can safely be returned
// as the output of a function (it is a shared pointer).
int main() {
    Emitter em;
    dr::slot<const EventData> s = em.signal.subscribe(
        []( const EventData& dat ){ dat.print(); }
    );
    em.hip();
    em.hip();
}