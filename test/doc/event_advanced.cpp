#include <string>
#include <iostream>

#include "drayn.h"
namespace dr = drayn; // alias 

// can also be a qualified POD type (e.g. const unsigned)
struct EventData { 
    std::string msg;
    EventData(const char *m): msg(m) {}
    void print() const { std::cout << msg << std::endl; }
};

dr::events<const EventData> hub;
dr::slot<const EventData> add_subscriber( std::string chan ) {
    return hub.channel(chan).subscribe();
}

int main() {
    auto s = add_subscriber("foo")->enable([]( const EventData& dat ){ dat.print(); });
    hub.channel("foo").publish("Hello world!");
}