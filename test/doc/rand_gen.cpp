#include <iostream>

#include "drayn.h"
namespace dr = drayn;

int main() {
    auto U = dr::uniform_gen();
    auto G = dr::normal_gen( 2.0, 1.0 );
    auto E = dr::exponential_gen( 0.5 );

    for ( int i=0; i < 15; i++ ) std::cout << U() << " "; std::cout << std::endl;
    for ( int i=0; i < 15; i++ ) std::cout << G() << " "; std::cout << std::endl;
    for ( int i=0; i < 15; i++ ) std::cout << E() << " "; std::cout << std::endl;
}