#include <iostream>

#include "drayn.h"
namespace dr = drayn;

int main() {
    std::cout << dr::rand( 15 );
    std::cout << dr::randn( 15, 2.0, 1.0 );
    std::cout << dr::randi( 15, -5, 7 );
}