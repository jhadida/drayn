
//==================================================
// @title        wrapper.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "wrapper.h"
#include "../drayn.cpp"

#ifdef INC_TESTS
    #include "src/tests.cpp"
#endif
