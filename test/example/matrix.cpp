
#include <iostream>
#include <vector>
#include "drayn.h"

namespace dr = drayn;
using namespace dr::integer;

int main() {

    auto range = dr::range<sidx_t>( 1, 27, 3 ); // 1, 4, 7, 10, 13, 16, 19, 22, 25
    auto data = dr::svec<double>({ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
    std::vector<int> vec = { 1, -1, 2, -2, 3, -3 };

    dr::indexer ind( {3,2}, {1,6} );

    dr::matrix< dr::range<sidx_t> > M1(range,3,3);
    dr::matrix< dr::pvec<int> > M2( dr::vec2ptr(vec), 2, 3, dr::layout::Rows );
    dr::matrix< dr::svec<double> > M3( data, ind );
    dr::matrix< dr::pvec<int> > M4( dr::vec2ptr(vec), 1, 6 );

    std::cout << M1;
        // 1, 10, 19
        // 4, 13, 22
        // 7, 16, 25
    std::cout << M1.row(1); //  4, 13, 22
    std::cout << M1.col(2); // 19, 22, 25

    std::cout << M2.index();
    std::cout << M2;
        //  1, -1,  2
        // -2,  3, -3
    std::cout << M2.unsafe_col(1);

    std::cout << ind;
    std::cout << M3.transpose();
    std::cout << M3;
        // 1, 7
        // 2, 8
        // 3, 9

    std::cout << M4;
}