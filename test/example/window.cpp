
#include <iostream>
#include "drayn.h"

using namespace drayn::integer;

int main() {

    const uidx_t n = 15;

    std::cout << drayn::window<double>( drayn::win::Hamming, n );
    std::cout << drayn::window<float>( drayn::win::Tukey, n );
    std::cout << drayn::win_tukey<double>( n, 0.6 );
    std::cout << drayn::window<double>( drayn::win::Gauss, n );
    std::cout << drayn::window<float>( drayn::win::Blackman, n );
}
