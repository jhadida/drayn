
#include <cmath>
#include <iostream>
#include "drayn.h"

namespace dr = drayn;
using namespace dr::integer;

using time_type = double;
using value_type = float;

using ts = dr::timeseries< dr::svec<value_type>, dr::svec<time_type> >;
using refts = dr::timeseries< dr::pvec<value_type>, dr::pvec<time_type> >;

using tc = dr::timecourse< dr::svec<value_type>, dr::svec<time_type> >;
using reftc = dr::timecourse< dr::pvec<value_type>, dr::pvec<time_type> >;

// ------------------------------------------------------------------------

tc interp_tc( const tc& in, const dr::svec<time_type>& t ) {
    const uidx_t n = t.size();
    auto d = dr::svec<value_type>(n);

    for ( uidx_t i=0; i < n; i++ )
        d[i] = in.pinterp(t[i]);
    
    return tc(t,d);
}

tc make_sinusoid( double omega, double phi, double fs, double len ) {
    auto t = dr::linstep<time_type>( 0, len, 1/fs );
    auto d = dr::svec<value_type>(t.size());

    for ( uidx_t i=0; i < t.size(); i++ )
        d[i] = sin( 2*dr::math::Pi*omega*t[i] + phi );

    return tc(t,d);
}

tc make_randn( double mu, double sigma, double fs, double len ) {
    auto t = dr::linstep<time_type>( 0, len, 1/fs );
    auto d = dr::svec<value_type>(t.size());
    auto r = dr::normal_gen( mu, sigma );

    for ( uidx_t i=0; i < t.size(); i++ )
        d[i] = r();

    return tc(t,d);
}

// ------------------------------------------------------------------------

int main() {

    auto x = make_sinusoid( 3, sqrt(2), 25, 3 );
    auto y = interp_tc( x, dr::linstep<time_type>(0,3,1.0/31) );
    
    // try iterating
    for ( auto it=y.iter(); it; it.next() ) {}
    std::cout << y;
}
