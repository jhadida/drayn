
#include <vector>
#include "drayn.h"

namespace dr = drayn;
using namespace dr::integer;

int main() {

    std::vector<double> v = { 2.0, 13.0, -5.0, 2.1 }; // ranks: []
    auto r = dr::rank(dr::vec2ptr(v));
    std::cout << r; 
        // 2, 0, 3, 1

    std::cout << dr::rank(dr::ini2ptr<double>( {8.7, 3.2, 7.6, 4.3, 2.1, 5.4, 6.5, 1.0} ));
        // 7, 4, 1, 3, 5, 6, 2, 0

    // apply ranking
    dr::svec<double> u(v.size());
    dr::map( r, u, [&v]( uidx_t i ){ return v[i]; } );
    std::cout << u;

    // test ternary insertion
    auto w = dr::xrange(4);
    auto urand = dr::uniform_gen<double>( -1.0, 5.0 );

    uidx_t k1, k2; double val;
    for ( uidx_t i = 0; i < 100; ++i )
	{
		val = urand();
        k1 = dr::upper_bound( w, val );
		k2 = std::upper_bound( w.begin(), w.end(), val ) - w.begin();
        DRAYN_ASSERT_ERR( k1 == k2, "Bad upper bound." )
	}
}
