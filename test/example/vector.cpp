
#include <iostream>
#include <vector>

#include "drayn.h"
namespace dr = drayn;

int main() {

    // shared containers can be initialised with an inilist
    dr::shared<unsigned> t = {1,2,3};
    std::cout << t;

    // standard vectors can be converted to pointers (shallow copy)
    std::vector<unsigned> x = {4,5,6};
    for ( auto it = dr::vec2ptr(x).iter(); it; it.next() )
        std::cout << *it << std::endl;
        
}