
#include <iostream>
#include "drayn.h"

namespace dr = drayn;
using namespace dr::integer;

int main() {

    auto range = dr::range<sidx_t>( 1, 48, 2 );
    dr::volume< dr::range<sidx_t> > V( range, 3, 4, 2, dr::layout::Rows );

    auto M = V.slice(0,2);

    std::cout << V;
    std::cout << V.slice(1,1);

    std::cout << M.index();
    std::cout << M;
    std::cout << V.slice(0,2).col(1);
    // std::cout << M.unsafe_col(1); // cannot be used with range
}