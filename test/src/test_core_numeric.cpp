
/**
 * TODO: write these tests...
 */

bool test_core_numeric_bit()
{
    ASSERT_RF( dr::bit_count<int>( 0 ) == 0, "[dr.core.numeric.bit.bit_count] FAIL 1" );
    ASSERT_RF( dr::bit_count<uint16_t>( -1 ) == 16, "[dr.core.numeric.bit.bit_count] FAIL 2" );
    ASSERT_RF( dr::bit_count<unsigned>( 23 ) == 4, "[dr.core.numeric.bit.bit_count] FAIL 3" );

    ASSERT_RF( dr::bit_gcd<unsigned>( 54, 24 ) == 6, "[dr.core.numeric.bit.bit_gcd] FAIL" );
    ASSERT_RF( dr::bit_lcm<unsigned>( 4, 6 ) == 12, "[dr.core.numeric.bit.bit_lcm] FAIL" );

    ASSERT_RF( dr::bit_bin2gray<unsigned>(7) == 4, "[dr.core.numeric.bit.bit_bin2gray] FAIL" );
    ASSERT_RF( dr::bit_gray2bin<unsigned>(4) == 7, "[dr.core.numeric.bit.bit_gray2bin] FAIL" );

    ASSERT_RF( dr::bit_reverse<uint32_t>( 3, 5 ) == 24, "[dr.core.numeric.bit.bit_reverse] FAIL" );

    unsigned a = 42, b = 23;
    dr::bit_swap( a, b );
    ASSERT_RF( 2*a == b+4, "[dr.core.numeric.bit.bit_swap] FAIL" );

    ASSERT_RF( dr::bit_ispow2<int>(256), "[dr.core.numeric.bit.bit_ispow2] FAIL" );
    ASSERT_RF( dr::bit_log2<int>(1024) == 10, "[dr.core.numeric.bit.bit_log2] FAIL" );
    ASSERT_RF( dr::bit_nextpow2<int>(31) == 32, "[dr.core.numeric.bit.bit_nextpow2] FAIL" );

    return true;
}

bool test_core_numeric_fun()
{
    return true;
}

bool test_core_numeric_op()
{
    return true;
}

bool test_core_numeric_pred()
{
    return true;
}

bool test_core_numeric()
{
    ASSERT_RF( test_core_numeric_bit(), "[dr.core.numeric.bit] FAIL" );
    ASSERT_RF( test_core_numeric_fun(), "[dr.core.numeric.fun] FAIL" );
    ASSERT_RF( test_core_numeric_op(), "[dr.core.numeric.op] FAIL" );
    ASSERT_RF( test_core_numeric_pred(), "[dr.core.numeric.pred] FAIL" );

    return true;
}
