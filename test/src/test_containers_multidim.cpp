
bool test_containers_multidim_indexing()
{
    dr::nd_dimensions d({2,4,8});
    dr::vector<unsigned> c( dr::wrap_inilst<unsigned>({0,0,8}) );

    ASSERT_RF( d.ndims() == 3 && d.numel() == 64, "[dr.containers.multidim.indexing] FAIL 1" );
    ASSERT_RF( d.is_in<unsigned>({1,0,5}) && d.is_out(c), "[dr.containers.multidim.indexing] FAIL 2" );

    d.sub_decrement(c);
    ASSERT_RF( d.is_in(c), "[dr.containers.multidim.indexing] FAIL 3" );

    return true;
}

bool test_containers_multidim_matrix()
{
    auto v = dr::xrange<double>( 8 );
    auto M = dr::matrix<double,decltype(v)>( v, dr::wrap_inilst<usize_t>({2,4}) );

    /**

    0.000     2.000     4.000     6.000
    1.000     3.000     5.000     7.000

    **/

    ASSERT_RF( M(1,1) == 3 && M.col(3)[0] == 6, "[dr.containers.multidim.matrix] FAIL 1" );

    auto N = M.submat( {0,2}, {2,2} );
    ASSERT_RF( N(1,1) == 7 && N(1,0) == 5, "[dr.containers.multidim.matrix] FAIL 2" );

    auto s = M.subvector( {1,2}, 1 ).reverse();
    ASSERT_RF( s[1] == 5, "[dr.containers.multidim.matrix] FAIL 3" );

    return true;
}

bool test_containers_multidim_volume()
{
    auto V = dr::create_volume<float>( 4, 5, 2 );

    unsigned val = 0;
	for ( auto it = V.iter(); it; ++it, ++val )
		*it = val;

    V.permute({1,0,2});
    /**

    0.000     1.000     2.000     3.000
    4.000     5.000     6.000     7.000
    8.000     9.000    10.000    11.000
    12.000    13.000    14.000    15.000
    16.000    17.000    18.000    19.000

    20.000    21.000    22.000    23.000
    24.000    25.000    26.000    27.000
    28.000    29.000    30.000    31.000
    32.000    33.000    34.000    35.000
    36.000    37.000    38.000    39.000

    **/

    ASSERT_RF( V(2,2,0) == 10 && V(3,1,1) == 33, "[dr.containers.multidim.volume] FAIL 1" );
    ASSERT_RF( V.stride(0) == 4, "[dr.containers.multidim.volume] FAIL 2" );

    auto U = V.subvol( {2,1,0}, {3,2,2} );
    ASSERT_RF( U(2,0,1) == 37, "[dr.containers.multidim.volume] FAIL 3" );

    auto s1 = U.slice(0);   // [9 10; 13 14; 17 18]
    auto s2 = U.slice(1,0); // [13 14; 33 34]

    s2 /= 2;
    ASSERT_RF( V(3,1,1) == (33.0/2), "[dr.containers.multidim.volume] FAIL 4" );
    ASSERT_RF( s1(1,1) == (14.0/2), "[dr.containers.multidim.volume] FAIL 5" );

    return true;
}

bool test_containers_multidim_nd_array()
{
    // nothing for now
    return true;
}

bool test_containers_multidim_accessors()
{
    // there's just the default one for now
    return true;
}

bool test_containers_multidim()
{
    ASSERT_RF( test_containers_multidim_indexing(), "[dr.containers.multidim.indexing] FAIL" );
    ASSERT_RF( test_containers_multidim_matrix(), "[dr.containers.multidim.matrix] FAIL" );
    ASSERT_RF( test_containers_multidim_volume(), "[dr.containers.multidim.volume] FAIL" );
    ASSERT_RF( test_containers_multidim_nd_array(), "[dr.containers.multidim.nd_array] FAIL" );
    ASSERT_RF( test_containers_multidim_accessors(), "[dr.containers.multidim.accessors] FAIL" );

    return true;
}
