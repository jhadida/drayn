
using namespace dr::int_types;

// ------------------------------------------------------------------------

#include "test_core.cpp"
#include "test_core_numeric.cpp"

bool test_core()
{
    ASSERT_RF( test_core_traits(), "[dr.core.traits] FAIL" );
    ASSERT_RF( test_core_macro(), "[dr.core.macro] FAIL" );
    ASSERT_RF( test_core_patterns(), "[dr.core.patterns] FAIL" );
    ASSERT_RF( test_core_allocators(), "[dr.core.allocators] FAIL" );
    ASSERT_RF( test_core_pubsub(), "[dr.core.pubsub] FAIL" );
    ASSERT_RF( test_core_numeric(), "[dr.core.numeric] FAIL" );

    // Uncomment these if you don't mind the output to console:
    //
    // ASSERT_RF( test_core_messages(), "[dr.core.messages] FAIL" );
    // ASSERT_RF( test_core_randoms(), "[dr.core.randoms] FAIL" );
    // ASSERT_RF( test_core_timer(), "[dr.core.timer] FAIL" );

    return true;
}

// ------------------------------------------------------------------------

#include "test_containers_sequence.cpp"
#include "test_containers_multidim.cpp"

bool test_containers()
{
    ASSERT_RF( test_containers_sequence(), "[dr.containers.sequence] FAIL" );
    ASSERT_RF( test_containers_multidim(), "[dr.containers.multidim] FAIL" );

    return true;
}

// ------------------------------------------------------------------------

#include "test_algorithms.cpp"

bool test_algorithms()
{
    ASSERT_RF( test_algorithms_interp(), "[dr.algorithms.interp] FAIL" );
    ASSERT_RF( test_algorithms_math(), "[dr.algorithms.math] FAIL" );
    ASSERT_RF( test_algorithms_sort(), "[dr.algorithms.sort] FAIL" );
    ASSERT_RF( test_algorithms_stats(), "[dr.algorithms.stats] FAIL" );

    return true;
}



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



int main()
{
    ASSERT_RVAL( test_core(), 1, "[drayn.core] FAIL" );
    ASSERT_RVAL( test_containers(), 1, "[drayn.containers] FAIL" );
    ASSERT_RVAL( test_algorithms(), 1, "[drayn.algorithms] FAIL" );

    dr_println("Everything went fine. Bye now :)");
}
