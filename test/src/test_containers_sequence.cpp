
bool test_containers_sequence_array()
{
    static dr::fp_close<double> fp_close(1e-6);

    // Conversion from initialiser list
    auto ini_list = dr::inilst<double>({1.0, 2.1, 3.2, 4.3, 5.4, 6.5, 7.6, 8.7});
    auto array    = dr::wrap_inilst(ini_list);
    auto subarray = array.subarray(2,7,-2); // [8.7, 6.5, 4.3]

    ASSERT_RF( array[3] == 4.3, "[dr.containers.sequence.array] FAIL 1" );
    ASSERT_RF( fp_close(dr::alg_sum(array), 38.8), "[dr.containers.sequence.array] FAIL 2" );

    ASSERT_RF( subarray.size() == 3, "[dr.containers.sequence.array] FAIL 3" );
    ASSERT_RF( subarray[0] == 8.7 && subarray[1] == 6.5 && subarray[2] == 4.3,
        "[dr.containers.sequence.array] FAIL 4" );

    ASSERT_RF( fp_close(dr::alg_sum(subarray), 19.5), "[dr.containers.sequence.array] FAIL 5" );

    return true;
}

void test_multiple_array_shared()
{
    dr::vector<double> a(dr::wrap_inilst<double>( {1.0, 2.1, 3.2, 4.3, 5.4, 6.5, 7.6, 8.7} ));
    dr::vector<double> b = a;
    dr::vector<double> c = a;

    c[3] = 3.14;
    std::cout << a;

    b.resize(12);
    b[3] = 9;;
    std::cout << b;
    std::cout << c;
}

bool test_containers_sequence_array_shared()
{
    static dr::fp_close<double> fp_close(1e-6);

    // Conversion from initialiser list
    dr::vector<double> array(dr::wrap_inilst<double>( {1.0, 2.1, 3.2, 4.3, 5.4, 6.5, 7.6, 8.7} ));
    auto subarray = array.subarray(1,6,-2); // [7.6, 5.4, 3.2]

    ASSERT_RF( array[3] == 4.3, "[dr.containers.sequence.array_shared] FAIL 1" );
    ASSERT_RF( dr::op_round(dr::alg_prod(array)) == 67062, "[dr.containers.sequence.array_shared] FAIL 2" );

    ASSERT_RF( subarray.size() == 3, "[dr.containers.sequence.array_shared] FAIL 3" );

    subarray.reverse();
    ASSERT_RF( subarray[0] == 3.2 && subarray[1] == 5.4 && subarray[2] == 7.6,
        "[dr.containers.sequence.array_shared] FAIL 4" );

    array /= 2;
    ASSERT_RF( fp_close(dr::alg_sum(subarray), 16.2/2), "[dr.containers.sequence.array_shared] FAIL 5" );

    // test_multiple_array_shared();
    return true;
}

bool test_containers_sequence_array_fixed()
{
    static dr::fp_close<double> fp_close(1e-6);

    // Conversion from initialiser list
    auto array    = dr::fixed<double,8>( {1.0, 2.1, 3.2, 4.3, 5.4, 6.5, 7.6, 8.7} );
    auto subarray = array.subarray(0,7,3); // [1.0, 4.3, 7.6]

    ASSERT_RF( array[3] == 4.3, "[dr.containers.sequence.array_fixed] FAIL 1" );
    ASSERT_RF( subarray.size() == 3, "[dr.containers.sequence.array_fixed] FAIL 2" );

    subarray.reverse();
    ASSERT_RF( subarray[0] == 7.6 && subarray[1] == 4.3 && subarray[2] == 1.0,
        "[dr.containers.sequence.array_fixed] FAIL 3" );

    dr::alg_cumsum( subarray += 10 );
    ASSERT_RF( fp_close(subarray.back(), 42.9), "[dr.containers.sequence.array_fixed] FAIL 4" );

    return true;
}

bool test_containers_sequence_array_chunked()
{
    dr::chunked<double> array( dr::wrap_inilst<double>({1, 2, 3, 4, 5, 6}), 5 );

    ASSERT_RF( array.size() == 6, "[dr.containers.sequence.array_chunked] FAIL 1" );
    ASSERT_RF( array.capacity() == 10, "[dr.containers.sequence.array_chunked] FAIL 2" );
    ASSERT_RF( array[5] == 6, "[dr.containers.sequence.array_chunked] FAIL 3" );
    ASSERT_RF( dr::alg_sum(array) == 21, "[dr.containers.sequence.array_chunked] FAIL 4" );

    return true;
}

bool test_containers_sequence_accessors()
{
    typedef dr::fixed<double,8> container;

    auto array = container( {1.0, 2.1, 3.2, 4.3, 5.4, 6.5, 7.6, 8.7} );

    auto a_default = dr::accessor_factory<container>::create( dr::AccessorId::Default, array );
    auto a_cyclic = dr::accessor_factory<container>::create( dr::AccessorId::Cyclic, array );
    auto a_bcond = dr::accessor_factory<container>::create( dr::AccessorId::ConstantBoundary, array, -1.0 );

    ASSERT_RF( (*a_default)(-1) == 1.0, "[dr.container.sequence.accessors] FAIL 1" );
    ASSERT_RF( (*a_cyclic)(-1) == 8.7, "[dr.container.sequence.accessors] FAIL 2" );
    ASSERT_RF( (*a_bcond)(-1) == -1.0, "[dr.container.sequence.accessors] FAIL 3" );

    return true;
}

bool test_containers_sequence_utils()
{
    ASSERT_RF( dr::xrange(20)[7] == 7, "[dr.container.sequence.utils] FAIL 1" );

    auto R = dr::linspace<double>( 13, 42, 5 );

    ASSERT_RF( R.size() == 5, "[dr.container.sequence.utils] FAIL 2" );
    ASSERT_RF( R.front() == 13 && R.back() == 42, "[dr.container.sequence.utils] FAIL 3" );

    return true;
}

bool test_containers_sequence()
{
    ASSERT_RF( test_containers_sequence_array(), "[dr.containers.sequence.array] FAIL" );
    ASSERT_RF( test_containers_sequence_array_shared(), "[dr.containers.sequence.array_shared] FAIL" );
    ASSERT_RF( test_containers_sequence_array_fixed(), "[dr.containers.sequence.array_fixed] FAIL" );
    ASSERT_RF( test_containers_sequence_array_chunked(), "[dr.containers.sequence.array_chunked] FAIL" );
    ASSERT_RF( test_containers_sequence_accessors(), "[dr.containers.sequence.accessors] FAIL" );
    ASSERT_RF( test_containers_sequence_utils(), "[dr.containers.sequence.utils] FAIL" );

    return true;
}
