
#include <chrono>
#include <thread>
#include <vector>
#include <type_traits>



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



bool test_core_traits()
{
    ASSERT_RF( dr::type_selector() == 0, "[dr.core.traits.type_selector] FAIL 1" );
    ASSERT_RF( (dr::type_selector<false,false>() == 0), "[dr.core.traits.type_selector] FAIL 2" );
    ASSERT_RF( (dr::type_selector<true,false,false,true>() == 1), "[dr.core.traits.type_selector] FAIL 3" );
    ASSERT_RF( (dr::type_selector<false,false,true,false>() == 3), "[dr.core.traits.type_selector] FAIL 4" );

    ASSERT_RF( dr::bool_type< 2 != 4 >::value, "[dr.core.traits.bool_type] FAIL" );
    ASSERT_RF( dr::bool_negate<std::false_type>::value, "[dr.core.traits.bool_negate] FAIL" );

    ASSERT_RF( dr::has_begin_end< std::vector<double> >::value, "[dr.core.traits.has_begin_end] FAIL" );

    return true;
}

// ------------------------------------------------------------------------

bool test_core_macro()
{
    ASSERT_RF( Drayn_MIN(1,2) == 1, "[dr.core.macro.Drayn_MIN] FAIL" );
    ASSERT_RF( Drayn_MAX(1,2) == 2, "[dr.core.macro.Drayn_MAX] FAIL" );

    ASSERT_RF( Drayn_MIN3(1,2,3) == 1, "[dr.core.macro.Drayn_MIN3] FAIL" );
    ASSERT_RF( Drayn_MAX3(1,2,3) == 3, "[dr.core.macro.Drayn_MAX3] FAIL" );

    ASSERT_RF( OP5(2,*) == 32, "[dr.core.macro.OP] FAIL" );

    return true;
}

// ------------------------------------------------------------------------

bool test_core_messages()
{
    dr_println   ( "Testing %s.", "dr_println" );
    DRAYN_WARN_  ( "This is a '%s' message.", "Warning" );
    DRAYN_INFO_  ( "This is a '%s' message.", "Info" );
    DRAYN_DEBUG_ ( "This is a '%s' message.", "Debug" );

    try {
        DRAYN_THROW( "This is a '%s' message.", "Debug" );
    } catch ( const std::exception& e ) {
        dr_println( e.what() );
    }

    return true;
}

// ------------------------------------------------------------------------

bool test_core_patterns()
{
    dr::static_val<double>() = 3.14;
    auto u = dr::factory_create( dr::factory_tag<unsigned>(), 42 );
    unsigned v = *u; delete u;

    ASSERT_RF( dr::static_val<double>() == 3.14, "[dr.core.pattern.singleton] FAIL" );
    ASSERT_RF( v == 42, "[dr.core.pattern.factory] FAIL" );

    return true;
}

// ------------------------------------------------------------------------

// This will make the tests segfault if there is an error
bool test_core_allocators()
{
    typedef dr::allocator_calloc<unsigned>            A1;
    typedef dr::allocator_malloc<double>              A2;
    typedef dr::allocator_new< std::complex<float> >  A3;

    auto p1 = A1::allocate( 23 );
    auto p2 = A2::allocate( 0 );
    auto p3 = A3::allocate( 1 );

    dr::allocator_noalloc<float>::release( p3 );
    A2::release( p2, 0 );

    dr::static_val< A1::deleter >()( p1, 23 );
    A3::release( p3, 1 );

    return true;
}

// ------------------------------------------------------------------------

bool test_core_randoms()
{
    auto U = dr::uniform_gen();
    auto G = dr::normal_gen( 2.0, 1.0 );
    auto E = dr::exponential_gen( 0.5 );

    std::cout << "[dr.core.randoms] Uniform distribution (0,1):" << std::endl;
    for ( usize_t i = 0; i < 15; ++i ) dr_print("%.2f ", U());
    std::cout << std::endl;

    std::cout << "[dr.core.randoms] Normal distribution (2,1):" << std::endl;
    for ( usize_t i = 0; i < 15; ++i ) dr_print("%.2f ", G());
    std::cout << std::endl;

    std::cout << "[dr.core.randoms] Exponential distribution (0.5):" << std::endl;
    for ( usize_t i = 0; i < 15; ++i ) dr_print("%.2f ", E());
    std::cout << std::endl;

    return true;
}

// ------------------------------------------------------------------------

typedef const unsigned    pubsub_data;
std::vector<unsigned>     pubsub_result;
dr::events<pubsub_data>   pubsub_hub;

void pubsub_callback_fun( pubsub_data& val )
    { pubsub_result.at(0) = val; }

void pubsub_temporary( dr::signal<pubsub_data>& c, pubsub_data& v )
{
    auto s = c.subscribe( []( pubsub_data& val ){ pubsub_callback_fun(val); } );
    c.publish(v);
}

struct pubsub_structure
{
    dr::slot<pubsub_data> subscriber;

    pubsub_structure()
        : subscriber(pubsub_hub.channel(0).subscribe(
            [this] ( pubsub_data& val ){ this->member_callback(val); }
        )) {}

    void member_callback( pubsub_data& val )
        { pubsub_result.at(1) = val; }
};

bool test_core_pubsub()
{
    // allocate space for results
    pubsub_result.assign(2,0);

    // temporary subscriber
    pubsub_temporary( pubsub_hub.channel(0), 1 );
    ASSERT_RF( pubsub_result[0] == 1 && pubsub_result[1] == 0,
        "[dr.core.pubsub] FAIL 1" );

    // previous subscriber should have been deleted when going out of scope
    pubsub_structure S;
    pubsub_result[0] = 0;

    pubsub_hub.channel(0).publish(2);
    ASSERT_RF( pubsub_result[0] == 0 && pubsub_result[1] == 2,
        "[dr.core.pubsub] FAIL 2" );

    // try unsubsciption on-demand
    pubsub_hub.channel(0).unsubscribe( S.subscriber );
    pubsub_hub.channel(0).publish(4);
    ASSERT_RF( pubsub_result[0] == 0 && pubsub_result[1] == 2,
        "[dr.core.pubsub] FAIL 3" );

    return true;
}

// ------------------------------------------------------------------------

bool test_core_timer()
{
    dr::time_monitor monitor;

    monitor.start("example200");
    std::this_thread::sleep_for( std::chrono::milliseconds(200) );
    monitor.stop("example200");

    monitor.start("example700");
    std::this_thread::sleep_for( std::chrono::milliseconds(700) );
    monitor.stop("example700");

    monitor.report();

    return true;
}
