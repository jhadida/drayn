
#include <algorithm>
#include <vector>

// ------------------------------------------------------------------------

void time_algorithm_ternary()
{
    auto    v = dr::xrange(500000);
    auto    U = dr::uniform_gen<double>( -1.0, 5.0 );
    auto    T = dr::time_monitor();
    usize_t N = 100000;

    auto ternary = dr::vector<usize_t>(N);
    auto upper   = dr::vector<usize_t>(N);

    T.start("ternary");
    for ( usize_t i = 0; i < N; ++i )
        ternary[i] = dr::ternary_insert(v,v.size(),U());
    T.stop("ternary");

    T.start("upper");
    for ( usize_t i = 0; i < N; ++i )
        upper[i] = std::upper_bound(v.begin(),v.end(),U()) - v.begin();
    T.stop("upper");

    T.report();
}

bool test_algorithms_sort()
{
    std::vector<double> v = { 2.0, 13.0, -5.0, 2.1 }; // ranks: [2 0 3 1]

    // sort an initialier list
    auto r = dr::rank(dr::wrap(v));
    ASSERT_RF( r.size() == 4, "[dr.algorithm.rank] FAIL 1" );
    ASSERT_RF( r[0]==2 && r[1]==0 && r[2]==3 && r[3]==1, "[dr.algorithm.rank] FAIL 2" );

    // sort a vector
    dr::vector<double> a(dr::wrap_inilst<double>( {8.7, 3.2, 7.6, 4.3, 2.1, 5.4, 6.5, 1.0} ));
    auto s = dr::rank(a);
    ASSERT_RF( s.size() == a.size(), "[dr.algorithm.rank] FAIL 3" );
    ASSERT_RF( s[0]==7 && s[1]==4 && s[2]==1 && s[3]==3 && s[4]==5 && s[5]==6 && s[6]==2, "[dr.algorithm.rank] FAIL 4" );

    // apply ranking
    dr::vector<double> u(v.size());
    map( r.iter(), u.iter(), [&v]( usize_t i ){ return v[i]; } );
    ASSERT_RF( dr::is_ascending(u), "[dr.algorithm.rank] FAIL 5" );

    // test ternary insertion
    auto w     = dr::xrange(4);
    auto urand = dr::uniform_gen<double>( -1.0, 5.0 );

    for ( usize_t i = 0; i < 100; ++i )
	{
		double val = urand();

		usize_t x = dr::ternary_insert(w,w.size(),val);
		usize_t y = std::upper_bound(w.begin(),w.end(),val) - w.begin();

		ASSERT_RF( x == y, "[dr.algorithm.rank] FAIL 6" );
	}

    // time_algorithm_ternary();
    return true;
}

bool test_algorithms_interp()
{
    auto x = dr::xrange(7);
    auto I = dr::interp_linear<decltype(x)>(x);

    ASSERT_RF( I(3.4) == 3.4, "[dr.algorithm.interp_linear] FAIL 1" );

    return true;
}

bool test_algorithms_math()
{
    return true;
}

bool test_algorithms_stats()
{
    return true;
}
