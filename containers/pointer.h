
//==================================================
// @title        pointer.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <utility>

/**
 * C-pointer wrapper with explicit step-size (allowing for "jumps" instead of contiguous steps).
 *
 * Note, from http://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B#cite_note-arrowptr-6
 * The return type of operator->() must be a type for which the -> operation can be applied, such as a pointer type.
 * If x is of type C where C overloads operator->(), x->y gets expanded to x.operator->()->y.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T>
class pointer
	: public _random_access_iterator< T, pointer<T> >
{
public:

	CORE_TRAITS(T);
	typedef pointer<T>                     self;
	typedef bool_negate<std::is_const<T>>  is_mutable;


	pointer()
		{ clear(); }
	pointer( ptr_t p, isize_t s = 1 ) // allow implicit conversion from pointer
		{ assign(p,s); }

	// Implicit conversion when static_cast allows
	template <class U>
	pointer( const pointer<U>& p )
		{ assign( static_cast<ptr_t>(p.memptr()), p.get_step() ); }

	// Cast to pointer
	inline operator ptr_t () const
		{ return m_ptr; }

	// Legacy: _random_access_iterator
	void        clear      ();
	void        swap       ( self& that );
	isize_t     operator-  ( const self& that ) const;
	inline bool comparable ( const self& that ) const { return m_step == that.m_step; }

	inline void increment( usize_t n=1 ) { m_ptr += static_cast<isize_t>(n)*m_step; }
	inline void decrement( usize_t n=1 ) { m_ptr -= static_cast<isize_t>(n)*m_step; }

	inline ref_t operator[] ( usize_t n ) const
		{ return *(m_ptr + static_cast<isize_t>(n)*m_step); }

	// ------------------------------------------------------------------------

	// Validity
	inline bool    valid() const { return m_ptr; }
	inline bool   locked() const { return m_step == 0; }
	inline operator bool() const { return valid() && !locked(); }

	// Change pointer direction
	inline void lock   () { m_step = 0; }
	inline void reverse() { m_step = -m_step; }

	// Access members
	inline ptr_t   memptr() const
		{ return m_ptr; }
	inline isize_t get_step() const
		{ return m_step; }
	inline void    set_step( isize_t step )
		{ m_step = step; }

	// Assignment
	inline self& operator =( ptr_t p )
		{ assign(p,1); return *this; }
	inline void assign( ptr_t p, isize_t s = 1 )
		{ m_ptr=p; m_step=s; }

private:

	ptr_t    m_ptr;
	isize_t  m_step;
};



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class T>
void pointer<T>::clear()
{
	m_ptr  = nullptr;
	m_step = 0;
}

// ------------------------------------------------------------------------

template <class T>
void pointer<T>::swap( self& that )
{
	std::swap( m_ptr , that.m_ptr  );
	std::swap( m_step, that.m_step );
}

// ------------------------------------------------------------------------

template <class T>
isize_t pointer<T>::operator- ( const self& that ) const
{
	if ( !comparable(that) )
		throw std::invalid_argument( "[dr.pointer.operator-] Input is not comparable." );

	return std::distance(that.m_ptr,m_ptr) / m_step;
}

DRAYN_NS_END_
