
//==================================================
// @title        array.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <stdexcept>
#include <utility>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T>
class array :
	public array_traits<T>::sequential_parent,
	public array_traits<T>::iterable_parent,
	public array_traits<T>::arithmetic_parent,
	public array_traits<T>::comparable_parent,
	public array_traits<T>::copyable_parent
{
public:

	CORE_TRAITS(T);
	typedef array<T>         self;
	typedef array_traits<T>  traits;
	typedef tag_array        tag;

	typedef typename traits::container_type  container_type;
	typedef typename traits::std_iterator    std_iterator;
	typedef typename traits::dr_iterator     dr_iterator;
	typedef typename traits::data_type       data_t;

	typedef sequence_wrapper<container_type,val_t> subarray_type;

	// ----------  =====  ----------


	array()
		{ clear(); }
	array( ptr_t ptr, usize_t len )
		{ assign(ptr,len); }

	// Accept implicit conversions when they compile
	template <class U>
	array( const array<U>& that )
		{ assign(that); }

	// Conversion to const variant
	inline operator array<const T> () const
		{ return array<const T>( data(), size() ); }


	// Instance management
	inline void clear() { m_ptr=nullptr; m_size=0; }
	inline self clone() const { return *this; }

	inline void swap( self& that )
		{ std::swap( m_ptr, that.m_ptr ); std::swap( m_size, that.m_size ); }


	// Extract subarray
	inline subarray_type subarray( usize_t first, usize_t last, isize_t step = 1 ) const
		{ return subarray_type( m_ptr, first, last, step ); }


	// Legacy: _sequential
	inline data_t   data  () const { return m_ptr; }
	inline usize_t  size  () const { return m_size; }
	inline bool     valid () const { return m_ptr; }

	// Legacy: _iterable
	inline std_iterator  begin () const { return std_iterator(m_ptr); }
	inline std_iterator    end () const { return begin()+size(); }

	inline std_iterator rbegin () const { auto p =   end(); p.reverse(); return ++p; }
	inline std_iterator   rend () const { auto p = begin(); p.reverse(); return ++p; }

	inline dr_iterator    iter () const { return dr_iterator(  begin(),  end() ); }
	inline dr_iterator   riter () const { return dr_iterator( rbegin(), rend() ); }

	// Legacy: _comparable
	inline bool operator== ( const self& that ) const
		{ return m_size==that.m_size && m_ptr==that.m_ptr; }


	// Assignment
	template <class U>
	inline self& assign( const array<U>& that )
		{ return assign( that.data(), that.size() ); }

	template <class U>
	inline self& operator= ( const array<U>& that )
		{ return assign(that); }

	inline self& assign( ptr_t ptr, usize_t len )
		{ m_ptr = ptr; m_size = len; return *this; }

protected:

	ptr_t    m_ptr;
	usize_t  m_size;
};

DRAYN_NS_END_
