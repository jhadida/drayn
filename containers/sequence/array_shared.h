
//==================================================
// @title        array_shared.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <algorithm>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T, class A = allocator_new<T>>
class array_shared :
	public array_shared_traits<T,A>::sequential_parent,
	public array_shared_traits<T,A>::iterable_parent,
	public array_shared_traits<T,A>::arithmetic_parent,
	public array_shared_traits<T,A>::comparable_parent,
	public array_shared_traits<T,A>::copyable_parent
{
public:

	CORE_TRAITS(T);
	typedef array_shared<T,A>         self;
	typedef array_shared_traits<T,A>  traits;
	typedef tag_array_shared          tag;

	typedef A                                 allocator_type;
	typedef typename traits::container_type   container_type;
	typedef typename traits::std_iterator     std_iterator;
	typedef typename traits::dr_iterator      dr_iterator;
	typedef typename traits::data_type        data_t;

	typedef sequence_wrapper<container_type,val_t> subarray_type;

	static_assert( traits::is_mutable::value,
		"Immutable array_shared are useless, use array instead." );

	static_assert( std::is_same<T,typename A::val_t>::value,
		"Type mismatch between value type and allocator." );

	// ----------  =====  ----------


	array_shared()
		{ clear(); }
	array_shared( const shared<T,A>& ptr )
		{ assign(ptr); }

	// Create
	explicit
	array_shared( usize_t n )
		{ create(n); }
	array_shared( usize_t n, const T& val )
		{ create(n,val); }

	// Deep-copy
	template <class U, class S, class D>
	explicit array_shared( const _iterable<U,S,D>& vals )
		{ copy(vals); }

	// Conversion to array
	template <class U>
	inline operator array<U>() const
		{ return array<U>( m_ptr.get(), size() ); }


	// Instance management methods
	inline void clear()
		{ m_ptr.clear(); }
	inline self clone() const
		{ return self(*this); }
	inline void swap( self& that )
		{ m_ptr.swap( that.m_ptr ); }

	template <class U>
	void copy( const U& that );


	// Resizing (create: discard current, resize: copy current)
	inline self& create( usize_t n ) { m_ptr.create(n); return *this; }
	inline self& create( usize_t n, const T& val ) { m_ptr.create(n,val); return *this; }
	self& resize( usize_t n );


	// Extract subarray
	inline subarray_type subarray( usize_t first, usize_t last, isize_t step = 1 ) const
		{ return subarray_type( m_ptr, first, last, step ); }


	// Legacy: _sequential
	inline data_t   data  () const { return m_ptr; }
	inline usize_t  size  () const { return m_ptr.size(); }
	inline bool     valid () const { return m_ptr; }

	// Legacy: _iterable
	inline std_iterator  begin () const { return std_iterator( m_ptr.get() ); }
	inline std_iterator    end () const { return begin()+size(); }

	inline std_iterator rbegin () const { auto p =   end(); p.reverse(); return ++p; }
	inline std_iterator   rend () const { auto p = begin(); p.reverse(); return ++p; }

	inline dr_iterator    iter () const { return dr_iterator(  begin(),  end() ); }
	inline dr_iterator   riter () const { return dr_iterator( rbegin(), rend() ); }

	// Legacy: _comparable
	inline bool operator== ( const self& that ) const
		{ return m_ptr==that.m_ptr; }


	// Assignment
	inline self& operator= ( const shared<T,A>& ptr ) { assign(ptr); return *this; }
	inline self& assign    ( const shared<T,A>& ptr ) { m_ptr = ptr; return *this; }

protected:

	shared<T,A> m_ptr;
};



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class T, class A>
template <class U>
void array_shared<T,A>::copy( const U& that )
{
	create( that.size() );
	map( iterable_get_iterator(that), iter(), f_cast<T>() );
}

// ------------------------------------------------------------------------

template <class T, class A>
auto array_shared<T,A>::resize( usize_t n ) -> self& // copy existing contents
{
	if ( n == 0 )
		{ clear(); return *this; }

	if ( n != size() )
	{
		// New allocation
		self that(n);

		// Copy what we can
		if ( !this->empty() )
			map( iter(), that.iter(), []( const T& val ){ return val; } );

		// Overwrite current array
		*this = that;
	}
	return *this;
}

DRAYN_NS_END_
