
//==================================================
// @title        accessors.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <memory>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * Abstract sequential accessor interface.
 */

/**
 * Accessor traits for containers.
 */
#define ACCESSOR_TRAITS  \
typedef usize_t  uidx;   \
typedef isize_t  iidx;   \
typedef double   ridx;

// ----------  =====  ----------

template <class Container>
struct accessor
{
	FORWARD_TRAITS(Container);
	ACCESSOR_TRAITS

	const Container *m_ctn;

	// Assignment
	inline virtual void clear()
		{ m_ctn = nullptr; }
	inline virtual void assign( const Container& ctn )
		{ m_ctn = &ctn; }

	// Validity
	inline virtual bool valid() const
		{ return m_ctn; }
	inline virtual operator bool() const
		{ return valid(); }

	// Required implementations
	inline virtual usize_t size() const
		{ return valid()? m_ctn->size() : 0; }

	virtual ref_t operator() ( const uidx& x ) const =0;
	virtual ref_t operator() ( const iidx& x ) const =0;
	virtual ref_t operator() ( const ridx& x ) const =0;
};

// ------------------------------------------------------------------------

// Include implementations
#include "accessors/default.h"
#include "accessors/cyclic.h"
#include "accessors/bcond.h"

// ------------------------------------------------------------------------

enum class AccessorId
	: char { Default=0, Cyclic, WrapAround, Bcond, ConstantBoundary };

template <class C>
struct accessor_factory
{
	typedef C                                container_type;
	typedef accessor<C>                      accessor_type;
	typedef std::shared_ptr<accessor_type>   output_type;
	typedef AccessorId                       id_type;


	template <class... Args>
	static output_type create( id_type id, Args&&... args )
	{
		output_type out;
		switch ( id )
		{
			case id_type::Cyclic:
			case id_type::WrapAround:
				out.reset( factory_create(
					factory_tag< accessor_cyclic<C> >(),
					std::forward<Args>(args)...
				) );
				break;

			case id_type::Bcond:
			case id_type::ConstantBoundary:
				out.reset( factory_create(
					factory_tag< accessor_bcond<C> >(),
					std::forward<Args>(args)...
				) );
				break;

			default:
				out.reset( factory_create(
					factory_tag< accessor_default<C> >(),
					std::forward<Args>(args)...
				) );
				break;
		}
		return out;
	}
};

DRAYN_NS_END_
