
//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "sequence_traits.h"
#include "utils/is_contiguous.h"
#include "utils/is_resizeable.h"

// Generic sequence objects
#include "container_wrapper.h"
#include "sequence_wrapper.h"
#include "sequence_iterator.h"

// Arrays
#include "array.h"
#include "array_shared.h"
#include "array_fixed.h"
#include "array_chunked.h"

// Accessors
#include "accessors.h"

// Utils
#include "utils/aliases.h"
#include "utils/wrap.h"
#include "utils/linspace.h"
