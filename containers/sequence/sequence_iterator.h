
//==================================================
// @title        sequence_iterator.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <tuple>
#include <utility>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class S, class T = typename S::val_t >
class sequence_iterator
	: public _random_access_iterator< T, sequence_iterator<S,T> >
{
public:

	CORE_TRAITS(T);
	typedef sequence_iterator<S,T>         self;
	typedef sequence_wrapper<S,T>          wrapper_t;
	typedef typename wrapper_t::data_t     data_t;
	typedef bool_negate<std::is_const<T>>  is_mutable;

	// ----------  =====  ----------


	sequence_iterator()
		{ clear(); }
	sequence_iterator( const wrapper_t& seq )
		{ assign(seq); }
	sequence_iterator( data_t seq, usize_t first, usize_t last, isize_t step = 1 )
		{ assign(seq,first,last,step); }


	// Legacy: _random_access_iterator
	inline void clear ()             { m_seq.clear(); m_ind=0; }
	inline void swap  ( self& that ) { m_seq.swap(that.m_seq); bit_swap(m_ind,that.m_ind); }

	inline void increment ( usize_t n=1 ) { m_ind += n; }
	inline void decrement ( usize_t n=1 ) { m_ind -= Drayn_MIN(n,m_ind); }
	inline void reset     ()              { m_ind  = 0; }

	inline bool comparable ( const self& that ) const
		{ return m_seq == that.m_seq; }
	inline isize_t operator-  ( const self& that ) const
	{
		if ( !comparable(that) )
			throw std::invalid_argument( "[dr.sequence_iterator.operator-] Input is not comparable." );

		return static_cast<isize_t>(m_ind) - static_cast<isize_t>(that.m_ind);
	}

	// Value access
	inline const wrapper_t& data() const { return m_seq; }
	inline usize_t          size() const { return m_seq.size(); }

	inline ref_t operator[] ( usize_t n ) const { return m_seq[m_ind+n]; }

	// Validity
	inline bool      valid () const { return m_seq.valid(); }
	inline operator  bool  () const { return valid() && m_ind < size(); }

	// Assignment
	inline void assign( data_t seq, usize_t first, usize_t last, isize_t step = 1 )
		{ m_seq.assign(seq,first,last,step); reset(); }

	inline void assign( const wrapper_t& seq )
		{ m_seq = seq; reset(); }

private:

	usize_t    m_ind;
	wrapper_t  m_seq;
};

DRAYN_NS_END_
