
//==================================================
// @title        container_wrapper.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



DRAYN_NS_START_

template <class C>
struct container_wrapper
{
	typedef container_wrapper<C>  self;
    typedef const C&              data_type;

	const C *m_ptr;

	container_wrapper() { clear(); }
	container_wrapper( data_type c ) { set(c); }

	inline void clear() { m_ptr = nullptr; }
    inline operator bool() const { return m_ptr; }

	inline self&     set ( data_type c ) { m_ptr = &c; return *this; }
	inline data_type get () const { return *m_ptr; }
};

template <class T>
struct container_wrapper<T*>
{
	typedef container_wrapper<T*>  self;
    typedef T*                     data_type;

	T *m_ptr;

	container_wrapper() { clear(); }
	container_wrapper( data_type p ) { set(p); }

	inline void clear() { m_ptr = nullptr; }
    inline operator bool() const { return m_ptr; }

	inline self&     set ( data_type p ) { m_ptr = p; return *this; }
	inline data_type get () const { return m_ptr; }
};

DRAYN_NS_END_
