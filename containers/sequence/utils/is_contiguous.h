
//==================================================
// @title        is_contiguous.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * This question can only be answered at run-time.
 */

template <class Array>
inline bool is_contiguous( const Array& a, tag_array )
	{ return true; }

template <class Array>
inline bool is_contiguous( const Array& a, tag_array_fixed )
	{ return true; }

template <class Array>
inline bool is_contiguous( const Array& a, tag_array_shared )
	{ return true; }

template <class Array>
inline bool is_contiguous( const Array& a, tag_array_chunked )
	{ return a.n_chunks() == 1; }

// ------------------------------------------------------------------------

template <class Array>
inline bool is_contiguous( const Array& a )
	{ return is_contiguous( a, typename Array::tag() ); }

DRAYN_NS_END_
