
//==================================================
// @title        wrap.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <vector>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// Convert initializer list to array
template <class T>
inline constexpr array<const T> wrap_inilst( const inilst<T>& lst )
	{ return array<const T>( lst.begin(), lst.size() ); }

// Wrap std::vector into dr::array
template <class T>
inline constexpr array<T> wrap( std::vector<T>& vec )
	{ return array<T>( vec.data(), vec.size() ); }

template <class T>
inline constexpr array<const T> wrap( const std::vector<T>& vec )
	{ return array<const T>( vec.data(), vec.size() ); }

DRAYN_NS_END_
