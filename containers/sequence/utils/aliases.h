
//==================================================
// @title        aliases.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// Fixed-size array
template <class T, uint16_t n> 
using fixed = array_fixed<T,n>;

template <uint16_t n> using fix_b   = array_fixed<    bool,n>;
template <uint16_t n> using fix_i8  = array_fixed<  int8_t,n>;
template <uint16_t n> using fix_u8  = array_fixed< uint8_t,n>;
template <uint16_t n> using fix_i16 = array_fixed< int16_t,n>;
template <uint16_t n> using fix_u16 = array_fixed<uint16_t,n>;
template <uint16_t n> using fix_i32 = array_fixed< int32_t,n>;
template <uint16_t n> using fix_u32 = array_fixed<uint32_t,n>;
template <uint16_t n> using fix_i64 = array_fixed< int64_t,n>;
template <uint16_t n> using fix_u64 = array_fixed<uint64_t,n>;
template <uint16_t n> using fix_f   = array_fixed<   float,n>;
template <uint16_t n> using fix_d   = array_fixed<  double,n>;

// Shared array
template <class T, class A = allocator_new<T>>
using vector = array_shared<T,A>;

// Chunked array
template <class T>
using chunked = array_chunked<T>;

// Array of immutable elements
template <class T>
using const_array = array<const T>;

DRAYN_NS_END_
