
//==================================================
// @title        is_resizeable.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * Resizeability can be answered at compile-time.
 */

template <class Array, class Tag = typename Array::tag >
struct is_resizeable: bool_type<false> {};

template <class Array>
struct is_resizeable<Array,tag_array_shared>
	: bool_negate<std::is_same<
		typename Array::allocator_type::tag,
		allocator_noalloc<typename Array::val_t>
	>> {};

template <class Array>
struct is_resizeable<Array,tag_array_chunked>
	: bool_negate<std::is_same<
		typename Array::allocator_type::tag,
		allocator_noalloc<typename Array::val_t>
	>> {};

DRAYN_NS_END_
