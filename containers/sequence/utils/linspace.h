
//==================================================
// @title        linspace.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// N = x.size(), x = { first + k*step, k=0..N }
template <class T>
void linstep( const array<T>& x, double first, double step )
{
	const usize_t n = x.size();
	for ( usize_t i = 0; i < n; ++i )
		x[i] = static_cast<T>( first + i*step );
}

// ------------------------------------------------------------------------

// x = { 0..N }
template <class T = usize_t>
inline vector<T> xrange( usize_t n )
{
	vector<T> v(n);
	linstep<T>(v, 0, 1); return v;
}

// ------------------------------------------------------------------------

// N = x.size(), x = { first + k*(last-first)/N, k=0..N }
template <class T>
void linrange( const array<T>& x, double first, double last )
{
	const usize_t n = x.size();
	ASSERT_R( n > 1, "Array must contain more than one element." );
	linstep<T>( x, first, (last-first)/(n-1.0) );
}

// ------------------------------------------------------------------------

// ditto with memory allocation
template <class T>
vector<T> linspace( T first, T last, usize_t n )
{
	vector<T> v(n);
	linrange<T>( v, first, last );
	return v;
}

DRAYN_NS_END_
