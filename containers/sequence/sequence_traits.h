
//==================================================
// @title        sequence_traits.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <deque>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class S,class T> class sequence_iterator;

template <
	class Value, class Container, class Self,
	class StdIterator = sequence_iterator< Container, Value >,
	class DrIterator  = sequence_iterator< Container, Value >
>
struct sequence_traits
{
	using self_type          = Self;
	using container_type     = Container;

	using value_type         = Value;
	using is_mutable         = bool_negate<std::is_const<value_type>>;

	using std_iterator       = StdIterator;
	using dr_iterator        = DrIterator;


	using iterable_parent    = _iterable< Value, StdIterator, DrIterator >;
	using sequential_parent  = _sequential< Value, Container, Self >;
	using arithmetic_parent  = _arithmetic< Value, Self >;
	using comparable_parent  = _comparable< Value, Self >;
	using copyable_parent    = _copyable< Value, DrIterator, is_mutable::value >;

	using data_type          = typename sequential_traits<sequential_parent>::data_type;
};

// ------------------------------------------------------------------------

// See: containers/sequence/sequence_wrapper
template <class S,class T> class sequence_wrapper;
template <class S,class T> using sequence_wrapper_traits =
	sequence_traits< T, S, sequence_wrapper<S,T> >;

// Array without memory management.
// See: containers/sequence/array
struct tag_array {};
template <class T> class array;
template <class T> using array_traits =
	sequence_traits< T, T*, array<T>, pointer<T>, iterator_wrapper<pointer<T>,T> >;

// Array using shared memory.
// See: containers/sequence/array_shared
struct tag_array_shared {};
template <class T,class A> class array_shared;
template <class T,class A> using array_shared_traits =
	sequence_traits< T, shared<T,A>, array_shared<T,A>, pointer<T>, iterator_wrapper<pointer<T>,T> >;

// Array on stack memory.
// See: containers/sequence/array_fixed
struct tag_array_fixed {};
template <class T, uint16_t N> class array_fixed;
template <class T, uint16_t N> using array_fixed_traits =
	sequence_traits< T, std::array<T,N>, array_fixed<T,N>, pointer<T>, iterator_wrapper<pointer<T>,T> >;

// Array with non-contiguous block-memory storage.
// See: containers/sequence/array_chunked
struct tag_array_chunked {};
template <class T,class A> class array_chunked;
template <class T,class A> using array_chunked_traits =
	sequence_traits< T, std::deque<array_shared<T,A>>, array_chunked<T,A>,
		sequence_iterator<array_chunked<T,A>,T>,
		sequence_iterator<array_chunked<T,A>,T> >;

DRAYN_NS_END_
