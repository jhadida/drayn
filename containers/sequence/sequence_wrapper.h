
//==================================================
// @title        sequence_wrapper.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <utility>
#include <tuple>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class S, class T = typename S::val_t >
class sequence_wrapper :
	public sequence_wrapper_traits<S,T>::sequential_parent,
	public sequence_wrapper_traits<S,T>::iterable_parent,
	public sequence_wrapper_traits<S,T>::arithmetic_parent,
	public sequence_wrapper_traits<S,T>::comparable_parent,
	public sequence_wrapper_traits<S,T>::copyable_parent
{
public:

	CORE_TRAITS(T);
	typedef sequence_wrapper<S,T>         self;
	typedef sequence_wrapper_traits<S,T>  traits;

	typedef self                           subarray_type;
	typedef typename traits::std_iterator  std_iterator;
	typedef typename traits::dr_iterator   dr_iterator;

	typedef container_wrapper<S>           seq_t;
	typedef typename seq_t::data_type      data_t;

	// ----------  =====  ----------


	sequence_wrapper()
		{ clear(); }
	sequence_wrapper( data_t seq, usize_t first, usize_t last, isize_t step = 1 )
		{ assign(seq,first,last,step); }


	void clear();
	void swap( self& that );

	// Extract subarray
	inline subarray_type subarray( usize_t i, usize_t j, isize_t step = 1 ) const
		{ return subarray_type( m_seq.get(), index(i), index(j), m_step*step ); }

	// Convert to contiguous array (may throw error)
	inline array<T> to_array() const;


	// Legacy: _sequential
	inline data_t   data  () const { return m_seq.get(); }
	inline usize_t  size  () const { return m_size; }
	inline bool     valid () const { return m_seq && m_step != 0 && m_size > 0; }

	// Legacy: _iterable
	inline std_iterator  begin () const { return std_iterator( *this ); }
	inline std_iterator    end () const { return  begin() + size(); }

	inline std_iterator rbegin () const { return std_iterator( m_seq.get(), m_last, m_first, -m_step ); }
	inline std_iterator   rend () const { return rbegin() + size(); }

	inline dr_iterator    iter () const { return  begin(); }
	inline dr_iterator   riter () const { return rbegin(); }

	// Legacy: _comparable
	inline bool operator== ( const self& that ) const
	{
		return m_seq   ==  that.m_seq
			&& m_first ==  that.m_first
			&& m_last  ==  that.m_last
			&& m_step  ==  that.m_step;
	}

	// Assignment
	self& assign( data_t seq, usize_t first, usize_t last, isize_t step = 1 );

	// Reverse the sequence
	inline self& reverse()
		{ bit_swap(m_first,m_last); m_step = -m_step; return *this; }

	// Get current step
	inline isize_t get_step() const
		{ return m_step; }

	// Hide inherited subscripting
	inline ref_t operator[] ( usize_t n ) const
		{ return const_cast<ref_t>( data()[index(n)] ); }

protected:

	inline usize_t index( usize_t k ) const
		{ return m_first + Drayn_IDX(k,m_size) * m_step; }

	seq_t    m_seq;
	usize_t  m_first, m_last, m_size;
	isize_t  m_step;
};



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class S, class T>
void sequence_wrapper<S,T>::clear()
{
	m_seq.clear();

	m_step  = 0;
	m_first = m_last = m_size = 0;
}

// ------------------------------------------------------------------------

template <class S, class T>
void sequence_wrapper<S,T>::swap( self& that )
{
	std::swap(
		stay(std::tie( m_seq, m_first, m_last, m_size, m_step )),
		stay(std::tie( that.m_seq, that.m_first, that.m_last, that.m_size, that.m_step ))
	);
}

// ------------------------------------------------------------------------

template <class S, class T>
array<T> sequence_wrapper<S,T>::to_array() const
{
	if ( m_step != 1 || !is_contiguous(data()) )
		throw std::runtime_error("[dr.sequence_wrapper.to_array] Underlying sequence cannot be converted to an array.");

	return array<T>( &(data()[0]), size() );
}

// ------------------------------------------------------------------------

template <class S, class T>
auto sequence_wrapper<S,T>::assign( data_t seq, usize_t first, usize_t last, isize_t step ) -> self&
{
	ASSERT_RVAL( step != 0, *this,
		"[dr.sequence_wrapper.assign] Step should be non-zero." );

	if ( first < last && step < 0 ) bit_swap( first, last );
	if ( first > last && step > 0 ) step = -step;

	// Assign members
	m_seq.set(seq);
	m_first = first;
	m_last  = last;
	m_step  = step;

	if ( m_step > 0 )
	{
		DBG_ASSERT( m_first <= m_last, "[dr.sequence_wrapper.assign] This is a bug." );
		m_size = 1 + (m_last-m_first) / m_step;
	}
	else
	{
		DBG_ASSERT( m_first >= m_last, "[dr.sequence_wrapper.assign] This is a bug." );
		m_size = 1 + (m_first-m_last) / (-m_step);
	}

	m_last = index( m_size-1 );
	return *this;
}

DRAYN_NS_END_
