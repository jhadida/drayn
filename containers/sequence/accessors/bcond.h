
//==================================================
// @title        bcond.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/


/**
 * Accessor with constant-value boundary-condition.
 */

template <class Container>
class accessor_bcond
	: public accessor<Container>
{
public:

	FORWARD_TRAITS( Container );
	ACCESSOR_TRAITS
	typedef accessor_bcond<Container> self;

	// Boundary value
	mutable typename std::remove_const<val_t>::type bvalue;


	accessor_bcond()
		{ this->clear(); }
	accessor_bcond( const Container& ctn, cref_t val = val_t() )
		{ this->assign(ctn); set_bvalue(val); }

	// Set value at the boundary
	inline void set_bvalue( cref_t val ) { bvalue = val; }

	// Forward operator() to _value_of
	inline ref_t operator() ( const uidx& x ) const { return _value_of(x); }
	inline ref_t operator() ( const iidx& x ) const { return _value_of(x); }
	inline ref_t operator() ( const ridx& x ) const { return _value_of(x); }

private:

	template <class T>
	ref_t _value_of( const T& x ) const
	{
		DBG_ASSERT_RVAL( this->valid(), bvalue,
			"[dr.accessor_bcond] Accessor is not ready yet!" );

		iidx k = op_iround(x);
		if ( k >= 0 && k < static_cast<iword_t>(this->size()) )
			return (*this->m_ctn)[k];
		else
			return bvalue;
	}
};
