
//==================================================
// @title        default.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class Container>
class accessor_default
	: public accessor<Container>
{
public:

	FORWARD_TRAITS( Container );
	ACCESSOR_TRAITS
	typedef accessor_default<Container> self;


	accessor_default()
		{ this->clear(); }
	accessor_default( const Container& ctn )
		{ this->assign(ctn); }

	// Forward operator() to _value_of
	inline ref_t operator() ( const uidx& x ) const
		{ return _value_of(x); }
	inline ref_t operator() ( const iidx& x ) const
		{ return _value_of(x); }
	inline ref_t operator() ( const ridx& x ) const
		{ return _value_of(x); }

private:

	template <class T>
	inline ref_t _value_of( const T& x ) const
	{
		DBG_ASSERT_RVAL( this->valid(), static_val<val_t>(),
			"[dr.accessor_default] Accessor is not ready yet!" );
		usize_t idx = op_clamp< iword_t >( op_iround(x), 0, this->size()-1 );
		return (*this->m_ctn)[idx];
	}
};
