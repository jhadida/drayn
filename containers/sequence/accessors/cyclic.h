
//==================================================
// @title        cyclic.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



/**
 * Accessor with wrap-arround bounary-condition.
 */

template <class Container>
class accessor_cyclic
	: public accessor<Container>
{
public:

	FORWARD_TRAITS( Container );
	ACCESSOR_TRAITS
	typedef accessor_cyclic<Container> self;


	accessor_cyclic()
		{ this->clear(); }
	accessor_cyclic( const Container& ctn )
		{ this->assign(ctn); }

	// Forward operator() to _value_of
	inline ref_t operator() ( const uidx& x ) const
		{ return _value_of(x); }
	inline ref_t operator() ( const iidx& x ) const
		{ return _value_of(x); }
	inline ref_t operator() ( const ridx& x ) const
		{ return _value_of(x); }

private:

	template <class T>
	inline ref_t _value_of( const T& x ) const
	{
		DBG_ASSERT_RVAL( this->valid(), static_val<val_t>(),
			"[dr.accessor_cyclic] Accessor is not ready yet!" );
		return (*this->m_ctn)[ static_cast<usize_t>(op_mod(
			op_iround(x), static_cast<iword_t>(this->size())
		)) ];
	}

	inline ref_t _value_of( const uidx& x ) const
	{
		DBG_ASSERT_RVAL( this->valid(), static_val<val_t>(),
			"Accessor is not ready yet!" );
		return (*this->m_ctn)[ x % this->size() ];
	}
};
