
//==================================================
// @title        shared.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <new>
#include <utility>
#include <algorithm>
#include <stdexcept>

/**
 * Custom shared-pointer class with allocation methods.
 *
 * Note:
 * This is only suitable if the value type is default-constructible,
 * otherwise use std::shared_ptr.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T, class Allocator = allocator_new<T> >
class shared
{
public:

	CORE_TRAITS(T);
	typedef shared<T,Allocator>  self;
	typedef Allocator            alloc;

	// ----------  =====  ----------


	shared()
		: m_ref_count(nullptr)
		{ clear(); }
	shared( ptr_t ptr, usize_t size )
		: m_ref_count(nullptr)
		{ assign(ptr,size); }
	shared( const self& that )
		: m_ref_count(nullptr)
		{ assign(that); }
	~shared()
		{ clear(); }

	explicit
	shared( usize_t n )
		: m_ref_count(nullptr)
		{ create(n); }
	shared( usize_t n, const T& val )
		: m_ref_count(nullptr)
		{ create(n,val); }

	// Cast to pointer
	inline operator ptr_t () const
		{ return m_ptr; }


	void clear();
	void swap( self& that );

	self& create( usize_t n );
	inline self& create( usize_t n, const T& val )
		{ create(n); std::fill_n( get(), n, val ); return *this; }


	// Access data
	inline usize_t size () const { return m_size; }
	inline ptr_t   get  () const { return m_ptr; }

	inline ref_t operator*  () const { return *get(); }
	inline ptr_t operator-> () const { return  get(); }

	inline ref_t operator[] ( usize_t n ) { return m_ptr[n]; }

	// Reference counting
	inline usize_t ref_count () const { return *m_ref_count; }
	inline bool    unique    () const { return ref_count() == 1; }

	// Validity
	inline bool    valid () const { return m_ref_count; }
	inline bool    empty () const { return m_size == 0; }
	inline operator bool () const { return valid() && !empty(); }

	// Comparison
	inline bool operator== ( const self& that ) const { return m_ref_count == that.m_ref_count; }
	inline bool operator!= ( const self& that ) const { return !operator==(that); }

	// Assignment
	inline self& operator= ( const self& that )
		{ assign(that); return *this; }

	self& assign( const self& that );
	self& assign( ptr_t ptr, usize_t size );

private:

	ptr_t     m_ptr;
	usize_t  *m_ref_count;
	usize_t   m_size;
};



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class T, class A>
void shared<T,A>::clear()
{
	// Free memory or decrease references count
	if ( valid() )
	{
		if ( unique() )
		{
			alloc::release( m_ptr, m_size );
			delete m_ref_count;
		}
		else
			--(*m_ref_count);
	}

	// Reset members
	m_ptr       = nullptr;
	m_ref_count = nullptr;
	m_size      = 0;
}

// ------------------------------------------------------------------------

template <class T, class A>
void shared<T,A>::swap( self& that )
{
	std::swap( m_ptr      , that.m_ptr       );
	std::swap( m_ref_count, that.m_ref_count );
	std::swap( m_size     , that.m_size      );
}

// ------------------------------------------------------------------------

template <class T, class A>
auto shared<T,A>::create( usize_t n ) -> self&
{
	if ( n == 0 )
		{ clear(); return *this; }

	if ( size() != n )
		assign( alloc::allocate(n), n );

	return *this;
}

// ------------------------------------------------------------------------

template <class T, class A>
auto shared<T,A>::assign( const self& that ) -> self&
{
	if ( &that != this )
	{
		clear();

		// Copy input
		m_ref_count = that.m_ref_count;
		m_size      = that.m_size;
		m_ptr       = that.m_ptr;

		if ( valid() )
			++(*m_ref_count);
	}
	return *this;
}

// ------------------------------------------------------------------------

template <class T, class A>
auto shared<T,A>::assign( ptr_t ptr, usize_t size ) -> self&
{
	ASSERT_RVAL( ptr, *this, "[dr.shared.assign] Null pointer in input." );
	clear();

	// Create new references count
	m_ref_count = new usize_t(1);

	// Assign members
	m_ptr  = ptr;
	m_size = size;

	return *this;
}

DRAYN_NS_END_
