#ifndef DRAYN_CONTAINERS_H_INCLUDED
#define DRAYN_CONTAINERS_H_INCLUDED

//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "concepts/includes.h"
#include "pointer.h"
#include "shared.h"

#include "sequence/includes.h"
#include "multidim/includes.h"

#endif
