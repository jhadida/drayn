
//==================================================
// @title        bidirectional.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <iterator>
#include <stdexcept>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T, class F>
struct _bidirectional_iterator
	: public _drayn_iterator< T, std::bidirectional_iterator_tag >
{
	CORE_TRAITS(T);
	typedef F self;

	// CRTP
	inline       F& _bd_iterator_crtp()       { return static_cast<      F&>(*this); }
	inline const F& _bd_iterator_crtp() const { return static_cast<const F&>(*this); }

	// ----------  =====  ----------
	
	// Interface
	virtual void clear () =0;
	virtual void swap  ( self& that ) =0;

	// Drayn iterator methods
	virtual ref_t value() const =0;
	virtual void  next () =0;
	virtual void  prev () =0;

	// Validity
	virtual operator bool() const =0;

	// Comparison
	virtual bool comparable ( const self& that ) const =0;
	virtual bool operator== ( const self& that ) const =0;

	// Dependent
	inline  bool operator!= ( const self& that ) const
		{ return !operator==(that); }

	// ----------  =====  ----------
	
	// R-value
	inline ref_t operator*  () const { return  value(); }
	inline ptr_t operator-> () const { return &value(); }
	inline ptr_t operator&  () const { return &value(); }

	// Increments
	inline self& operator ++()    { next(); return _bd_iterator_crtp(); }
	inline self& operator --()    { prev(); return _bd_iterator_crtp(); }
	inline self  operator ++(int) { self P(_bd_iterator_crtp()); next(); return P; }
	inline self  operator --(int) { self P(_bd_iterator_crtp()); prev(); return P; }

};

DRAYN_NS_END_
