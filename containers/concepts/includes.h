
//==================================================
// @title        utils.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Iterator wrapper
#include "iterators/drayn.h"
#include "utils/iterator_wrapper.h"
#include "utils/iterator_functional.h"

// Iterators
#include "iterators/forward.h"
#include "iterators/bidirectional.h"
#include "iterators/random_access.h"

// Concepts
#include "sequential.h"
#include "iterable.h"
#include "utils/iterable_get_iterator.h"
#include "copyable.h"
#include "arithmetic.h"
#include "comparable.h"

// Utils building on the previous concepts
#include "utils/iterable_ostream.h"
#include "utils/iterable_algebra.h"

