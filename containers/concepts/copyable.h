
//==================================================
// @title        copyable.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Copyable classes provide an element-wise method copy().
 * This method shall warn or raise an exception in case of size mismatch with the input.
 * Non-const overloads (eg with silent resizing) can also be defined if derived classes
 * allow it, but this concept does not require such definition.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class Value, class DrIterator, bool Mutable>
struct _copyable
{
	virtual usize_t     size() const =0;
	virtual DrIterator  iter() const =0;
};

// ------------------------------------------------------------------------

template <class Value, class DrIterator>
struct _copyable<Value, DrIterator, true>
{
	virtual usize_t     size() const =0;
	virtual DrIterator  iter() const =0;

	template <class T>
	void copy( const T& that ) const
	{
		ASSERT_R( that.size() == size(),
			"[dr._copyable] Input size mismatch (received " DPus ", expected " DPus ").",
			static_cast<usize_t>(that.size()), size() );

		map( iterable_get_iterator(that), iter(), f_cast<Value>() );
	}
};

DRAYN_NS_END_
