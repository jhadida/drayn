
//==================================================
// @title        iterable_get_iterator.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Helper function to either
 *    extract the iterator type from a Drayn container, or
 *    wrap the input container into a generic Drayn iterator (cf iterator_wrapper).
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// This finds the type of the iterator T().end()
template <class T>
using iterable_iterator_type =
	iterator_wrapper< decltype(std::declval<T>().end()) >;

// For a standard iterable container, wrap the iterators into an iterator_wrapper
template <class T>
inline constexpr iterable_iterator_type<T>
iterable_get_iterator( const T& ctn )
	{ return iterable_iterator_type<T>( ctn.begin(), ctn.end() ); }

// For an _iterable derived class, use the iter() method
template <class V, class S, class D>
inline constexpr D
iterable_get_iterator( const _iterable<V,S,D>& ctn )
	{ return ctn.iter(); }

DRAYN_NS_END_
