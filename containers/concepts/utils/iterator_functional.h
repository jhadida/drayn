
//==================================================
// @title        iterable_functional.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Different functional patterns of iteration on either one or two Drayn iterators.
 * This is essentially turning the imperative code using iterable classes into a weak 
 * functional language geared towards element-wise processing.
 *
 * Some functions are provided for convenience only, eg the function "fill" below is 
 * equivalent to:
 *     apply( in, [val]( T& x ){ x=val; } )
 */



        /********************     **********     ********************/
        /********************     **********     ********************/



DRAYN_NS_START_

// Apply function to elements
template <class I, class UnaryOperator>
inline void apply( I in, UnaryOperator op )
{
	for (; in; in.next() ) op( *in );
}

// Fill with value 
template <class I, class T>
inline void fill( I in, const T& val )
{
	for (; in; in.next() ) *in = val;
}

// Fill A from output of unary function applied to elements of B
template <class I, class O, class UnaryOperator>
inline void map( I in, O out, UnaryOperator op )
{
	for (; in && out; in.next(), out.next() ) *out = op( *in );
}

template <class I, class O>
inline void copy( I in, O out )
{
	for (; in && out; in.next(), out.next() ) *out = *in;
}

// ------------------------------------------------------------------------

// Fill A from output of binary function applied to B and C
template <class I1, class I2, class O, class BinaryOperator>
inline void combine( I1 in1, I2 in2, O out, BinaryOperator op )
{
	for (; in1 && in2 && out; in1.next(), in2.next(), out.next() ) *out = op( *in1, *in2 );
}

template <class I1, class T, class O, class BinaryOperator>
inline void combine_s( I1 in1, const T& val, O out, BinaryOperator op )
{
	for (; in1 && out; in1.next(), out.next() ) *out = op( *in1, val );
}

// ------------------------------------------------------------------------

// Recursively set scalar A to result of binary function applied to A and element of B
template <class I, class T, class BinaryOperator>
inline T reduce( I in, T start, BinaryOperator bop )
{
	for (; in; in.next() ) start = bop( start, *in );
	return start;
}

template <class I, class T, class BinaryOperator, class UnaryOperator>
inline T reduce( I in, T start, BinaryOperator bop, UnaryOperator uop )
{
	for (; in; in.next() ) start = bop( start, uop( *in ) );
	return start;
}

// ------------------------------------------------------------------------

// Recursively and cumulatively apply operation to elements of iterable A
template <class I, class BinaryOperator>
void cumulate( I in, BinaryOperator bop )
{
	// 1-step delayed iterator
	I buf = in;

	if ( in ) for ( in.next(); in; in.next(), buf.next() )
		*in = bop( *in, *buf );
}

// ------------------------------------------------------------------------

// Same but with two input arrays
template <class I1, class I2, class T, class BinaryOperator>
inline T inner( I1 in1, I2 in2, T start, BinaryOperator bop )
{
	for (; in1 && in2; in1.next(), in2.next() ) start += bop( *in1, *in2 );
	return start;
}

template <class I1, class U, class T, class BinaryOperator>
inline T inner_s( I1 in1, const U& val, T start, BinaryOperator bop )
{
	for (; in1; in1.next() ) start += bop( *in1, val );
	return start;
}

template <class I1, class I2, class T>
inline T inner( I1 in1, I2 in2, T start )
{
	for (; in1 && in2; in1.next(), in2.next() ) start += (*in1) * (*in2);
	return start;
}

template <class I1, class U, class T>
inline T inner_s( I1 in1, const U& val, T start )
{
	for (; in1; in1.next() ) start += (*in1) * val;
	return start;
}

// ------------------------------------------------------------------------

// Test if all elements of an iterator verify a predicate
template <class I, class Predicate>
inline bool all( I in, Predicate test )
{
	for (; in; in.next() ) if ( !test( *in ) ) return false;
	return true;
}

template <class I1, class I2, class Predicate>
inline bool all( I1 in1, I2 in2, Predicate test )
{
	for (; in1 && in2; in1.next(), in2.next() ) if ( !test( *in1, *in2 ) ) return false;
	return true;
}

template <class I1, class T, class Predicate>
inline bool all_s( I1 in1, const T& val, Predicate test )
{
	for (; in1; in1.next() ) if ( !test( *in1, val ) ) return false;
	return true;
}

// ------------------------------------------------------------------------

// Test if any element of an iterator verifies a predicate
template <class I, class Predicate>
inline bool any( I in, Predicate test )
{
	for (; in; in.next() ) if ( test( *in ) ) return true;
	return false;
}

template <class I1, class I2, class Predicate>
inline bool any( I1 in1, I2 in2, Predicate test )
{
	for (; in1 && in2; in1.next(), in2.next() ) if ( test( *in1, *in2 ) ) return true;
	return false;
}

template <class I1, class T, class Predicate>
inline bool any_s( I1 in1, const T& val, Predicate test )
{
	for (; in1; in1.next() ) if ( test( *in1, val ) ) return true;
	return false;
}

// ------------------------------------------------------------------------

// Test if each element of an iterator verifies a corresponding predicate
template <class I, class PredicateIterator>
inline bool each( I in, PredicateIterator pred )
{
	for (; in && pred; in.next(), pred.next() ) if ( !(*pred)(*in) ) return false;
	return true;
}

DRAYN_NS_END_
