
//==================================================
// @title        iterable_algebra.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <type_traits>

/**
 * Simple element-wise arithmetic operations for iterable classes.
 * 
 * The first section regroups element-wise operations with scalar output (dot, sum, product).
 * The second section regroups in-place element-wise operations (scalar inversion, cumulative sum/product).
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T>
using _alg_type = typename std::remove_const<T>::type;

// Dot product
template <class V1, class S1, class D1, class V2, class S2, class D2, 
	class U = _alg_type< typename std::common_type<V1,V2>::type > >
inline U alg_dot( const _iterable<V1,S1,D1>& in1, const _iterable<V2,S2,D2>& in2 ) 
	{ return inner( in1.iter(), in2.iter(), U(0) ); }

// Sum of elements
template <class V, class S, class D, class U = _alg_type<V> >
inline U alg_sum( const _iterable<V,S,D>& in )
	{ return reduce( in.iter(), U(0), f_add<U>() ); }

// Product of elements
template <class V, class S, class D, class U = _alg_type<V> >
inline U alg_prod( const _iterable<V,S,D>& in )
	{ return reduce( in.iter(), U(1), f_mul<U>() ); }

// ----------  =====  ----------

// In-place inversion
template <class V, class S, class D>
inline void alg_invert( const _iterable<V,S,D>& in )
	{ map( in.iter(), in.iter(), f_inv< _alg_type<V> >() ); }

// In-place cumulative sum
template <class V, class S, class D>
inline void alg_cumsum( const _iterable<V,S,D>& in ) 
	{ cumulate( in.iter(), f_add< _alg_type<V> >() ); }

// In-place cumulative product
template <class V, class S, class D>
inline void alg_cumprod( const _iterable<V,S,D>& in ) 
	{ cumulate( in.iter(), f_mul< _alg_type<V> >() ); }

DRAYN_NS_END_
