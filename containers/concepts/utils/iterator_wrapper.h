
//==================================================
// @title        iterator_wrapper.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <utility>
#include <iterator>
#include <type_traits>

/**
 * The iterator wrapper encapsulates standard bidirectional or random-access iterators
 * to emulate the interface of a Drayn iterator (cf iterators).
 */


		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

// Note: class StdIterator should be either random-access or bidirectional
// Note: we use iterator_traits::reference and not value_type because of the case (const T*)
template <
	class StdIterator,
	class Value = typename std::remove_reference<
		typename std::iterator_traits<StdIterator>::reference
	>::type
>
class iterator_wrapper
	: public _drayn_iterator<Value, std::bidirectional_iterator_tag>
{
public:

	ITERATOR_TRAITS(StdIterator);
	typedef iterator_wrapper<StdIterator>  self;
	typedef StdIterator                    std_iter;


	iterator_wrapper()
		{ m_it = m_end = StdIterator(); }
	iterator_wrapper( const std_iter& start, const std_iter& end )
		{ set(start,end); }

	inline void set( const std_iter& start, const std_iter& end )
		{ m_it = start;	m_end = end; }


	// Validity
	inline operator bool() const
		{ return m_it != m_end; }

	// Can be costly depending on underlying iterator type.
	inline usize_t size() const
		{ return std::distance(m_it,m_end); }


	// Main methods of Drayn iterators
	inline void next() { ++m_it; }
	inline void prev() { --m_it; }


	// Standard increments
	inline self& operator++ () { next(); return *this; }
	inline self& operator-- () { prev(); return *this; }


	// R-value
	inline ref_t value      () const { return const_cast<ref_t>(*m_it); }
	inline ref_t operator*  () const { return value(); }
	inline ptr_t operator-> () const { return &value(); }
	inline ptr_t operator&  () const { return &value(); }

private:

	std_iter m_it, m_end;
};

DRAYN_NS_END_
