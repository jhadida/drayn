
//==================================================
// @title        arithmetic.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <type_traits>

/**
 * Requires an iterable interface.
 *
 * Called _arithmetic for short, but this is really a concept for
 * "arithmetic-compound-assignable" classes. Such classes provide
 * operator overloads for in-place element-wise arithmetic-compound
 * operations:
 *     +=, -=, *= and /=
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class V, class F,
	bool Enable = !std::is_const<V>::value && is_arithmetic<V>::value >
struct _arithmetic {};

// ------------------------------------------------------------------------

#define DRAYN_ARITHMETIC_OPERATOR( Op, Fun )                                         \
template <class U, class S, class D>                                                 \
void _arithmetic_ ## Fun ( const _iterable<U,S,D>& u, std::false_type ) const        \
{                                                                                    \
	ASSERT_R( u.size() == _ar_crtp().size(),                                         \
		"[dr._arithmetic] Wrong input size (received " DPus ", expected " DPus ")",  \
		u.size(), _ar_crtp().size() );                                               \
	combine( _ar_crtp().iter(), u.iter(), _ar_crtp().iter(), Fun<V,U,V>() );         \
}                                                                                    \
                                                                                     \
template <class U>                                                                   \
inline void _arithmetic_ ## Fun ( const U& u, std::true_type ) const                 \
	{ combine_s( _ar_crtp().iter(), u, _ar_crtp().iter(), Fun<V,U,V>() ); }          \
                                                                                     \
template <class U>                                                                   \
inline const F& operator Op ( const U& u ) const                                     \
	{ _arithmetic_ ## Fun ( u, std::is_arithmetic<U>() ); return _ar_crtp(); }       \
                                                                                     \
template <class U>                                                                   \
inline F& operator Op ( const U& u )                                                 \
	{ _arithmetic_ ## Fun ( u, std::is_arithmetic<U>() ); return _ar_crtp(); }

// ------------------------------------------------------------------------

template <class V, class F>
struct _arithmetic<V,F,true>
{
	inline       F& _ar_crtp()       { return static_cast<      F&>(*this); }
	inline const F& _ar_crtp() const { return static_cast<const F&>(*this); }

	// ----------  =====  ----------

	DRAYN_ARITHMETIC_OPERATOR( +=, f_add );
	DRAYN_ARITHMETIC_OPERATOR( -=, f_sub );
	DRAYN_ARITHMETIC_OPERATOR( *=, f_mul );
	DRAYN_ARITHMETIC_OPERATOR( /=, f_div );
};

DRAYN_NS_END_
