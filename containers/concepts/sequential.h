
//==================================================
// @title        sequential.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <type_traits>
#include <stdexcept>

/**
 * Sequential classes provide single-index subscripting to an underlying container.
 * Containers can either be custom classes or C-pointers.
 *
 * For example, a sequential wrapper of a pointer to type const T would derive from:
 *     _sequential< T, const T* >
 * and an instance of the concrete class would provide the methods:
 *     X.data() // pointer or const ref to underlying container
 *     X.size() // number of indexed elements
 *     (bool) X // valid container, and non-zero number of elements
 *     X.front(), X.back(), X[i] and X.at(i) // element access
 */


		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class Sequential>
struct sequential_traits
{
	using value_type      = typename Sequential::_sequential_value_type;
	using container_type  = typename Sequential::_sequential_container_type;
	using self_type       = typename Sequential::_sequential_self;
	using data_type       = typename Sequential::_sequential_data_type;
	using is_mutable      = bool_negate<std::is_const<value_type>>;
};

// ------------------------------------------------------------------------

template <class Value, class Container, class Forward>
struct _sequential
{
	CORE_TRAITS(Value);

	typedef Value      _sequential_value_type;
	typedef Container  _sequential_container_type;
	typedef Forward    _sequential_self;

	typedef typename std::conditional<
		std::is_pointer<Container>::value,
		Container, const Container&
	>::type _sequential_data_type;

	typedef _sequential_data_type data_t; // for short


	// CRTP
	inline       Forward& _sq_crtp()       { return static_cast<      Forward&>(*this); }
	inline const Forward& _sq_crtp() const { return static_cast<const Forward&>(*this); }


	// Required definitions in order to enable the interface below
	virtual data_t  data() const =0;
	virtual usize_t size() const =0;


	// Validity
	virtual bool valid() const =0;
	inline  bool empty() const { return size() == 0; }
	virtual inline operator bool() const
		{ return valid() && !empty(); }

	/**
	 * "It is unspecified whether or not an implementation implicitly instantiates
	 * a virtual member function of a class template if the virtual member function
	 * would not otherwise be instantiated." (C++11 14.7.1/10)
	 *
	 * So we hide instead (..of overloading a virtual method) when redefinition is
	 * required, even though it's not pretty.
	 */

	// Subscript assignment/access
	Value& at( usize_t n ) const
	{
		if ( n >= size() )
			throw std::out_of_range("[dr.sequential.at] Index out of sequence.");
		return _sq_crtp().operator[](n);
	}
	inline Value& operator[] ( usize_t n ) const
		{ return const_cast<Value&>(data()[ Drayn_IDX(n,size()) ]); }

	// Convenient front/back accessors
	inline ref_t front() const { return at(0); }
	inline ref_t back () const { return at(size()-1); }
};

DRAYN_NS_END_
