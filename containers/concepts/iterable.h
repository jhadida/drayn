
//==================================================
// @title        iterable.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <utility>
#include <type_traits>

/**
 * Iterable classes can be iterated either forward or backward.
 * Iteration is carried out either the standard way:
 *     for ( it = X.begin(); it != X.end(); ++it ) // ...
 * or the Drayn way:
 *     for ( it = X.iter(); it; it.next() ) // ...
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class Value, class StandardIterator, class DraynIterator>
struct _iterable
{
	virtual usize_t            size() const =0;

	// Drayn iteration
	virtual DraynIterator      iter() const =0;
	virtual DraynIterator     riter() const =0;

	// Standard iteration
	virtual StandardIterator  begin() const =0;
	virtual StandardIterator    end() const =0;

	virtual StandardIterator rbegin() const =0;
	virtual StandardIterator   rend() const =0;
};

DRAYN_NS_END_
