
//==================================================
// @title        comparable.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <type_traits>

/**
 * Requires an iterable interface.
 *
 * Comparable classes provide element-wise comparison methods:
 *     lt(), gt(), leq(), geq(), eq(), neq()
 * as well as instance comparison overloads:
 *     operator== and operator!=
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class Value, class Forward, bool Enable = is_comparable<Value>::value >
struct _comparable {};

// ------------------------------------------------------------------------

#define DRAYN_COMPARABLE_OPERATOR( Name, Fun )                                       \
template <class U, class S, class D>                                                 \
bool _comparable_ ## Fun ( const _iterable<U,S,D>& u, std::false_type ) const        \
{                                                                                    \
	ASSERT_RVAL( u.size() == _cm_crtp().size(), false,                               \
		"[dr._comparable] Wrong input size (received " DPus ", expected " DPus ")",  \
		u.size(), _cm_crtp().size() );                                               \
	return all( _cm_crtp().iter(), u.iter(), Fun<V,U>() );                           \
}                                                                                    \
                                                                                     \
template <class U>                                                                   \
inline bool _comparable_ ## Fun ( const U& u, std::true_type ) const                 \
	{ return all_s( _cm_crtp().iter(), u, Fun<V,U>() ); }                            \
                                                                                     \
template <class U>                                                                   \
inline bool Name ( const U& u ) const                                                \
	{ return _comparable_ ## Fun ( u, std::is_arithmetic<U>() ); }

// ------------------------------------------------------------------------

template <class V, class F>
struct _comparable<V,F,true>
{
	inline       F& _cm_crtp()       { return static_cast<      F&>(*this); }
	inline const F& _cm_crtp() const { return static_cast<const F&>(*this); }

	// Element-wise more-/less-than comparison
	DRAYN_COMPARABLE_OPERATOR( lt , f_lt  );
	DRAYN_COMPARABLE_OPERATOR( leq, f_leq );
	DRAYN_COMPARABLE_OPERATOR( gt , f_gt  );
	DRAYN_COMPARABLE_OPERATOR( geq, f_geq );

	// Element-wise equality comparison
	DRAYN_COMPARABLE_OPERATOR( eq , f_eq  );
	DRAYN_COMPARABLE_OPERATOR( neq, f_neq );

	// Container comparison
	virtual bool operator== ( const F& that ) const =0;
	inline  bool operator!= ( const F& that ) const
		{ return !operator==(that); }
};

DRAYN_NS_END_
