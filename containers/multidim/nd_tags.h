
//==================================================
// @title        nd_tags.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

struct tag_nd_array {};
struct tag_matrix   {};
struct tag_volume   {};

DRAYN_NS_END_
