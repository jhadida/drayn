
//==================================================
// @title        nd_array.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T, class C = array_shared<T>>
class nd_array
	: public nd_interface<T,C>
{
public:

	CORE_TRAITS(T);
	typedef nd_array<T,C>      self;
	typedef nd_interface<T,C>  parent;
	typedef nd_traits<T,C>     traits;
	typedef tag_nd_array       tag;

	typedef MemoryLayout                     Layout;
	typedef typename traits::container_type  Container;


	nd_array()
		{ this->clear(); }

	nd_array( const self& that )
		{ this->assign(that); }
	nd_array( self&& that )
		{ this->assign(std::move(that)); }

	nd_array( const Container& _data, const nd_dimensions& _dims, Layout _lay = Layout::Columns )
		{ this->assign(_data,_dims,_lay); }
	nd_array( const Container& _data, const nd_dimensions& _dims, const nd_strides& _strides )
		{ this->assign(_data,_dims,_strides); }


	// ------------------------------------------------------------------------


	using parent::assign;
	void swap( self& that );

	inline self& operator= ( const self& that ) { assign(that); return *this; }
	       void  assign    ( const self& that );

	inline self& operator= ( self&& that ) { assign(std::move(that)); return *this; }
	       void  assign    ( self&& that );


	// ------------------------------------------------------------------------


	// Will segfault if you feed the wrong number of inputs!
	using parent::operator();
	ref_t operator() ( usize_t i, ... ) const
	{
		va_list vl; va_start(vl,i);

		i = this->offset() + Drayn_IDX(i,this->dim(0)) * this->stride(0);

		for ( usize_t k = 1; k < this->ndims(); ++k )
			i += Drayn_IDX( va_arg(vl,usize_t), this->dim(k) ) * this->stride(k);

		va_end(vl); return this->m_data[i];
	}

protected:

	inline bool _check_dims( const nd_dimensions& _dims ) const
		{ ASSERT_RF( _dims.ndims() >= 2, "Ndims should be at least 2." ); return true; }
};

// ------------------------------------------------------------------------

template <class T, class C>
void nd_array<T,C>::swap( self& that )
{
	this->m_data    .swap( that.m_data );
	this->m_dims    .swap( that.m_dims );
	this->m_strides .swap( that.m_strides );
}

template <class T, class C>
void nd_array<T,C>::assign( const self& that )
{
	this->m_data    = that.m_data;
	this->m_dims    .copy( that.dims()    );
	this->m_strides .copy( that.strides() );
}

template <class T, class C>
void nd_array<T,C>::assign( self&& that )
{
	this->m_data    = that.m_data;
	this->m_dims    = that.dims();
	this->m_strides = that.strides();
}



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class T, class C>
std::ostream& operator<<( std::ostream& os, const nd_array<T,C>& x )
{
	const usize_t n = x.ndims();

	if ( n == 2 ) return os<< reinterpret_cast< matrix<T,C> >(x);
	if ( n == 3 ) return os<< reinterpret_cast< volume<T,C> >(x);

	os<< "[dr.nd_array instance] {ndims: " << n << ", size: ";
		for ( auto &d: x.dims() ) os<< d << "x";
	os<< ", numel: " << x.size() << "}" << std::endl;

	return os<< std::endl;
}

DRAYN_NS_END_
