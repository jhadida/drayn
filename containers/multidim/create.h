
//==================================================
// @title        create.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T, class C = array_shared<T> >
inline matrix<T,C> create_col( usize_t n )
	{ return nd_array<T,C>( C( n ), wrap_inilst<usize_t>({n,1}), MemoryLayout::Columns ); }

template <class T, class C = array_shared<T> >
inline matrix<T,C> create_row( usize_t n )
	{ return nd_array<T,C>( C( n ), wrap_inilst<usize_t>({1,n}), MemoryLayout::Rows ); }

template <class T, class C = array_shared<T> >
inline matrix<T,C> create_vector( usize_t n )
	{ return create_col<T,C>(n); }

// ------------------------------------------------------------------------

template <class T, class C = array_shared<T> >
inline matrix<T,C> create_matrix( usize_t nr, usize_t nc, MemoryLayout lay = MemoryLayout::Columns )
	{ return matrix<T,C>( C( nr*nc ), wrap_inilst<usize_t>({nr,nc}), lay ); }

template <class T, class C = array_shared<T> >
inline volume<T,C> create_volume( usize_t nr, usize_t nc, usize_t ns, MemoryLayout lay = MemoryLayout::Columns )
	{ return volume<T,C>( C( nr*nc*ns ), wrap_inilst<usize_t>({nr,nc,ns}), lay ); }

template <class T, class C = array_shared<T> >
inline nd_array<T,C> create_array( const nd_dimensions& dims, MemoryLayout lay = MemoryLayout::Columns )
	{ return nd_array<T,C>( C( dims.numel() ), dims, lay ); }

DRAYN_NS_END_
