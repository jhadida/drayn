
//==================================================
// @title        default.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class Container>
class nd_accessor_default
	: public nd_accessor<Container>
{
public:

	FORWARD_TRAITS( Container );
	ND_ACCESSOR_TRAITS

	typedef nd_accessor_default<Container> self;


	nd_accessor_default()
		{ this->clear(); }
	nd_accessor_default( const Container *_ctn, nd_dimensions *_dims, nd_strides *_strides )
		{ this->assign(_ctn,_dims,_strides); }
	~nd_accessor_default()
		{ this->clear(); }


	// Forward operator() to _value_of
	inline ref_t operator() ( const ucoord& x ) const { return _value_of(x); }
	inline ref_t operator() ( const icoord& x ) const { return _value_of(x); }
	inline ref_t operator() ( const rcoord& x ) const { return _value_of(x); }

	// Signed integer / floating point
	ref_t operator() ( uidx n, va_list& vl ) const
	{
		DBG_ASSERT_RVAL( this->valid(), static_val<val_t>(),
			"[dr.nd_accessor_default] Accessor is not ready yet!" );

		uidx ind = this->offset() +
			Drayn_IDX(n,this->dim(0)) * this->stride(0);

		for ( usize_t i = 1; i < this->ndims(); ++i )
			ind += Drayn_IDX( va_arg(vl,unsigned), this->dim(i) ) * this->stride(i);

		va_end(vl); return (*this->m_ctn)[ind];
	}

private:

	template <class T>
	ref_t _value_of( const T& x ) const
	{
		DBG_ASSERT_RVAL( this->valid(), static_val<val_t>(),
			"[dr.nd_accessor_default] Accessor is not ready yet!" );
		DBG_ASSERT_RVAL( x.size() == this->ndims(), static_val<val_t>(),
			"[dr.nd_accessor_default] Invalid size in input (expected " DPus ", got " DPus ").",
			this->ndims(), static_cast<usize_t>(x.size()) );

		uidx ind = this->offset();
		for ( usize_t i = 0; i < this->ndims(); ++i )
			ind += Drayn_IDX( op_uround(x[i]), this->dim(i) ) * this->stride(i);

		return (*this->m_ctn)[ind];
	}
};
