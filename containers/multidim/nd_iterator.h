
//==================================================
// @title        nd_iterator.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Iterator on multidimensional memory mapping.
 *
 * Note, from http://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B#cite_note-arrowptr-6
 * The return type of operator->() must be a type for which the -> operation can be applied, such as a pointer type. 
 * If x is of type C where C overloads operator->(), x->y gets expanded to x.operator->()->y.
 */

#include <tuple>
#include <utility>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class C, class T = typename C::val_t>
class nd_iterator
	: public _bidirectional_iterator<T, nd_iterator<C,T> >
{
public:

	CORE_TRAITS( T );
	typedef nd_iterator<C,T>  self;
	typedef C                 Container;
	
	
	nd_iterator()
		{ clear(); }
	nd_iterator( 
		const Container     *_ctn  ,
		const nd_dimensions *_dims ,
		const nd_strides    *_str  ,
		bool  fwd           = true )
		{ assign(_ctn,_dims,_str,fwd); }


	// Legacy: _bidirectional_iterator
	void clear();
	void swap( self& that );

	// R-value
	inline ref_t value() const { return (*m_ctn)[ m_idx ]; }
	inline void  next ()       { advance( m_fwd); }
	inline void  prev ()       { advance(!m_fwd); }

	// Validity
	inline bool     valid() const { return m_ctn && m_dims && m_strides; }
	inline bool     empty() const { return size() == 0; }
	inline operator  bool() const { return valid() && !empty(); }

	// Comparison
	inline bool comparable ( const self& that ) const
	{
		return m_strides == that.m_strides
			&& m_dims    == that.m_dims
			&& m_ctn     == that.m_ctn 
			&& m_fwd     == that.m_fwd;
	}
	inline bool operator== ( const self& that ) const
	{ 
		if ( !comparable(that) )
			throw std::invalid_argument("[nd_iterator.operator==] Input is not comparable.");
		return m_idx == that.m_idx;
	}

	// ------------------------------------------------------------------------
	
	// Iterator direction
	inline void go_forward () { if (!m_fwd) reverse(); }
	inline void go_backward() { if ( m_fwd) reverse(); }
	inline void reverse    () { m_fwd = !m_fwd; m_len = numel()-m_len; }

	// Dimensions
	inline usize_t size () const { return m_len; }
	inline usize_t ndims() const { return m_dims? m_dims->ndims() : 0; }
	inline usize_t numel() const { return m_dims? m_dims->numel() : 0; }
	
	// Accessors
	inline ndc_ucoord  subs  ()  const { return m_sub; }
	inline usize_t     index ()  const { return m_idx; }
	inline usize_t     offset()  const { return m_strides? m_strides->offset() : 0; }

	inline usize_t stride ( usize_t k ) const { return m_strides? (*m_strides)[k] : 0; }
	inline usize_t dim    ( usize_t k ) const { return m_dims? (*m_dims)[k] : 0; }
	inline usize_t sub    ( usize_t k ) const { return m_sub[k]; }

	// Assignment
	void assign( 
		const Container     *_ctn  ,
		const nd_dimensions *_dims ,
		const nd_strides    *_str  ,
		bool  fwd           = true );


private:

	void advance( bool fwd );

	// Data members
	const Container     *m_ctn;
	const nd_dimensions *m_dims;
	const nd_strides    *m_strides;

	bool     m_fwd;
	usize_t  m_idx, m_len;
	vector<usize_t> m_sub;
};



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class C, class T>
void nd_iterator<C,T>::clear()
{
	m_strides = nullptr;
	m_dims    = nullptr;
	m_ctn     = nullptr;

	m_fwd = true;
	m_idx = m_len = 0;
	m_sub.clear();
}

// ------------------------------------------------------------------------

template <class C, class T>
void nd_iterator<C,T>::swap( self& that )
{
	std::swap(
		stay(std::tie(m_ctn,m_dims,m_strides,m_fwd,m_idx,m_len,m_sub)),
		stay(std::tie(that.m_ctn,that.m_dims,that.m_strides,that.m_fwd,that.m_idx,that.m_len,that.m_sub))
	);
}

// ------------------------------------------------------------------------

template <class C, class T>
void nd_iterator<C,T>::assign( 
	const Container     *_ctn     ,
	const nd_dimensions *_dims    ,
	const nd_strides    *_strides ,
	bool forward )
{ 
	m_strides = _strides;
	m_dims    = _dims;
	m_ctn     = _ctn;
	m_fwd     = forward;

	// Initialize coordinates to (0,.. 0)
	m_sub.create( ndims(), 0 );

	// If we're going backwards, start from the end
	if ( m_dims && !m_fwd ) 
		m_dims->sub_decrement(m_sub);

	// Number of elements
	m_len = numel();

	// Set corresponding index
	sub2ind( m_sub, *m_dims, *m_strides, m_idx );
}

// ------------------------------------------------------------------------

template <class C, class T>
void nd_iterator<C,T>::advance( bool fwd )
{
	ASSERT_R( *this, "Iterator is invalid or empty." );

	if ( m_fwd ^ fwd ) // prev
		m_len = Drayn_MIN( m_len+1, numel() );
	else // next
		--m_len; // >0 guaranteed by assert

	if ( fwd ) // increment
	{
		for ( usize_t i=0; i < ndims() && 
			(m_idx += stride(i)) && (++m_sub[i] == dim(i)); ++i )
		{
			m_sub[i] = 0;
			m_idx   -= stride(i)*dim(i);
		}
	}
	else // decrement
	{
		usize_t i = 0;

		for (; i < ndims() && m_sub[i] == 0; ++i )
		{
			m_sub[i] = dim(i)-1;
			m_idx   += stride(i) * m_sub[i];
		}

		if ( i < ndims() )
		{
			--m_sub[i];
			m_idx -= stride(i);
		}
	}
}

DRAYN_NS_END_
