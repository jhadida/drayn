
//==================================================
// @title        indexing.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

#include "indexing/dimensions.cpp"
#include "indexing/strides.cpp"

// ------------------------------------------------------------------------

void sub2ind( const array<const usize_t>& sub, const nd_dimensions& dims,
	const nd_strides& strides, usize_t& ind )
{
	DBG_ASSERT_R( dims.ndims() == sub.size(),
		"Input size mismatch between dimensions and sub." );
	DBG_ASSERT_R( dims.ndims() == strides.ndims(),
		"Input size mismatch between dimensions and strides." );

	usize_t nd = dims.ndims();

	ind = strides.offset();
	for ( usize_t i = 0; i < nd; ++i )
		ind += Drayn_IDX(sub[i],dims[i]) * strides[i];
}

usize_t sub2ind( const array<const usize_t>& sub, const nd_dimensions& dims,
	const nd_strides& strides )
{
	usize_t ind = 0; 
	sub2ind(sub,dims,strides,ind);
	return ind;
}

// ------------------------------------------------------------------------

void ind2sub( usize_t ind, const nd_dimensions& dims,
	const array<usize_t>& sub, MemoryLayout lay )
{
	DBG_ASSERT_R( dims.ndims() == sub.size(),
		"Input size mismatch between dimensions and sub." );

	usize_t nd      = dims.ndims();
	usize_t cumprod = dims.numel();

	for ( int i = nd-1; i >= 0; --i )
	{
		cumprod /= dims[i];
		sub[i]   = ind / cumprod;
		ind      = ind % cumprod;
	}

	if ( lay == MemoryLayout::Rows )
		bit_swap( sub[0], sub[1] );
}

DRAYN_NS_END_
