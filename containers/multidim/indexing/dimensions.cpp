
//==================================================
// @title        dimensions.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



void nd_dimensions::clear()
{
	m_dims.clear();
	m_numel = 0;
}

// ------------------------------------------------------------------------

void nd_dimensions::swap( self& other )
{
	m_dims.swap( other.m_dims );
	bit_swap( m_numel, other.m_numel );
}

// ------------------------------------------------------------------------

void nd_dimensions::assign( const ndc_ucoord& d )
{
	m_dims.copy(d);
	m_numel = alg_prod(m_dims);
}
