
//==================================================
// @title        strides.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



void nd_strides::clear() 
{
	m_strides.clear();
	m_offset = 0;
}

// ------------------------------------------------------------------------

void nd_strides::swap( self& other )
{
	m_strides.swap( other.m_strides );
	bit_swap( m_offset, other.m_offset );
}

// ------------------------------------------------------------------------

void nd_strides::assign( const ndc_ucoord& _strides, usize_t _offset )
{
	set_strides(_strides);
	set_offset(_offset);
}
