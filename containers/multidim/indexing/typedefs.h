
//==================================================
// @title        typedefs.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



typedef array<usize_t> nd_ucoord;
typedef array<isize_t> nd_icoord;
typedef array<double>  nd_rcoord;
typedef nd_ucoord      nd_subs;

typedef const_array<usize_t> ndc_ucoord;
typedef const_array<isize_t> ndc_icoord;
typedef const_array<double>  ndc_rcoord;
typedef ndc_ucoord           ndc_subs;
