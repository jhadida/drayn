
//==================================================
// @title        accessors.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <memory>
#include <cstdarg>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

/**
 * Abstract accessor interface.
 * Accessors should not cache any property from nd_arrays; these should 
 * be accessed dynamically as needed.
 */

/**
 * Accessor traits for containers.
 */
#define ND_ACCESSOR_TRAITS          \
ACCESSOR_TRAITS                     \
typedef array<const uidx>  ucoord;  \
typedef array<const iidx>  icoord;  \
typedef array<const ridx>  rcoord;  

// ----------  =====  ----------

template <class Container>
struct nd_accessor
{
	FORWARD_TRAITS(Container);
	ND_ACCESSOR_TRAITS

	const Container  *m_ctn;
	nd_dimensions    *m_dims;
	nd_strides       *m_strides;

	// ----------  =====  ----------

	void clear()
	{
		m_ctn     = nullptr;
		m_dims    = nullptr;
		m_strides = nullptr;
	}

	void assign( const Container *_ctn, nd_dimensions *_dims, nd_strides *_strides )
	{
		m_ctn     = _ctn;
		m_dims    = _dims;
		m_strides = _strides;
	}

	// Validity
	inline bool    valid() const { return m_ctn && m_dims && m_strides; }
	inline operator bool() const { return valid(); }

	// Accessors
	inline usize_t ndims  () const { return m_dims->ndims(); }
	inline usize_t offset () const { return m_strides->offset(); }
	inline usize_t dim    ( usize_t k ) const { return (*m_dims)[k]; }
	inline usize_t stride ( usize_t k ) const { return (*m_strides)[k]; }

	// Unsigned integer
	virtual ref_t operator() ( const ucoord& x )     const =0;
	virtual ref_t operator() ( uidx i, va_list& vl ) const =0;

	// Signed integer / floating point
	virtual ref_t operator() ( const icoord& x ) const =0;
	virtual ref_t operator() ( const rcoord& x ) const =0;

	// Accept initializer lists
	inline ref_t operator() ( const inilst<uidx>& x ) const { return (*this)(wrap_inilst(x)); }
	inline ref_t operator() ( const inilst<iidx>& x ) const { return (*this)(wrap_inilst(x)); }
	inline ref_t operator() ( const inilst<ridx>& x ) const { return (*this)(wrap_inilst(x)); }
};

// ------------------------------------------------------------------------

// Include implementations
#include "accessors/default.h"

// ------------------------------------------------------------------------

enum class NdAccessorId
	: char { Default=0 };

template <class C>
struct nd_accessor_factory
{
	typedef C                                container_type;
	typedef nd_accessor<C>                   accessor_type;
	typedef std::shared_ptr<accessor_type>   output_type;
	typedef NdAccessorId                     id_type;


	template <class... Args>
	static output_type create( id_type id, Args&&... args )
	{
		output_type out;
		switch ( id )
		{
			default:
				out.reset( factory_create(
					factory_tag< nd_accessor_default<C> >(),
					std::forward<Args>(args)...
				) ); 
				break;
		}
		return out;
	}
};

DRAYN_NS_END_
