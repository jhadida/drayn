
//==================================================
// @title        nd_interface.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <type_traits>
#include <algorithm>
#include <vector>



		/********************     **********     ********************/
		/********************     **********     ********************/



DRAYN_NS_START_

template <class T, class C> class nd_interface;
template <class T, class C> using nd_traits =
	sequence_traits< T, C, nd_interface<T,C>, nd_iterator<C>, nd_iterator<C> >;


//-------------------------------------------------------------
// Copy interface
//-------------------------------------------------------------

template <class T, class C, bool Mutable = C::traits::is_mutable::value >
class nd_interface_copy
	: public nd_traits<T,C>::iterable_parent
{
public:

	inline const nd_dimensions&  dims   () const { return m_dims; }
	inline const nd_strides&     strides() const { return m_strides; }

protected:

	nd_dimensions m_dims;
	nd_strides    m_strides;
};

template <class T, class C>
class nd_interface_copy<T,C,true> // specialisation for mutable value-types
	: public nd_traits<T,C>::iterable_parent
{
public:

	inline const nd_dimensions&  dims   () const { return m_dims; }
	inline const nd_strides&     strides() const { return m_strides; }

	template <class U, class D>
	void copy( const nd_interface_copy<U,D>& that )
	{
		ASSERT_R( that.size() == this->size(),
			"Input size mismatch (received " DPus ", expected " DPus ").",
			that.size(), this->size() );

		m_dims.copy( that.dims() );
		// strides are sequence-container-specific, don't copy them

		map( that.iter(), this->iter(), f_cast<T>() );
	}

protected:

	nd_dimensions m_dims;
	nd_strides    m_strides;
};


//-------------------------------------------------------------
// Abstract implementation
//-------------------------------------------------------------

template <class T, class C = array_shared<T>>
class nd_interface :
	public nd_interface_copy<T,C>,
	public nd_traits<T,C>::arithmetic_parent,
	public nd_traits<T,C>::comparable_parent
{
public:

	CORE_TRAITS(T);
	typedef nd_interface<T,C> self;
	typedef nd_traits<T,C>    traits;

	typedef MemoryLayout                     Layout;
	typedef typename traits::container_type  Container;
	typedef typename traits::dr_iterator     dr_iterator;
	typedef typename traits::std_iterator    std_iterator;

	static_assert( std::is_same<T,typename C::val_t>::value,
		"Type mismatch between container and value type." );

	// ----------  =====  ----------


	void clear();

	// Emulate: _sequential
	inline const Container&  data  () const { return m_data; }
	inline usize_t           size  () const { return dims().numel(); }
	inline bool              valid () const { return m_data.valid(); }
	inline bool              empty () const { return size() == 0; }
	inline ptr_t             memptr() const { return valid()? &m_data[0] : nullptr; }
	inline          operator bool  () const { return valid() && !empty(); }


	template <class U, class D>
	inline bool compare( const nd_interface<U,D>& that ) const
	{
		return that.m_data    == this->m_data
			&& that.m_dims    == this->m_dims
			&& that.m_strides == this->m_strides;
	}

	inline bool operator== ( const self& that ) const { return compare(that); }
	template <class U, class D>
	inline bool operator== ( const nd_interface<U,D>& that ) const { return compare( that ); }
	template <class U, class D>
	inline bool operator!= ( const nd_interface<U,D>& that ) const { return !compare( that ); }

	// Legacy: _iterable
	inline std_iterator  begin() const { return std_iterator(&m_data,&dims(),&strides()); }
	inline std_iterator    end() const { return --begin(); }

	inline std_iterator rbegin() const { return std_iterator(&m_data,&dims(),&strides(),false); }
	inline std_iterator   rend() const { return --rbegin(); }

	inline dr_iterator    iter() const { return  begin(); }
	inline dr_iterator   riter() const { return rbegin(); }


	// ------------------------------------------------------------------------


	// Access dimension members
	inline const nd_dimensions& dims   () const { return this->m_dims; }
	inline const nd_strides&    strides() const { return this->m_strides; }

	// Dimensions
	inline usize_t ndims   () const { return dims().ndims(); }
	inline usize_t numel   () const { return dims().numel(); }
	inline usize_t capacity() const { return m_data.size(); }
	inline usize_t offset  () const { return strides().offset(); }

	inline usize_t dim    ( usize_t k ) const { return dims().dim(k); }
	inline usize_t stride ( usize_t k ) const { return strides().stride(k); }


	// ------------------------------------------------------------------------


	// Assignment
	void assign( const Container& _data, const nd_dimensions& _dims, Layout _lay = Layout::Columns );
	void assign( const Container& _data, const nd_dimensions& _dims, const nd_strides& _strides );

	// Change access pattern
	void reshape( const nd_dimensions& _dims, Layout _lay = Layout::Columns );
	void reshape( const nd_dimensions& _dims, const nd_strides& _strides );

	       void permute( const ndc_ucoord& p );
	inline void permute( const inilst<usize_t>& p ) { permute(wrap_inilst(p)); }

	// TODO: implement squeeze (remove unit dimensions) and rebuild (deep copy current layout)


	// ------------------------------------------------------------------------


	// Extract vector
	sequence_wrapper<C,T> subvector( const ndc_ucoord& _start, usize_t _dim ) const;
	sequence_wrapper<C,T> subvector( const inilst<usize_t>& _start, usize_t _dim ) const
		{ return subvector( wrap_inilst(_start), _dim ); }

	// Extract array
	template <class U> U subarray( const ndc_ucoord& _start, const nd_dimensions& _dims ) const;
	template <class U> U subarray( const inilst<usize_t>& _start, const nd_dimensions& _dims ) const
		{ return subarray( wrap_inilst(_start), _dims ); }


	// ------------------------------------------------------------------------


	inline ref_t operator() ( const ndc_ucoord& x ) const
		{ return m_data[ sub2ind( x, dims(), strides() ) ]; }


protected:

	// Children may apply dimensional constraints upon assignment
	virtual bool _check_dims( const nd_dimensions& _dims ) const =0;

	Container m_data;
};

// ------------------------------------------------------------------------

// Include implementation
#include "nd_interface.hpp"

DRAYN_NS_END_
