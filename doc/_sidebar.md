
* General

  <!-- * [About](start/about) -->
  * [Installation](start/install)
  * [Reference](ref/index)
  <!-- * [Architecture](start/architecture) -->

* Data

  * [Container](data/container)
  * [Iterable](data/iterable)
  * [Array](data/array)
  * [Time-series](data/timeseries)
  * [Imaging](data/imaging)

* Utilities

  * [Random](utils/random)
  * [Events](utils/events)
  * [Timer](utils/timer)

* Algorithms

  * [Metrics](algo/metrics)
  * [Windows](algo/windows)
  * [Ordering](algo/ordering)
  * [Numerical](algo/numerical)
