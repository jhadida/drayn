
# Containers

Containers are a fundamental data-type in Drayn. They derive from the abstract `_sequence` class, which requires the implementation of a `size()` function, and an overload of `operator[]`, and in return enables seamless iteration and display, as well as arithmetic and comparison operations for numeric value-types.

The two containers you are most likely to interact with are: the `pointer` class (referring to external memory), and the `shared` class (self-managed allocation).

## Pointer

Pointer containers consist of: a pointer, a size (cardinality), and a step ($\geq 1$). They do not manage the underlying memory, and are therefore prone to segfaults if used improperly. For instance they generally **cannot** be returned by functions, unless the allocation they point to persists that function's scope. 

```cpp
#include <vector>

#include "drayn.h"
namespace dr = drayn;

int main() {
    std::vector<unsigned> x = {1,2,3,4,5,6};

    // const pointer to x skipping every second element
    dr::pointer<const unsigned> p( &x[0], 3, 2 );
}
```

Remarks:

 - **The size is relative to the step-size.** In other words, the size is an upper-bound on the index, and not the number of items allocated.
 - Assignments require the size (here 3), and optionally the step-size (here 2, by default 1).
 - The template type can be const-qualified.
 - The underlying pointer is returned by `get()`.
 - Usual pointer-offset arithmetic is supported (e.g. `p += 1` or `++p`), and pointers can be compared (`==` and `!=`) but not diff'd (e.g. not `p - q`).
 - Use pointer arithmetic with care, it can lead to code that is difficult to debug and maintain.

## Shared

Shared containers implement "smart" memory management using reference counting. This means that allocated memory is kept alive as long as at least one container refers to it (several containers can refer to the same allocation), and automatically released otherwise. Practical consequences are that:

 - shared containers can safely be returned by functions (allocations persist outside local scopes);
 - they are slightly more difficult to copy (`operator=` yields a shallow copy by default).

> **Note:** contrary to `std::shared_ptr`, the template type **has to** be default-constructible.

```cpp
#include <iostream>

#include "drayn.h"
namespace dr = drayn;

int main() {
    // shared container of length 5
    dr::shared<double> x(5);

    // re-allocate with fill-value
    x.create(7,42.0);

    // create shared container from initializer list
    dr::shared<unsigned> y({1,2,3,4,5,6});
}
```

Remarks:

 - Implicit casts to `pointer<T>` are supported.
 - Comparison with other shared containers is supported (`==` and `!=`), but pointer arithmetic is not (obviously). 
 - Copy from any type defining `size()` and `operator[]` with automatic resizing.
 - Copy from `initializer_list` is also supported.

## Other

Talk about `range`, `wrapper` and `sliced`.
