> **DISCLAIMER**
>
> I wrote this library from scratch, and although parts of it have been battle tested over the years, it is **NOT** suitable for production code or security applications. 
>
> I chose to make this library freely available in the hope that it is useful to others, but cannot provide any warranty that it does not contain bugs, or that it is fit for the purpose that you had in mind.