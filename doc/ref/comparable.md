
# comparable interface

If the value-type of an iterable is [arithmetic](http://www.cplusplus.com/reference/type_traits/is_arithmetic), this interface defines comparison methods:

```cpp
// element-wise comparison with scalar
bool lt( Scalar x ) const;
bool gt( Scalar x ) const;
bool eq( Scalar x ) const;
bool leq( Scalar x ) const;
bool geq( Scalar x ) const;
bool neq( Scalar x ) const;

// element-wise comparison with other iterable (same size required)
bool lt( const Iterable& other ) const;
bool gt( const Iterable& other ) const;
bool eq( const Iterable& other ) const;
bool leq( const Iterable& other ) const;
bool geq( const Iterable& other ) const;
bool neq( const Iterable& other ) const;
```

