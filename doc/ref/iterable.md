
# iterable interface

> TODO: importantly this interface implements printing for all sequential containers.

```cpp
uidx_t size() const; // has a length
Iterator iter() const; // returns a forward-iterator
```

Where `Iterator` is a template-type implementing the `_iterator` interface:
```cpp
operator bool() const; // validity
ref_t value() const; // current value (if valid)
void next(); // advance

// pointer-like behaviour
ref_t operator* () const;
ptr_t operator& () const;
ptr_t operator-> () const;
```
