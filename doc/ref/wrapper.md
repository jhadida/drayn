
# wrapper

Implements the [sequence interface](ref/sequence). Wrappers depend on a `Container` type, which should implement `size()` and `operator[]`. 
They store a pointer to `Container` internally (equivalent to a weakly managed pointer), so do not use the wrapper when the container goes out of scope (dangling reference).

```cpp
// constructor
wrapper( Container *x ); // e.g. wrapper( new std::vector )
wrapper( Args&&... ); // construct Container on-the-fly

void clear(); // make container invalid
Container& get() const; // underlying container
operator Container&() const; // implicit cast
```
