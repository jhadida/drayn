
# Functional programming

```
// in iterable/functional
apply( in, uop )                     in[] = uop( in[] )
fill( in, val )                      in[] = val
map( in, out, uop )                  out[] = uop( in[] )
copy( in, out )                      out[] = in[]
combine( in1, in2, out, bop )        out[] = bop( in1[], in2[] )
combine_s( in, val, out, bop )       out[] = bop( in[], val )
reduce( in, start, bop )             start = bop( start, in[] )
reduce( in, start, bop, uop )        start = bop( start, uop(in[]) )
cumul( in, bop )                     in[+1] = bop( in[+1], in[+0] )
inner( in1, in2, start, bop )        start += bop( in1[], in2[] )
inner( in1, in2, start )             start += in1[] * in2[]
inner_s( in, val, start, bop )       start += bop( in[], val )
inner_s( in, val, start )            start += in[] * val
all( in, test )                      when ( !test( in[] ) ) return false;
all( in1, in2, test )                when ( !test( in1[], in2[] ) ) return false;
all_s( in, val, test )               when ( !test( in[], val ) ) return false;
any( in, test )                      when ( test( in[] ) ) return true;
any( in1, in2, test )                when ( test( in1[], in2[] ) ) return true;
any_s( in, val, test )               when ( test( in[], val ) ) return true;

// in iterable/algebra
alg_dot( in1, in2 )
alg_sum( in )
alg_prod( in )
alg_cumsum( in )
alg_cumprod( in )
alg_invert( in )
```
