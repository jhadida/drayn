
# arithmetic interface

If the value-type of an iterable is [arithmetic](http://www.cplusplus.com/reference/type_traits/is_arithmetic), this interface defines compound-assignment operators:

```cpp
// with arithmetic scalars
self& operator+= ( Scalar x ) const;
self& operator-= ( Scalar x ) const;
self& operator*= ( Scalar x ) const;
self& operator/= ( Scalar x ) const;

// with other iterables (same size required)
self& operator+= ( const Iterable& other ) const;
self& operator-= ( const Iterable& other ) const;
self& operator*= ( const Iterable& other ) const;
self& operator/= ( const Iterable& other ) const;
```
