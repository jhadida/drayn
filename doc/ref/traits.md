
# Drayn traits

## Container types

All containers in Drayn define the following types, for a given base-type `T` (the value-type of the container):
```cpp
val_t   = T
ref_t   = T&
ptr_t   = T*

cval_t  = const T
cref_t  = const T&
cptr_t  = const T*
```

For example:
```cpp
#include <iostream>
#include <type_traits>
#include "drayn.h"
using T = double;

int main() {
    std::cout << std::boolalpha << std::is_same<
            drayn::shared<T>::cref_t,
            drayn::shared<const T>::ref_t,
        >::value << std::endl;
}
```

Note that if the base-type `T` is const, then `val_t` and `cval_t` are the same (idem for other pairs). 

## Integer types

Throughout the Drayn library, the following integer types are used:
```cpp
// 64 bits unless DRAYN_USE_32BITS_NUMBER
uint_t // unsigned
sint_t // signed

// 64 bits unless DRAYN_USE_32BITS_INDEX
uidx_t // unsigned
sidx_t // signed
```

These types are defined in a separate namespace, which you can use in your code with:
```
using namespace drayn::integer;
```

## Formatting macros

It can be difficult to keep track of the different formats when using `printf`, especially when using templated code and allowing different compilation flags. The following formatting macros are defined to match the previous integer types when using `printf`:

```cpp
uint_f
sint_f

uidx_f
sidx_f
```

For example:
```cpp
printf( "There are " uidx_f " element(s) in my container.\n", c.size() );
```
