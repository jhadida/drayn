
# Indexed interface

```cpp
// whether the container can be used
bool valid() const;

// size of the container
uidx_t size() const;
bool empty() const;

// access item at index k
ref_t operator[] (uidx_t k) const;
ref_t at(uidx_t k) const; // throws if out of bounds

// convenience methods
ref_t front() const;
ref_t back() const;
```

Additionally, indexed containers are _copyable_ when the value-type is mutable (this does **NOT** involve resizing):
```cpp
template <class Indexed>
void copy( const Indexed& other ) const;
```

> **Note:** copy between indexed containers requires them to have the same size. There is no resizing involved in the default implementation (contrary to the specialised implementation in [shared containers](ref/shared) for instance).
