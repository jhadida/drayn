
# sequence

> TODO: **OUTDATED: sequence is now an abstract interface. Features are similar to below, but the class is not an adapter anymore (i.e. container inherits from sequence, instead of being contained by it).**

Sequences in Drayn act as adapters for the various containers defined previously. In other words, they abstract the way in which the data is "contained", and expose a consistent set of features that one might expect "vector-like" objects to implement (e.g. iteration, arithmetic operators, etc.) regardless of the backend. Specifically, sequences implement the following interfaces:

 - [indexed](ref/indexed) (proxy to underlying container)
 - [iterable](ref/iterable)
 - [arithmetic](ref/arithmetic) (if value-type is arithmetic)
 - [comparable](ref/comparable) (if value-type is arithmetic)

In addition, they implement [standard random-access iteration](http://www.cplusplus.com/reference/iterator/RandomAccessIterator/), so you can write range-based for loops for instance:
```cpp
for ( auto& x: seq ) { /* use x */ }
```

Although it is actually more efficient to use [Drayn iterators](ref/iterable) instead:
```cpp
for ( auto it = seq.iter(); it; it.next() ) { /* use (*it) */ }
```
