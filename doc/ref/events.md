
# events

In the following:
```cpp
// EventData is any data-type
EventData edat;

// example callback for a method within an object
const auto cb = [this]( EventData& dat ){ this->do_something(dat); };
```

## signal

Signals contain a list of subscribers (see slots below), and are mainly used to publish data:

```cpp
// publish data
s.publish( edat )

// count subscribers
s.subscriber_count();
```

## slot

Slots are instances of `std::shared_ptr< drayn::ps_relay<EventData> >`. We describe practical use below:

```cpp
// test validity
if ( s && s->valid() ) {}

// enabled instanciation
s = signal.subscribe(cb);

// disabled instanciation (enable later)
s = signal.subscribe();
    // ...
s->enable(cb);

// unsubscribe
signal.unsubscribe(s);

// disable/reenable (once enabled)
s->disable();
s->enable(cb);

// callback without trigger (once enabled)
s->call(edat);
```