
# range

Implements the [sequence interface](ref/sequence) with const value-type (elements are NOT mutable). A range is an efficient way of representing integer sequences without actually storing them. They _can_ be used as base containers with Drayn arrays and sequences, but this is more of a hack than a feature..

```cpp
// contructor (from lower to upper bound inclusive)
range( sidx_t up ); 
range( sidx_t lo, sidx_t up, sidx_t step=1 );

// reverse the range
range reverse() const;
```
