
# pointer

Implements the [sequence interface](ref/sequence), and additionally defines:
```cpp
// constructor
pointer( ptr_t ptr, uidx_t size, uidx_t step=1 );

void clear(); // make container invalid
operator ptr_t() const; // implicit cast

// assignment
self& assign( ptr_t ptr, uidx_t size, uidx_t step=1 );

// underlying pointer
ptr_t get() const;
ptr_t operator-> () const;
ref_t operator* () const;

// pointer arithmetic
self& operator+= (sidx_t);
self& operator-= (sidx_t);
self& operator++ ();
self& operator-- ();

self operator+ (sidx_t) const;
self operator- (sidx_t) const;
self operator++ (int);
self operator-- (int);
```