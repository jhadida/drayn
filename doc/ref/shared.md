
# shared

Implements the [sequence interface](ref/sequence), and additionally defines:
```cpp
// constructors
shared( ptr_t ptr, uidx_t size ); // take ownership
shared( uidx_t size ); // allocation
shared( uidx_t size, cref_t val ); // allocation + fill
shared( inilst<val_t> ini ); // allocation + copy

void clear(); // make container invalid
ptr_t get() const; // underlying pointer

// assignment
self& assign( ptr_t ptr, uidx_t size );

// resizing
self& resize( uidx_t size );
self& resize( uidx_t size, cref_t val ); // fill with value

// copy anything that defines size() and operator[]
template <class C> self& copy( const C& other );

// reference counting
uidx_t ref_count() const;
bool unique() const;
```

> **Note:** the copy method here is a specialisation of the [indexed interface](ref/indexed) implementation. Specifically, `shared::copy` resizes the container before copying the contents of `other`.
