
# indexer

An indexer maps an n-tuple of indices (also called "subindices") to a unique index in some container, which effectively gives the "illusion" of an n-dimensional grid. It does so by associating a step with each dimension, such that an increment of 1 to coordinate `k` leads to an increase of `step[k]` in the final index.

```cpp
// constructors
index( uidx_t[] size, layout cv = layout::Columns );
index( inilst<uidx_t> size, uidx_t step );

void clear(); // make indexer invalid

// indexing
uidx_t sub2ind( uidx_t[] sub ) const;

// index offset
uidx_t offset() const; // get
self& offset( uidx_t ); // set

// shape access
uidx_t[] size() const;
uidx_t[] step() const;

uidx_t size(uidx_t k) const;
uidx_t step(uidx_t k) const;

uidx_t ndims() const;
uidx_t numel() const;

// assignment
self& assign( uidx_t[] size, layout cv = layout::Columns );

// transform
self& swap( uidx_t d1, uidx_t d2 );
self& permute( uidx_t[] perm );
self slice( uidx_t dim, uidx_t k ) const;
```

Remarks:

 - member properties are stored using `std::vector` to ensure that copies are not shallow;
 - both column and row layouts are supported, use `layout::Columns` for row-major and `layout::Rows` for column-major;