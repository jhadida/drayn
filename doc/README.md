
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

# Drayn: C++11 core library

This library provides essential utilities, data containers, and numerical routines, to build libraries for specific applications in data-science (e.g. solving [systems of differential equations](https://gitlab.com/jhadida/disol), or running [simulations of whole-brain activity](https://gitlab.com/jhadida/lsbm)). It is by no means complete, and contributions are welcome if you would like to propose extensions/enhancements (see the [to-do list](https://gitlab.com/jhadida/drayn/blob/master/todo.md) or [open an issue](https://gitlab.com/jhadida/drayn/issues)).
