
# About

 - Previous version is [here](https://bitbucket.org/jhadida/drayn)
 - Was relatively heavy, complicated to maintain
 - New version simplifies a lot of concepts without affecting performance
 - Interface code (with Matlab and armadillo) moved to another repo
 - Comes with documentation!
