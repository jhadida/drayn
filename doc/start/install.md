
# Installation

## Cloning the repository

Easy:

```
git clone https://gitlab.com/jhadida/drayn.git
```

Make sure you have a C++11 compiler (clang 3.3+, gcc 4.8.1+) installed on your machine. 

## Updating your Makefile

```makefile
# the following are optional, and should be appended to existing variables
# see also "simply expanded variables" here: http://web.mit.edu/gnu/doc/html/make_6.html
WARNING = -Wall -Wno-unused-variable
DEFINE = -DDRAYN_SHOW_INFO

# the following should already be defined in your makefile
# make sure -std=c++11 (or later) is specified
CC = clang++
CXXFLAGS = $(DEFINE) $(WARNING) -std=c++11

# this rule compiles drayn
DRAYN_SOURCE = path/to/drayn/src
DRAYN_OBJECT = bin/drayn.o
drayn:
    $(CC) -c -o $(DRAYN_OBJECT) $(CXXFLAGS) $(DRAYN_SOURCE)/drayn.cpp

# then in an existing rule (for example)
foo: drayn
    $(CC) -c -o bin/foo.o $(CXXFLAGS) path/to/foo.cpp $(DRAYN_OBJECT)
```

## Adding as a submodule

Here is one way of adding Drayn as a dependency to an existing git codebase:

```bash
# create a folder "bin" to store compiled objects
mkdir bin
echo "*" >> bin/.gitignore
git add  -f bin/.gitignore

# create a folder "external"
mkdir external

# add Drayn to it as a submodule (equivalent of a pointer for repos)
git submodule add https://gitlab.com/jhadida/drayn.git external/drayn
```

Then, simply add a section to your Makefile (see previous section) with `DRAYN_SOURCE = external/drayn/src`.
