
# Architecture

 - Core utilities
 - Random generators, timer and events
 - Iterable containers
 - Containers and arrays
 - Time-series
 - Algorithms
