
# Random

## Random arrays

You can easily create vectors of random numbers distributed according to uniform (discrete or continuous) and normal distributions:
```cpp
#include <iostream>

#include "drayn.h"
namespace dr = drayn;

int main() {
    std::cout << dr::rand( 15 );
    std::cout << dr::randn( 15, 2.0, 1.0 );
    std::cout << dr::randi( 15, -5, 7 );
}
```

Matrices and volumes can be created with:
```
dr::mrand( nr, nc, lo=0, up=1, cv = dr::layout::Cols );
dr::mrandn( nr, nc, mu=0, sig=1, cv = dr::layout::Cols );
dr::mrandi( nr, nc, lo, up, cv = dr::layout::Cols );

dr::vrand( nr, nc, ns, lo=0, up=1, cv = dr::layout::Cols );
dr::vrandn( nr, nc, ns, mu=0, sig=1, cv = dr::layout::Cols );
dr::vrandi( nr, nc, ns, lo, up, cv = dr::layout::Cols );
```

## Using generators

```cpp
#include <iostream>

#include "drayn.h"
namespace dr = drayn;

int main() {
    auto U = dr::uniform_gen();
    auto G = dr::normal_gen( 2.0, 1.0 );
    auto E = dr::normal_gen( 0.5 );

    for ( int i=0; i < 15; i++ ) std::cout << U() << " "; std::cout << std::endl;
    for ( int i=0; i < 15; i++ ) std::cout << G() << " "; std::cout << std::endl;
    for ( int i=0; i < 15; i++ ) std::cout << E() << " "; std::cout << std::endl;
}
```

## More info

[Random generators](http://www.cplusplus.com/reference/random/) in C++11 have 3 components:

 - A **distribution** which takes parameters and "transforms" a stream of uniformly random integers into a random variable with the desired statistical properties.
 - A **random engine**, which generates the stream of integers to be transformed by the distribution.
 - An initial **seed** for the random engine.

Drayn uses `std::mt19937_64` as a random engine, and defines the following generators:
```
bernoulli_gen        ( double p );
binomial_gen    <Int>( Int t, double p = 0.5 );
uniform_int_gen <Int>( Int a, Int b );
geometric_gen   <Int>( double p = 0.5 );
poisson_gen     <Int>( double mu = 1 );

uniform_gen     <Real>( Real a=0, Real b=1 );
normal_gen      <Real>( Real mu=0, Real sigma=1 );
exponential_gen <Real>( Real lambda=1 );
lognormal_gen   <Real>( Real mu=0, Real sigma=1 );
gamma_gen       <Real>( Real alpha=1, Real beta=1 );

student_t_gen   <Real>( Real n=1 );
chi_squared_gen <Real>( Real n=1 );
```

## Seeding

By default, the engines are seeded randomly using the [time since epoch](http://www.cplusplus.com/reference/chrono/time_point/time_since_epoch/). This can be modified, for example if you want to reproduce the exact same output between different runs (useful for debugging). However, there are a few gotchas.

### Deterministic seeding

Use this to specify an initial seed manually (typically just before your program begins). All generators created subsequently will each get a different seed, but from the same sequence that you initially seeded. This is what most people want. 

> WARNING
>
> If random generators are initialised within concurrent threads, it is almost impossible (lest significant effort with semaphores) to guarantee that they are seeded in a specified order.
>
> However, provided that threads are identical (or that you can preserve a mapping thread-generator manually), deterministic seeding will still have the intended effect if distinct generators are used within each thread, but initialised outside the thread pool. 

To use deterministic seeding, Drayn **needs** to be compiled with the flag `-DDRAYN_DETERMINISTIC_RANDOM_SEED=123`. The value specified at compiled time can then be overridden at runtime by calling:
```
drayn::deterministic_seed_generator::seed( new_seed );
```

### Fixed seeding

This is almost never what people want, but it is also possible to seed _all_ random engines bound to a distribution with the exact same seed. This means that all generator instances throughout the program will produce the exact same random number after `k` calls. It is equivalent to imagine that each random generator is an index within a global array of random numbers.

To use fixed seeding, Drayn needs to be compiled with the flag `-DDRAYN_FIXED_RANDOM_SEED=123`. The value specified at compile time can then be overridden at runtime with:
```
drayn::fixed_seed_generator::seed = 123;
```
