
# Events

> See also the [reference](ref/events) for signals and slots.

## Common usage

```cpp
#include <string>
#include <iostream>

#include "drayn.h"
namespace dr = drayn; // alias 

// can also be a qualified POD type (e.g. const unsigned)
struct EventData { 
    std::string msg;
    EventData(const char *m): msg(m) {}
    void print() const { std::cout << msg << std::endl; }
};

struct Emitter {
    dr::signal<const EventData> signal;
    Emitter() {}
    void hip() { signal.publish("Hurray!"); }
};

// Note: s is called a "subscriber", and can safely be returned
// as the output of a function (it is a shared pointer).
int main() {
    Emitter em;
    dr::slot<const EventData> s = em.signal.subscribe(
        []( const EventData& dat ){ dat.print(); }
    );
    em.hip();
    em.hip();
}
```

Explanations:

 - We define a simple event-data class `EventData`, supporting implicit casts from `const char*` and defining a method to `print()` the message to the console.
 - We define a class `Emitter` with a method `hip()`, which publishes the message `"Hurray!"` to a member `signal`.
 - We then subscribe a lambda function in `main()`, which calls the method `print()` on the input instance of `EventData`.
 - By subsequently calling the method `hip()` of the `Emitter` class, the message `"Hurray!"` is implicitly cast to an instance of `EventData`, which is then passed on to the subscriber.
 - [gotcha] Note that the template event type is defined as `const EventData`. The const-qualifier is needed because event data is passed by lvalue reference, but `"Hurray!"` is an rvalue.

## Built-in hub

```cpp
#include <string>
#include <iostream>

#include "drayn.h"
namespace dr = drayn; // alias 

// can also be a qualified POD type (e.g. const unsigned)
struct EventData { 
    std::string msg;
    EventData(const char *m): msg(m) {}
    void print() const { std::cout << msg << std::endl; }
};

dr::events<const EventData> hub;
dr::slot<const EventData> add_subscriber( std::string chan ) {
    return hub.channel(chan).subscribe();
}

int main() {
    auto s = add_subscriber("foo")->enable([]( const EventData& dat ){ dat.print(); });
    hub.channel("foo").publish("Hello world!");
}
```

Explanations:

 - Same as previously, but using an event-hub with dynamic channel allocation, and illustrating that subscribers can safely be returned from function calls (here `add_subscriber`).
 - [gotcha] Subscribers are shared pointers to `drayn::ps_relay` classes, which define methods `enable( callback )/disable()` to dynamically "turn off" a slot without removing it from the list of subscribers.
 - [optional] The hub could also be used as a singleton with `dr::static_val< dr::events<const EventData> >()`.
