
# Timers

High-resolution timers can be created dynamically in your program in order to monitor execution times:

```cpp
#include <thread>
#include <chrono>

#include "drayn.h"
namespace dr = drayn;

int main() {
    dr::time_monitor monitor;

    monitor.start("example200");
    std::this_thread::sleep_for( std::chrono::milliseconds(200) );
    monitor.stop("example200");

    monitor.start("example700");
    std::this_thread::sleep_for( std::chrono::milliseconds(700) );
    monitor.stop("example700");

    monitor.report();
}
```

The `report()` method prints the durations measured by all timers in descending order:
```
[Timer report]:
	[example700]: 705.040 ms
	[example200]: 200.107 ms
```
