
//==================================================
// @title        drayn.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "drayn.h"

// ------------------------------------------------------------------------

#include "core/includes.cpp"
#include "containers/includes.cpp"
#include "algorithms/includes.cpp"

// External interfaces
#include "matlab/includes.cpp"
